let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js')
 //  .sass('resources/assets/sass/app.scss', 'public/css');

/**
mix.js('resources/assets/js/user/user.js', 'public/js').sass('resources/assets/sass/user.scss', 'public/css');
mix.js('resources/assets/js/admin/admin.js', 'public/js').sass('resources/assets/sass/admin.scss', 'public/css');
mix.js('resources/assets/js/moderator/moderator.js', 'public/js').sass('resources/assets/sass/moderator.scss', 'public/css');
mix.js('resources/assets/js/jury/jury.js', 'public/js').sass('resources/assets/sass/jury.scss', 'public/css');
*/

mix.js('resources/assets/js/user/user.js', 'public/js'); //.sass('resources/assets/sass/user.scss', 'public/css');
mix.js('resources/assets/js/admin/admin.js', 'public/js'); //.sass('resources/assets/sass/admin.scss', 'public/css');
mix.js('resources/assets/js/moderator/moderator.js', 'public/js'); //.sass('resources/assets/sass/moderator.scss', 'public/css');
mix.js('resources/assets/js/jury/jury.js', 'public/js'); //.sass('resources/assets/sass/jury.scss', 'public/css');
   