<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_config', function (Blueprint $table) {
            $table->increments('id');

            //register
            $table->integer('user_register');
            $table->text('user_spam_emails');

            $table->integer('user_project_register');
            $table->integer('user_student_register');

            $table->integer('user_project_edit');
            $table->integer('user_student_edit');

            $table->integer('user_project_delete');
            $table->integer('user_student_delete');

            $table->integer('user_conversation_open');
            $table->text('user_conversation_blocked');

            $table->integer('user_conversation_send_message');

            $table->integer('user_profile_edit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_config');
    }
}
