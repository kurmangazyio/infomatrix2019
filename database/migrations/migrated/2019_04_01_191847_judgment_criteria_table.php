<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JudgmentCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jcriteria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jccategory')->unsigned();
            $table->integer('jctour')->unsigned();
            $table->string('jctitle');
            $table->integer('jcmin');
            $table->integer('jcmax');

            $table->foreign('jccategory')->references('id')->on('categories');
            $table->foreign('jctour')->references('id')->on('jtours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jcriteria');
    }
}
