<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfflineinvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offline_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('i_supervisor')->unsigned();
            $table->string('i_code', 15);
            $table->string('i_fullname');
            $table->string('i_role');
            $table->string('i_project');
            $table->string('i_method');
            $table->string('i_amount');
            $table->string('i_file');
            $table->text('i_comment');
            

            $table->foreign('i_supervisor')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offline_invoice');
    }
}
