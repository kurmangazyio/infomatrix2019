<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('t_team_id');
            $table->string('t_name');

            $table->integer('t_supervisor')->unsigned();

            $table->string('t_project_code');
            $table->string('t_f_student_code');
            $table->string('t_s_student_code');
            $table->string('t_t_student_code');
            
            $table->foreign('t_supervisor')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
