<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConversationsAnswersTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations_answers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('conv_answer_user_id');
            $table->integer('conv_answer_user_level');

            $table->integer('conv_answer_conv_id')->unsigned();
            $table->string('conv_answer_code');
            $table->text('conv_answer_content');

            $table->foreign('conv_answer_conv_id')->references('id')->on('conversations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations_answers');
    }
}
