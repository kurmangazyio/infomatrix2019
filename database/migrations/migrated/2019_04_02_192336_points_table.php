<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jpcriteria_id')->unsigned();
            $table->integer('jpstudent_id')->unsigned();
            $table->integer('jpproject_id')->unsigned();
            $table->integer('jpjury_id')->unsigned();
            $table->integer('jppoint');
            $table->text('jpcomment');

            $table->foreign('jpcriteria_id')->references('id')->on('jcriteria');
            $table->foreign('jpstudent_id')->references('id')->on('students');
            $table->foreign('jpproject_id')->references('id')->on('projects');
            $table->foreign('jpjury_id')->references('id')->on('juries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jpoints');
    }
}
