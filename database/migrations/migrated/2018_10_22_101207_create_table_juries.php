<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jury_id', 15);

            $table->string('name');
            $table->string('surname');
            $table->text('bio');
            $table->string('image')->default('/general/images/user.png');

            $table->integer('category')->unsigned();

            $table->string('email')->unique();
            $table->string('password');
            $table->string('b_password');

            $table->string('phone')->default('');
            $table->string('phone_code', 5)->default('');

            
            $table->integer('email_notification_state')->default(1);

            $table->integer('is_email_activated')->default(0);
            $table->integer('is_phone_activated')->default(0);

            $table->foreign('category')->references('id')->on('categories');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jury');
    }
}
