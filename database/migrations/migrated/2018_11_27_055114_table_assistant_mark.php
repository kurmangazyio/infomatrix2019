<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableAssistantMark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_marks', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('assistant')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->integer('mark');
            $table->text('comment');

            $table->foreign('assistant')->references('id')->on('juries');
            $table->foreign('team_id')->references('id')->on('teams');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_marks');
    }
}
