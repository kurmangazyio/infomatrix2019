<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');

            $table->string('p_project_id');

            $table->integer('p_supervisor')->unsigned();
            $table->integer('p_category')->unsigned();

            $table->text('p_title');
            $table->text('p_description');
            $table->text('p_file');
            $table->text('p_addins');

            $table->foreign('p_supervisor')->references('id')->on('users');
            $table->foreign('p_category')->references('id')->on('categories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
