<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('conv_creator_level');
            $table->string('conv_creator');

            $table->string('conv_code');
            $table->string('conv_title');
            $table->text('conv_content');
            
            $table->integer('conv_category')->unsigned();
            $table->integer('conv_state');

            $table->foreign('conv_category')->references('id')->on('conversations_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
