<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JudgmentToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jtours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jtcategory')->unsigned();
            $table->string('jttitle');
            $table->boolean('jtfirst');

            $table->foreign('jtcategory')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jtours');
    }
}
