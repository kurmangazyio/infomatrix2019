<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');

            $table->string('s_student_id');
            $table->integer('s_supervisor')->unsigned();

            $table->string('s_en_name');
            $table->string('s_en_surname');
            $table->string('s_en_lastname');

            $table->string('s_ru_name');
            $table->string('s_ru_surname');
            $table->string('s_ru_lastname');

            $table->date('s_birthdate');
            $table->integer('s_gender');

            $table->string('s_passport');
            $table->string('s_phone');

            $table->integer('s_country')->unsigned();
            $table->string('s_region');
            $table->string('s_zip');
            $table->string('s_city');
            $table->string('s_school');
            $table->string('s_grade');

            $table->foreign('s_supervisor')->references('id')->on('users');
            $table->foreign('s_country')->references('id')->on('countries');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
