<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guest_id');
            $table->integer('p_supervisor')->unsigned();

            $table->string('name');
            $table->string('surname');
            $table->string('lastname');

            $table->string('passport');
            $table->string('email');
            $table->string('phone');

            $table->integer('country')->unsigned();
            $table->string('region');
            $table->string('city');
            $table->string('address');

            $table->foreign('p_supervisor')->references('id')->on('users');
            $table->foreign('country')->references('id')->on('countries');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
