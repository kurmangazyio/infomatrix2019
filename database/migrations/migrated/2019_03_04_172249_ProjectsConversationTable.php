<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectsConversationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_conversations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('messager');
            $table->integer('role');
            $table->integer('teams_id')->unsigned();
            $table->text('message');

            $table->foreign('teams_id')->references('id')->on('teams');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_conversations');
    }
}
