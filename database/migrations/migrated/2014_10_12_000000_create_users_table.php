<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 15);
            
            $table->string('name');
            $table->string('surname');
            $table->text('bio');
            $table->string('image');

            $table->string('email')->unique();
            $table->string('password');
            $table->string('b_password');

            $table->string('phone');
            $table->string('phone_code', 5);
            
            $table->integer('email_notification_state');

            $table->integer('is_email_activated');
            $table->integer('is_phone_activated');

            $table->integer('country')->unsigned();
            $table->string('region');
            $table->string('city');
            $table->string('zip');
            $table->string('school');

            $table->foreign('country')->references('id')->on('countries');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
