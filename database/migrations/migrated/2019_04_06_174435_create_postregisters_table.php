<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostregistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postregisters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('role');

            $table->boolean('is_participant');
            $table->boolean('is_food');
            $table->boolean('is_transport');
            $table->boolean('is_gift');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postregisters');
    }
}
