<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificationsFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('n_notification_id')->unsigned();
            $table->integer('n_admin_id')->unsigned();

            $table->foreign('n_notification_id')->references('id')->on('notifications');
            $table->foreign('n_admin_id')->references('id')->on('admins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications_file');
    }
}
