<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentGuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('guest_id')->unsigned();

            $table->string('payment_unique');
            $table->integer('payment_status');
            $table->string('payment_option');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_payments');
    }
}
