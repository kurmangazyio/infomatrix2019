<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{{ $appname }}</title>
        
        <link type="text/css" rel="shortcut icon" href="{{ route('main') }}/general/images/components/logo.jpg">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="InfoMatrix is an international computer project competition, born from a desire to bring the world's best IT students together. The competition is not just about promoting professional excellence ...">

        <link href="{{ url('/') }}/gli/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/theme-serpent.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/font-rubiklato.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.phancy.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

           ym(52297417, "init", {
                id:52297417,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
           });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/52297417" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->

    </head>
    <body class=" ">
        <section class="imageblock switchable feature-large height-100">
                <div class="imageblock__content col-md-4 col-sm-12 pos-right itissmallwhensm" data-overlay="1">
                    <div class="background-image-holder">
                        <img alt="image" src="{{route('main')}}/general/images/register-cover.jpg" />
                    </div>
                    <div class="modal-instance text-center">
                        <div class="video-play-icon modal-trigger"></div>  
                        <span style="color: white;">
                            <strong>Watch our film</strong>&nbsp;&nbsp;&nbsp;212 Seconds
                        </span>
                        <div class="modal-container">
                            <div class="modal-content bg-dark" data-width="60%" data-height="60%">
                                <iframe src="https://www.youtube.com/embed/xq4gkpbUK30?autoplay=1" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container pos-vertical-center" style="margin-top: 5px;">
                    <div class="row">
                        <div class="col-md-8 col-sm-12 text-center">
                            <span class="h6 nomargin">SULEYMAN DEMIREL UNIVERSITY</span>
                            <span class="h3 color--primary-1 nomargin" style="font-weight: bold;">INFOMATRIX-ASIA 2020 </span>
                            <h3 class="nomargin" style="padding-bottom: 15px;">Dear participants and supervisors! </h3>
                            <span class="h2 nomargin" style=" font-weight: 500;padding-bottom: 20px;">Organizing Committee of “Infomatrix-Asia 2020” is refill to officially announce cancellation of the event due to the spread of coronavirus means the Covid-19. 
                            </span>
                            
                            <!--<div class="row">
                                <div class="col-md-12 text-center">
                                    <span class="h4 nomargin color--primary-1">Infomatrix-Asia 2020 starts after</span>
                                    <span class="h1 countdown" data-date="04/9/2020" data-date-fallback="Welcome to Infomatrix-Asia!"></span>
                                    <br>
                                </div>
                            </div>-->
                            <!--
                            <p class="lead nomargin" style="padding-bottom: 20px;">
                                <br>
                                /*Registration is opened now! <strong>Note:</strong>When you registered, it does not mean you are participant of Infomatrix-Asia 2020! You should provide your project and submit till registration ends! Good luck! 
                            </p>
                            
                            -->

                            <!--<a class="btn btn--primary-3 type--uppercase" href="{{ route('main')}}\general\files\rules\Infomatrix Asia.pdf" target="_blank">
                                <span class="btn__text">
                                    Infomatrix Asia 2020 (General rule)
                                </span>
                            </a>-->

                            <!--<a class="btn btn--primary-1 type--uppercase" href="{{ route('online')}}">
                                <span class="btn__text">
                                    Final list
                                </span>
                            </a>-->

                            <div class="row" style="padding-top: 15px;">
                                <div class="col-sm-12">
                                    <a class="btn btn--primary-1 type--uppercase" href="https://docs.google.com/document/d/1FcnTkxFzFhKeOmSRlXyFr6q4jFjSWhorgK5ZBkCQ_Rk/edit">
                                        <span class="btn__text">
                                            Official document
                                        </span>
                                    </a>

                                    <a class="btn btn--primary-1 type--uppercase" href="https://docs.google.com/document/d/1-LfVkD8jw8Ey1ZDwaFYfOvXEPClfsCBIH96fGGeTiRo">
                                        <span class="btn__text">
                                            Официальный документ
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <!--<a class="btn btn--primary-3"  href="{{route('payment')}}">
                                <span class="btn__text">
                                    Payments
                                </span>
                            </a>
                            <a class="btn btn--primary-1 type--uppercase" href="{{ route('main')}}\general\files\rules\docs\Rules (EN).pdf" target="_blank">
                                <span class="btn__text">
                                    Rules (EN)
                                </span>
                            </a>
                            <a class="btn btn--primary-1 type--uppercase" href="{{ route('main')}}\general\files\rules\docs\Rules (RU).pdf" target="_blank">
                                <span class="btn__text">
                                    Rules (RU)
                                </span>
                            </a>
                            <a class="btn btn--sm btn--primary type--uppercase" target="_blank" href="{{route('main')}}/stand.pdf" style="margin-top: 20px;">
                                <span class="btn__text">
                                    Project Stand Properties
                                </span>
                            </a>-->
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ url('/') }}/gli/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/') }}/gli/js/flickity.min.js"></script>
        <script src="{{ url('/') }}/gli/js/easypiechart.min.js"></script>
        <script src="{{ url('/') }}/gli/js/parallax.js"></script>
        <script src="{{ url('/') }}/gli/js/typed.min.js"></script>
        <script src="{{ url('/') }}/gli/js/datepicker.js"></script>
        <script src="{{ url('/') }}/gli/js/isotope.min.js"></script>
        <script src="{{ url('/') }}/gli/js/ytplayer.min.js"></script>
        <script src="{{ url('/') }}/gli/js/lightbox.min.js"></script>
        <script src="{{ url('/') }}/gli/js/granim.min.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.steps.min.js"></script>
        <script src="{{ url('/') }}/gli/js/countdown.min.js"></script>
        <script src="{{ url('/') }}/gli/js/twitterfetcher.min.js"></script>
        <script src="{{ url('/') }}/gli/js/spectragram.min.js"></script>
        <script src="{{ url('/') }}/gli/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/') }}/gli/js/scripts.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.phancy.js"></script> 

        <script>
        $( document ).ready(function() {$(function() {$( "#note" ).customScroll({ scrollbarWidth: 5 });});$('a[href^="#"]').on('click', function(event) {var target = $(this.getAttribute('href'));if( target.length) {event.preventDefault();$('html, body').stop().animate({scrollTop: target.offset().top}, 1000);}});</script>
    </body>
</html>