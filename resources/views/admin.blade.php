<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link type="text/css" rel="shortcut icon" href="{{ route('main') }}/logo.jpg">

        <link rel="stylesheet" href="{{ url('/') }}/plugins/custombox/css/custombox.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/plugins/footable/css/footable.core.css">

        <link href="{{ url('/') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="{{ url('/') }}/assets/js/modernizr.min.js"></script>

    <body>
        <div id="app">
            <adminpanel></adminpanel>
        </div>

        <script type="text/javascript">
            window.Infomatrix = <?php echo json_encode([
                'csrfToken' => csrf_token(),
                'main' => route('main'),
            ]); ?>
        </script>

        <script src="{{ asset('js/admin.js') }}"></script>

    </body>
</html>
