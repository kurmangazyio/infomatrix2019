<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Activation link for your INFOMATRIX-ASIA account!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <link href="{{ url('/') }}/mainpanel/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/mainpanel/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/mainpanel/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/mainpanel/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/mainpanel/css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/mainpanel/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">

    </head>
    <body data-smooth-scroll-offset="77">
        <div class="nav-container"> </div>
        <div class="main-container">
        	<section class="switchable feature-large">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-6 switchable__text">
                            <h2>Activation link for your INFOMATRIX-ASIA account!</h2>
                            <p class="lead">Dear {{ $fullname }},<br> to activate your account - click "Activate my account Account" button. </p>
                            <hr class="short">
                            <a class="btn btn--primary type--uppercase" href="{{ $link }}" target="_blank"> 
                            	<span class="btn__text">
		                        	Activate my account Account
		                    	</span> 
		                    </a>
                        </div>
                        <div class="col-md-4 col-sm-6 col-md-pull-1 col-xs-12">
                            <div class="boxed boxed--lg boxed--border box-shadow-wide">
                                <div class="slider" data-paging="true">
                                    <ul class="slides">
                                        <li class="col-xs-12">
                                            <div class="feature feature-3 text-center"> 
                                                <h4 style="padding-top: 20px;">Categories</h4>
                                                <p> There are 7 main categories : Robotics, Computer Programming, Computer Graphics and Art, Short Movie, Hardware Control Systems, Drone Racing, and Mathematics Project.</p> 
                                                <a href="{{route('main').'#categories'}}" target="_blank">
			                                        Learn More
			                                    </a> 
			                                </div>
                                        </li>
                                        <li class="col-xs-12">
                                            <div class="feature feature-3 text-center"> 
                                            	<i class="icon icon--lg icon-Calendar-2 color--primary"></i>
                                                <h4>Event Schedule</h4>
                                                <p> IThe competition will last for 4 days. The first day is optional: participants register, enroll in a hotel, ... </p> <a target="_blank" href="{{route('main').'#schedule'}}">
                                        Learn More
                                    </a> </div>
                                        </li>
                                        <li class="col-xs-12">
                                            <div class="feature feature-3 text-center"> <i class="icon icon--lg icon-Map-Marker color--primary"></i>
                                                <h4>Our address:</h4>
                                                <p> Street Abylaikhana 1/1 Kaskelen, Kazakhstan <br>Email: <a href="infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a></p>
                                                <a target="_blank" href="{{route('main').'#map'}}">
                                        Learn More
                                    </a> </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="text-center-xs space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7">
                            <ul class="list-inline">
                                <li> <a href="{{route('main')}}"><span class="h6 type--uppercase medium-editor-element" spellcheck="true" data-medium-editor-element="true" role="textbox" aria-multiline="true" data-medium-editor-editor-index="11" medium-editor-index="6dccbc88-2df2-0d82-77a1-85d268cddb76" data-placeholder="Type your text">About</span></a> </li>
                                <li> <a href="{{route('main').'/#categories'}}"><span class="h6 type--uppercase medium-editor-element" spellcheck="true" data-medium-editor-element="true" role="textbox" aria-multiline="true" data-medium-editor-editor-index="11" medium-editor-index="6fbe7f01-ccae-9464-e9d4-fc80ec103c25" data-placeholder="Type your text"><p>Categories</p></span></a> </li>
                                <li> <a href="mailto:infomatrix@bil.edu.kz"><span class="h6 type--uppercase medium-editor-element" spellcheck="true" data-medium-editor-element="true" role="textbox" aria-multiline="true" data-medium-editor-editor-index="11" medium-editor-index="b74cf35f-c0ba-9211-9810-a3a287a873f4" data-placeholder="Type your text"><p>Support</p></span></a> </li>
                            </ul>
                        </div>
                        <div class="col-sm-5 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li>
                                    <a href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q" target="_blank">
                                        <i class="socicon socicon-youtube icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://vk.com/infomatrixasia" target="_blank">
                                        <i class="socicon socicon-vkontakte icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/infomatix2017/" target="_blank">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/infomatrixasia/" target="_blank">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7"> <span class="type--fine-print">2018 Infomatrix - Asia 2025</span> <a class="type--fine-print" href="{{route('terms')}}">Terms</a>
                            <a class="type--fine-print" href="#"> </a>
                        </div>
                        <div class="col-sm-5 text-right text-center-xs"> <a class="type--fine-print" href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a> </div>
                    </div>
                </div>
            </footer>
        </div>
        <script src="{{ url('/') }}/mainpanel/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/') }}/mainpanel/js/flickity.min.js"></script>
        <script src="{{ url('/') }}/mainpanel/js/parallax.js"></script>
        <script src="{{ url('/') }}/mainpanel/js/countdown.min.js"></script>
        <script src="{{ url('/') }}/mainpanel/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/') }}/mainpanel/js/scripts.js"></script>

    </body>

</html>