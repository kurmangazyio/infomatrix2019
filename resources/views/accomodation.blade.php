<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>InfoMatrix - Asia 2025</title>
        
        <link type="text/css" rel="shortcut icon" href="{{ url('/') }}/general/images/components/logo.jpg">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="InfoMatrix is an international computer project competition, born from a desire to bring the world's best IT students together. The competition is not just about promoting professional excellence ...">

        <link href="{{ url('/') }}/gli/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/theme-serpent.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/font-rubiklato.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.phancy.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />

    </head>
    <body class=" ">
        <a id="start"></a>
        <div class="nav-container ">
            <nav class="bar bar-toggle bar--transparent bar--absolute bg--dark thisistransparent">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 col-sm-2 col-xs-3">
                            <div class="bar__module" style="padding-top: 7px;">
                                <a href="{{ url('/') }}" >
                                    <img class="logo logo-dark" alt="logo" src="{{ url('/') }}/general/images/components/logo-light.png" />
                                    <img class="logo logo-light" alt="logo" src="{{ url('/') }}/general/images/components/logo-light.png" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-10 col-xs-9">
                            <div class="bar__module">
                                <div class="modal-instance pull-right">
                                    <a class="modal-trigger menu-toggle" href="javascript:void(0);">
                                        <i class="stack-interface stack-menu"></i>
                                    </a>
                                    <div class="modal-container menu-fullscreen">
                                        <div class="modal-content" data-width="100%" data-height="100%">
                                            <div class="pos-vertical-center pos-asbolute text-center">
                                                <div class="heading-block">
                                                    <img alt="Image" src="{{ url('/') }}/general/images/components/logo-light.png" class="logo image--xs" />
                                                    <p class="lead" style="color: white;">
                                                        Let's make future better together.
                                                    </p>
                                                </div>
                                                <ul class="menu-vertical">
                                                    <li class="h4">
                                                        <a href="{{ route('main') }}">Home page</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('main') }}#about">About Infomatrix</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('kazakhstan') }}">About Kazakhstan</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('rules') }}">Rules</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('faqs') }}">Faqs</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="pos-absolute pos-bottom text-center">
                                                <ul class="social-list list-inline list--hover">
                                                    <li>
                                                        <a href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q" target="_blank">
                                                            <i class="socicon socicon-youtube icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://vk.com/infomatrixasia" target="_blank">
                                                            <i class="socicon socicon-vkontakte icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/infomatix2017/" target="_blank">
                                                            <i class="socicon socicon-facebook icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.instagram.com/infomatrixasia/" target="_blank">
                                                            <i class="socicon socicon-instagram icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <p class="type--fine-print type--fade">Infomatrix 2025</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>      
        <div class="main-container">
            <section class="cover imagebg height-60">
                <div class="background-image-holder" style="background: url('{{url('/')}}/general/images/accomodation/cover.jpg'); opacity: 10;">
                    <img alt="background" src="{{url('/')}}/general/images/accomodation/cover.jpg">
                </div>
                <div class="container pos-vertical-center text-center" style="padding-top: 20px;padding-bottom: 30px;">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <span class="h1">Accomodation</span>
                        </div>
                    </div>
                </div>
            </section>
            <section class="switchable bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <h2>Accomodation</h2>
                            <p class="lead">
                                We invite participants to stay in comfortable cottages “Arman Dala” located 15 minutes from SDU.
                            </p>
                            <p class="lead">
                                Accommodation of participants ain capital new brick cottages and modern 3-storey a separate bathroom (toilet, sink and shower), warm electric floor. The room has three bunk beds, a chest of drawers with shelves for clothes (one shelf per child), three wall hangers for clothes. Bed linen - 100% cotton.
                            </p>
                            <p class="lead">
                                On the territory: Natural grass football field, volleyball court. Track (200m h120m) for rollers, scooters and Segway. Shields for street basketball. Decorative pond The territory of the camp is fenced and guarded     around the clock.
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="text-block">
                                <h3>Accomodation fee include</h5>
                                <p style="font-size: 18px;">
                                    · Transfer from SDU to camp and back to SDU
                                    <br />· Residence in comfortable room

                                    <br />· In the room 6-8 beds
                                </p>
                            </div>
                            <div class="text-block">
                                <h3>Meals</h5>
                                <p style="font-size: 18px;">
                                   ·  Guests provide their own meals
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="container">
                    <div class="row text-center">
                        <div class="col-sm-12 col-md-12" style="padding-bottom: 30px;">
                            <h2>
                                Gallery
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="masonry">
                            <div class="masonry__container">
                                <div class="masonry__item col-sm-6 col-xs-12"></div>
                                <?php for ($i=1; $i < 17; $i++) { ?>
                                    <div class="masonry__item col-sm-6 col-xs-12" data-masonry-filter="Accomodation">
                                        <a href="{{url('/')}}/general/images/accomodation/1.jpg" data-lightbox="gallery">
                                            <img alt="Image" src="{{url('/')}}/general/images/accomodation/<?php echo $i;?>.jpg" />
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="map" class="switchable bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="switchable__text text-right">
                                <h3>Street Abylaikhana 1/1
                                    <br>Kaskelen, Kazakhstan</h3>
                                <p class="lead">
                                    Email:
                                    <a class="nodecoration" href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a>
                                </p>
                                <p class="lead">
                                    Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days.
                                </p>
                                <div class="modal-instance block">
                                    <p class="lead">
                                        <a class="modal-trigger nodecoration" href="javascript:void(0)" data-modal-index="10">
                                            <span class="btn__text">
                                                Bank account
                                            </span>
                                        </a>
                                    </p>
                                    <div class="modal-container">
                                        <div class="modal-content">
                                            <section class="imageblock feature-large bg--white border--round ">
                                                <div class="imageblock__content col-md-5 col-sm-3 pos-left">
                                                    <div class="background-image-holder">
                                                        <img alt="image" src="{{ url('/') }}/general/images/subcover.jpg" />
                                                    </div>
                                                </div>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-5 col-md-push-6 col-sm-7 col-sm-push-4">
                                                            <h1>Requisites</h1>
                                                            <p class="lead">
                                                                Учреждение «Университет имени Сулеймана Демиреля»<br/>
                                                                    Р/с KZ339261802128707006<br/>
                                                                    АФ АО «Казкоммерцбанк»<br/>
                                                                    БИН 960240000550 <br/>
                                                                    Кбе 18<br>
                                                                    г. Алматы БИК KZKOKZKX<br/><br>

                                                                    г.Каскелен, ул.Абылай хана 1/1<br/>
                                                                    Тел. +8/727/  3079565 | факс 8/727/ 3079558
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-instance block">
                                    <p class="lead">
                                        <a class="modal-trigger nodecoration" href="javascript:void(0)" data-modal-index="11">
                                            <span class="btn__text">
                                                Directions to SDU from Almaty
                                            </span>
                                        </a>
                                    </p>
                                    <div class="modal-container">
                                        <div class="modal-content">
                                            <section class="imageblock feature-large bg--white border--round ">
                                                <div class="imageblock__content col-md-4 col-sm-4 pos-left">
                                                    <div class="background-image-holder">
                                                        <img alt="image" src="{{ url('/') }}/general/images/direction.png" />
                                                    </div>
                                                </div>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-6 col-md-push-5 col-sm-8 col-sm-push-4 nomargin" style="padding-bottom: 0px;">
                                                            <h4>Directions to SDU from Almaty</h4>
                                                            <p class="lead" style="font-size: 16px;" align="justify">
                                                                If you want to get to SDU by bus, firstly you will have to get to Sayran bus station:<br>
                                                                <span class="nomargin color--primary-1">“Almaty 1” train station – Sayran bus station:<span> Bus numbers: 72. Taxi will cost approximately 1500 tenge.<br>
                                                                <span class="nomargin color--primary-1">“Almaty 2” train station – Sayran bus station:<span> Bus numbers: 37, 50, 59, 100, 137. Taxi will cost approximately 500 tenge.<br>
                                                                <span class="nomargin color--primary-1">“Sayahat” bus station – Sayran:<span> Bus numbers: 37, 50, 59, 100. Taxi will cos approximately 600 tenge.<br>
                                                                <span class="nomargin color--primary-1">From “Sayran” you can get to SDU by following buses:<span> 345, 330. You can get to SDU from Sayran(Kaskelen bus stop) by taxi. It will cost approximately 200-300 tenge from a person.<br><br>
                                                                <span class="nomargin color--primary-1">Attention for those who are coming from Kyrgyzstan:<span> You may not go to Sayran bus station instead you should stop on Bishkek-Almaty main road near Kaskelen turn.<br>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2908.0593981368493!2d76.66723851513443!3d43.20824347913905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38834f7675d8a6c3%3A0x7b7d14aec270c056!2sSDU!5e0!3m2!1sen!2skz!4v1537024013581" width="100%" height="400px" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section class="text-center bg--secondary space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-inline list-inline--images">
                                <li>
                                    <span class="h2">Sponsors: </span>
                                </li>
                                <li>
                                    <img alt="Partners" src="{{ url('/') }}/general/images/daryn.png">
                                </li>
                                <li>
                                     <img alt="Partners" src="{{ url('/') }}/general/images/sdulogo.png">
                                </li>
                                <li>
                                     <img alt="Partners" src="{{ url('/') }}/general/images/engsdu.png">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="footer-6 unpad--bottom  bg--dark" style="background: url(http://sdu.edu.kz/static/images/campus_footer.a1d96cfb37bc.svg) no-repeat center; background-opacity: 2; background-size: auto 80%;background-color: #252529;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <h6 class="type--uppercase">Pages</h6>
                            <p>
                                <a class="nodecoration" href="{{ route('main') }}"><span class="h6 type--uppercase">Home page</span></a>
                                <a class="nodecoration" href="{{ route('kazakhstan') }}"><span class="h6 type--uppercase">About Kazakhstan</span></a>
                                <a class="nodecoration" href="{{ route('rules') }}"><span class="h6 type--uppercase">Rules</span></a>
                                <a class="nodecoration" href="{{ route('faqs') }}"><span class="h6 type--uppercase">FAQs</span></a>
                                <a class="nodecoration" href="{{ route('accommodation') }}"><span class="h6 type--uppercase">Accommodation</span></a>
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <h6 class="type--uppercase">Quick links</h6>
                            <p>
                                <a class="nodecoration" href="#about"><span class="h6 type--uppercase"> About</span></a>
                                <a class="nodecoration" href="#improtantdates"><span class="h6 type--uppercase">  Important Dates</span></a>
                                <a class="nodecoration" href="#categories"><span class="h6 type--uppercase">  Categories </span></a>
                                <a class="nodecoration" href="#schedule"><span class="h6 type--uppercase">  Schedule</span> </a>
                                <a class="nodecoration" href="#map"><span class="h6 type--uppercase"> Bank account</span></a>
                                <a class="nodecoration" href="#map"><span class="h6 type--uppercase"> Requisites</span></a>
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <h6 class="type--uppercase">Contacts</h6>
                            <p>
                                Address: 1/1 Abylai Khan Street<br />
                                Kaskelen, Almaty<br />
                                Kazakhstan, 040900<br />
                                
                                Email: <a class="nodecoration" href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a><br />
                            </p>
                        </div>
                    </div>
                </div>
                <div class="footer__lower text-center-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="type--fine-print">
                                    <span class="update-year">2025</span> Infomatrix Team.
                                </span>
                                <a class="type--fine-print" href="{{ route('terms') }}">Terms of use</a>
                            </div>
                            <div class="col-sm-6 text-right text-center-xs">
                                <ul class="social-list list-inline">
                                    <li>
                                        <a href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q" target="_blank">
                                            <i class="socicon socicon-youtube icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://vk.com/infomatrixasia" target="_blank">
                                            <i class="socicon socicon-vkontakte icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/infomatix2017/" target="_blank">
                                            <i class="socicon socicon-facebook icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/infomatrixasia/" target="_blank">
                                            <i class="socicon socicon-instagram icon icon--xs"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ url('/') }}/gli/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/') }}/gli/js/flickity.min.js"></script>
        <script src="{{ url('/') }}/gli/js/easypiechart.min.js"></script>
        <script src="{{ url('/') }}/gli/js/parallax.js"></script>
        <script src="{{ url('/') }}/gli/js/typed.min.js"></script>
        <script src="{{ url('/') }}/gli/js/datepicker.js"></script>
        <script src="{{ url('/') }}/gli/js/isotope.min.js"></script>
        <script src="{{ url('/') }}/gli/js/ytplayer.min.js"></script>
        <script src="{{ url('/') }}/gli/js/lightbox.min.js"></script>
        <script src="{{ url('/') }}/gli/js/granim.min.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.steps.min.js"></script>
        <script src="{{ url('/') }}/gli/js/countdown.min.js"></script>
        <script src="{{ url('/') }}/gli/js/twitterfetcher.min.js"></script>
        <script src="{{ url('/') }}/gli/js/spectragram.min.js"></script>
        <script src="{{ url('/') }}/gli/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/') }}/gli/js/scripts.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.phancy.js"></script> 

    </body>
</html>