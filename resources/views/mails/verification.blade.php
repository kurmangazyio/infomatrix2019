<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Activation link for your INFOMATRIX-ASIA account!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body data-smooth-scroll-offset="77">
        <div class="nav-container"> </div>
        <div class="main-container">
        	<section class="switchable feature-large">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-6 switchable__text">
                            <h2>Activation link for your INFOMATRIX-ASIA account!</h2>
                            <p class="lead">Dear {{ $user }}, to activate your account - click "Activate my account Account" button. And bottom below is your Infomatrix Account - credentials: </p>
                            <hr class="short">
                            <a class="btn btn--primary type--uppercase" href="{{ $link }}" target="_blank"> 
                                <span class="btn__text">
                                    Activate my account Account
                                </span> 
                            </a>
                            <br><br>
                            <p class="lead">
                                Email : {{$emailer}}<br>
                                Password : {{$newpassword}}
                            </p>
                            
                        </div>
                    </div>
                </div>
            </section>
             <footer class="text-center-xs space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7"> <span class="type--fine-print">Infomatrix - Asia 2025</span> 
                            <a class="type--fine-print" href="{{route('terms')}}">Terms</a> 
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>

</html>