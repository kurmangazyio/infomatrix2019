<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head>
  <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
  <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" name="viewport" />
  <link href="https://fonts.googleapiscom/css?family=Roboto:400,500,600,700,800" rel="stylesheet" type="text/css" />
  <style type="text/css">
    .ReadMsgBody {
      width: 100%;
      background-color: #ffffff;
    }
    
    .ExternalClass {
      width: 100%;
      background-color: #ffffff;
    }
    
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
      line-height: 100%;
    }
    
    html {
      width: 100%;
    }
    
    body {
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
      margin: 0;
      padding: 0;
    }
    
    table {
      border-spacing: 0;
      table-layout: fixed;
      margin: 0 auto;
    }
  
    table table table {
      table-layout: auto;
    }
    
    img {
      display: block ! important;
      overflow: hidden ! important;
    }
  
    table td {
      border-collapse: collapse;
    }
    
    .yshortcuts a {
      border-bottom: none ! important;
    }
    
    a {
      color: #0cb930;
      text-decoration: none;
    }
    
    .textbutton a {
      font-family: 'Roboto', sans-serif !importat;
      color: #0cb930 ! important;
    }
    
    .footer-link a {
      color: #979797 ! important;
    }
    .tpl-content {
        padding:0px ! important;
    
    }   
    
  
  
  /*Responsive*/@media only screen and (max-width: 680px) {
    body {
      width: auto ! important;
    }
    table[class="table680"] {
      width: 440px ! important;
    }
    
    table[class="table-inner"] {
      width: 82% ! important;
      text-align: center ! important;     
    }
    
    table[class="table1"] {
      width: 100% ! important;
      text-align: center ! important;
    }
        

    
    table[class="table2"] {
      width: 100% ! important;
      text-align: center ! important;
      margin-bottom:35px ! important;
    }       
      
    table[class="table3"] {
      width: 100% ! important;
      text-align: center ! important;
      margin-bottom:45px ! important;
    }
          
      
      
    td[class="td_600"] {
       width:100% !important;
       text-align:center ! important; 
       display:block !important;margin:auto ;
       border:none ! important;
    }   

    
    td[class="text-center"] {
      text-align:center ! important;
    }

    
    td[class="padding_hide"] {
      padding:0px ! important;
    }
        
    
    
    td[class="td_hide"] {
      width: 0% !important; 
      height: 0px ! important;
      display:none ! important;
    }
    
    /* image */img[class="img1"] {
      width: 100% ! important;
      height: 100% ! important;       
    }
        

    
  }
  
  @media only screen and (max-width: 479px) {
    body {
      width: auto ! important;
    }
    
    table[class="table680"] {
      width: 300px ! important;
    }
    
    table[class="table-inner"] {
      width: 82% ! important;
      text-align: center ! important;     
    }
    
    
    table[class="table1"] {
      width: 100% ! important;
      text-align: center ! important;
    }    
        
    table[class="table-1"] {
      width: 172px ! important;
      border: none ! important;
      text-align: center ! important;
    } 
    
    
    table[class="table2"] {
      width: 100% ! important;
      text-align: center ! important;
      margin-bottom:35px ! important;
    }   

    
    table[class="table3"] {
      width: 100% ! important;
      text-align: center ! important;
      margin-bottom:45px ! important;
    }   
    


    
    td[class="td_600"] {
       width:100% !important;
       text-align:center ! important; 
       display:block !important;margin:auto ;
       border:none ! important;
       padding:0px;
    }   

    
    td[class="text-center"] {
      text-align:center ! important;
    }
    
    td[class="padding_hide"] {
      padding:0px ! important;
    }
        
    

    
    td[class="td_hide"] {
      width: 0% !important; 
      height: 0px ! important;
      display:none ! important;
    }
    
    /* image */
    
    img[class="img1"] {
      width: 100% ! important;
      height: 100% ! important;       
    }   
    
    

 }


</style>
 </head>
 <body>
  <!-- Start template -->
  <!-- Start module1 -->
  <table align="center" bgcolor="#212121" border="0" cellpadding="0" cellspacing="0" data-bgcolor="background" data-module="module1" data-thumb="" style="background-color: rgb(33, 33, 33); opacity: 1; position: relative; z-index: 0;" width="100%">
   <tr>
    <td>
     <table align="center" bgcolor="#272727" border="0" cellpadding="0" cellspacing="0" class="table680" data-bg="bg-photo1" data-bgcolor="body1" style="background-color: rgb(39, 39, 39);  background-position: 50% 100%; background-size: cover; max-width: 100%;height: 150px;" width="100%">
      <tr>
       <td>
        <table align="center" border="0" cellpadding="0" cellspacing="0" class="table-inner" width="100">
         <tr>
          <td class="td_600" valign="top">
           <table align="left" border="0" cellpadding="0" cellspacing="0" class="table1" width="400">
            <tr>
             <td>
              <table align="left" border="0" cellpadding="0" cellspacing="0" class="table1" width="105">
               <tr>
                <td>
                 <table align="center" border="0" cellpadding="0" cellspacing="0" width="103">
                  <tr>
                   <td height="25">
                   </td>
                  </tr>
                  <!-- Logo -->
                  <tr>
                   <td align="left" class="text-center" data-color="module1_text1" data-size="module1_text1" mc:edit="ab1" style="color: rgb(255, 255, 255); font-family: 'Roboto', sans-serif; font-size: 18px; font-weight: 600; letter-spacing: 2px; line-height: 36px; text-transform: uppercase;"><a data-color="module1_text1" href="#" style="color: rgb(255, 255, 255); text-decoration: none;"><multiline label="ab1">InfoMatrix </multiline></a></td>
                  </tr>
                  <!-- End Logo -->
                 </table>
                </td>
               </tr>
              </table>
             </td>
            </tr>
           </table>
          </td>
          <td class="td_600" valign="top">
           <table align="right" border="0" cellpadding="0" cellspacing="0" class="table1" width="250">
            <tr>
             <td class="td_hide" height="20">
             </td>
            </tr>
            <tr>
             <td valign="top">
              <table align="center" border="0" cellpadding="0" cellspacing="0" width="250">
               <!-- Menu -->
               <tr>
                <td align="left" data-color="module1_text2" data-size="module1_text2" mc:edit="ab2" style="color: rgb(255, 255, 255); font-family: 'Roboto', sans-serif; font-size: 14px; font-weight: 500; letter-spacing: 1px; line-height: 18px; text-transform: uppercase;"><a data-color="module1_text2" href="http://infomatrix.asia" style="color: rgb(255, 255, 255); text-decoration: none;"><multiline label="ab2">Home </multiline></a></td>
                <td align="center" data-color="module1_text2" data-size="module1_text2" mc:edit="ab3" style="color: rgb(255, 255, 255); font-family: 'Roboto', sans-serif; font-size: 14px; font-weight: 500; letter-spacing: 1px; line-height: 18px; padding: 18px; text-transform: uppercase;"><a data-color="module1_text2" href="http://infomatrix.asia/#categories" style="color: rgb(255, 255, 255); text-decoration: none;"><multiline label="ab3"> Categories</multiline></a></td>
               </tr>
               <!-- End Menu -->
              </table>
             </td>
            </tr>
            <tr>
             <td class="" height="10">
             </td>
            </tr>
           </table>
          </td>
         </tr>
        </table>
       </td>
      </tr>
      <tr>
       <td>
        <table align="center" border="0" cellpadding="0" cellspacing="0" class="table-inner" width="800">
         <!-- Content -->
         <tr>
          <td align="center" data-color="module1_text3" data-size="module1_text3" mc:edit="ab5" style="padding-top: 40px;color: rgb(246, 237, 236); font-family: 'Roboto', sans-serif; font-size: 32px; font-weight: 700; letter-spacing: 3px; line-height: 48px; text-transform: uppercase;"><multiline label="ab5">INFOMATRIX - ASIA 2025
</multiline></td>
         </tr>
         <!-- End Content -->
         <!-- Content -->
         <tr style="margin-bottom: 40px;">
          <td align="center" data-color="module1_text4" data-size="module1_text4" mc:edit="ab6" style="color: rgb(246, 237, 236); font-family: 'Roboto', sans-serif; font-size: 15px; font-weight: 500; letter-spacing: 1px; line-height: 26px; padding-left: 30px; padding-right: 30px; padding-top: 10px; text-transform: uppercase;"><multiline label="ab6">Payment Invoice</multiline></td>
         </tr>
         <!-- End See More -->
         <tr>
          <td class="td_hide" height="70">
          </td>
         </tr>
                 </table>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
  <!-- End module1 --><repeater> <layout label="module-2"><!-- Start module2 -->
    
    <table align="center" bgcolor="#212121" border="0" cellpadding="0" cellspacing="0" data-bgcolor="background" data-module="module13" data-thumb="" style="background-color: rgb(33, 33, 33); opacity: 1; position: relative; z-index: 0;" width="100%">
     <tr>
      <td>
       <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="table680" data-bgcolor="body13" style="background-color: rgb(255, 255, 255); max-width: 100%;" width="100%">
        <tr>
        </tr>
        <tr>
         <td valign="top">
          <table align="center" border="0" cellpadding="0" cellspacing="0" class="table-inner" width="80%">
           <tr>
            <td valign="top">
             <table align="left" border="0" cellpadding="0" cellspacing="0" class="table1" width="100%">
              <!-- Content -->
              <tr>
               <td align="left" class="text-center" data-color="module13_text1" data-size="module13_text1" mc:edit="ab58" style="padding-top: 40px; color: rgb(89, 89, 89); font-family: 'Roboto', sans-serif; font-size: 18px; font-weight: 500; letter-spacing: 2px; line-height: 26px; text-transform: uppercase;" valign="top"><multiline label="ab58">Infomatrix - Asia 2025</multiline></td>
              </tr>
              <!-- Content -->
              <tr>
               <td align="left" class="text-center" data-color="module13_text2" data-size="module13_text2" mc:edit="ab59" style="color: rgb(143, 150, 161); font-family: 'Roboto', sans-serif; font-size: 15px; font-weight: 400; line-height: 26px;" valign="top">


<p class="lead" style="white-space: pre-line;color:black;">
                                InvoiceID: {{id}}
                                <table style="width: 80%;text-align: left;color:black;">
                                  <tr>
                                    <th>Supervisor/Student/Guest</th>
                                    <th>Project</th>
                                    <th>Payment option</th>
                                    <th>Amount</th>
                                  </tr>
                                    @foreach ($details as $dt)
                                    <tr>
                                        <td>{{ $dt['supervisorstudent'] }}</td>
                                        <td>{{ $dt['project'] }}</td>
                                        <td>{{ $dt['option'] }}</td>
                                        <td>{{ $dt['amount'] }}</td>
                                    </tr>
                                    @endforeach
                                </table>
                                <span>PAID: {{ $paid }}</span>

                            </p>
                            <p class="lead text-center" style="white-space: pre-line;color:black;text-align: center;">
                                  Our web-site: <a href="infomatrix.asia" target="_blank">infomatrix.asia</a> 
                  E-mail: <a href="mailto:infomatrix@bil.edu.kz" target="_blank">infomatrix@bil.edu.kz</a>
                  Instagram: <a href="https://www.instagram.com/infomatrix.asia/" target="_blank">https://www.instagram.com/infomatrix.asia/</a> 
                  Facebook: <a href="https://www.facebook.com/infomatixasia/" target="_blank">https://www.facebook.com/infomatixasia/</a>
                  Youtube:  <a href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q" target="_blank">https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q</a>
                            </p>

               </td>
              </tr>
              <!-- End Content -->
             </table
 >           </td>
           </tr>
          </table>
         </td>
        </tr>
        <tr>
         <td height="70">
         </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>
</layout>   <layout label="module-14"><!-- Start module14 -->
    <table align="center" bgcolor="#212121" border="0" cellpadding="0" cellspacing="0" data-bgcolor="background" data-module="module14" data-thumb="" style="background-color: rgb(33, 33, 33); opacity: 1; position: relative; z-index: 0;" width="100%">
     <tr>
      <td>
       <table align="center" bgcolor="#272727" border="0" cellpadding="0" cellspacing="0" class="table680" data-bg="bg-photo4" data-bgcolor="body14" style="background-color: rgb(39, 39, 39); background-image: url(images/bg4.jpg); background-position: 50% 100%; background-size: cover; max-width: 100%;" width="100%">
        <tr>
         <td height="60">
         </td>
        </tr>
        <tr>
         <td valign="top">
          <table align="center" border="0" cellpadding="0" cellspacing="0" class="table-inner" width="600">
           <tr>
            <td class="td_600" valign="top">
             <table align="left" border="0" cellpadding="0" cellspacing="0" class="table1" width="125">
              <tr>
               <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" class="table2" width="125">
                 <!-- Content -->
                 <tr>
                  <td align="left" class="text-center" data-color="module14_text1" data-size="module14_text1" mc:edit="ab60" style="color: rgb(255, 255, 255); font-family: 'Roboto', sans-serif; font-size: 24px; font-weight: 600; letter-spacing: 2px; line-height: 24px; text-transform: uppercase;"><multiline label="ab60">Infomatrix.asia</multiline></td>
                 </tr>
                 <!-- Content -->
                 <tr>
                  <td align="left" class="text-center" data-color="module14_text2" data-size="module14_text2" mc:edit="ab62" style="color: rgb(255, 255, 255); font-family: 'Roboto', sans-serif; font-size: 15px; font-weight: 400; line-height: 26px; padding-top: 10px;" valign="top"><multiline label="ab62">website</multiline></td>
                 </tr>
                 <!-- End Content -->
                </table>
               </td>
              </tr>
             </table>
            </td>
            <td class="td_600" valign="top">
             <table align="left" border="0" cellpadding="0" cellspacing="0" class="table1" width="325">
              <tr>
               <td valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="table2" width="280">
                 <!-- images -->
                 <tr>
                  <td align="center" style="padding-bottom: 15px;color:white;font-size: 20px;" valign="top">E-mail:</td>
                 </tr>
                 <tr>
                  <td align="center" class="text-center" data-color="module14_text2" data-size="module14_text2" mc:edit="ab63" style="color: rgb(255, 255, 255); font-family: 'Roboto', sans-serif; font-size: 15px; font-weight: 400; line-height: 26px;" valign="top"><multiline label="ab63">infomatrix@bil.edu.kz</multiline></td>
                 </tr>
                 <!-- End Content -->
                </table>
               </td>
              </tr>
             </table>
            </td>
            <td class="td_600" valign="top">
             <table align="left" border="0" cellpadding="0" cellspacing="0" class="table1" width="150">
              <!-- images-->
              <tr>
               <td align="center" style="padding-bottom: 15px;color:white;font-size: 20px;" valign="top">Instagram</td>
              </tr>
              <!-- End images -->
              <!-- Content -->
              <tr>
               <td align="center" class="text-center" data-color="module14_text2" data-size="module14_text2" mc:edit="ab64" style="color: rgb(255, 255, 255); font-family: 'Roboto', sans-serif; font-size: 15px; font-weight: 400; line-height: 26px;" valign="top"><multiline label="ab64">@infomatrix.asia </multiline></td>
              </tr>
              <!-- End Content -->
             </table>
            </td>
           </tr>
          </table>
         </td>
        </tr>
        <tr>
         <td height="40">
         </td>
        </tr>
       </table>
      </td>
     </tr>
    

    </table>
    <!-- End module14 --></layout> </repeater>

 </body>
</html>
