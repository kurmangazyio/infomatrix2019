<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Infomatrix Asia 2025 - Mailer</title>
        <link type="text/css" rel="shortcut icon" href="http://infomatrix.asia/general/images/components/logo.jpg">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <meta name="description" content="InfoMatrix is an international computer project competition, born from a desire to bring the world's best IT students together. The competition is not just about promoting professional excellence ...">

        <link href="http://infomatrix.asia/gli/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/theme-serpent.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/font-rubiklato.css" rel="stylesheet" type="text/css" media="all" />
        <link href="http://infomatrix.asia/gli/css/jquery.phancy.css" rel="stylesheet" type="text/css" media="all" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />
    </head>
    <body class=" ">
        <a id="start"></a>
        <div class="main-container">
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h1 class="text-center">Infomatrix-Asia 2025</h1>
                            <div class="row text-center ">
                                <div class="col-sm-12">
                                    <ul class="list-inline list-inline--images">
                                        <li>
                                            <img alt="Partners" style="max-height: 5em;" src="http://infomatrix.asia/general/images/daryn.png">
                                        </li>
                                        <li>
                                             <img alt="Partners" style="max-height: 5em;" src="http://infomatrix.asia/general/images/sdulogo.png">
                                        </li>
                                        <li>
                                             <img alt="Partners" style="max-height: 5em;" src="http://infomatrix.asia/general/images/engsdu.png">
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <h1 style="padding-top: 40px;" class="text-center ">Title</h1>
                            <p class="lead" style="white-space: pre-line;color:black;   ">
                                University of Suleyman Demirel together with the Republican Scientific and Practical center "Daryn" holds the International competition on computer projects “INFOMATRIX-ASIA 2025" and has the honor to invite you to the international competition  which will be held from 10 to 14 April 2025 in the city of Kaskelen, Almaty region.

           More than 400 participants from all over the world will demonstrate their skills in the field of information technology’s  international competition. 
You can participate in the competition and defend your project in the following sections: robotics, programming, computer graphics and art, systems controls (hardware), short film, drone racing and math projects. 

In order to take part in the upcoming event, you can register your project on infomatrix-asia.com until March 5, 2025. Registration is open for participants from Kazakhstan and foreign students.

You have to pay the registration fee (payment) after the approval of your project from March 1-15, 2025.

                            </p>
                            <p class="lead">
                                Payment is divided for participation and accommodation by 3 category: <strong> participants, advisors, parents or quests.</strong> 
                            </p>
                            <h4>
                            <span class="color--primary-1">*Notice:</span> Kazakhstani participant may pay online in Infomatrix.asia website. International participants pay on arrival at the competition, it is important to bring the required amount, the organizers do not give change.<br><br>
                            If participant want to register in 2 different project, they must pay participation fee twice. 
                            </h4><br>
                            <p class="lead">
                                <strong>Kazakhstani participants</strong> - 39400 tenge for participation (includes meals 3 times a day). Accommodation in hotel 4 nights - 24 600 tenge.<br><br>

                                <strong>Advisors of Kazakhstan participants:</strong> 10 000 tenge for participation (includes meals 3 times a day). 
                                Accommodation in hotel 4 nights - 24 600 tenge.<br><br>
                                 
                                <strong>Parents</strong> - 10 000 tenge for participation (includes meals 3 times a day)
                                Accommodation in hotel 4 nights - 24 600 tenge.<br><br>
                                 
                                <strong>For international participants</strong> - 120$ for participation (includes meals 3 times a day).
                                Accommodation in hotel 4 nights - 80 $.<br><br>
                                 
                                <strong>Advisors</strong> 30$ for participation (includes meals 3 times a day). Accommodation in hotel 4 nights - 80 $.<br><br>

                                <strong>Parents</strong> 30$ for participation (includes meals 3 times a day). Accommodation in hotel 4 nights - 80 $.<br><br>

                            </p>
                            <h2>For Kazakhstani participant:</h2>
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Participation(includes meals 3 times a day)</th>
                                            <th>Accommodation in hotel 4 nights</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Participants</td>
                                            <td>39400 tenge</td>
                                            <td>24 600 tenge.</td>
                                        </tr>
                                        <tr>
                                            <td>Advisors</td>
                                            <td>10 000 tenge</td>
                                            <td>24 600 tenge.</td>
                                        </tr>
                                        <tr>
                                            <td>Parents or guests</td>
                                            <td>10 000 tenge</td>
                                            <td>24 600 tenge.</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <h2>For international participant:</h2>
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Participation(includes meals 3 times a day)</th>
                                            <th>Accommodation in hotel 4 nights</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Participants</td>
                                            <td>120$</td>
                                            <td>80$</td>
                                        </tr>
                                        <tr>
                                            <td>Advisors</td>
                                            <td>30$</td>
                                            <td>80$</td>
                                        </tr>
                                        <tr>
                                            <td>Parents or guests</td>
                                            <td>30$</td>
                                            <td>80$</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <p class="lead" style="white-space: pre-line;color:black;   ">
                                    It will be a big pleasure if you can help us with the sharing this information among your families and friends to take part in such a large-scale event as the international competition «INFOMATRIX - ASIA 2025» and get a chance to find out the innovative projects of gifted students from more than 20 countries of the world.

More information about the event can be found on our website Infomatrix.asia. Also, Tech. Support will be able to answer all your questions about the competition, categories, and so on.
                            </p>
                            <p class="lead text-center" style="white-space: pre-line;color:black;">
                                    Our web-site: <a href="infomatrix.asia" target="_blank">infomatrix.asia</a> 
                                    E-mail: <a href="mailto:infomatrix@bil.edu.kz" target="_blank">infomatrix@bil.edu.kz</a>
                                    Instagram: <a href="instagram.com/infomatrix.asia" target="_blank">@infomatrix.asia</a> 
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="footer-3 text-center-xs space--xs "><div class="container"><div class="row"><div class="col-sm-6"><img alt="Image" src="http://infomatrix.asia/general/images/components/main.png" class="logo"> <ul class="list-inline list--hover"><li><a href="http://infomatrix.asia"><span>Home page</span></a></li> <li><a href="http://infomatrix.asia/kazakhstan"><span>About Kazakhstan</span></a></li> <li><a href="http://infomatrix.asia/venue"><span>Venue</span></a></li></ul></div> <div class="col-sm-6 text-right text-center-xs"><ul class="social-list list-inline list--hover"><li><a href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q" target="_blank"><i class="socicon socicon-youtube icon icon--xs"></i></a></li> <li><a href="https://vk.com/infomatrixasia" target="_blank"><i class="socicon socicon-vkontakte icon icon--xs"></i></a></li> <li><a href="https://www.facebook.com/infomatix2017/" target="_blank"><i class="socicon socicon-facebook icon icon--xs"></i></a></li> <li><a href="https://www.instagram.com/infomatrix.asia/" target="_blank"><i class="socicon socicon-instagram icon icon--xs"></i></a></li></ul></div></div> <div class="row"><div class="col-sm-6"><p class="type--fine-print">
                        Suleyman Demirel University
                    </p></div> <div class="col-sm-6 text-right text-center-xs"><a href="mailto:infomatrix@bil.edu.kz" target="_blank" class="type--fine-print"><span class="type--fine-print">infomatrix@bil.edu.kz</span></a> <span class="type--fine-print">
                        Infomatrix - Asia <span class="update-year">2025</span>.
                    </span></div></div></div></footer>
        </div>
        <script src="http://infomatrix.asia/gli/js/jquery-3.1.1.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/flickity.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/easypiechart.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/parallax.js"></script>
        <script src="http://infomatrix.asia/gli/js/typed.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/datepicker.js"></script>
        <script src="http://infomatrix.asia/gli/js/isotope.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/ytplayer.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/lightbox.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/granim.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/jquery.steps.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/countdown.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/twitterfetcher.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/spectragram.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/smooth-scroll.min.js"></script>
        <script src="http://infomatrix.asia/gli/js/scripts.js"></script>
        <script src="http://infomatrix.asia/gli/js/jquery.phancy.js"></script> 
    </body>
</html>