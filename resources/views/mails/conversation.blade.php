<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Conversation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body data-smooth-scroll-offset="77">
        <div class="nav-container"> </div>
        <div class="main-container">
        	<section class="switchable feature-large">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-6 switchable__text">
                            <h4>Converation id - #{{$convid}}</h4>
                            <h2>{{$convtitle}}</h2>
                            <p class="lead" style="font-size: 22px;">{{$convcontent}}</p>
                            <br>
                            <h2>Answer:</h2>
                            <p class="lead" style="font-size: 24px;">{{$convreply}}</p>
                        </div>
                    </div>
                </div>
            </section>
            <hr>
             <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td class="es-p15r" valign="top" align="center">
                            <a target="_blank" href="https://www.facebook.com/infomatixasia/"><img title="Facebook" src="https://tlr.stripocdn.email/content/assets/img/social-icons/rounded-gray/facebook-rounded-gray.png" alt="Fb" width="32" height="32"></a>
                        </td>
                        <td class="es-p15r" valign="top" align="center">
                            <a target="_blank" href="https://www.instagram.com/infomatrix.asia/"><img title="Instagram" src="https://tlr.stripocdn.email/content/assets/img/social-icons/rounded-gray/instagram-rounded-gray.png" alt="Inst" width="32" height="32"></a>
                        </td>
                        <td valign="top" align="center">
                            <a target="_blank" href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q"><img title="Youtube" src="https://tlr.stripocdn.email/content/assets/img/social-icons/rounded-gray/youtube-rounded-gray.png" alt="Yt" width="32" height="32"></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>

</html>