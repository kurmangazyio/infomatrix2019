<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Temporary password for your INFOMATRIX-ASIA account!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body data-smooth-scroll-offset="77">
        <div class="nav-container"> </div>
        <div class="main-container">
        	<section class="switchable feature-large">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-6 switchable__text">
                            <h2>Temporary password for your INFOMATRIX-ASIA account!</h2>
                            <p class="lead">Dear {{ $user }}, bottom below is your Infomatrix Account - credentials: </p>
                            <hr class="short">
                            <p class="lead">
                                Email : {{$emailer}}<br>
                                Password : {{$newpassword}}
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>

</html>