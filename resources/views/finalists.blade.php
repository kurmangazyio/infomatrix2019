<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{{ $appname }}</title>
        
        <link type="text/css" rel="shortcut icon" href="{{ route('main') }}/general/images/components/logo.jpg">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="InfoMatrix is an international computer project competition, born from a desire to bring the world's best IT students together. The competition is not just about promoting professional excellence ...">

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.12.0/r-2.3.0/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.12.0/r-2.3.0/datatables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

           ym(52297417, "init", {
                id:52297417,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
           });
        </script>
        <style type="text/css">
            /* Sticky footer styles
            -------------------------------------------------- */
            html {
              position: relative;
              min-height: 100%;
            }
            body {
              margin-bottom: 60px; /* Margin bottom by footer height */
            }

            .footer {
              position: absolute;
              bottom: 0;
              width: 100%;
              height: 60px; /* Set the fixed height of the footer here */
              line-height: 60px; /* Vertically center the text there */
              background-color: #f5f5f5;
            }

            /* Custom page CSS
            -------------------------------------------------- */
            /* Not required for template or sticky footer method. */
            hr {
              margin-top: 1rem;
              margin-bottom: 1rem;
              border: 0;
              border-top: 1px solid rgba(0, 0, 0, 0.1);
            }

            .fbig {
                font-size: 40px;
            }

            .container-2 {
              width: auto;
              max-width: 900px;
              padding: 0 15px;
            }

            #finalists {
                margin-bottom: 60px !important;
            }
        </style>
        <noscript><div><img src="https://mc.yandex.ru/watch/52297417" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    </head>
    <body class=" ">
        <main role="main" class="container container-2" style="margin-bottom: 20px;">
          <h1 class="mt-5 fbig" style="color: #3a506b !important">INFOMATRIX ASIA 2025 – LIST OF FINALISTS</h1>
          <p class="lead" style="font-weight: bold; margin: 0">Official Announcement</p>
          <p>We are pleased to announce the list of finalists for Infomatrix Asia 2025. The selection process was highly competitive, with numerous outstanding projects demonstrating innovation, technical expertise, and creativity. We extend our congratulations to all teams who have made it to the final stage.</p>
          <p class="lead" style="font-weight: bold; margin: 0">Project Category Adjustments</p>
          <p>In order to ensure fair and appropriate evaluation, the Organizing Committee has reviewed all submitted projects. Some projects have been transferred from their initially selected categories to a more suitable category based on their content and purpose. These adjustments were made to enhance the competitiveness and alignment of the projects with the competition criteria. </p>
          <p class="lead" style="font-weight: bold; margin: 0">Important Instructions for Finalists</p>
          <p style="text-align:justify;">
            <ol>
              <li style="text-align:justify;">
                <b>Teams Qualified in Multiple Categories or Student Qualified in Multiple Projects: </b><br/>
                <ul style="list-style-type:circle;">
                  <li>If a team has qualified for the final stage in more than one category, they must select a single project to present at the competition. Additionally, if a student has qualified with multiple projects, they also need to choose only one project for the final stage.</li>
                  <li>The selected project must be confirmed by sending an email to [<a href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a>] no later than 7 March. Failure to confirm will result in automatic selection of one project by the Organizing Committee.</li>
                </ul>  
              </li>
              <li style="text-align:justify;">
                <b>Final Round Details:</b> <br/>
                <ul style="list-style-type:circle;">
                  <li>The final round of Infomatrix Asia 2025 will take place from March 29 to 31, 2025, at Spectrum International School, Astana, Kazakhstan.</li>
                  <li>Teams must be present at the competition venue and prepared to defend their projects before the judging panel.</li>
                </ul>  
              </li>
              <li style="text-align:justify;">
                <b>Presentation Requirements:</b> <br/>
                <ul style="list-style-type:circle;">
                  <li>Finalists must ensure that their project documentation, presentation materials, and technical setup meet the competition requirements.</li>
                  <li>All project presentations must be conducted in English, and teams should be prepared for a Q&A session with the jury.</li>
                </ul>  
              </li>
              <li style="text-align:justify;">
                <b>Stand Display Requirement for Selected Categories:</b> <br/>
                <ul style="list-style-type:circle;">
                  <li>All finalists competing in AI Programming, Hardware Control, Applied Science, Computer Art, Short Movie, and Startup Pitch must prepare a stand for the final round.</li>
                  <li>The stand size can be referenced in Picture 1 for accurate dimensions. Each stand consists of three internal walls, and only these three walls can be used for posters and visuals related to the project, decorations that enhance the presentation, and printed materials such as abstracts, diagrams, and key findings. Additionally, each team will be provided with a table, which may also be used to display project-related materials, prototypes, models, or demonstration equipment.</li>
                  <li>It is important to note that no decorations, posters, or additional materials may be placed outside the allocated stand area.</li>
                  <li>The presentation stand will be evaluated as part of the competition, so teams should ensure a professional and well-organized layout, clear and informative content explaining the project, and a visually appealing and engaging design. <br/> A well-prepared stand can positively impact the final score <b>(up to 10%)</b>, so teams are encouraged to put significant effort into its design.</li>
                </ul>  
              </li>
            </ol>
            <div style="display:flex; align-items: center; justify-content: center;">
            <img src="/2025/stand.jpg" height="400px" />
          </div>
          </p>
          <p class="lead" style="font-weight: bold; margin: 0">Final Notes</p>
          <p>
            Any requests for modifications, such as a change of category or team members, must be submitted in writing to [<a href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a>] and will be reviewed on a case-by-case basis.
            <br/><br/>
            For any inquiries, please contact us at [<a href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a>].<br/>
            We look forward to seeing you in <i>Astana, Kazakhstan, for an exciting and competitive final round of Infomatrix Asia 2025!</i><br/><br/>
            <i>Infomatrix Asia 2025 Organizing Committee</i>

          </p>
          <p style="font-weight: bold; margin-bottom: 10px">Go to <a href="https://infomatrix.asia/">Main page</a></p>
          <p style="font-weight: bold;">Download document: <a href="/2025/finalists.pdf">Finalists.pdf</a></p>
          <p class="lead">
            <strong>
                <span class="pt-2 mt-2">You can view the list of finalists below: </span>
            </strong>
          </p>
          
        </main>

        <hr>
        <div class="text-center" style="font-size: 20px; color: #3a506b !important;">Finalists are announced</div>
        <div class="text-center" style="font-size: 14px; color: #3a506b !important;">The list of finalists can be cached, please hard reload (CTRL+SHIFT+R)</div>
        <div class="container" style="margin-bottom: 80px !important;" id="finalists">
        </div>

        <footer class="footer">
          <div class="container">
            <span class="text-muted">{{ $appname }}</span>
          </div>
        </footer>

        <script type="text/javascript">
            $( document ).ready(function() {
                $.getJSON('/finalists.json', function(data) {
                    var finalists = data.finalists

                    let table = document.createElement('table');
                    table.setAttribute('id', 'dataTable')
                    table.classList.add("table");
                    table.classList.add("table-striped");
                    table.classList.add('table-bordered');
                    table.classList.add('table-hover');

                    let thead = document.createElement('thead');
                    let tbody = document.createElement('tbody');

                    let columns = [
                      "No",
                      "Team Name",
                      "Category",
                      "Country",
                      "Participant 1",
                      "Participant 2",
                      "Participant 3"
                    ]
                    
                    let row_1 = document.createElement('tr');

                    for(var i = 0; i < columns.length; i++) {
                      let heading_1 = document.createElement('th');
                      heading_1.innerHTML = columns[i];
                      row_1.appendChild(heading_1);
                    }
                    
                    thead.appendChild(row_1);

                    for(var i = 0; i < finalists.length; i++){
                        let row = document.createElement('tr');
                        var fin = finalists[i];
                        var finarr = Object.values(fin)
                        var exp = columns.length - finarr.length

                        for(var j = 0; j < finarr.length; j++){
                            let col = document.createElement('td');
                            col.innerHTML = finarr[j];
                            row.appendChild(col);
                        }

                        for(var u = 0; u < exp; u++){
                            let col = document.createElement('td');
                            col.innerHTML = ''
                            row.appendChild(col);
                        }

                        tbody.appendChild(row);
                    }

                    table.appendChild(thead);
                    table.appendChild(tbody);

                    // Adding the entire table to the body tag
                    document.getElementById('finalists').appendChild(table);
                }).then(() => {
                    $('#dataTable').DataTable({
                        paging: false,
                        aaSorting: [[2, 'asc']]
                    });
                });
            });
        </script>
    </body>
</html>