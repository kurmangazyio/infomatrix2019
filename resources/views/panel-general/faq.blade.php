@extends('panel-general.layouts.footers')
@extends('panel-general.layouts.others')
@extends('panel-general.layouts.menus')
@extends('panel-general.layouts.headers')

<!--headers-->
@section('header-one') @parent @endsection
<!--menus-->
@section('menu-one') @endsection
@section('menu-two') @parent @endsection
<!--maincovers-->
@section('main-one') @endsection
@section('main-two') @parent @endsection
<!--components-->
@section('about') @endsection
@section('categories') @endsection

@section('important-date')@parent @endsection  

@section('kz-title') 
            <section class=" ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="h1 text-center">Frequently Asked Questions</span>
                            <span class="lead"><br/></span>
                        </div>
                        <div class="col-md-6">
                            <div class="text-block">
                                <h5>What is the main language of competition? Can I present the project in other language?</h5>
                                <p>
                                    Since Infomatrix Asia is an international project competition and current lingua franca is English language, we decided that everything(documentation, presentation, submission) will be in English.
                                </p>
                            </div>
                            <div class="text-block">
                                <h5>How can I qualify for competition? what is needed?</h5>
                                <p>
                                    Look carefully at category requirements, but common thing is: great presentation, clear documentation, completed project, short video demonstrating your knowledge of English Participants from Kazakhstan should have participated on regional, city, republic project competitions before.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-block">
                                <h5>I plan to participate, but school doesn’t want to send supervisor. What can I do?</h5>
                                <p>
                                    All participants who study at secondary school must participate with their supervisors. Due to the security concerns of the participants.
                                </p>
                            </div>
                            <div class="text-block">
                                <h5>What is the stand? Should we design it?</h5>
                                <p>
                                    Tthe stand dimensions are provided on a website. If you wonder, how it looks like, you can find photos in gallery.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--<section class="text-center ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="h1">Did not find what you were looking for?</span>
                             <span class="h3">Don't worry, Email us!</span>
                            <span class="lead"><br/></span>
                        </div>
                        <div class="col-sm-10 col-md-10 col-sm-offset-1">
                            <div class="row">
                                {{ Form::open(array('url' => '/faqs')) }}
                                    <div class="col-sm-6 text-left">
                                        <label>Your Name:</label>
                                        <input type="text" name="name" class="validate-required" required>
                                    </div>
                                    <div class="col-sm-6 text-left">
                                        <label>Email Address:</label>
                                        <input type="email" name="email" class="validate-required validate-email" required>
                                    </div>
                                    <div class="col-sm-12 text-left">
                                        <label>Message:</label>
                                        <textarea rows="6" name="message" class="validate-required" required></textarea>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <button type="submit" class="btn btn--primary type--uppercase">Send Enquiry</button>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section>-->
            <div id="notificationhere">
                @if(Session::has('message'))
                    <p class="alert text-center {{ Session::get('alert-class', 'form-success') }}">{{ Session::get('message') }}</p>
                @endif
            </div>
@endsection
@section('schedule') @endsection
<!--footers-->
@section('sponsors') @parent @endsection
@section('map') @parent @endsection
@section('footer-one') @parent @endsection
@section('footer-two') @endsection