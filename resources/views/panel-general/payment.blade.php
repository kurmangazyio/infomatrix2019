@extends('panel-general.layouts.footers')
@extends('panel-general.layouts.others')
@extends('panel-general.layouts.menus')
@extends('panel-general.layouts.headers')

<!--headers-->
@section('header-one') @parent @endsection
<!--menus-->
@section('menu-one') @endsection
@section('menu-two') @parent @endsection
<!--maincovers-->
@section('main-one') @endsection
@section('main-two') @parent @endsection
<!--components-->
@section('about') @endsection
@section('categories') @endsection

@section('important-date')@parent @endsection  

@section('kz-title') 
            <section class=" ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <span class="h1 text-center" style="margin-bottom: 5px;">Payments</span>
                            <a class="btn btn--primary-3" style="margin-bottom: 40px;" target="_blank" href="{{route('main')}}/general/files/Payment.pdf">
                                <span class="btn__text">
                                    Download Payments File
                                </span>
                            </a><br>
                            <!--<a href="{{route('terms')}}" style="font-size: 18px;">
                                    Payment Rules & Privacy Policy (политики конфиденциальности)
                            </a>-->

                            <br>
                            <span class="lead"><br/></span>
                        </div>
                        <div class="col-md-12">
                            <div class="text-block">
                                <p class="lead">
                                    Payment is divided for participation and accommodation by 3 category: participants, advisors, parents or quests. 
                                </p>
                                <h3 style="text-align: justify;">
                                    <strong>
                                        <span class="color--primary-1">*Notice:</span>
                                        <ul style="padding-left: 30px;">
                                            <li>● Kazakhstani participant may pay online in Infomatrix.asia website</li>
                                            <li>● International participants pay on arrival at the competition, it is important to
                                        bring the required amount, the organizers do not give change.</li>
                                            <li>● If participant want to register in 2 different project, they must pay participation
                                        fee twice.</li>
                                        </ul>
                                    </strong>
                                    <br>
                                    <strong class="color--primary-1">Kazakhstani participants</strong> - 45 000 tenge for participation (39 000 tenge discount price for early registration until 15.12) . Accommodation in hotel 4 nights - 36 000 tenge(30 000 tenge
                                discount price for early registration until 15.12).<br><br>

                                    <strong class="color--primary-1">Advisors of Kazakhstan participants</strong> - 12 800 tenge for participation (11 000 tenge discount price for early registration until 15.12). Accommodation in hotel 4 nights - 36 000 tenge (30000 tenge discount price for early registration until 15.12).<br><br>

                                    <strong class="color--primary-1">Parents and guests</strong> - 12 800 tenge for participation (11 000 tenge discount price for early registration until 15.12). Accommodation in hotel 4 nights - 36 000 tenge(30 000 tenge discount price for early registration until 15.12).<br>

                                    <hr>

                                    <strong class="color--primary-1">For international participants</strong> - 140$ for participation(120$ discount price for early registration until 15.12). Accommodation in hotel 4 nights - 100$ (80$ discount price for early registration until 15.12).<br><br>

                                    <strong class="color--primary-1">For international Advisors</strong> - 34$ for participation(30$ discount price for early registration until 15.12). Accommodation in hotel 4 nights - 100$ (80$ discount price for early registration until 15.12).<br><br>

                                    <strong class="color--primary-1">For international Parents</strong> - 34$ for participation(30$ discount price for early registration until 15.12). Accommodation in hotel 4 nights - 100$ (80$ discount price for early registration until 15.12).<br><br>
                                </h3>

                                <h2>For Kazakhstani participant:</h2>
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-center" colspan="2">Participation(includes meals 3 times a day)</th>
                                            <th class="text-center" colspan="2">Accommodation in hotel 4 nights</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td>Registration until 15.12</td>
                                            <td>Registration until 30.12</td>
                                            <td>Registration until 15.12</td>
                                            <td>Registration until 30.12</td>
                                        </tr>
                                        <tr>
                                            <td>Participants</td>
                                            <td>39 000 тенге</td>
                                            <td>45 000 тенге</td>
                                            <td>30 000 тенге</td>
                                            <td>36 000 тенге</td>
                                        </tr>
                                        <tr>
                                            <td>Advisors</td>
                                            <td>11 000 тенге</td>
                                            <td>12 800 тенге</td>
                                            <td>30 000 тенге</td>
                                            <td>36 000 тенге</td>
                                        </tr>
                                        <tr>
                                            <td>Parents or guests</td>
                                            <td>11 000 тенге</td>
                                            <td>12 800 тенге</td>
                                            <td>30 000 тенге</td>
                                            <td>36 000 тенге</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <h2>For international participant:</h2>
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-center" colspan="2">Participation(includes meals 3 times a day)</th>
                                            <th class="text-center" colspan="2">Accommodation in hotel 4 nights</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td>Registration until 15.12</td>
                                            <td>Registration until 30.12</td>
                                            <td>Registration until 15.12</td>
                                            <td>Registration until 30.12</td>
                                        </tr>
                                        <tr>
                                            <td>Participants</td>
                                            <td>120$</td>
                                            <td>140$</td>
                                            <td>80$</td>
                                            <td>100$</td>
                                        </tr>
                                        <tr>
                                            <td>Advisors</td>
                                            <td>30$</td>
                                            <td>34$</td>
                                            <td>80$</td>
                                            <td>100$</td>
                                        </tr>
                                        <tr>
                                            <td>Parents or guests</td>
                                            <td>30$</td>
                                            <td>34$</td>
                                            <td>80$</td>
                                            <td>100$</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--<section class="text-center ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="h1">Did not find what you were looking for?</span>
                             <span class="h3">Don't worry, Email us!</span>
                            <span class="lead"><br/></span>
                        </div>
                        <div class="col-sm-10 col-md-10 col-sm-offset-1">
                            <div class="row">
                                {{ Form::open(array('url' => '/faqs')) }}
                                    <div class="col-sm-6 text-left">
                                        <label>Your Name:</label>
                                        <input type="text" name="name" class="validate-required" required>
                                    </div>
                                    <div class="col-sm-6 text-left">
                                        <label>Email Address:</label>
                                        <input type="email" name="email" class="validate-required validate-email" required>
                                    </div>
                                    <div class="col-sm-12 text-left">
                                        <label>Message:</label>
                                        <textarea rows="6" name="message" class="validate-required" required></textarea>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <button type="submit" class="btn btn--primary type--uppercase">Send Enquiry</button>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section>-->
            <div id="notificationhere">
                @if(Session::has('message'))
                    <p class="alert text-center {{ Session::get('alert-class', 'form-success') }}">{{ Session::get('message') }}</p>
                @endif
            </div>
@endsection
@section('schedule') @endsection
<!--footers-->
@section('sponsors') @parent @endsection
@section('map') @parent @endsection
@section('footer-one') @parent @endsection
@section('footer-two') @endsection