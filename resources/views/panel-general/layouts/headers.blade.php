@section('header-one')
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{{ $appname }}</title>
        
        <link type="text/css" rel="shortcut icon" href="{{ route('main') }}/general/images/components/logo.jpg">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="InfoMatrix is an international computer project competition, born from a desire to bring the world's best IT students together. The competition is not just about promoting professional excellence ...">

        <link href="{{ url('/') }}/gli/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/theme-serpent.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/font-rubiklato.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.phancy.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

           ym(52297417, "init", {
                id:52297417,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
           });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/52297417" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->

    </head>
    <body class=" ">
    @show