@section('main-one')
<section class="imageblock switchable feature-large height-100">
    <div class="imageblock__content col-md-4 col-sm-12 pos-right itissmallwhensm" data-overlay="1">
        <div class="background-image-holder">
            <img alt="image" src="{{route('main')}}/general/images/register-cover.jpg" />
        </div>
        <div class="modal-instance text-center">
            <div class="video-play-icon modal-trigger"></div> <br>
            <span style="color: white;">
                <strong>Watch our film</strong>&nbsp;&nbsp;&nbsp;212 Seconds
            </span>
            <div class="modal-container">
                <div class="modal-content bg-dark" data-width="60%" data-height="60%">
                    <iframe src="https://www.youtube.com/embed/xq4gkpbUK30?autoplay=1"
                        allowfullscreen="allowfullscreen"></iframe>
                </div>
            </div>
        </div>
    </div>
    <style>
        .nomargin {
            margin: 0px !important;
        }
    </style>
    <div class="container pos-vertical-center" style="margin-top: 5px;">
        <div class="row">
            <div class="col-md-8 col-sm-12 text-center ptif" style="padding-top: 50px;">
                <span style="display:none;" class="h6 nomargin">Abylaikhana 1/1, Kaskelen, Kazakhstan</span>
                <span style="display:none;" class="h3 color--primary-1 nomargin" style="font-weight: bold;">SULEYMAN
                    DEMIREL UNIVERSITY</span>
                <h1 class="nomargin" style="padding-bottom: 15px;">WELCOME TO INFOMATRIX-ASIA 2025</h1>
                <span class="h2 nomargin" style="font-weight: 500;padding-bottom: 20px;">March 29-31, 2025</span>

                
                <div class="row" style="display:none;">
                    <div class="col-md-12 text-center" style="padding-bottom: 20px;">
                        <span class="h3 nomargin" style="font-weight: 500;">
                            Registration Deadline <span class="color--primary-1"> February 17</span>
                        </span>
                        <span class="h5 nomargin color--primary-1">Registration closes after</span>
                        <span class="h5 countdown nomargin" data-date="02/18/2025"
                            data-date-fallback="Welcome to Infomatrix-Asia!"></span>
                        <br>
                        (Registration is extended till 17st February)
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <span class="h4 nomargin color--primary-1">Infomatrix-Asia 2025 starts after</span>
                        <span class="h1 countdown" data-date="03/29/2025" style="margin-bottom: 10px"
                            data-date-fallback="Welcome to Infomatrix-Asia!"></span>
                        <br>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-12 text-center" style="padding-bottom: 20px;">
                        <span class="h4 nomargin" style="font-weight: 500;">
                            <span class="color--primary-1"> List of Finalists are announced</span>
                        </span>
                    </div>
                </div>
                <!--
                            <p class="lead nomargin" style="padding-bottom: 20px;">
                            	<br>
                                /*Registration is opened now! <strong>Note:</strong>When you registered, it does not mean you are participant of Infomatrix-Asia 2020! You should provide your project and submit till registration ends! Good luck! 
                            </p>
                            
                            -->

                <a class="btn btn--primary-3 type--uppercase" href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                    target="_blank">
                    <span class="btn__text">
                        Infomatrix Asia 2025 (General rule)
                    </span>
                </a>

                <a class="btn btn--primary-1 type--uppercase"  href="/finalists">
                    <span class="btn__text">
                        Finalists
                    </span>
                </a>

                <a class="btn btn--primary-1 type--uppercase" style="display:none;"
                    href="{{ route('main')}}\user/register">
                    <span class="btn__text">
                        Register
                    </span>
                </a>
                <!--<a class="btn btn--primary-3"  href="{{route('payment')}}">
                                <span class="btn__text">
                                    Payments
                                </span>
                            </a>
                            <a class="btn btn--primary-1 type--uppercase" href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdfdocs\Rules (EN).pdf" target="_blank">
                                <span class="btn__text">
                                    Rules (EN)
                                </span>
                            </a>
                            <a class="btn btn--primary-1 type--uppercase" href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdfdocs\Rules (RU).pdf" target="_blank">
                                <span class="btn__text">
                                    Rules (RU)
                                </span>
                            </a>
                            <a class="btn btn--sm btn--primary type--uppercase" target="_blank" href="{{route('main')}}/stand.pdf" style="margin-top: 20px;">
                                <span class="btn__text">
                                    Project Stand Properties
                                </span>
                            </a>-->
            </div>
        </div>
    </div>
</section>
<div class="modal-instance">
    <a class="btn modal-trigger" id="openmodal" href="#" style="display: none;">
        <span class="btn__text">
            TRIGGER MODAL
        </span>
    </a>
    <div class="modal-container">
        <div class="modal-content">
            <section class="cover height-60 imagebg border--round" data-overlay="2">
                <div class="modal-close modal-close-cross" style="font-size: 30px;"></div>
                <div class="background-image-holder" style="background: url(&quot;/promo.jpg&quot;); opacity: 1;">
                    <img alt="background" src="/promo.jpg">
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-md-2 col-lg-2"></div>
                        <div class="col-md-10 col-lg-10">
                            <div class="pl-5">
                                <h2 style="font-size: 55px; margin-bottom: 0px;">
                                    Finalist announced
                                </h2>
                                <p class="lead">
                                    View all finalists of INFOMATRIX-ASIA 2025.
                                </p>
                                <h3 style="margin-bottom: 0px;"> Announcements </h3>
                                <p>
                                    INFOMATRIX Asia Robotics Lego Missions Category video uploaded: <a
                                        href="https://www.youtube.com/watch?v=aQu2fHqJgN4" target="_blank">Watch the
                                        video</a>
                                </p>

                                <p>
                                    INFOMATRIX Asia Robotics Arduino Hackathon Category video uploaded: <a
                                        href="https://youtu.be/y1XD1QuB_yA" target="_blank">Watch the video</a>
                                </p>

                                <a style="display:none" class="btn btn--primary type--uppercase" href="/finalists">
                                    <span class="btn__text">
                                        View finalists list
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
        </div>
    </div>
</div>
@show
@section('main-two')
<section class="cover imagebg height-60">
    <div class="background-image-holder" style="background: url('{{$pageimage}}'); opacity: 10;">
        <img alt="background" src="{{$pageimage}}">
    </div>
    <div class="container pos-vertical-center text-center" style="padding-top: 20px;padding-bottom: 30px;">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <span class="h1">{{$pagetitle}}</span>
            </div>
        </div>
    </div>
</section>
@show

@section('about')
<section class="text-center" id="about" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <span class="h1" style="padding-bottom:  0px; margin-bottom: 0px;">Schedule: Infomatrix 2025</span>
                <span> Schedule for the event is available below!</span>
                <p class="lead" style="padding-top: 30px;">
                    <span class="h3">6th Jun</span>

                <table class="border--round table--alternate-row text-center">
                    <thead>
                        <tr class="text-center">
                            <th class="text-center" style="width: 25%;">6 Jun</th>
                            <th class="text-center" style="width: 25%;">Opening/Competition Day</th>
                            <th class="text-center" style="width: 25%;">Details</th>
                            <th class="text-center" style="width: 25%;">Links</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="display: none;">
                            <td>10:00 - 11:00</td>
                            <td>Official opening</td>
                            <td>
                                SDU Rector's speech, Daryn Director's speech,
                                Faculty of Engineering & Natural Sciences Dean's speech
                            </td>
                            <td>
                                Join from the meeting link<br>
                                <a
                                    href="https://onlinesdu.webex.com/onlinesdu/j.php?MTID=m54aabfade214c7a2f938b7e1bcde50fb">https://onlinesdu.webex.com/onlinesdu/j.php?MTID=m54aabfade214c7a2f938b7e1bcde50fb</a><br>

                                Join by meeting number <br>
                                Meeting number (access code): 184 002 9494<br>
                                Meeting password: MGbjQ55qtQ2<br>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>11:00 - 12:00</td>
                            <td>QA Session with juries</td>
                            <td>
                                Scheduled WebEx meeting for each section, Rules and Regulations briefing
                            </td>
                            <td>
                                <strong>Links will be provided throughout supervisors emails</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">Schedule will be announced soon!</td>
                        </tr>
                    </tbody>
                </table>

                <span class="h3">7th Jun</span>
                <table class="border--round table--alternate-row text-center">
                    <thead>
                        <tr class="text-center">
                            <th class="text-center" style="width: 25%;">7 Jun</th>
                            <th class="text-center" style="width: 25%;">Competition Day 2</th>
                            <th class="text-center" style="width: 25%;">Details</th>
                            <th class="text-center" style="width: 25%;">Links</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="4">Schedule will be announced soon!</td>
                        </tr>
                    </tbody>
                </table>

                <span class="h3">8th Jun</span>
                <table class="border--round table--alternate-row text-center">
                    <thead>
                        <tr class="text-center">
                            <th class="text-center" style="width: 25%;">8 Jun</th>
                            <th class="text-center" style="width: 25%;">Appeal and Results</th>
                            <th class="text-center" style="width: 25%;">Details</th>
                            <th class="text-center" style="width: 25%;">Links</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="4">Schedule will be announced soon!</td>
                        </tr>
                    </tbody>
                </table>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="text-center" id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <span class="h1">About INFOMATRIX</span>
                <p class="lead" align="justify">
                    <strong>INFOMATRIX</strong> is an international computer-based project competition, born from a
                    desire to unite the world's best IT students. The INFOMATRIX-ASIA was originated from the INFOMATRIX
                    competition that is held in Bucharest Romania, in order to expand the horizons of the competition
                    and provide more opportunity for students to participate in. The competition is not just about
                    promoting professional excellence; it also serves to promote intercultural relationship and
                    cooperation, through the involvement of students and teachers from many different countries.

                    <br><br>
                    INFOMATRIX-ASIA, as an international and large-scale competition, provides students with many
                    opportunities, such as obtaining higher preferences for admission to the best universities in the
                    world and Kazakhstan. The Ministry of education and science of the Republic of Kazakhstan recognizes
                    and appreciates the medals and certificates received at the competition as technological.
                    <br><br>
                    Don't miss this great opportunity to prove yourself in INFOMATRIX-ASIA and become a winner! take
                    advantage of this great chance to prove that you are one of the best.


                <div class="typed-headline">
                    <span class="h4 inline-block">Infomatrix is </span>
                    <span class="h4 inline-block typed-text typed-text--cursor color--primary"
                        data-typed-strings=" innovative project ideas..., new technological advances..., new opportunities for the future..., your turn to bring some changes to the World!"></span>
                </div>
                </p>
            </div>
        </div>
    </div>
</section>
@show
@section('categories')
<section id="categories" class="imagebg" data-overlay="4">
    <div class="background-image-holder"
        style="background: url(&quot;{{ route('main') }}/general/images/back-category.jpg&quot;); opacity: 1;">
        <img alt="background" src="{{ route('main') }}/general/images/back-category.jpg">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 text-center pbottom30">
                <span class="h1">Categories</span>
                <p class="lead" align="justify">
                    There are 9 main categories: Computer Graphics and Art, Short Movie, Hardware Control, Applied
                    Science
                    Projects, Arduino Hackathon, LEGO Relay Race Competition, AI Programming, AI Hackathon Startup. The
                    detailed information is provided inside
                    the particular category sections below.
                </p>
                <!-- Robotics, Graphics and, Applied Science Projects, Drone Racing, and  Short Movie -->
            </div>
            <div class="col-sm-12 col-md-12 text-left pbottom30" style="display: none">
                <p class="lead" align="justify">
                    <span style="font-size:21px;">Stand dimensions:</span><br>
                    Back/right/left sides - 1.6 meters, height - 2 m, example stand details provided here. Note: no
                    stand needed for Robotics and Drone Racing categories.
                </p>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="AI Programming" src="{{ route('main') }}/general/images/components/programming.png"
                        class="letit-100-center" width="100px;">
                    <div class="feature__body letit-100 ">
                        <h5>AI Programming</h5>
                        <p align="justify">
                            You are expected to create an application that is beneficial to
                            society, user-friendly, and reliable. The purpose of this
                            category is to assess participants in the field of computer
                            programming...
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="{{ route('main') }}/general/images/programming.jpg" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2>AI Programming</h2>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                        You are expected to create an application that is beneficial to
                                                        society, user-friendly, and reliable. The purpose of this
                                                        category is to assess participants in the field of computer
                                                        programming. You will need to be able to implement various
                                                        algorithms and data structures and solve complex problems. You
                                                        may use any programming or scripting language, such as C++,
                                                        Java, Pascal, Python, or PHP.
                                                        <br><br>
                                                        This year, the competition particularly welcomes projects with a
                                                        focus on Artificial Intelligence (AI), recognizing the rapid
                                                        advancements in this field and its substantial impact on
                                                        society. Such projects are highly relevant, and the committee is
                                                        inclined to favor solutions that incorporate AI. However,
                                                        traditional projects that meet the competition criteria and
                                                        demonstrate social value will also be considered.

                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for AI Programming (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="AI Hackathon" src="{{ route('main') }}/general/images/components/programming.png"
                        class="letit-100-center" width="100px;">
                    <div class="feature__body letit-100 ">
                        <h5>AI Hackathon</h5>
                        <p align="justify">
                            For the first time within the framework of INFOMATRIX-ASIA, we
                            are launching a large-scale AI HACKATHON! This is a unique
                            opportunity for students to showcase their skills in prog..
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="{{ route('main') }}/general/images/programming.jpg" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2>AI Hackathon</h2>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                        For the first time within the framework of INFOMATRIX-ASIA, we
                                                        are launching a large-scale AI HACKATHON! This is a unique
                                                        opportunity for students to showcase their skills in programming
                                                        and development, as well as to integrate the latest artificial
                                                        intelligence technologies into their projects. The hackathon
                                                        will give participants the chance to work with leading mentors
                                                        and AI professionals, helping them unlock their potential and
                                                        enhance their projects.
                                                        <br><br>
                                                        In this hackathon, students are required to submit completed
                                                        projects with CRUDL(read in documentation) functionality
                                                        (Create, Read, Update, Delete, List). In the first stage,
                                                        participants will submit their projects online without any AI
                                                        elements, as AI functions will be randomly assigned in the
                                                        second stage.

                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for AI Hackathon (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="Hardware control" src="{{ route('main') }}/general/images/components/control.png"
                        class="letit-100-center" width="100px;">
                    <div class="feature__body letit-100">
                        <h5>Hardware control</h5>
                        <p align="justify">
                            In this section, you are expected to create software that
                            controls or interacts with an electronic or mechanical device.
                            You have the freedom to choose almost any type of device!
                            Ideally, you’ll assemble an...
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://i.ytimg.com/vi/HKjEun-NbJs/maxresdefault.jpg" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2>Hardware control</h2>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                        In this section, you are expected to create software that
                                                        controls or interacts with an electronic or mechanical device.
                                                        You have the freedom to choose almost any type of device!
                                                        Ideally, you’ll assemble an original device using any spare
                                                        parts and tools available to you. However, it must include a
                                                        software component that interacts with the hardware.
                                                        <br><br>
                                                        Your creativity is just as important as your mechanical and
                                                        programming skills. It’s highly recommended that your project
                                                        benefits people in some way, providing a service or
                                                        functionality that makes life easier. The only limit is your
                                                        imagination!

                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for Applied Science Projects (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="Applied Science Projects " src="{{ route('main') }}/general/images/components/atom.png"
                        class="letit-100-center" width="100px;">
                    <div class="feature__body letit-100">
                        <h5>Applied Science Projects </h5>
                        <p align="justify">
                            The applied science project challenges participants to apply
                            scientific concepts from any field whether biology, mathematics,
                            chemistry, geography, or physics to solve real-world problems.
                            Projects might include developing...
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://i.ytimg.com/vi/HKjEun-NbJs/maxresdefault.jpg" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2>Applied Science Projects </h2>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                        The applied science project challenges participants to apply
                                                        scientific concepts from any field whether biology, mathematics,
                                                        chemistry, geography, or physics to solve real-world problems.
                                                        Projects might include developing prototypes, creating
                                                        technology to address environmental or social issues, or
                                                        designing experiments to explore scientific principles.
                                                        Successful projects will showcase scientific knowledge and
                                                        technical skills while addressing societal challenges, offering
                                                        a unique chance to make a positive impact.


                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for Applied Science Projects (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="Computer Graphics and Art" src="{{ route('main') }}/general/images/components/art.png"
                        class="letit-100-center" width="100px;">
                    <div class="feature__body letit-100">
                        <h5>Computer Graphics and Art</h5>
                        <p align="justify">
                            Computer Art, also known as digital art, is
                            artwork created or presented using digital
                            technology. This category includes purely computer-generated art
                            (e.g., fractals and algorithmic art) as well as pieces developed...

                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://i.ytimg.com/vi/vTcDE1El2wk/maxresdefault.jpg" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2>Computer Graphics and Art</h2>
                                                    <p class="lead size14">Computer Art, also known as digital art, is
                                                        artwork created or presented using digital
                                                        technology. This category includes purely computer-generated art
                                                        (e.g., fractals and
                                                        algorithmic art) as well as pieces developed from other sources,
                                                        like scanned photographs or
                                                        images drawn using vector graphics software. Accepted formats
                                                        include: 2D Artwork, 3D
                                                        Artwork, 3D Animation, 3D Animation
                                                    </p>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                        You may use any software, such as Illustrator, Photoshop, 3D
                                                        Studio Max, AutoCAD, etc. For
                                                        animations, the duration should not exceed 5 minutes. While
                                                        there is no set theme, the
                                                        artwork should communicate a message on its own.
                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for Computer Graphics and Art (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="Short Movie" src="{{ route('main') }}/general/images/components/movie.png"
                        class="letit-100-center" width="100px;">
                    <div class="feature__body letit-100">
                        <h5>Short Movie</h5>
                        <p align="justify">
                            Participants in the Short Movie category are encouraged to create a film based on one of the
                            following topics. Note that these topics are general themes and are not intended to be the
                            titles of the films...
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://s3.amazonaws.com/pbblogassets/uploads/2015/10/Film-Production-Australia-865x505.jpg" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2> General Short Movie Rules</h2>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                        Participants in the Short Movie category are encouraged to
                                                        create a film based on one of the following topics. Note that
                                                        these topics are general themes and are not intended to be the
                                                        titles of the films. Participants should select a unique title
                                                        that specifically relates to their chosen theme.
                                                        <br><br>
                                                        <strong>Topics:</strong> <br>

                                                        Digital Addiction and Screen Time <br>
                                                        The Future of AI in Everyday Life <br>
                                                        Education of the Future: Online Learning and Personalization
                                                        <br>
                                                        Balancing Tradition and Modernity <br>
                                                        Acts of Kindness and Helping Others <br>
                                                        Empowering Youth and Leadership <br>


                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for Short Movie (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="Arduino Hackathon" src="{{ route('main') }}/general/images/components/robotics.png"
                        width="100px;" class="letit-100-center">
                    <div class="feature__body letit-100">
                        <h5>Arduino Hackathon</h5>
                        <p align="justify">
                            The Arduino Hackathon is a dynamic and engaging robotics competition designed for teams of
                            students to showcase their technical and creative skills. Using Arduino-based kits,
                            participants...
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://store-usa.arduino.cc/cdn/shop/files/ABX00021_03.front_934x700.jpg?v=1727102708" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-8 col-sm-push-4">
                                                    <h2>Arduino Hackathon</h2>
                                                    <p class="lead" align="justify" style="font-size: 18px;">
                                                        The Arduino Hackathon is a dynamic and engaging robotics
                                                        competition designed for teams of students to showcase their
                                                        technical and creative skills. Using Arduino-based kits,
                                                        participants will build and program robot cars capable of both
                                                        manual (Bluetooth-controlled) and autonomous operation.
                                                        This competition encourages innovation, teamwork, and hands-on
                                                        learning in an exciting and competitive environment. Each team
                                                        must complete a series of missions, testing their robot's
                                                        functionality, speed, and precision.

                                                    </p>

                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for Arduino Hackathon (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="LEGO Relay Race Competition" src="{{ route('main') }}/general/images/components/drone.png"
                        width="100px;" class="letit-100-center">
                    <div class="feature__body letit-100">
                        <h5>LEGO Relay Race Competition</h5>
                        <p align="justify">
                            The LEGO Relay Race Competition is a dynamic and engaging robotics competition designed for
                            teams of
                            students to showcase their technical and creative skills. Using Arduino-based kits,
                            participants...
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://www.lego.com/cdn/cs/set/assets/blt39fce3ad71743813/Mindstroms-Build_Bot-SPIK3R-Sidekick-Standard.jpg?fit=crop&format=jpg&quality=80&width=800&height=600&dpr=1" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-8 col-sm-push-4">
                                                    <h2>LEGO Relay Race Competition</h2>
                                                    <p class="lead" align="justify" style="font-size: 18px;">
                                                        Design, build and program robots! <br><br>
                                                        Currently, there is a huge interest in robotics all over the
                                                        world. Organizations and people have a certain interest in
                                                        robotics and it is growing very fast. To make the new generation
                                                        more curious and enthusiastic about robots, several competitions
                                                        were held in different countries of the world.


                                                    </p>

                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for LEGO Relay Race Competition (General rule)
                                                        </span>
                                                    </a>
                                                    <a class="btn btn--primary-2 type--uppercase"
                                                        href="{{ route('main')}}/2025/infomatrix_map_tiff.zip"
                                                        target="_blank" style="margin:10px">
                                                        <span class="btn__text">
                                                            Lego Map (Updated zip)
                                                        </span>
                                                    </a>
                                                    <a class="btn btn--primary-2 type--uppercase"
                                                        href="{{ route('main')}}/2025/banner.jpeg"
                                                        target="_blank" style="margin:10px">
                                                        <span class="btn__text">
                                                            Lego Map (Dimension details)
                                                        </span>
                                                    </a>
                                                    <a class="btn btn--primary-2 type--uppercase"
                                                        href="{{ route('main')}}/2025/infomatrix_map_tiff.pdf"
                                                        target="_blank" style="margin:10px">
                                                        <span class="btn__text">
                                                            Lego Map (Dimension pdf)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="Startup" src="{{ route('main') }}/general/images/components/start-up.png"
                        class="letit-100-center" width="140px;">
                    <div class="feature__body letit-100">
                        <h5>Startup</h5>
                        <p align="justify">
                            In this category, we are looking for innovative ideas and projects that can change the
                            future. If you have a unique solution to a problem or a new product that will make the world
                            a better place, join us! Show how your startup can be the next big thing in tech.
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://res.cloudinary.com/people-matters/image/upload/q_auto,f_auto/v1691982397/1691982395.jpg" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2>Startup</h2>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                    In this category, we are looking for innovative ideas and projects that can change the future. If you have a unique solution to a problem or a new product that will make the world a better place, join us! Show how your startup can be the next big thing in tech.


                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for Startup (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Edited-->

            <div class="col-sm-6" style="display: none;">
                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                    <img alt="Drone Racing" src="{{ route('main') }}/general/images/components/drone.png"
                        class="letit-100-center" width="100px;">
                    <div class="feature__body letit-100">
                        <h5>Drone Racing</h5>
                        <p align="justify">
                            During the competition, the main tasks are the design and piloting of the drone. Pilots must
                            fly their drones through courses at high speed and work to “cross” the finish line first.
                        </p>
                        <div class="modal-instance block">
                            <a class="modal-trigger" style="cursor: pointer;">
                                Learn More
                            </a>
                            <div class="modal-container">
                                <div class="modal-content">
                                    <section class="imageblock feature-large bg--white border--round ">
                                        <div class="imageblock__content col-md-4 col-sm-3 pos-left">
                                            <div class="background-image-holder">
                                                <img alt="image"
                                                    src="https://s.yimg.com/ny/api/res/1.2/W8fv96DSNoMcs_ZutU_2sg--/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/http://media.zenfs.com/en-US/homerun/digital_trends_973/2307b778e9d462558977b620051b2a38" />
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-push-5 col-sm-7 col-sm-push-4">
                                                    <h2>Drone Racing</h2>
                                                    <p class="lead size14" align="justify" style="font-size: 18px;">
                                                        During the competition, the main tasks are the design and
                                                        piloting of the drone. Pilots must fly their drones through
                                                        courses at high speed and work to “cross” the finish line first.
                                                    </p>
                                                    <a class="btn btn--primary-1 type--uppercase"
                                                        href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf"
                                                        target="_blank">
                                                        <span class="btn__text">
                                                            Rules for Drone Racing (General rule)
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
@show
@section('important-date')
<section id="improtantdates" class="" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 text-center pbottom30">
                <span class="h1">Important dates</span>
            </div>
            <div class="col-sm-12">
                <style type="text/css">
                    @media only screen and (max-width: 900px) {
                        .widthfullwhensmall {
                            width: 100% !important;
                            margin: 15px !important;
                        }

                        .tab__content {
                            overflow-y: scroll !important;
                        }
                    }

                    @media only screen and (min-width: 901px) {
                        .widthfullwhensmall {
                            margin-bottom: 30px !important;
                            padding-right: 40px;
                        }
                    }
                </style>
                <ul class="process-stepper clearfix" data-process-steps="4">
                    <li class="active widthfullwhensmall">
                        <span class="leaderme">1 April 2021</span>
                        <p class="leader">
                            Online registration and Project submissions!
                        </p>
                    </li>
                    <li class="active widthfullwhensmall" style="padding-right: 40px;">
                        <span class="leaderme">21 February - 16 April 2020</span>
                        <p class="leader">
                            Project submissions!
                        </p>
                    </li>
                    <!--
                                <li class="widthfullwhensmall">
                                    <span class="leaderme">18 February - 3 March 2020</span>
                                    <p class="leader">
                                        Payment processes
                                    </p>
                                </li>
                                <li class="widthfullwhensmall">
                                    <span class="leaderme">3 - 7 March 2020</span>
                                    <p class="leader">
                                        Invitations and visa processes
                                    </p>
                                </li>
                                <li class="widthfullwhensmall">
                                    <span class="leaderme">7-8 April 2020</span>
                                    <p class="leader">
                                        Check-in Dormitory
                                    </p>
                                </li>-->
                    <li class="widthfullwhensmall">
                        <span class="leaderme">3 May 2021</span>
                        <p class="leader">
                            Notification of Final Participants (Daryn)!
                        </p>
                    </li>
                    <li class="widthfullwhensmall">
                        <span class="leaderme">5-7 May 2021</span>
                        <p class="leader">
                            “INFOMATRIX-ASIA” 2021 Competition
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-12 text-center pbottom30" style="padding-top: 50px; display: none;">
                <span class="h2">Important dates for Kazakhstan's participants</span>
            </div>
            <div class="col-sm-12" style="display: none;">
                <ul class="process-stepper clearfix" data-process-steps="6">
                    <li class="active widthfullwhensmall">
                        <!--<li>-->
                        <span class="leaderme">10 October - 29 February 2020</span>
                        <p class="leader">
                            Registration and Project submissions dates are till 29 February!
                        </p>
                    </li>
                    <li class="widthfullwhensmall" style="padding-right: 40px;">
                        <span class="leaderme">01-15 March 2020</span>
                        <p class="leader">
                            Notification of Final Participants!
                        </p>
                    </li>
                    <li class="widthfullwhensmall">
                        <span class="leaderme">15-25 March 2020</span>
                        <p class="leader">
                            Payment processes.
                        </p>
                    </li>
                    <li class="widthfullwhensmall">
                        <span class="leaderme">25 - 30 March 2020</span>
                        <p class="leader">
                            Invitation letters.
                        </p>
                    </li>
                    <li class="widthfullwhensmall">
                        <span class="leaderme">7-8 April 2020</span>
                        <p class="leader">
                            Check-in Dormitory
                        </p>
                    </li>
                    <li class="widthfullwhensmall">
                        <span class="leaderme">9-12 April 2020</span>
                        <p class="leader">
                            “INFOMATRIX-ASIA” 2020 Competition
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
@show
@section('kz-title')
<section class="cover height-80 imagebg text-center" data-overlay="6">
    <div class="background-image-holder"
        style="background: url(&quot;{{ route('main') }}/general/images/kazakhstan.jpg&quot;); opacity: 1;">
        <img alt="background" src="{{ route('main') }}/general/images/kazakhstan.jpg">
    </div>
    <div class="container pos-vertical-center">
        <div class="row">
            <div class="col-sm-6">
                <span class="h1">Welcome to Kazakhstan</span>
                <a href="{{route('kazakhstan')}}">Learn More</a>
                <div class="modal-instance block">
                    <div class="video-play-icon video-play-icon--sm modal-trigger"></div>
                    <span>
                        <strong>Introduction video</strong>&nbsp;&nbsp;&nbsp;90 Seconds</span>
                    <div class="modal-container">
                        <div class="modal-content bg-dark" data-width="60%" data-height="60%">
                            <iframe src="https://www.youtube.com/embed/v48orMA8Yfk?autoplay=0"
                                allowfullscreen="allowfullscreen"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@show
@section('schedule')
<section class="bg--secondary space--xs" id="schedule">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 tbp-60" id="schedule">
                <div class="modal-instance block text-center">
                    <span class="h2 text-center">Event Schedule</span>
                    <div class="video-play-icon video-play-icon--sm modal-trigger "></div>
                    <span>
                        <strong>Watch Promo</strong>&nbsp;&nbsp;&nbsp;
                    </span>
                    <div class="modal-container">
                        <div class="modal-content bg-dark" data-width="60%" data-height="60%">
                            <iframe src="" allowfullscreen=""
                                data-src="https://www.youtube.com/embed/tVNeO_xofMg"></iframe>
                        </div>
                    </div>
                </div>
                <p class="lead">The competition lasts 3 days. The first day is designed for arrival and decoration of
                    your stands. Registration and payment can be done at any time during the day. The second day is the
                    presentation of projects. On the third day at 14:30 Closing Ceremony.</p>
                <div class="col-12" style="display: none;">
                    <section class="text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <span class="h3" style="padding-bottom:  0px; margin-bottom: 0px;">Schedule:
                                        Infomatrix 2025</span>
                                    <span> Schedule for the event is available below!</span>
                                    <p class="lead" style="padding-top: 30px;">
                                        <span class="h3">6th Jun</span>

                                    <table class="border--round table--alternate-row text-center">
                                        <thead>
                                            <tr class="text-center">
                                                <th class="text-center" style="width: 25%;">6 Jun</th>
                                                <th class="text-center" style="width: 25%;">Opening/Competition Day</th>
                                                <th class="text-center" style="width: 25%;">Details</th>
                                                <th class="text-center" style="width: 25%;">Links</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr style="display: none;">
                                                <td>10:00 - 11:00</td>
                                                <td>Official opening</td>
                                                <td>
                                                    SDU Rector's speech, Daryn Director's speech,
                                                    Faculty of Engineering & Natural Sciences Dean's speech
                                                </td>
                                                <td>
                                                    Join from the meeting link<br>
                                                    <a
                                                        href="https://onlinesdu.webex.com/onlinesdu/j.php?MTID=m54aabfade214c7a2f938b7e1bcde50fb">https://onlinesdu.webex.com/onlinesdu/j.php?MTID=m54aabfade214c7a2f938b7e1bcde50fb</a><br>

                                                    Join by meeting number <br>
                                                    Meeting number (access code): 184 002 9494<br>
                                                    Meeting password: MGbjQ55qtQ2<br>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td>11:00 - 12:00</td>
                                                <td>QA Session with juries</td>
                                                <td>
                                                    Scheduled WebEx meeting for each section, Rules and Regulations
                                                    briefing
                                                </td>
                                                <td>
                                                    <strong>Links will be provided throughout supervisors
                                                        emails</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">Schedule will be announced soon!</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <span class="h3">7th Jun</span>
                                    <table class="border--round table--alternate-row text-center">
                                        <thead>
                                            <tr class="text-center">
                                                <th class="text-center" style="width: 25%;">7 Jun</th>
                                                <th class="text-center" style="width: 25%;">Competition Day 2</th>
                                                <th class="text-center" style="width: 25%;">Details</th>
                                                <th class="text-center" style="width: 25%;">Links</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4">Schedule will be announced soon!</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <span class="h3">8th Jun</span>
                                    <table class="border--round table--alternate-row text-center">
                                        <thead>
                                            <tr class="text-center">
                                                <th class="text-center" style="width: 25%;">8 Jun</th>
                                                <th class="text-center" style="width: 25%;">Appeal and Results</th>
                                                <th class="text-center" style="width: 25%;">Details</th>
                                                <th class="text-center" style="width: 25%;">Links</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4">Schedule will be announced soon!</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-sm-12 col-md-6" id="scheduleevent">
                    <ol class="process-3">
                        <li class="process_item">
                            <div class="process__number">
                                <span style="font-size: .8em">Day 1</span>
                            </div>
                            <div class="process__body">
                                <span class="label">MARCH 29, 2025 - SATURDAY</span>
                                <p style="margin-top: 20px;">
                                <h4 class="nomargin" style="padding-top: 15px;">Registration / Poster Installation </h4>
                                </p>
                            </div>

                        </li>
                        <li class="process_item">
                            <div class="process__number">
                                <span style="font-size: .8em">Day 2</span>
                            </div>
                            <div class="process__body">
                                <span class="label">MARCH 30, 2025 - SUNDAY</span>
                                <p style="margin-top: 20px;">
                                <h4 class="nomargin" style="padding-top: 15px;">Opening / Competition</h4>
                                </p>
                            </div>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-12 col-md-6">
                    <ol class="process-3">
                        <li class="process_item">
                            <div class="process__number">
                                <span style="font-size: .8em">Day 3</span>
                            </div>
                            <div class="process__body">
                                <span class="label">MARCH 31, 2025 - MONDAY </span>
                                <p style="margin-top: 20px;">
                                <h4 class="nomargin" style="padding-top: 15px;"> Award Ceremony and Closing</h4>
                                </p>
                            </div>
                        </li>
                        <li class="process_item" style="display: none;">
                            <div class="process__number">
                                <span style="font-size: .8em">Day 4</span>
                            </div>
                            <div class="process__body">
                                <span class="label">MAY 7, 2021 - FRIDAY</span>
                                <p>
                                <h4 class="nomargin" style="padding-top: 15px;">Results</h4>

                                <strong>10:00 - 18:00</strong> - Award Ceremony and Closing

                                </p>
                            </div>
                        </li>
                        <li class="process_item" style="display: none;">
                            <div class="process__number">
                                <span>Day 5</span>
                            </div>
                            <div class="process__body">
                                <span class="label">April 12, 2020 - Sunday</span>
                                <p>
                                </p>
                                <h4 class="nomargin" style="padding-top: 15px;">Closing Ceremony</h4>
                                <strong>08:00 - 10:00</strong> – Breakfast
                                <br>
                                <strong>10:00 - 13:00</strong> - Award Ceremony and Closing
                                <br>
                                <strong>13:00 - 15:00</strong> – Invited dinner for advisors
                                <br>
                                <strong>13:00 - 15:00</strong> – Lunch for participants
                                </p>
                            </div>
                        </li>
                    </ol>
                </div>
            </div>

            <section class="switchable feature-large " style="display: none">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <img alt="Image" class="border--round box-shadow-wide"
                                src="{{ route('main') }}/general/images/accomodation/sdu.jpg">
                        </div>
                        <div class="col-sm-6 col-md-5">
                            <div class="switchable__text">
                                <h2>Accommodation</h2>
                                <p class="lead">
                                    We are inviting participants to stay in our comfortable and modern “Student House”
                                    located on the territory of SDU.
                                </p>
                                <p>
                                    The student house of Suleyman Demirel University offers students not only
                                    accommodation and meals on the territory of the Smart Campus during their studies,
                                    but also the opportunity to improve their social skills and strengthen international
                                    relationships.
                                </p>
                                <!--<a href="{{ route('accommodation') }}" style="margin-top:0px;font-size: 16px;">Learn More »</a>-->
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>

            <div style="display:none;" class="col-md-12 col-sm-12 text-center tbp-60" id="gallery">
                <span class="h2 pdt-20">Event Gallery</span>
                <div class="slider slider--columns" data-arrows="true" data-paging="true">
                    <ul class="slides">
                        <li class="col-sm-4 col-xs-6">
                            <a href="{{ route('main') }}/general/images/promo.jpg" data-lightbox="Gallery 1">
                                <img alt="Image" src="{{ route('main') }}/general/images/promo.jpg" />
                            </a>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <a href="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_7954.JPG?x-email=undefined"
                                data-lightbox="Gallery 1">
                                <img alt="Thumb"
                                    src="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_7954.JPG?x-email=undefined"
                                    class="img-responsive">
                            </a>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <a href="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_7961.JPG?x-email=undefined"
                                data-lightbox="Gallery 1">
                                <img alt="Thumb"
                                    src="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_7961.JPG?x-email=undefined"
                                    class="img-responsive">
                            </a>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <a href="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_7976.JPG?x-email=undefined"
                                data-lightbox="Gallery 1">
                                <img alt="Thumb"
                                    src="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_7976.JPG?x-email=undefined"
                                    class="img-responsive">
                            </a>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <a href="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8025.JPG?x-email=undefined"
                                data-lightbox="Gallery 1">
                                <img alt="Thumb"
                                    src="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8025.JPG?x-email=undefined"
                                    class="img-responsive">
                            </a>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <a href="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8015.JPG?x-email=undefined"
                                data-lightbox="Gallery 1">
                                <img alt="Thumb"
                                    src="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8015.JPG?x-email=undefined"
                                    class="img-responsive">
                            </a>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <a href="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8164.JPG?x-email=undefined"
                                data-lightbox="Gallery 1">
                                <img alt="Thumb"
                                    src="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8164.JPG?x-email=undefined"
                                    class="img-responsive">
                            </a>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <a href="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8195.JPG?x-email=undefined"
                                data-lightbox="Gallery 1">
                                <img alt="Thumb"
                                    src="https://cloclo20.datacloudmail.ru/weblink/thumb/xw1/DJu2/f1ErSopxL/IMG_8195.JPG?x-email=undefined"
                                    class="img-responsive">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div style="display:none;" class="col-md-12 col-sm-12 tbp-60" id="winners">
                <span class="h2 text-center">Winners Infomatrix 2013-2025</span>
                <div class="tabs-container tabs--vertical">
                    <ul class="tabs">
                        <li class="active">
                            <div class="tab__title">
                                <span class="h5">Infomatrix 2025</span>
                            </div>
                            <div class="tab__content">
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Country</th>
                                            <th>Participants</th>
                                            <th>Project</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Computer Graphics and Art</td>
                                            <td>Kazakhstan</td>
                                            <td>Terlikbayeva Aimira</td>
                                            <td>Education changes your world</td>
                                        </tr>
                                        <tr>
                                            <td>Computer Programming</td>
                                            <td>Kazakhstan</td>
                                            <td>Bazargeldiyev Renat</td>
                                            <td>education4you</td>
                                        </tr>
                                        <tr>
                                            <td>Drone Racing</td>
                                            <td>Kyrgyzstan</td>
                                            <td>OMOROV AMAL</td>
                                            <td>DroneCopter</td>
                                        </tr>
                                        <tr>
                                            <td>Hardware control</td>
                                            <td>Kazakhstan</td>
                                            <td>Saparali Bernar</td>
                                            <td>Clever Crutch</td>
                                        </tr>
                                        <tr>
                                            <td>Mathematics Project</td>
                                            <td>Turkmenistan</td>
                                            <td>SHAMUHAMMEDOV RESUL</td>
                                            <td>Two ways of solving inequalities</td>
                                        </tr>
                                        <tr>
                                            <td>Robotics</td>
                                            <td>Kazakhstan</td>
                                            <td>Nurkassym Asset <br> Smagul Aibar</td>
                                            <td>Golden Boys</td>
                                        </tr>
                                        <tr>
                                            <td>Short Movie</td>
                                            <td>Kazakhstan</td>
                                            <td>Oksyuz Alperen <br> Srymuly Ibrahim</td>
                                            <td>Talented</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">Infomatrix 2018</span>
                            </div>
                            <div class="tab__content">
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Country</th>
                                            <th>Project Name</th>
                                            <th>Category</th>
                                            <th>Students</th>
                                            <th>MEDAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Hong Kong</td>
                                            <td>POLICE STORY</td>
                                            <td>Short Movie</td>
                                            <td>WAVOO SEYED ABDUR RAHMAN FAREED</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>KHAN AJAB</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>REZA MD NAHYAN</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Iraq</td>
                                            <td>LOVE CONQUERS ALL</td>
                                            <td>&nbsp;</td>
                                            <td>TARA AHMED MOHAMMED</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Short Movie</td>
                                            <td>Rima Ahmed Sabri</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>RUSUL SAAD MOHAMMED</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Iraq</td>
                                            <td>TOGETHER AND FOREVER</td>
                                            <td>Short Movie</td>
                                            <td>FAEQ BASIL FAEQ</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>ABDULRAHMAN AHMED YASEEN</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>IRAQ</td>
                                            <td>LOVE IT TO SAVE IT</td>
                                            <td>&nbsp;</td>
                                            <td>ALI YOUSIF ALI AL-ASADI</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>IRAQ</td>
                                            <td>DON'T DIM THE LIGHT</td>
                                            <td>&nbsp;</td>
                                            <td>ALI MOHAMMED ABED</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Kyrgyzstan</td>
                                            <td>Love Conquers All</td>
                                            <td>Short Movie</td>
                                            <td>Ayara Ashirbekova</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Azem Ibraeva</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Kyrgyzstan</td>
                                            <td>You make your own way</td>
                                            <td>Short Movie</td>
                                            <td>Tulendybaev Nurmuhammad</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Mirbakiev Nazar</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Kyrgyzstan</td>
                                            <td>Corruption. Flight and Fall.</td>
                                            <td>Short Movie</td>
                                            <td>Kalmamatov Nurbek Almazbekovich</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Mamatazimov Amantur Mamatazimovich</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Orozbai Uulu Bektursun</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Kyrgyzstan</td>
                                            <td>BE FREE</td>
                                            <td>Short Movie</td>
                                            <td>ALTYNAI KABYLBEKOVA</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>DIANA SMANALIEVA</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Kyrgyzstan</td>
                                            <td>All starts with loving yourself.</td>
                                            <td>Short Movie</td>
                                            <td>Eliza Kozhoeva</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Perizat Mamasalieva</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>Kyrgyzstan</td>
                                            <td>Justice</td>
                                            <td>Short Movie</td>
                                            <td>Ademi Ashimova</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Altynai Uchkun kyzy</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>24</td>
                                            <td>Kyrgyzstan</td>
                                            <td>Day without a phone</td>
                                            <td>Short Movie</td>
                                            <td>Zhusubalieva Aigerim</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>25</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Kelsinai Keldibekova</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>26</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Turdakunova Aidai</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>27</td>
                                            <td>Kyrgyzstan</td>
                                            <td>THE NEW DRUG</td>
                                            <td>Short Movie</td>
                                            <td>Tulendybaeva Nasiba</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>28</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Belekova Aidana</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>29</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>30</td>
                                            <td>Kyrgyzstan</td>
                                            <td>DIGITAL SLAVERY</td>
                                            <td>Short Movie</td>
                                            <td>Zhumabaeva Akbermet</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>31</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Aelita Bolushbekova</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>32</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Muurzabek kyzy Zuura</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>33</td>
                                            <td>Tajikistan</td>
                                            <td>Forgotten Culture</td>
                                            <td>Short Movie</td>
                                            <td>Saidullaev Azizjon</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>34</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Umaraliev Romish</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>35</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Khojamir Abdurahmonijomii</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>36</td>
                                            <td>Tajikistan</td>
                                            <td>Battery of Life</td>
                                            <td>Short Movie</td>
                                            <td>Sharifzoda Sharaf</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>37</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Gulmahmadzoda Sarafali</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>38</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Abubakr Sohibov</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>39</td>
                                            <td>Tajikistan</td>
                                            <td>LOVE CONQUERS ALL</td>
                                            <td>Short Movie</td>
                                            <td>Ansori Aniskhoni</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>40</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Shahrom Makhmudov</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>41</td>
                                            <td>Tajikistan</td>
                                            <td>Awakening</td>
                                            <td>Short Movie</td>
                                            <td>Ghaniev Muhammadjon</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>42</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Kholov Rahmatullo</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>43</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Davlatzoda Aburaykhon</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>44</td>
                                            <td>Tajikistan</td>
                                            <td>A Mother's value</td>
                                            <td>Short Movie</td>
                                            <td>Rahmonov Anushervon</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>45</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>khusainov zafarjon</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>46</td>
                                            <td>Tajikistan</td>
                                            <td>Love conquers all</td>
                                            <td>Short Movie</td>
                                            <td>Anvar Aminov</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>47</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Parviz Rasulov</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>48</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Abdurasul Kalonov</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>49</td>
                                            <td>Tajikistan</td>
                                            <td>The influence of Internet Texhnology</td>
                                            <td>Short Movie</td>
                                            <td>Akramov Dier</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>50</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Karimov Eraj</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>51</td>
                                            <td>Tajikistan</td>
                                            <td>Technology in our life</td>
                                            <td>Short Movie</td>
                                            <td>Shahzoda Eshonova</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>52</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Shohina Qayumova</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>53</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Hojizoda Shirinmo</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>54</td>
                                            <td>Tajikistan</td>
                                            <td>Life's Letter</td>
                                            <td>Short Movie</td>
                                            <td>Nabiev Manuchehr</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>55</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Islomov Firdavs</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>56</td>
                                            <td>Tajikistan</td>
                                            <td>Goodness comes back</td>
                                            <td>Short Movie</td>
                                            <td>Mirvaisov Mirvaise</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>57</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Abdulloev Sultonbek</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>58</td>
                                            <td>Tajikistan</td>
                                            <td>Reasonable Choice</td>
                                            <td>Short Movie</td>
                                            <td>Kobiljon Khusainov</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>59</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Azizzhon Dzhuraev</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>60</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Khurshed muminov</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>61</td>
                                            <td>Turkmenistan</td>
                                            <td>PERSECUTION OF THE SISTER-IN-LAW</td>
                                            <td>Short Movie</td>
                                            <td>GULNUR ISHANOVA</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>62</td>
                                            <td>Turkmenistan</td>
                                            <td>PERSECUTION OF THE SISTER-IN-LAW</td>
                                            <td>&nbsp;</td>
                                            <td>ISHAN ISHANOV</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>63</td>
                                            <td>Turkmenistan</td>
                                            <td>Life lesson</td>
                                            <td>Short Movie</td>
                                            <td>Serdarov Sover</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>64</td>
                                            <td>Bangladesh</td>
                                            <td>Love Paved Road</td>
                                            <td>Short Movie</td>
                                            <td>Zareen Subah</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>65</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Mayabi Marzan Pranty</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>66</td>
                                            <td>Bangladesh</td>
                                            <td>Colours of Life</td>
                                            <td>Short Movie</td>
                                            <td>Afia Mahbuba Saba</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>67</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Noushin Sonon Chandona</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>68</td>
                                            <td>Bangladesh</td>
                                            <td>Love Transcends all Boundaries</td>
                                            <td>Short Movie</td>
                                            <td>Amrin Jahan Eshika</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>69</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Patali Warangana Perera Meegodage</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>70</td>
                                            <td>Kazakhstan</td>
                                            <td>Digital slavery</td>
                                            <td>Short Movie</td>
                                            <td>Aday Turar</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>71</td>
                                            <td>Kazakhstan</td>
                                            <td>INDUSTRIOUS</td>
                                            <td>Short Movie</td>
                                            <td>Adilkhan Nurgaliyev</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>72</td>
                                            <td>Kazakhstan</td>
                                            <td>INDUSTRIOUS</td>
                                            <td>Short Movie</td>
                                            <td>Adil Kazkenov</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>73</td>
                                            <td>Kazakhstan</td>
                                            <td>Victims of social network(Digital Slavery)</td>
                                            <td>Short Movie</td>
                                            <td>Aibek Tugelbayev</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>74</td>
                                            <td>Kazakhstan</td>
                                            <td>LIKE</td>
                                            <td>Short Movie</td>
                                            <td>Batyrbek Kayipbay</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>75</td>
                                            <td>Kazakhstan</td>
                                            <td>Boomerang</td>
                                            <td>Short Movie</td>
                                            <td>Serikuly Zhasulan</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>76</td>
                                            <td>Kazakhstan</td>
                                            <td>Boomerang</td>
                                            <td>Short Movie</td>
                                            <td>Mahsutov Sultan</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>77</td>
                                            <td>Kazakhstan</td>
                                            <td>Boomerang</td>
                                            <td>Short Movie</td>
                                            <td>Marat Ramadan</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>78</td>
                                            <td>Kazakhstan</td>
                                            <td>Time (Digital Slavery)</td>
                                            <td>Short Movie</td>
                                            <td>Nigina Adam</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>79</td>
                                            <td>Kazakhstan</td>
                                            <td>Time (Digital Slavery)</td>
                                            <td>Short Movie</td>
                                            <td>Mira El Mostafa</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>80</td>
                                            <td>Kazakhstan</td>
                                            <td>INTERVIEW</td>
                                            <td>Short Movie</td>
                                            <td>ASSIMOV KASSYMZHAN</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>81</td>
                                            <td>Kazakhstan</td>
                                            <td>INTERVIEW</td>
                                            <td>Short Movie</td>
                                            <td>MYRZABAYEV DANIYAR</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>82</td>
                                            <td>Kazakhstan</td>
                                            <td>MY FILM</td>
                                            <td>Short Movie</td>
                                            <td>TALKANBAYEV SHYNGYSKHAN</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>83</td>
                                            <td>Kazakhstan</td>
                                            <td>MY FILM</td>
                                            <td>Short Movie</td>
                                            <td>NURLAN ZANGGAR</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>84</td>
                                            <td>Kazakhstan</td>
                                            <td>Justice</td>
                                            <td>Short Movie</td>
                                            <td>Zhusipbek Zhomart</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>85</td>
                                            <td>Kazakhstan</td>
                                            <td>Justice</td>
                                            <td>Short Movie</td>
                                            <td>Darkhan Zhanggirkhan</td>
                                            <td>Silver</td>
                                        </tr>
                                        <tr>
                                            <td>86</td>
                                            <td>Kazakhstan</td>
                                            <td>RAVE VICTIM</td>
                                            <td>Short Movie</td>
                                            <td>Adil Kazkenov</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>87</td>
                                            <td>Kazakhstan</td>
                                            <td>RAVE VICTIM</td>
                                            <td>Short Movie</td>
                                            <td>Arnur Nurkeyev</td>
                                            <td>Gold</td>
                                        </tr>
                                        <tr>
                                            <td>88</td>
                                            <td>Tajikistan</td>
                                            <td>Love conquers All</td>
                                            <td>Short Movie</td>
                                            <td>Zebojon Rasulova</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>89</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Farzonakhon Bobojonova</td>
                                            <td>Bronze</td>
                                        </tr>
                                        <tr>
                                            <td>90</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>Odinazoda Madinakhon</td>
                                            <td>Bronze</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">Infomatrix 2017</span>
                            </div>
                            <div class="tab__content">
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Country</th>
                                            <th>Participants</th>
                                            <th>Project</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Computer Programming</td>
                                            <td>Turkmenistan</td>
                                            <td>Dovletgeldi Rozymuradov</td>
                                            <td>STP connection</td>
                                        </tr>
                                        <tr>
                                            <td>Computer Programming</td>
                                            <td>Kazakhstan</td>
                                            <td>Dinmukhammet Zholdybai</td>
                                            <td>TPS-Teacher Personal System</td>
                                        </tr>
                                        <tr>
                                            <td>Hardware control</td>
                                            <td>Kazakhstan</td>
                                            <td>Kuanysh Kyrykbayev</td>
                                            <td>ARS</td>
                                        </tr>
                                        <tr>
                                            <td>Hardware control</td>
                                            <td>Kazakhstan</td>
                                            <td>Raukhat Arkulov</td>
                                            <td>SmartLife</td>
                                        </tr>
                                        <tr>
                                            <td>Robotics</td>
                                            <td>Kazakhstan</td>
                                            <td>Sapayev Izzatilla</td>
                                            <td>Mini Sumo</td>
                                        </tr>
                                        <tr>
                                            <td>Robotics</td>
                                            <td>Kazakhstan</td>
                                            <td>Aiganym Serik</td>
                                            <td>Lego Sumo</td>
                                        </tr>
                                        <tr>
                                            <td>Computer Graphics and Art</td>
                                            <td>Kazakhstan</td>
                                            <td>Damir Seisenbekov</td>
                                            <td>Exoskeleton</td>
                                        </tr>
                                        <tr>
                                            <td>Computer Graphics and Art</td>
                                            <td>Kazakhstan</td>
                                            <td>Aidar Turlanov</td>
                                            <td>3D моделирование планетных систем </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">Infomatrix 2016</span>
                            </div>
                            <div class="tab__content">
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Country</th>
                                            <th>Participants</th>
                                            <th>Project</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Computer Art</td>
                                            <td>Tajikistan</td>
                                            <td>
                                                Melod Pirzoda
                                                <br>
                                                Niyazov Iso
                                            </td>
                                            <td>
                                                Waste or Survive
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Hardware Control</td>
                                            <td>Kazakhstan</td>
                                            <td>
                                                Abishev Galymbek
                                                <br>
                                                Abylkairov Marat
                                            </td>
                                            <td>
                                                Automated Robotic Prosthetic Arm
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Programming</td>
                                            <td>Turkmenistan</td>
                                            <td>
                                                Jelaleddin Sultanov
                                                <br>
                                                Yakup Garayev
                                            </td>
                                            <td>
                                                Let's Brain
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Short Movie</td>
                                            <td>Kazakhstan</td>
                                            <td>
                                                Chaizatov Rassul
                                                <br>
                                                Zhuman Abylay
                                            </td>
                                            <td>
                                                Aldar Kose
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Robotics</td>
                                            <td>Kazakhstan</td>
                                            <td>
                                                Aibek Shakentayev
                                                <br>
                                                Adil Shakayev
                                            </td>
                                            <td>
                                                MRX
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">Infomatrix 2015</span>
                            </div>
                            <div class="tab__content">
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Country</th>
                                            <th>Participants</th>
                                            <th>Project</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Computer Art</td>
                                            <td>Tajikistan</td>
                                            <td>
                                                Barzu Mirzoev
                                                <br>
                                                Nozimov Abdullo
                                            </td>
                                            <td>
                                                Save the World , Save Yourself
                                                <br>
                                                <a
                                                    href=" https://www.youtube.com/watch?v=td9G207W7uo&amp;feature=youtu.be">Watch
                                                    the video</a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Hardware Control</td>
                                            <td>Kazakhstan</td>
                                            <td>
                                                Mariyam Tleukhan
                                                <br>
                                                Assiya Zhiyenbek
                                            </td>
                                            <td>
                                                Voice controlled car
                                                <br>
                                                <a href="https://www.youtube.com/watch?v=E_iX36l0378">Watch the
                                                    video</a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Programming</td>
                                            <td>Kazakhstan</td>
                                            <td>
                                                Nurtay Bekarys
                                                <br>
                                                Saipolla Saken
                                            </td>
                                            <td>
                                                Application "Quy Band"
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Short Movie</td>
                                            <td>Kazakhstan</td>
                                            <td>
                                                Aisultan Seitov
                                                <br>
                                                Temirlan Uali
                                            </td>
                                            <td>
                                                Earth regeneration
                                                <br>
                                                <a href="https://www.youtube.com/watch?v=jLLP3ARd-lw">Watch the
                                                    video</a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Robotics</td>
                                            <td>Azerbaijan</td>
                                            <td>
                                                Guliyeva Pakiza
                                                <br>
                                                Alizada Alsu
                                            </td>
                                            <td>
                                                Line Warrior
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">Infomatrix 2014</span>
                            </div>
                            <div class="tab__content">
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Country</th>
                                            <th>Participants</th>
                                            <th>Project</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Computer Art</td>
                                            <td>Tajikistan</td>
                                            <td>
                                                Mukhtashami Oblokul
                                                <br>
                                                Davlatyorov Hasanshoh
                                            </td>
                                            <td>
                                                Journey of a letter
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Hardware Control</td>
                                            <td>Bosnia and Herzegovina</td>
                                            <td>
                                                Hasan Grosic
                                                <br>
                                                Enver Bakija
                                            </td>
                                            <td>
                                                Voice controlled car
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Programming</td>
                                            <td>Kazakhstan</td>
                                            <td>Bezruchenko Daniil</td>
                                            <td>
                                                Smart House DL14 via GSM alert
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Short Movie</td>
                                            <td>Kazakhstan</td>
                                            <td>Abdisadyk Gulnar
                                                <br>
                                                Abdisadyk Diana
                                            </td>
                                            <td>
                                                The warmth of Family
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Robotics</td>
                                            <td>Turkey</td>
                                            <td>Omer Durukan Ozturk</td>
                                            <td>
                                                Kanuni
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">Infomatrix 2013</span>
                            </div>
                            <div class="tab__content">
                                <table class="border--round table--alternate-row">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Country</th>
                                            <th>Participants</th>
                                            <th>Project</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Computer Art</td>
                                            <td>Ukraine</td>
                                            <td>
                                                Vadym Prokopov
                                                <br>
                                                Dmytro Kolyvai
                                            </td>
                                            <td>
                                                Educational Games
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Hardware Control</td>
                                            <td>Pakistan</td>
                                            <td>Waleed Bashir</td>
                                            <td>
                                                GreenAutomation
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Programming</td>
                                            <td>Turkmenistan</td>
                                            <td>Kerimberdi Agayev
                                                <br>
                                                Rahym Valiyev
                                            </td>
                                            <td>
                                                Gonurja
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Short Movie</td>
                                            <td>Kazakhstan</td>
                                            <td>
                                                Ablaykhan Kairzhanov
                                                <br>
                                                Yakuden Nursultanov
                                            </td>
                                            <td>
                                                Debutfilms - Chance
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@show

@section('map')
<section id="map" class="switchable bg--secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-5">
                <div class="switchable__text text-center">
                    <h3 style="display:none;">Street Abylaikhana 1/1
                        <br>Kaskelen, Kazakhstan
                    </h3>
                    <p class="lead">
                        Email:
                        <a class="nodecoration" href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a>
                    </p>
                    <p class="lead">
                        Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on
                        business days.
                    </p>
                    <div class="modal-instance block" style="display: none;">
                        <p class="lead">
                            <a class="modal-trigger nodecoration" href="javascript:void(0)" data-modal-index="10">
                                <span class="btn__text">
                                    Bank account
                                </span>
                            </a>
                        </p>
                        <div class="modal-container">
                            <div class="modal-content">
                                <section class="imageblock feature-large bg--white border--round ">
                                    <div class="imageblock__content col-md-5 col-sm-3 pos-left">
                                        <div class="background-image-holder">
                                            <img alt="image" src="{{ route('main') }}/general/images/subcover.jpg" />
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-5 col-md-push-6 col-sm-7 col-sm-push-4">
                                                <h1>Requisites</h1>
                                                <p class="lead">
                                                    <strong style="color:red;">You can see requisites and do payment in
                                                        your userpanel! You can pay online(online payment can do only
                                                        Kazakhstani participants) only when your project finally
                                                        approved by Infomatrix and Daryn (For Kazakhstan students),For
                                                        foreign participants, payments will be done only upon arrival at
                                                        the competition. Final approvement will be sent by email to
                                                        supervisors' emails. Stay tuned! Do not rush to make
                                                        payments</strong><br><br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="modal-instance block" style="display: none;">
                        <p class="lead">
                            <a class="modal-trigger nodecoration" href="javascript:void(0)" data-modal-index="11">
                                <span class="btn__text">
                                    Directions to SDU from Almaty
                                </span>
                            </a>
                        </p>
                        <div class="modal-container">
                            <div class="modal-content">
                                <section class="imageblock feature-large bg--white border--round ">
                                    <div class="imageblock__content col-md-4 col-sm-4 pos-left">
                                        <div class="background-image-holder">
                                            <img alt="image" src="{{ route('main') }}/general/images/direction.png" />
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 col-md-push-5 col-sm-8 col-sm-push-4 nomargin"
                                                style="padding-bottom: 0px;">
                                                <h4>Directions to SDU from Almaty</h4>
                                                <p class="lead" style="font-size: 16px;" align="justify">
                                                    If you want to get to SDU by bus, firstly you will have to get to
                                                    Sayran bus station:<br>
                                                    <span class="nomargin color--primary-1">“Almaty 1” train station –
                                                        Sayran bus station:<span> Bus numbers: 72. Taxi will cost
                                                            approximately 1500 tenge.<br>
                                                            <span class="nomargin color--primary-1">“Almaty 2” train
                                                                station – Sayran bus station:<span> Bus numbers: 37, 50,
                                                                    59, 100, 137. Taxi will cost approximately 500
                                                                    tenge.<br>
                                                                    <span class="nomargin color--primary-1">“Sayahat”
                                                                        bus station – Sayran:<span> Bus numbers: 37, 50,
                                                                            59, 100. Taxi will cos approximately 600
                                                                            tenge.<br>
                                                                            <span class="nomargin color--primary-1">From
                                                                                “Sayran” you can get to SDU by following
                                                                                buses:<span> 345, 330. You can get to
                                                                                    SDU from Sayran(Kaskelen bus stop)
                                                                                    by taxi. It will cost approximately
                                                                                    200-300 tenge from a person.<br><br>
                                                                                    <span
                                                                                        class="nomargin color--primary-1">Attention
                                                                                        for those who are coming from
                                                                                        Kyrgyzstan:<span> You may not go
                                                                                            to Sayran bus station
                                                                                            instead you should stop on
                                                                                            Bishkek-Almaty main road
                                                                                            near Kaskelen turn.<br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="modal-instance block" style="display: none">
                        <p class="lead">
                            <a class="modal-trigger nodecoration" href="javascript:void(0)" data-modal-index="11">
                                Payment Rules & Privacy Policy <br>(политики конфиденциальности)
                            </a>
                        </p>
                        <div class="modal-container">
                            <div class="modal-content">
                                <section class="imageblock feature-large bg--white border--round ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12" style="padding: 30px;">
                                                <h2 class="contant-title text-center" id="sample">Платежи. общие
                                                    сведения</h2>
                                                <br>
                                                <div class="documentation-block">
                                                    <span class="h2 text-center">Платежи. Оплата банковской картой
                                                        онлайн</span>
                                                    <div class="documentation-text" align="justify">
                                                        <p>
                                                            Наш сайт подключен к интернет-эквайрингу, и Вы можете
                                                            оплатить Услугу банковской картой Visa или Mastercard. После
                                                            подтверждения выбранного Товара либо услуги откроется
                                                            защищенное окно с платежной страницей процессингового центра
                                                            CloudPayments, где Вам необходимо ввести данные Вашей
                                                            банковской карты. Для дополнительной аутентификации
                                                            держателя карты используется протокол 3-D Secure. Если Ваш
                                                            Банк-эмитент поддерживает данную технологию, Вы будете
                                                            перенаправлены на его сервер для прохождения дополнительной
                                                            идентификации. Информацию о правилах и методах
                                                            дополнительной идентификации уточняйте в Банке, выдавшем Вам
                                                            банковскую карту.
                                                        </p>
                                                        <p>
                                                            Услуга онлайн-оплаты осуществляется в соответствии с
                                                            правилами Международных платежных систем Visa и MasterCard
                                                            на принципах соблюдения конфиденциальности и безопасности
                                                            совершения платежа, для этого используются самые актуальные
                                                            методы проверки, шифрования и передачи данных по закрытым
                                                            каналам связи. Ввод данных банковской карты осуществляется в
                                                            защищенном окне на платежной странице CloudPayments.
                                                        </p>
                                                        <p>
                                                            В поля на платежной странице требуется ввести номер карты,
                                                            имя владельца карты, срок действия карты, трёхзначный код
                                                            безопасности (CVV2 для VISA или CVC2 для MasterCard). Все
                                                            необходимые данные отображены на поверхности банковской
                                                            карты.
                                                        </p>
                                                        <p>
                                                            CVV2/ CVC2 — это трёхзначный код безопасности, находящийся
                                                            на оборотной стороне карты.
                                                        </p>
                                                        <p>
                                                            Далее в том же окне откроется страница Вашего банка-эмитента
                                                            для ввода 3-D Secure кода. В случае, если у вас не настроен
                                                            статичный 3-D Secure, он будет отправлен на ваш номер
                                                            телефона посредством SMS. Если 3-D Secure код к Вам не
                                                            пришел, то следует обратится в ваш банк-эмитент.
                                                        </p>
                                                        <p>3-D Secure — это самая современная технология обеспечения
                                                            безопасности платежей по картам в сети интернет. Позволяет
                                                            однозначно идентифицировать подлинность держателя карты,
                                                            осуществляющего операцию, и максимально снизить риск
                                                            мошеннических операций по карте.</p>
                                                    </div>
                                                </div>
                                                <div class="documentation-block" style="padding-top: 25px;">
                                                    <strong><span class="h2 text-center">Гарантии
                                                            безопасности</span></strong>
                                                    <div class="documentation-text">
                                                        <p>Процессинговый центр CloudPayments защищает и обрабатывает
                                                            данные Вашей банковской карты по стандарту безопасности PCI
                                                            DSS 3.0. Передача информации в платежный шлюз происходит с
                                                            применением технологии шифрования SSL. Дальнейшая передача
                                                            информации происходит по закрытым банковским сетям, имеющим
                                                            наивысший уровень надежности. CloudPayments не передает
                                                            данные Вашей карты нам и иным третьим лицам. Для
                                                            дополнительной аутентификации держателя карты используется
                                                            протокол 3-D Secure. </p>
                                                        <p>В случае, если у Вас есть вопросы по совершенному платежу, Вы
                                                            можете обратиться в службу поддержки клиентов платежного
                                                            сервиса по электронной почте <a
                                                                href="mailto:support@cloudpayments.kz">support@cloudpayments.kz</a>.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="documentation-block" style="padding-top: 25px;">
                                                    <span class="h2 text-center">Безопасность онлайн платежей</span>
                                                    <div class="documentation-text">
                                                        <p>Предоставляемая Вами персональная информация (имя, адрес,
                                                            телефон, e-mail, номер кредитной карты) является
                                                            конфиденциальной и не подлежит разглашению. Данные Вашей
                                                            кредитной карты передаются только в зашифрованном виде и не
                                                            сохраняются на нашем Web-сервере.</p>
                                                        <p>Мы рекомендуем вам проверить, что ваш браузер достаточно
                                                            безопасен для проведения платежей онлайн, на <a
                                                                href="https://my.cloudpayments.ru/ru/browser">специальной
                                                                странице</a>.</p>
                                                        <p>Безопасность обработки Интернет-платежей гарантирует ТОО
                                                            «CloudPayments Kazakhstan». Все операции с платежными
                                                            картами происходят в соответствии с требованиями VISA
                                                            International, MasterCard и других платежных систем. При
                                                            передаче информации используются специализированные
                                                            технологии безопасности карточных онлайн-платежей, обработка
                                                            данных ведется на безопасном высокотехнологичном сервере
                                                            процессинговой компании.</p>
                                                        <p>Оплата платежными картами безопасна, потому что:</p>
                                                        <ul>
                                                            <li>Система авторизации гарантирует покупателю, что
                                                                платежные реквизиты его платежной карты (номер, срок
                                                                действия, CVV2/CVC2) не попадут в руки мошенников, так
                                                                как эти данные не хранятся на сервере авторизации и не
                                                                могут быть похищены.</li>
                                                            <li>Покупатель вводит свои платежные данные непосредственно
                                                                в системе авторизации CloudPayments, а не на сайте
                                                                интернет-магазина, следовательно, платежные реквизиты
                                                                карточки покупателя не будут доступны третьим лицам.
                                                            </li>
                                                        </ul>
                                                        <div>
                                                            <img src="https://cloudpayments.kz/images/docs/logo-small.png"
                                                                alt="CloudPayments" />&nbsp;&nbsp;
                                                            <img src="https://cloudpayments.kz/images/docs/mastercard-securecode.png"
                                                                alt="MasterCard SecureCode" height="153" />&nbsp;&nbsp;
                                                            <img src="https://cloudpayments.kz/images/docs/vbv.png"
                                                                alt="Verifyed By Visa" width="170" />&nbsp;&nbsp;
                                                            <!--<img src="~/images/docs/MIRaccept.png" alt="MirAccept" style="vertical-align: bottom" />-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="documentation-block" style="padding-top: 25px;">
                                                    <span class="h2 text-center">Возврат денежных средств</span>
                                                    <div class="documentation-text">
                                                        <p>
                                                            При проведении онлайн-оплаты посредством платежных карт не
                                                            допускается возврат наличными денежными средствами. Порядок
                                                            возврата регулируется правилами международны
                                                        </p>
                                                        <ol>
                                                            <li>Команда вправе отказаться от участия в любое время,
                                                                после оплаты отказ необходимо оформить в течение 14
                                                                дней;</li>
                                                        </ol>
                                                        <p>
                                                            Для возврата денежных средств на банковскую карту необходимо
                                                            заполнить «Заявление о возврате денежных средств», которое
                                                            высылается по требованию компанией на электронный адрес, и
                                                            оправить его вместе с приложением копии документа,
                                                            удостоверяющего личность, по адресу <a
                                                                href="mailto:infomatrix@bil.edu.kz"><span
                                                                    class="brown bold">infomatrix@bil.edu.kz</span></a>.
                                                        </p>
                                                        <p>
                                                            Возврат денежных средств будет осуществлен на банковскую
                                                            карту в течение 10 рабочего дня со дня получения «Заявление
                                                            о возврате денежных средств» Компанией.
                                                        </p>
                                                        <p>
                                                            Для возврата денежных средств по операциям, проведенным с
                                                            ошибками, необходимо обратиться с письменным заявлением и
                                                            приложением копии документа, удостоверяющего личность, и
                                                            чеков/квитанций, подтверждающих ошибочное списание. Данное
                                                            заявление необходимо направить по адресу <a
                                                                href="mailto:infomatrix@bil.edu.kz"><span
                                                                    class="brown bold">infomatrix@bil.edu.kz</span></a>.
                                                        </p>
                                                        <p>
                                                            Сумма возврата будет равняться сумме пополнения. Срок
                                                            рассмотрения Заявления и возврата денежных средств начинает
                                                            исчисляться с момента получения Компанией Заявления и
                                                            рассчитывается в рабочих днях без учета праздников/выходных
                                                            дней.

                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="documentation-block" style="padding-top: 25px;">
                                                    <span class="h2 text-center">Случаи отказа в совершении
                                                        платежа:</span>
                                                    <div class="documentation-text">
                                                        <ul>
                                                            <li>банковская карта не предназначена для совершения
                                                                платежей через интернет, о чем можно узнать, обратившись
                                                                в Ваш Банк-эмитент;</li><br>
                                                            <li>недостаточно средств для оплаты на банковской карте.
                                                                Подробнее о наличии средств на платежной карте Вы можете
                                                                узнать, обратившись в банк, выпустивший банковскую
                                                                карту;</li>
                                                            <li>данные банковской карты введены неверно;</li>
                                                            <li>истек срок действия банковской карты. Срок действия
                                                                карты, как правило, указан на лицевой стороне карты (это
                                                                месяц и год, до которого действительна карта). Подробнее
                                                                о сроке действия карты Вы можете узнать, обратившись в
                                                                банк-эмитент.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="documentation-block" style="padding-top: 25px;">
                                                    <span class="h2 text-center">Конфиденциальность</span>
                                                    <div class="documentation-text">
                                                        <p>1. Определения</p>
                                                        <p>Интернет проект <a href="http://infomatrix.asia"><span
                                                                    class="brown bold">http://infomatrix.asia</span></a>
                                                            (далее – URL, «мы») серьезно относится к вопросу
                                                            конфиденциальности информации своих клиентов и посетителей
                                                            сайта <a href="http://infomatrix.asia"><span
                                                                    class="brown bold">http://infomatrix.asia</span></a>
                                                            (далее – «вы», «посетители сайта»). Персонифицированной мы
                                                            называем информацию, содержащую персональные данные
                                                            (например: ФИО, логин или название компании) посетителя
                                                            сайта, а также информацию о действиях, совершаемых вами на
                                                            сайте URL. (например: заказ посетителя сайта с его
                                                            контактной информацией). Анонимными мы называем данные,
                                                            которые невозможно однозначно идентифицировать с конкретным
                                                            посетителем сайта (например: статистика посещаемости сайта).
                                                        </p>
                                                        <!--<p>2. Использование информации</p>
                                                                    <p>Мы используем персонифицированную информацию конкретного посетителя сайта исключительно для обеспечения ему качественного оказания услуг и их учета. Мы не раскрываем персонифицированных данных одних посетителей сайта URL другим посетителям сайта. Мы никогда не публикуем персонифицированную информацию в открытом доступе и не передаем ее третьим лицам. Исключением являются лишь ситуации, когда предоставление такой информации уполномоченным государственным органам предписано действующим законодательством Республики Казахстан. Мы публикуем и распространяем только отчеты, построенные на основании собранных анонимных данных. При этом отчеты не содержат информацию, по которой было бы возможным идентифицировать персонифицированные данные пользователей услуг. Мы также используем анонимные данные для внутреннего анализа, целью которого является развитие продуктов и услуг URL </p>
                                                                    <p>3. Ссылки </p>
                                                                    <p>Сайт <a href="http://infomatrix.asia"><span class="brown bold">http://infomatrix.asia</span></a> может содержать ссылки на другие сайты, не имеющие отношения к нашей компании и принадлежащие третьим лицам. Мы не несем ответственности за точность, полноту и достоверность сведений, размещенных на сайтах третьих лиц, и не берем на себя никаких обязательств по сохранению конфиденциальности информации, оставленной вами на таких сайтах.</p>
                                                                    <p>4. Ограничение ответственности</p>-->
                                                        <p>Мы делаем все возможное для соблюдения настоящей политики
                                                            конфиденциальности, однако, мы не можем гарантировать
                                                            сохранность информации в случае воздействия факторов
                                                            находящихся вне нашего влияния, результатом действия которых
                                                            станет раскрытие информации. Сайт <a
                                                                href="http://infomatrix.asia"><span
                                                                    class="brown bold">http://infomatrix.asia</span></a>
                                                            и вся размещенная на нем информация представлены по принципу
                                                            "как есть” без каких-либо гарантий. Мы не несем
                                                            ответственности за неблагоприятные последствия, а также за
                                                            любые убытки, причиненные вследствие ограничения доступа к
                                                            сайту URL или вследствие посещения сайта и использования
                                                            размещенной на нем информации.</p>
                                                        <p>2. Контакты</p>
                                                        <p>По вопросам, касающимся настоящей политики, просьба
                                                            обращаться по адресу <a
                                                                href="mailto:infomatrix@bil.edu.kz"><span
                                                                    class="brown bold">infomatrix@bil.edu.kz</span></a>
                                                        </p>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-7 col-xs-12">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2908.0593981368493!2d76.66723851513443!3d43.20824347913905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38834f7675d8a6c3%3A0x7b7d14aec270c056!2sSDU!5e0!3m2!1sen!2skz!4v1537024013581"
                    width="100%" height="400px" frameborder="0" style="display:none; border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@show

@section('sponsors')
<section class="text-center bg--secondary space--xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="list-inline list-inline--images">
                    <li>
                        <span class="h2">Sponsors: </span>
                    </li>
                    <li>
                        <img alt="Partners" src="{{ route('main') }}/2024/hub.png">
                    </li>
                    <li>
                        <img alt="Partners" src="{{ route('main') }}/2024/sergek.svg" height="100px">
                    </li>
                    <li>
                        <img alt="Partners" src="{{ route('main') }}/2024/doclog.svg" width="100px">
                    </li>
                    <li>
                        <img alt="Partners" src="{{ route('main') }}/general/images/daryn.png">
                    </li>
                    <li style="display:none;">
                        <img alt="Partners" src="{{ route('main') }}/logo.png">
                    </li>
                    <li style="display:none;">
                        <img alt="Partners" src="{{ route('main') }}/general/images/engsdu.png">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
@show