@section('footer-one')
            <footer class="footer-6 unpad--bottom  bg--dark" style="background: url(http://sdu.edu.kz/static/images/campus_footer.a1d96cfb37bc.svg) no-repeat center; background-opacity: 2; background-size: auto 80%;background-color: #252529;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <h6 class="type--uppercase">Pages</h6>
                            <p>
                                <a class="nodecoration" href="{{ route('main') }}"><span class="h6 type--uppercase">Home page</span></a>
                                <a class="nodecoration" href="{{ route('kazakhstan') }}"><span class="h6 type--uppercase">About Kazakhstan</span></a>
                                <a class="nodecoration" href="{{ route('rules') }}"><span class="h6 type--uppercase">Rules</span></a>
                                <a class="nodecoration" href="{{ route('faqs') }}"><span class="h6 type--uppercase">FAQs</span></a>
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <h6 class="type--uppercase">Quick links</h6>
                            <p>
                                <a class="nodecoration" href="#about"><span class="h6 type--uppercase"> About</span></a>
                                <a class="nodecoration" href="#improtantdates"><span class="h6 type--uppercase">  Important Dates</span></a>
                                <a class="nodecoration" href="#categories"><span class="h6 type--uppercase">  Categories </span></a>
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <h6 class="type--uppercase">Contacts</h6>
                            <p style="display:none;">
                                Address: 1/1 Abylai Khan Street<br />
                                Kaskelen, Almaty<br />
                                Kazakhstan, 040900<br />
                                
                                
                            </p>
                            <p>
                                Email: <a class="nodecoration" href="mailto:infomatrix@bil.edu.kz">infomatrix@bil.edu.kz</a><br />
                            </p>
                        </div>
                    </div>
                </div>
                <div class="footer__lower text-center-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="type--fine-print">
                                    <span class="update-year">2021</span> Infomatrix Team.
                                </span>
                                <a class="type--fine-print" style="display: none;" href="{{ route('terms') }}">Terms of use</a>
                            </div>
                            <div style="display: none;" class="col-sm-6 text-right text-center-xs">
                                <ul class="social-list list-inline">
                                    <li>
                                        <a href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q" target="_blank">
                                            <i class="socicon socicon-youtube icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://vk.com/infomatrixasia" target="_blank">
                                            <i class="socicon socicon-vkontakte icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/infomatix2017/" target="_blank">
                                            <i class="socicon socicon-facebook icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/infomatrixasia/" target="_blank">
                                            <i class="socicon socicon-instagram icon icon--xs"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ url('/') }}/gli/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/') }}/gli/js/flickity.min.js"></script>
        <script src="{{ url('/') }}/gli/js/easypiechart.min.js"></script>
        <script src="{{ url('/') }}/gli/js/parallax.js"></script>
        <script src="{{ url('/') }}/gli/js/typed.min.js"></script>
        <script src="{{ url('/') }}/gli/js/datepicker.js"></script>
        <script src="{{ url('/') }}/gli/js/isotope.min.js"></script>
        <script src="{{ url('/') }}/gli/js/ytplayer.min.js"></script>
        <script src="{{ url('/') }}/gli/js/lightbox.min.js"></script>
        <script src="{{ url('/') }}/gli/js/granim.min.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.steps.min.js"></script>
        <script src="{{ url('/') }}/gli/js/countdown.min.js"></script>
        <script src="{{ url('/') }}/gli/js/twitterfetcher.min.js"></script>
        <script src="{{ url('/') }}/gli/js/spectragram.min.js"></script>
        <script src="{{ url('/') }}/gli/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/') }}/gli/js/scripts.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.phancy.js"></script> 

        <script>
        $( document ).ready(function() { $(function() {$( "#note" ).customScroll({ scrollbarWidth: 5 });});$('a[href^="#"]').on('click', function(event) {var target = $(this.getAttribute('href'));if( target.length) {event.preventDefault();$('html, body').stop().animate({scrollTop: target.offset().top}, 1000);}});</script>

        <script type="text/javascript">
                   /* $( document ).ready(function() { $('#openmodal').click(); });*/
                </script>
    </body>
</html>
@show

@section('footer-two')
        </div>
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ url('/') }}/gli/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/') }}/gli/js/flickity.min.js"></script>
        <script src="{{ url('/') }}/gli/js/easypiechart.min.js"></script>
        <script src="{{ url('/') }}/gli/js/parallax.js"></script>
        <script src="{{ url('/') }}/gli/js/typed.min.js"></script>
        <script src="{{ url('/') }}/gli/js/datepicker.js"></script>
        <script src="{{ url('/') }}/gli/js/isotope.min.js"></script>
        <script src="{{ url('/') }}/gli/js/ytplayer.min.js"></script>
        <script src="{{ url('/') }}/gli/js/lightbox.min.js"></script>
        <script src="{{ url('/') }}/gli/js/granim.min.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.steps.min.js"></script>
        <script src="{{ url('/') }}/gli/js/countdown.min.js"></script>
        <script src="{{ url('/') }}/gli/js/twitterfetcher.min.js"></script>
        <script src="{{ url('/') }}/gli/js/spectragram.min.js"></script>
        <script src="{{ url('/') }}/gli/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/') }}/gli/js/scripts.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.phancy.js"></script> 
    </body>
</html>
@show