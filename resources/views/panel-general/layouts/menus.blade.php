@section('menu-one') 
        <a id="start"></a>
        <div class="nav-container">
            <div class="bar bar--sm visible-xs visible-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3 col-sm-2">
                            <a href="{{ route('main') }}">
                                <img class="logo logo-dark" alt="logo" src="{{ route('main') }}/2025/logo.png" />
                                <img class="logo logo-light" alt="logo" src="{{ route('main') }}/2025/logo.png" />
                            </a>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-right">
                            <a href="javascript:void(0);" class="hamburger-toggle" data-toggle-class="#menu2;hidden-xs hidden-sm">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <nav id="menu2" class="bar bar-2 hidden-xs bar--transparent bar--absolute pos-fixed" data-scroll-class='150px:pos-fixed'>
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 text-center text-left-sm hidden-xs hidden-sm col-md-push-5">
                            <div class="bar__module">
                                <a href="{{ route('main') }}">
                                    <img class="logo logo-dark" alt="logo" src="{{ route('main') }}/2025/logo.png" />
                                    <img class="logo logo-light" alt="logo" src="{{ route('main') }}/2025/logo.png" />
                                </a>
                            </div>                        
                        </div>
                        <div class="col-md-5 col-md-pull-2">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <span class="dropdown__trigger">INFOMATRIX 2025</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content dropdown__content--lg col-md-8">
                                                        <div class="pos-absolute col-md-5 imagebg hidden-sm hidden-xs" data-overlay="6">
                                                            <div class="background-image-holder" >
                                                                <img alt="background" src="{{ route('main') }}/general/images/menu-cover.jpg" />
                                                            </div>
                                                            <div class="container pos-vertical-center">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <span class="h2 color--white nomargin" >
                                                                            INFOMATRIX - ASIA 2025
                                                                        </span>
                                                                        <a href="{{ route('register') }}" class="btn btn--primary type--uppercase">
                                                                            <span class="btn__text">Register now</span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-md-offset-6">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <ul class="menu-vertical">
                                                                        <li>
                                                                            <a href="#about">
                                                                                About
                                                                            </a>
                                                                        </li>
                                                                        <li style="display:none;">
                                                                            <a href="#improtantdates">
                                                                                Important Dates
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#categories">
                                                                                Categories
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#schedule">
                                                                                Schedule
                                                                            </a>
                                                                        </li>
                                                                        <li style="display: none;">
                                                                            <a href="http://sdu.edu.kz/en/sdu-life/students-house/" target="_blank">
                                                                                Accommodation
                                                                            </a>
                                                                        </li>
                                                                        <li style="display: none;">
                                                                            <a href="#map">
                                                                                Bank account
                                                                            </a>
                                                                        </li>
                                                                        <li style="display: none;">
                                                                            <a href="#map">
                                                                                Requisites
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <ul class="menu-vertical">
                                                                        <li>
                                                                            <a href="{{ route('kazakhstan')}}">
                                                                                About Kazakhstan
                                                                            </a>
                                                                        </li>
                                                                        <!--<li>
                                                                            <a href="{{ route('faqs')}}">
                                                                                FAQs
                                                                            </a>
                                                                        </li>-->
                                                                        <li style="display:none;">
                                                                            <a href="#map">
                                                                                Venue
                                                                            </a>
                                                                        </li>
                                                                        <li style="display:none;">
                                                                            <a href="#gallery">
                                                                                Gallery
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#schedule">
                                                                                Promotional Video
                                                                            </a>
                                                                        </li>
                                                                        <li style="display:none;">
                                                                            <a href="#winners">
                                                                                Winners
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="{{ route('faqs')}}">
                                            FAQs
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('main')}}/INFOMATRIX 2025 Regulations.pdf">
                                            Rules
                                        </a>
                                    </li>
                                    <li style="display: none;">
                                        <a href="{{route('payment')}}">
                                            Payments
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5 text-right text-left-xs text-left-sm">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <!--<li>
                                        <div class="modal-instance block">
                                            <a class="modal-trigger nodecoration" href="javascript:void(0)">
                                                <span class="btn__text">
                                                    Bank account
                                                </span>
                                            </a>
                                            
                                            <div class="modal-container">
                                                <div class="modal-content">
                                                    <section class="imageblock feature-large bg--white border--round ">
                                                        <div class="imageblock__content col-md-5 col-sm-3 pos-left">
                                                            <div class="background-image-holder">
                                                                <img alt="image" src="{{ route('main') }}/general/images/subcover.jpg" />
                                                            </div>
                                                        </div>
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-5 col-md-push-6 col-sm-7 col-sm-push-4">
                                                                    <h1>Requisites</h1>
                                                                    <p class="lead">
                                                                        Учреждение «Университет имени Сулеймана Демиреля»<br/>
                                                                        Р/с KZ339261802128707006<br/>
                                                                        АФ АО «Казкоммерцбанк»<br/>
                                                                        БИН 960240000550 <br/>
                                                                        Кбе 18<br>
                                                                        г. Алматы БИК KZKOKZKX<br/><br>

                                                                        г.Каскелен, ул.Абылай хана 1/1<br/>
                                                                        Тел. +8/727/  3079565 | факс 8/727/ 3079558
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" id="notifier" data-notification-link="side-menu">
                                            <span class="dropdown__trigger">
                                                <i class="stack-interface stack-bell"></i> Notifications
                                            </span>
                                        </a>
                                    </li>-->
                                    <li class="hidden-sm" style="padding-right: 25px;">
                                        <a href="#schedule">
                                            Schedule
                                        </a>
                                        
                                    </li>
                                    <li class="hidden-sm" style="padding-right: 25px;">
                                        <a href="#categories">
                                            Categories
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                @if (Auth::guard('api')->check())
                                    <a class="btn btn--sm btn--primary type--uppercase" href="{{route('userpanel')}}">
                                        <span class="btn__text">Userpanel</span>
                                    </a>
                                @else
                                    <a class="btn btn--sm btn--primary type--uppercase" href="{{route('login')}}">
                                        <span class="btn__text">Log in</span>
                                    </a>
                                    <!--<a class="btn btn--sm btn--primary type--uppercase" href="{{route('register')}}">
                                        <span class="btn__text">Register</span>
                                    </a>-->
                                @endif                  
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="notification pos-right pos-top side-menu bg--white" data-notification-link="side-menu" data-animation="from-right">
                <div class="side-menu__module" >
                    <div id="note" style="width: 100%; height: 550px; padding-right: 5px;">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pdb-20">  
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <h5>Save Development Time</h5>
                                    <p>
                                        Drastically reduce the time it takes to move from initial concept to production-ready with Stack and Variant Page Builder. Your clients will love you for it!
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                        <span class="btn__text">
                                            Learn More
                                        </span>
                                    </a>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container">
        @show

@section('menu-two')
    <a id="start"></a>
        <div class="nav-container ">
            <nav class="bar bar-toggle bar--transparent bar--absolute bg--dark thisistransparent">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 col-sm-2 col-xs-3" style="cursor: pointer;">
                            <div class="bar__module" style="padding-top: 7px;">
                                <a href="{{ route('main') }}" style="cursor: pointer;">
                                    <img class="logo logo-dark" alt="logo" src="{{ route('main') }}/general/images/components/logo-light.png" style="cursor: pointer;" />
                                    <img class="logo logo-light" alt="logo" src="{{ route('main') }}/general/images/components/logo-light.png" style="cursor: pointer;" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-10 col-xs-9">
                            <div class="bar__module">
                                <div class="modal-instance pull-right">
                                    <a class="modal-trigger menu-toggle" href="javascript:void(0);">
                                        <i class="stack-interface stack-menu"></i>
                                    </a>
                                    <div class="modal-container menu-fullscreen">
                                        <div class="modal-content" data-width="100%" data-height="100%">
                                            <div class="pos-vertical-center pos-asbolute text-center">
                                                <div class="heading-block">
                                                    <img alt="Image" src="{{ route('main') }}/general/images/components/logo-light.png" class="logo image--xs" />
                                                    <p class="lead" style="color: white;">
                                                        Let's make future better together.
                                                    </p>
                                                </div>
                                                <ul class="menu-vertical">
                                                    <li class="h4">
                                                        <a href="{{ route('main') }}">Home page</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('main') }}#about">About Infomatrix</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('kazakhstan') }}">About Kazakhstan</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('rules') }}">Rules</a>
                                                    </li>
                                                    <li class="h4">
                                                        <a href="{{ route('faqs') }}">Faqs</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="pos-absolute pos-bottom text-center">
                                                <ul class="social-list list-inline list--hover">
                                                    <li>
                                                        <a href="https://www.youtube.com/channel/UCkG_He9sxMsnNWYSkWVsw5Q" target="_blank">
                                                            <i class="socicon socicon-youtube icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://vk.com/infomatrixasia" target="_blank">
                                                            <i class="socicon socicon-vkontakte icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/infomatix2017/" target="_blank">
                                                            <i class="socicon socicon-facebook icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.instagram.com/infomatrixasia/" target="_blank">
                                                            <i class="socicon socicon-instagram icon icon--xs"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <p class="type--fine-print type--fade">Infomatrix 2025</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>      
        <div class="main-container">
            @show
