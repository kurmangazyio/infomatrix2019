<?php
/*
try {
    \DB::connection()->getPdo();
} catch (\Exception $e) {
    die("Could not connect to the database.  Please check your configuration. error:" . $e );
}*/

?>
@extends('panel-general.layouts.footers')
@extends('panel-general.layouts.others')
@extends('panel-general.layouts.menus')
@extends('panel-general.layouts.headers')

<!--headers-->
@section('header-one') @parent @endsection
<!--menus-->
@section('menu-one') @parent @endsection
@section('menu-two') @endsection
<!--maincovers-->
@section('main-one') @parent @endsection
@section('main-two') @endsection
<!--components-->
@section('about') @parent @endsection
@section('categories') @parent @endsection

@section('important-date')@parent @endsection    
@section('kz-title') @parent @endsection

@section('schedule') @parent @endsection
<!--footers-->
@section('sponsors') @parent @endsection

@section('map') @parent @endsection


@section('footer-one') @parent @endsection
@section('footer-two') @endsection