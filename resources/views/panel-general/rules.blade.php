@extends('panel-general.layouts.footers')
@extends('panel-general.layouts.others')
@extends('panel-general.layouts.menus')
@extends('panel-general.layouts.headers')

<!--headers-->
@section('header-one') @parent @endsection
<!--menus-->
@section('menu-one') @endsection
@section('menu-two') @parent @endsection
<!--maincovers-->
@section('main-one') @endsection
@section('main-two') @parent @endsection
<!--components-->
@section('about') @endsection
@section('categories') @endsection

@section('important-date')@parent @endsection  

@section('kz-title') 
			<section class="text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 mb--1">
                            <div class="notification pos-right pos-bottom col-sm-6 col-md-6" data-animation="from-bottom" data-notification-link="newsletter-2" style="margin: 1.85714286em !important;">
                                <div class="feature feature-1 text-center">
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <div class="modal-close modal-close-cross"></div>
                                        <div class="text-block">                                            
                                            <img src="http://infomatrix.kz/aimages/sizer2.png" style="height: 50%;width: 50%;">
                                            <h3>Project Stand Properties</h3>
                                            <p>
                                                The project stand sizes are as follows width: 85 cm and height: 168 cm. The stand has 3 walls which must be filled by the information related to the project.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="tabs-container" data-content-align="left">
                                <ul class="tabs">
                                    <li class="active">
                                        <div class="tab__title">
                                            <span class="h5">Rules in English</span>
                                        </div>
                                        
                                    </li>
                                    <li class="">
                                        <div class="tab__title">
                                            <span class="h5">Rules in Russian</span>
                                        </div>
                                        
                                    </li>
                                    <li class="">
                                        <div class="tab__title">
                                            <span class="h5">Rules in Kazakh</span>
                                        </div>
                                        
                                    </li>
                                </ul>
                                <ul class="tabs-content">
                                    <li class="active">
                                        <div class="tab__content" align="justify">
                                            <div style="text-align: center;">
                                                <strong>
                                                    <span class="h2 nomargin">INFOMATRIX-Asia 2018 Rules and Regulations</span>
                                                    <span class="h5">
                                                        INFOMATRIX-ASIA is an international competition based on computer projects, held annually in the city of Kaskelen, Kazakhstan.<br>
The competition starts on the 10th of April and ends on the 14th of April.

                                                    </span>

                                                    <a class="btn btn--primary-1 type--uppercase" href="{{ route('main')}}\general\files\rules\Infomatrix Asia.pdf" target="_blank">
                                                        <span class="btn__text">
                                                            Infomatrix Asia 2025 (General rule)
                                                        </span>
                                                    </a>
                                                    <a class="btn btn--sm btn--primary type--uppercase" href="javascript:void(0)" data-notification-link="newsletter-2">
                                                        <span class="btn__text">
                                                            Project Stand Properties
                                                        </span>
                                                    </a>
                                                    <br><br>
                                                </strong>
                                            </div>
                                            <p class="lead">

												1. Registration takes place on the website: <a href="http://infomatrix.asia/">www.infomatrix.asia</a>. Here you will find all the necessary forms to fill out and explain to them. <strong>You must follow the updates on the site!</strong>
<div class="lead" style="padding-left: 40px;">
1. The start of registration and uploading projects to the site is 01/01/2025, the end is - 15.02.2025, for Kazakhstan participants the end is 05.03.2025. <strong>Dates must be checked - changes are possible!</strong> For information on changes and the date of commencement of registration, you can pre-register from 01.10.2018.3. The competition involved projects uploaded to the system. Announcements of projects admitted to the competition are posted on the website www.infomatrix.kz in March 2025. Managers and participants are informed by email and sms.<br><br>

2. The competition involved projects uploaded to the system. Announcements of projects admitted to the competition are posted on the website www.infomatrix.asia after the end of registration within 5-10 days. Managers and participants are informed by email and sms.<br><br>
3. Eligible members must pay:</div>
<div class="lead" style="padding-left: 80px;">
a) Kazakhstan teams: until <strong>March 20, 2025, until 18:00.</strong> To confirm the payment, it is necessary to send a scan / photo of payment receipts to infomatrix@bil.edu.kz.<br><br>
b) teams from other countries until <strong>February 25, 2025.</strong></div>
<div class="lead" style="padding-left: 40px;">
    4. The procedure for payment for participation and accommodation you will find on the website www.infomatrix.asia. <strong>Registration fee is required.</strong><br><br>


 5. If you are planning to stay at the SDU Guest House, you will need to fill out the check-in form at www.infomatrix.asia, <strong>after the official admission of your team to the competition.</strong><br><br>

6. For participants from other countries, transfer from / to the airport is provided.</div>

                                            </p>


<div style="text-align: center;padding-top: 21px;padding-bottom: 21px;">
                                                <strong><span class="h2">Rules and requirements for participants (required criteria of participants)
</span></strong>
                                            </div>
                                            <p class="lead">
                                                1. The team leader (teacher or other school representative) has the right to register several projects, as well as to represent several teams at the same time.<br><br>
2. Pupils of secondary schools, gymnasiums and lyceums aged from 10 to 18 participate in the competition. The number of team members is no more than three.<br><br>
3. Schoolchildren can participate in the categories: robotics (robotics), programming (programming), computer graphics and design (computer graphics and design), control systems (hardware control), short films (short film), drone racing (drone racing) and math projects.<br><br>
4. It is forbidden to register projects that participated in previous competitions “INFOMATRIX-Asia”, if they do not contain significant additions.<br><br>
5. Registration is carried out on the website Infomatrix.kz<br>
<div class="lead" style="padding-left: 40px;">
The Organizing Committee is not responsible for the information entered incorrectly by the contestants (last name, first name, etc.). The contact uses the email address of the manager (be sure to check the email address)<br><br>

Enter the names of the participants in the registration form in accordance <strong>with the passport data</strong>. Otherwise, you will have problems when applying for a visa and registration. You must also provide a photo of your passport.<br><br>

For Kazakhstan participants, personal data are indicated in 3 languages ​​(Kazakh, Russian, English)</div>

                                
                                            </p>


<div style="text-align: center;padding-top: 21px;padding-bottom: 21px;">
                                                <strong><span class="h2">Rules for drafting projects</span></strong>
                                            </div>
                                            <p class="lead">
                                                1. The project report should be short, clear, cover the main results. During the competition in each category a task is proposed that the team must solve and submit within the specified time. Documentation includes presentation, portfolio, video, booklets and additional materials are welcome.<br><br>
2. The dimensions of the racks (panels) are listed on the site. The presentation of your project should take into account the specified size.<br><br>
3. It is advisable to consider an attractive stand design, and participants are recommended to wear national costumes.<br><br>
4. Participants must have the equipment necessary for the presentation. The CDS cannot always solve this problem.
 <br><br>
                                
                                            </p>


                                            <div style="text-align: center;padding-top: 21px;padding-bottom: 21px;">
                                                <strong><span class="h2">Project Details and Evaluation Criteria’s</span></strong>
                                            </div>
                                            <p class="lead">
                                                1. Each project is accompanied by a report containing information on how the project is prepared, technical details, a short video with the project participants.<br><br>
												2.	Project report must be written only <strong>in English.</strong><br><br>
												3. In each category, competitions are held in two stages.  <br>

												a) The first stage is the presentation of the project by the participants.
<br>
b) The second stage - the participants take the exam.
Points for the project are awarded to the team as a whole. All points are summarized and the average is displayed. For example, a team of 2 participants in the first stage received the first participant 20 points, the second 30, and 30 and 40 points in the second stage respectively. <br>The number of points received (20 + 30 + 30 + 40) / 4 = 30<br><br>

                                                4. The jury has the right to establish various criteria for project evaluation in different categories.   <br><br>
                                
                                            </p>
                                            <div style="text-align: center;padding-top: 21px;padding-bottom: 21px;">
                                                <strong><span class="h2">Prizes and Awards</span></strong>
                                            </div>
                                            <p class="lead">
                                                <strong>1. In each category awards are distributed as follows:</strong><br>
                                                I place –100% discount on education at the Faculty of Engineering and Natural Sciences at the University named after Suleyman Demirel (internal grant of the SDU).
<br><br>
												II place - 50% discount on education at the Faculty of Engineering and Natural Sciences at the University named after Suleyman Demirel (internal grant of the SDU). <br><br>
												III place - 30% discount on education at the Faculty of Engineering and Natural Sciences at the University named after Suleyman Demirel (internal grant of the SDU).
 <br><br>
												Distribution in each section: 5% - gold, 20% - silver, 30% - bronze (% of the number of participants in this section)
  <br><br>
  <strong>2. All competitors receive certificates.</strong><br>
												<strong style="color: #c75146">Note:</strong> The grant of the CDS is issued regardless of the age and class of the participant, and is valid for 1 year for its use. The grant is personal, not transferable and does not correspond to third parties.


                                            </p>
                                            <div style="text-align: center;padding-top: 21px;padding-bottom: 21px;">
                                                <strong><span class="h2">Payment procedure and accommodation
</span></strong>

                                            </div>
                                            <p class="lead">
    <strong>* Current prices will be listed on the site from the moment of registration.</strong><br>
    1. The cost of participation (participation includes 3 meals a day)<br><br>
    2. The cost of accommodation (in a comfortable dorm without meal)<br>
        a) Accommodation for 5 days / 4 nights from April 10 to April 14, 2025<br>
        b) The accommodation does not include meals.<br>
        c) Transfer in the morning and in the evening<br><br>
    3. Leisure, at the request of the participant, offers a choice of several tours of the city of Almaty<br><br>

</p>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div class="tab__content" align="justify">
                                           <div style="text-align: center;">
                                                <strong>
                                                    <span class="h2 nomargin">Правила и Положения “INFOMATRIX-Asia” 2025</span>
                                                    
                                                </strong>
                                                <span class="h5">
                                                        INFOMATRIX-ASIA-Қазақстан, Қаскелең қаласында өткізілетін компьютерлік жобалар негізінде
жыл сайынғы халықаралық жарыс.<br>
10 сәуірден 14 сәуірге дейін өткізілетін күн

                                                    </span>
                                                    <a class="btn btn--primary-1 type--uppercase" href="{{ route('main')}}\general\files\rules\Infomatrix Asia.pdf" target="_blank">
                                                        <span class="btn__text">
                                                            Infomatrix Asia 2025 (General rule)
                                                        </span>
                                                    </a>
                                                    <a class="btn btn--primary btn--primary type--uppercase" href="javascript:void(0)" data-notification-link="newsletter-2">
                                                        <span class="btn__text">
                                                            Свойства стенда проекта
                                                        </span>
                                                    </a>
                                            </div>
                                            
                                        </div>
                                    </li>
									<li class="">
                                        <div class="tab__content" align="justify">
                                            <div style="text-align: center;">
                                                <strong>
                                                    <span class="h2 nomargin">“INFOMATRIX-Asia 2025” Ережелері мен Нұсқаулары</span>
                                                    
                                                </strong>
                                                <span class="h5">
                                                       INFOMATRIX-ASIA – это ежегодное международное соревнование на основе IT проектов,
проводимое при поддержки Министерства образования и науки Республики Казахстан и
Республиканского научно-практического центра &quot;Дарын&quot; в городе Каскелен, Казахста.<br>
Даты проведения с 10-го апреля по 14-е апреля 2025 г.

                                                    </span>
                                                    <a class="btn btn--primary-1 type--uppercase" href="{{ route('main')}}\general\files\rules\Infomatrix Asia.pdf" target="_blank">
                                                        <span class="btn__text">
                                                            Infomatrix Asia 2025 (General rule)
                                                        </span>
                                                    </a>
                                                    <a class="btn btn--primary btn--primary type--uppercase" href="javascript:void(0)" data-notification-link="newsletter-2">
                                                        <span class="btn__text">
                                                            Свойства стенда проекта
                                                        </span>
                                                    </a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
@endsection
@section('schedule') @endsection
<!--footers-->
@section('sponsors') @parent @endsection
@section('map') @parent @endsection
@section('footer-one') @parent @endsection
@section('footer-two') @endsection