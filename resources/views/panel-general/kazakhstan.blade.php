@extends('panel-general.layouts.footers')
@extends('panel-general.layouts.others')
@extends('panel-general.layouts.menus')
@extends('panel-general.layouts.headers')

<!--headers-->
@section('header-one') @parent @endsection
<!--menus-->
@section('menu-one') @endsection
@section('menu-two') @parent @endsection
<!--maincovers-->
@section('main-one') @endsection
@section('main-two')
            <section class="cover height-90 imagebg switchable" data-overlay="8">
                <div class="background-image-holder">
                    <img alt="background" src="{{ $pageimage }}" />
                </div>
                <div class="container pos-vertical-center" style="padding-top: 50px;">
                    <div class="row">
                        <div class="col-md-6 col-sm-7">
                            <div class="switchable__text" style="padding-top: 25px;">    <p class="lead">
                                    <span class="h2">{{ $pagetitle }}</span>
                                    It is not necessary to search the entire world for fascinating tradition, beautiful, pristine nature or a delicate mixture of old and modern: simply visit Kazakhstan. In Kazakhstan, guests are made welcome as they experience the very heart of Eurasia.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4 col-xs-12">
                            <div class="video-cover border--round box-shadow-wide">
                                <div class="background-image-holder">
                                    <img alt="image" src="{{ route('main') }}/general/images/steppe.jpg" />
                                </div>
                                <div class="video-play-icon"></div>
                                <iframe src="https://www.youtube.com/embed/kpNAZYrjs5U" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
@endsection
<!--components-->
@section('about') @endsection
@section('categories') @endsection
@section('important-date') @endsection  

@section('kz-title') 
            <section class="text-center" id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <span class="h1">About Kazakhstan</span>
                            <p class="lead" align="justify">
                                The Republic of Kazakhstan, situated at the heart of Eurasia, appeared on the geopolitical map in 1991. The name Kazakh, of Turkic origin, is thought to mean “free man” – which is appropriate in a land historically populated by freedom-loving nomads.<br><br>For thousands of years, these nomads tended vast cattle herds, pasture lands and the fertile soils of the region’s foothills and river valleys. Both the historic Silk Road and Sable Route, along which expensive furs were transported, passed through the region. This made Kazakhstan an important cultural, trade and economic bridge between Asia and Europe.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="feature-large bg--secondary" >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <h3>The Land of Many Nationalities</h3>
                            <p class="lead" align="justify">
                                Kazakhstan is home to many different nationalities and faiths, all united by a common history. This variety of tradition, heritage and language is treasured by the people of the Republic of Kazakhstan, who believe there is much truth in the Japanese saying: “You can survive without your relatives; you cannot survive without your neighbours.” The people of Kazakhstan are proud of their diversity. Century after century, generation after generation, Kazakhstan has always sought to encourage friendship and tolerance among its people.
                            </p>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <img alt="Image" class="box-shadow" src="{{ route('main') }}/general/images/bayterek.jpg">
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <hr class="short">
                            <p class="lead" align="justify">
                                Currently around 130 nationalities populate Kazakhstan. Around 66% are Kazakhs, 21% are Russians and the remaining 13% constitutes Ukrainians, Uzbeks, Germans and Tartars. The predominant religions are Islam and Christianity.<br>The state language of Kazakhstan is Kazakh. However, the younger generations are becoming increasingly trilingual. Both Kazakh and Russian are used by the authorities and governing institutions of Kazakhstan. Kazakh is considered the language of the ancestors and Russian is widely spoken, while English is enjoying a growing popularity as the language most commonly used to communicate with foreign visitors.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="text-center" style="padding-top: 70px">
                <div class="container">
                    <div class="row pbotton20">
                        <div class="col-sm-12 col-md-12" style="padding-bottom: 30px">
                            <span class="h1">Gallery</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 pbotton10">
                            <a href="http://media.beam.usnews.com/74/9c/0cf386f44387ae47baa6e212c7f1/161205bc.kazakhstan_crop.jpg" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="http://media.beam.usnews.com/74/9c/0cf386f44387ae47baa6e212c7f1/161205bc.kazakhstan_crop.jpg" height="150px">
                            </a>
                        </div>

                        <div class="col-sm-3 pbotton10">
                            <a href="{{ route('main') }}/general/images/kazakhstan.jpg" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="{{ route('main') }}/general/images/kazakhstan.jpg" height="150px">
                            </a>
                        </div>

                        <div class="col-sm-3 pbotton10">
                            <a href="http://cdn.natgeotv.com.au/factsheets/thumbnails/TravelTips_Kazakhstan_kazakhstan%20(1).jpg?v=27&azure=false&scale=both&width=1600&height=900&mode=crop" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="http://cdn.natgeotv.com.au/factsheets/thumbnails/TravelTips_Kazakhstan_kazakhstan%20(1).jpg?v=27&azure=false&scale=both&width=1600&height=900&mode=crop" height="150px">
                            </a>
                        </div>

                        <div class="col-sm-3 pbotton10">
                            <a href="https://www.euractiv.com/wp-content/uploads/sites/2/2017/10/shutterstock_552441760-800x450.jpg" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="https://www.euractiv.com/wp-content/uploads/sites/2/2017/10/shutterstock_552441760-800x450.jpg" height="150px">
                            </a>
                        </div>

                        <div class="col-sm-3 pbotton10">
                            <a href="https://asseltour.kz/files/02.jpg" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="https://asseltour.kz/files/02.jpg" height="150px">
                            </a>
                        </div>

                        <div class="col-sm-3 pbotton10">
                            <a href="http://www.theenergycollective.com/wp-content/uploads/2017/08/1.jpg" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="http://www.theenergycollective.com/wp-content/uploads/2017/08/1.jpg" height="150px">
                            </a>
                        </div>

                        <div class="col-sm-3 pbotton10">
                            <a href="http://aboutkazakhstan.com/blog/wp-content/uploads/2017/01/medeu-skating-rink-almaty-kazakhstan-6.jpg" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="http://aboutkazakhstan.com/blog/wp-content/uploads/2017/01/medeu-skating-rink-almaty-kazakhstan-6.jpg" height="150px">
                            </a>
                        </div>

                        <div class="col-sm-3 pbotton10">
                            <a href="http://ihg.scene7.com/is/image/ihg/intercontinental-almaty-3997122568-2x1?wid=1440&fit=fit,1" data-lightbox="Gallery 1">
                                <img alt="Kazakhstan" src="http://ihg.scene7.com/is/image/ihg/intercontinental-almaty-3997122568-2x1?wid=1440&fit=fit,1" height="150px">
                            </a>
                        </div>                    
                    </div>
                </div>
            </section>
            <section class="text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <span class="h1">National symbols of the republic of Kazakhstan</span>
                            <p class="lead" align="justify"></p>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="feature feature-4 boxed boxed--lg boxed--border">
                               <img alt="Kazakhstan" src="{{ route('main') }}/general/images/flag.png">
                                <h4>NATIONAL FLAG OF THE REPUBLIC OF KAZAKHSTAN</h4>
                                <hr>
                                <p align="justify">
                                    Flag is one of the main symbols of the state that represents its sovereignty and identity. The Russian term “flag” for Flag comes from the Dutch word “vlag” and means cloth of officially adopted size and colour and commonly with an image of emblem (coat of arms).<br>The Flag of sovereign Kazakhstan was officially adopted in 1992. It was designed by Shaken Niyazbekov.<br>The State Flag of the Republic of Kazakhstan is a rectangular breadth of blue colour with the image of the sun in its center with a soaring steppe eagle underneath. Along the flagstaff there is a vertical band with the national ornamental patterns. The images of the sun, rays, eagle and ornament are of golden colour. The ratio of the Flag’s width to its length: 1:2.
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="feature feature-4 boxed boxed--lg boxed--border">
                                <img alt="Kazakhstan" src="{{ route('main') }}/general/images/emblem.png" width="65%;"/>
                                <h4>NATIONAL EMBLEM OF THE REPUBLIC OF KAZAKHSTAN</h4>
                                <hr>
                                <p align="justify">
                                    The Emblem is one of the main symbols of the state. The Russian term “gerb” for Emblem comes from the German word “Erbe” (legacy) and means hereditary distinctive sign that represents a combination of figures and objects to which a symbolic sense reflecting cultural and historical traditions of the state is given.<br>The history is an evidence of the fact that the nomads of the Bronze Age that inhabited the territory of modern Kazakhstan identified themselves with a special symbol which is called totem. Its graphical expression further got the name “tamga”. This term has been for the first time used in the Turkic Khaganate.<br>The Emblem of the sovereign Kazakhstan was officially adopted in 1992. The authors of the State Emblem are Kazakhstan’s famous architects Zhandarbek Malibekov and Shot-Aman Ualikhanov. 
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="feature feature-4 boxed boxed--lg boxed--border">
                                <img alt="Kazakhstan" src="{{ route('main') }}/general/images/note.png" width="50%">
                                <h4>NATIONAL ANTHEM OF THE REPUBLIC OF KAZAKHSTAN</h4>
                                <span><strong>Text:</strong> Nursultan Nazarbayev, Zhumeken Nazhimedenov <br> <strong>Composer:</strong> Shamshi Kaldayakov</span>
                                <hr>
                                <p >
                                    Sky of golden sun,<br>
                                    Steppe of golden seed,<br>
                                    Legend of courage -<br>
                                    Take a look at my country!<br>
                                    From the antiquity<br>
                                    Our heroic glory emerged,<br>
                                    They did not give up their pride<br>
                                    My Kazakh people are strong!<br>
                                    <br>
                                    Chorus:
                                    My country, my country,<br>
                                    As your flower I will be planted,<br>
                                    As your song I will stream, my country!<br>
                                    My native land – My Kazakhstan!<br>
                                    <br>
                                    The way was opened to the posterity<br>
                                    I have a vast land.<br>
                                    Its unity is proper,<br>
                                    I have an independent country.<br>
                                    It welcomed the time<br>
                                    Like an eternal friend,<br>
                                    Our country is happy,<br>
                                    Such is our country.<br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="text-center" id="about">
                <div class="container pbotton30">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <span class="h1">National meals of Kazakh nationality</span>
                            <p class="lead">KAZAKH SPECIALTIES ARE MEAT AND SOUR-MILK DISHES: besbarmak (meat and noodles), kazy, karta, shuzhuk, zhal, zhaya (types of horsemeat sausage), baursaks (savory dоughnuts), kurt (a hard curd chess), kozhe (milk soup), irimshik (curds) and airan (a уоghurt drink).</p>
                        </div>
                    </div>
                </div>
                <div class="tabs-container text-center" data-content-align="left">
                    <ul class="tabs">
                        <li class="active">
                            <div class="tab__title">
                                <span class="h5">KAZY</span>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">KUMYS</span>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">BESBARMAK</span>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">KUURDAK</span>
                            </div>
                        </li>
                        <li>
                            <div class="tab__title">
                                <span class="h5">BAURSAKS</span>
                            </div>
                        </li>
                    </ul>
                    <ul class="tabs-content">
                        <li class="active">
                            <div class="tab__content">
                                <section class="switchable feature-large unpad--bottom ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="switchable__text">
                                                    <h2>KAZY</h2>
                                                    <p class="lead">
                                                        <strong>KAZY</strong> is a common element on a dastarkhan, a table set for a festive meal. A smoked horse-meat sausage, is a particularly popular delicacy.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-xs-12 text-center">
                                                <img alt="Image" src="https://olxkz-ringkz01.akamaized.net/images_slandokz/189777784_2_644x461_azy-aynaldyramyn-fotografii.jpg"  style="padding-top: 30px;">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </li>
                        <li>
                            <div class="tab__content">
                                <section class="switchable feature-large unpad--bottom ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="switchable__text">
                                                    <h2>KUMYS</h2>
                                                    <p class="lead">
                                                        <strong>KUMYS</strong> - the national drink is made by fermenting raw unpasteurized mare's milk over the course of hours or days, often while stirring or churning. (The physical agitation has similarities to making butter). During the fermentation, lactobacilli bacteria acidify the milk, and yeasts turn it into a carbonated and mildly alcoholic drink.   
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-xs-12 text-center">
                                                <img alt="Image" src="https://upload.wikimedia.org/wikipedia/kk/4/49/Құйылған_қымыз.jpg">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </li>
                        <li>
                            <div class="tab__content">
                                <section class="switchable feature-large unpad--bottom ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="switchable__text">
                                                    <h2>BESBARMAK</h2>
                                                    <p class="lead">
                                                        <strong>BESBARMAK</strong> - the national kazakh dish,the term Beshbarmak means "five fingers", because the dish is eaten with one's hands. The boiled meat is usually diced with knives, often mixed with boiled noodles, and garnished with parsley and coriander.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-xs-12 text-center">
                                                <img alt="Image" src="http://kulinariya.lichnorastu.ru/wp-content/uploads/2015/03/3199.jpg"  style="padding-top: 25px;">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </li>
                        <li>
                            <div class="tab__content">
                                <section class="switchable feature-large unpad--bottom ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="switchable__text">
                                                    <h2>KUURDAK</h2>
                                                    <p class="lead">
                                                        <strong>KUURDAK (offal stew)</strong> - is a Central Asian dish which may sound exotic and complicated but it is really easy to prepare. It basically involves sautéing meat, potato and onions. With so few ingredients the quality of the ingredients becomes particularly important. Red meat is more commonly used but there is no rule against using chicken! Sometimes Kuurdak is made with innards such as intestines.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-xs-12 text-right">
                                                <img alt="Image" src="http://st.looloo.kz/420x260/d6/5c/d65c8c68a8b6687eaa588a9e9e822bd3_6158c0.jpg?oy=10&oh=415&ow=639&ox=0&s=1dff1edf5fd7edc05ed7bb46e67c6318"  style="padding-top: 30px;">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </li>
                        <li>
                            <div class="tab__content">
                                <section class="switchable feature-large unpad--bottom ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="switchable__text">
                                                    <h2>BAURSAKS</h2>
                                                    <p class="lead">
                                                        <strong>BAURSAKS (savoury doughnuts)</strong> - are must-have food in every Kazakh family. Now, baursaks are not only cooked and eaten by the Kazakhs. Baursaks are served to tea, before main course, to mare’s milk (‘kumys’), to snacks, and to strong broth (‘sorpa’). It is both festive and everyday food liked by many, both young and old.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-xs-12 text-right">
                                                <img alt="Image" src="http://old.el.kz/media/images/blog/orig_blog_1439_0.jpg" style="padding-top: 10px;">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
@endsection
@section('schedule') @endsection
<!--footers-->
@section('sponsors') @parent @endsection
@section('map') @parent @endsection
@section('footer-one') @parent @endsection
@section('footer-two') @endsection