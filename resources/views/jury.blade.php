<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link type="text/css" rel="shortcut icon" href="{{ route('main') }}/logo.jpg">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('/') }}/jli/css/admin.min.css?v=1.0">
    </head>
    <body>
        <div id="app">
            <adminpanel></adminpanel>
        </div>

        <script src="{{ url('/') }}/jli/js/admin.js?v=1.0"></script>
        <script type="text/javascript">
            window.Infomatrix = <?php echo json_encode([
                'csrfToken' => csrf_token(),
                'main' => route('main'),
            ]); ?>
        </script>

        <script src="{{ asset('js/jury.js') }}"></script>
    </body>
</html>
