<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">        
        <link type="text/css" rel="shortcut icon" href="{{ route('main') }}/general/images/components/logo.jpg">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="InfoMatrix is an international computer project competition, born from a desire to bring the world's best IT students together. The competition is not just about promoting professional excellence ...">

        <link href="{{ url('/') }}/gli/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/theme-serpent.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/font-rubiklato.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/') }}/gli/css/jquery.phancy.css" rel="stylesheet" type="text/css" media="all" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />

    </head>
    <body>
        <div class="main-container">
            <section class="text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-12 text-center">
                            <h2>Any errors? Describe your question!</h2>
                            <span>
                                <a href="#" class="h5">Back to main page</a>
                            </span>
                            <hr class="short">
                            {{ Form::open(array('url' => URL::to('/').'/sos', method=>'post')) }}
                                <div class="row">
                                    <input type="hidden" name="way" value="1">
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" name="name" placeholder="Your Name" required="">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="email" name="email" placeholder="Email Address" required="">
                                    </div>
                                    <div class="col-xs-12">
                                        <textarea type="text" name="content" rows="6" placeholder="Describe what happened!" required=""></textarea>
                                    </div>
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn--primary">Send</button>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ url('/') }}/gli/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/') }}/gli/js/flickity.min.js"></script>
        <script src="{{ url('/') }}/gli/js/easypiechart.min.js"></script>
        <script src="{{ url('/') }}/gli/js/parallax.js"></script>
        <script src="{{ url('/') }}/gli/js/typed.min.js"></script>
        <script src="{{ url('/') }}/gli/js/datepicker.js"></script>
        <script src="{{ url('/') }}/gli/js/isotope.min.js"></script>
        <script src="{{ url('/') }}/gli/js/ytplayer.min.js"></script>
        <script src="{{ url('/') }}/gli/js/lightbox.min.js"></script>
        <script src="{{ url('/') }}/gli/js/granim.min.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.steps.min.js"></script>
        <script src="{{ url('/') }}/gli/js/countdown.min.js"></script>
        <script src="{{ url('/') }}/gli/js/twitterfetcher.min.js"></script>
        <script src="{{ url('/') }}/gli/js/spectragram.min.js"></script>
        <script src="{{ url('/') }}/gli/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/') }}/gli/js/scripts.js"></script>
        <script src="{{ url('/') }}/gli/js/jquery.phancy.js"></script> 
    </body>
</html>
