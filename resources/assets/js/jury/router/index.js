import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './../view/auth/Login.vue'
import Logout from './../view/auth/Logout.vue'

import Jury from './../view/dashboard/Main.vue'
import Profile from './../view/dashboard/Profile.vue'
import Assistent from './../view/dashboard/Assistent.vue'
import Show from './../view/dashboard/Show.vue'
import Project from './../view/dashboard/Projects.vue'
import ViewUsers from './../view/dashboard/ViewUsers.vue'
import Faqs from './../view/dashboard/Faqs.vue'
import Export from './../view/dashboard/Export.vue'
import Error404 from './../view/errors/404.vue'
import Notification from './../view/dashboard/Notification.vue'

import Judgment from './../view/dashboard/Judgment.vue'
import Results from './../view/dashboard/Results.vue'
import Appeal from './../view/dashboard/Appeal.vue'
import Conversations from './../view/dashboard/Conversations.vue'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: '/jury/',
	routes:[
		{
			path: "/",
			component: Jury,
			meta : {
				forAuth : true,
				title : 'Jury panel | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/profile",
			component: Profile,
			meta : {
				forAuth : true,
				title : 'Profile | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/judge",
			component: Judgment,
			meta : {
				forAuth : true,
				title : 'Judgment | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/results",
			component: Results,
			meta : {
				forAuth : true,
				title : 'Results | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/appeal",
			component: Appeal,
			meta : {
				forAuth : true,
				title : 'Appeal | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/assistent",
			component: Assistent,
			meta : {
				forAuth : true,
				title : 'Assistent | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/show/:option",
			component: Show,
			meta : {
				forAuth : true,
				title : 'Jury-panel | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/mark/:projectID",
			component: Project,
			meta : {
				forAuth : true,
				title : 'Project | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/u/view/:role",
			component: ViewUsers,
			meta : {
				forAuth : true,
				title : 'Users | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/notification",
			component: Notification,
			meta : {
				forAuth : true,
				title : 'Notifications | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/conversations",
			component: Conversations,
			meta:{
				forAuth: true,
				title: 'Conversations'
			}
		},
		{
			path: "/export",
			component: Export,
			meta : {
				forAuth : true,
				title : 'Export | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/faqs",
			component: Faqs,
			meta : {
				forAuth : true,
				title : 'FAQs | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/login",
			component: Login,
			meta : {
				forVisitors : true,
				title : 'Login | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/logout",
			component: Logout,
			meta : {
				forAuth : true,
				title : 'Logging out ...', 
			}
		},
		{
			path: '*',
			component: Error404,
			meta : {
				title : '404! Page not Found!' 
			}
			
		}
	]
})

export default router