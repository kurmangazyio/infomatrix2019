
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Adminpanel from './App.vue'
import Auth from './package/auth/Auth.js'
import Router from './router/index.js'
import VueResource from 'vue-resource'
import VModal from 'vue-js-modal'
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })

Vue.use(VueResource)
Vue.use(Auth)

Vue.http.options.root = Infomatrix.main + "/api/jury/"
Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken()


Router.beforeEach(
    (to, from, next) => {
        document.title = to.meta.title
        
        if (to.path === '/logout') {
            localStorage.clear()
            
            next({
                path : '/'
            })
        }

        if(to.matched.some(record => record.meta.forVisitors)){
            if(Vue.auth.isAuthenticated()){
                next({
                    path : '/'
                })
            } else{
                next()
            }
        }

        else if(to.matched.some(record => record.meta.forAuth)){
            if(!Vue.auth.isAuthenticated()){
                next({
                    path : '/login'
                })
            } else{
                next()
            }
        } else{
            next()
        }
    }
)

const app = new Vue({
    el: '#app',
    render: h => h(Adminpanel),
    router : Router
});
