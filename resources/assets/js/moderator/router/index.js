import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './../view/auth/Login.vue'
import Logout from './../view/auth/Logout.vue'

import Moderator from './../view/dashboard/Main.vue'
import Daryn from './../view/dashboard/Daryn.vue'
import Logs from './../view/dashboard/Logs.vue'

import Error404 from './../view/errors/404.vue'

import Registration from './../view/dashboard/PostRegister.vue'
import Payments from './../view/dashboard/Payments.vue'
Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: '/moderator/',
	routes:[
		{
			path: "/",
			component: Moderator,
			meta : {
				forAuth : true,
				title : 'Moderator panel | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/daryn",
			component: Daryn,
			meta : {
				forAuth : true,
				title : 'Daryn | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/logs",
			component: Logs,
			meta : {
				forAuth : true,
				title : 'Logs | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/post",
			component: Registration,
			meta : {
				forAuth : true,
				title : 'Registration | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/payments",
			component: Payments,
			meta : {
				forAuth : true,
				title : 'Registration | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/login",
			component: Login,
			meta : {
				forVisitors : true,
				title : 'Login | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/logout",
			component: Logout,
			meta : {
				forAuth : true,
				title : 'Logging out ...', 
			}
		},
		{
			path: '*',
			component: Error404,
			meta : {
				title : '404! Page not Found!' 
			}
			
		}
	]
})

export default router