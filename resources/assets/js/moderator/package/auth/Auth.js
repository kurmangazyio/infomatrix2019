export default function(Vue){
    Vue.auth = {
        //set token
        setToken(access_token, expires_in, tokent_type){
            localStorage.setItem('j_access_token', access_token)
            localStorage.setItem('j_expires_in', expires_in)
            localStorage.setItem('j_tokent_type', tokent_type)
        },
        //get token
        getToken(){
            var token = localStorage.getItem('j_access_token')
            var expiration = localStorage.getItem('j_expires_in')
            var tokent_type = localStorage.getItem('j_tokent_type')

            if( !token || !expiration)
                return null
            
            if(Date.now() > parseInt(expiration)){
                this.destroyToken()
                return null
            } else{
                return token
            }
        },
        //destroy token
        destroyToken(){
            localStorage.removeItem('j_access_token')
            localStorage.removeItem('j_expires_in')
            localStorage.removeItem('j_tokent_type')
        },
        //is Authenticated
        isAuthenticated(){
            if(this.getToken() != null)
                return true
            else
                return false
        }
    }

    Object.defineProperties(Vue.prototype, {
        $auth:{
            get(){
                return Vue.auth
            }
        }
    })
}