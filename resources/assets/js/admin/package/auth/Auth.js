export default function(Vue){
    Vue.auth = {
        //set token
        setToken(access_token, expires_in, tokent_type){
            localStorage.setItem('a_access_token', access_token)
            localStorage.setItem('a_expires_in', expires_in)
            localStorage.setItem('a_tokent_type', tokent_type)
        },
        //get token
        getToken(){
            var token = localStorage.getItem('a_access_token')
            var expiration = localStorage.getItem('a_expires_in')
            var tokent_type = localStorage.getItem('a_tokent_type')

            if( !token || !expiration)
                return null
            
            if(Date.now() > parseInt(expiration)){
                this.destroyToken()
                return null
            } else{
                return token
            }
        },
        //destroy token
        destroyToken(){
            localStorage.removeItem('a_access_token')
            localStorage.removeItem('a_expires_in')
            localStorage.removeItem('a_tokent_type')
        },
        //is Authenticated
        isAuthenticated(){
            if(this.getToken() != null)
                return true
            else
                return false
        }
    }

    Object.defineProperties(Vue.prototype, {
        $auth:{
            get(){
                return Vue.auth
            }
        }
    })
}