import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './../view/auth/Login.vue'
import Logout from './../view/auth/Logout.vue'

import Admin from './../view/dashboard/Dashboard.vue'
import Account from './../view/account/Account.vue'

import Users from './../view/users/Users.vue'
import UsersVolunteer from './../view/users/UsersVolunteer.vue'

import NewUser from './../view/users/NewUsers.vue'
import Notifications from './../view/notifications/Notifications.vue'
import Projects from './../view/projects/Projects.vue'
import ProjectView from './../view/projects/ViewProject.vue'

import FinaleProjects from './../view/finalists/Projects.vue'

import Activities from './../view/activities/Activities.vue'
import Conversations from './../view/conversations/Conversations.vue'
import Setting from './../view/setting/Setting.vue'
import Faqs from './../view/faqs/Faqs.vue'

import Online from './../view/online/Online.vue'
import Error404 from './../view/errors/404.vue'

import Export from './../view/export/Export'
import Logs from './../view/logs/Logs'
import Payments from './../view/payments/Payments'

import Points from './../view/points/Points.vue'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: '/admin/',
	routes:[
		{
			path: "/",
			component: Admin,
			meta : {
				forAuth : true,
				title : 'Dashboard | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/account",
			component: Account,
			meta : {
				forAuth : true,
				title : 'My Account | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/users/:role",
			component: Users,
			meta : {
				forAuth : true,
				title : 'Infomatrix Users | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/volunteer",
			component: UsersVolunteer,
			meta : {
				forAuth : true,
				title : 'Infomatrix Volunteers | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		
		{
			path: "/users/:role/new",
			component: NewUser,
			meta : {
				forAuth : true,
				title : 'Infomatrix Users | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/projects/:category",
			component: Projects,
			meta : {
				forAuth : true,
				title : 'Infomatrix Projects | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/project/:category/:projectID",
			component: ProjectView,
			meta : {
				forAuth : true,
				title : 'Project View | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/finalists/projects",
			component: FinaleProjects,
			meta : {
				forAuth : true,
				title : 'Infomatrix Finalist Projects | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/notifications",
			component: Notifications,
			meta : {
				forAuth : true,
				title : 'Notifications | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/activities",
			component: Activities,
			meta : {
				forAuth : true,
				title : 'Activities | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/points",
			component: Points,
			meta : {
				forAuth : true,
				title : 'Points | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/conversations",
			component: Conversations,
			meta : {
				forAuth : true,
				title : 'Conversations | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/setting",
			component: Setting,
			meta : {
				forAuth : true,
				title : 'Conversations | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/faqs",
			component: Faqs,
			meta : {
				forAuth : true,
				title : 'Faqs | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/online",
			component: Online,
			meta : {
				forAuth : true,
				title : 'Online | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/export",
			component: Export,
			meta : {
				forAuth : true,
				title : 'Export | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/payments",
			component: Payments,
			meta : {
				forAuth : true,
				title : 'Payments | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/logs",
			component: Logs,
			meta : {
				forAuth : true,
				title : 'Logs | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		
		{
			path: "/login",
			component: Login,
			meta : {
				forVisitors : true,
				title : 'Login | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/logout",
			component: Logout,
			meta : {
				forAuth : true,
				title : 'Logging out ...', 
			}
		},
		{
			path: '*',
			component: Error404,
			meta : {
				title : '404! Page not Found!' 
			}
			
		}
	]
})

export default router