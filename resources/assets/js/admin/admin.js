
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Adminpanel from './App.vue'
import Auth from './package/auth/Auth.js'
import Router from './router/index.js'
import VueResource from 'vue-resource'
import VModal from 'vue-js-modal'

import ToggleButton from 'vue-js-toggle-button'
import SmartTable from 'vuejs-smart-table'

import VueSwal from 'vue-swal'
Vue.use(VueSwal)
Vue.use(ToggleButton)

Vue.use(VueResource)
Vue.use(Auth)
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
Vue.use({
    install: function(Vue, options){
        Vue.prototype.$jQuery = require('jquery'); // you'll have this.$jQuery anywhere in your vue project
    }
})
Vue.use(SmartTable)

require('imports-loader?this=>window!./assets/js/popper.min.js');
require('imports-loader?this=>window!./assets/js/bootstrap.min.js');
require('imports-loader?this=>window!./assets/js/metisMenu.min.js');
require('imports-loader?this=>window!./assets/js/waves.js');
require('imports-loader?this=>window!./assets/js/jquery.slimscroll.js');

require('imports-loader?this=>window!./plugins/custombox/js/custombox.min.js')
require('imports-loader?this=>window!./plugins/custombox/js/legacy.min.js')

require('imports-loader?this=>window!./plugins/bootstrap-inputmask/bootstrap-inputmask.min.js');
require('imports-loader?this=>window!./plugins/autoNumeric/autoNumeric.js');

require('imports-loader?this=>window!./plugins/footable/js/footable.all.min.js');


Vue.http.options.root = Infomatrix.main + "/api/admin/"
Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken()


Router.beforeEach(
    (to, from, next) => {
        document.title = to.meta.title
        
        if (to.path === '/logout') {
            localStorage.clear()
            
            next({
                path : '/'
            })
        }

        if(to.matched.some(record => record.meta.forVisitors)){
            if(Vue.auth.isAuthenticated()){
                next({
                    path : '/'
                })
            } else{
                next()
            }
        }

        else if(to.matched.some(record => record.meta.forAuth)){
            if(!Vue.auth.isAuthenticated()){
                next({
                    path : '/login'
                })
            } else{
                next()
            }
        } else{
            next()
        }
    }
)

const app = new Vue({
    el: '#app',
    render: h => h(Adminpanel),
    router : Router
});
