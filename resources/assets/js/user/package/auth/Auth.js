export default function(Vue){
    Vue.auth = {
        //set token
        setToken(access_token, expires_in, tokent_type){
            localStorage.setItem('access_token', access_token)
            localStorage.setItem('expires_in', expires_in)
            localStorage.setItem('tokent_type', tokent_type)
        },
        //get token
        getToken(){
            var token = localStorage.getItem('access_token')
            var expiration = localStorage.getItem('expires_in')
            var tokent_type = localStorage.getItem('tokent_type')

            if( !token || !expiration)
                return null
            
            if(Date.now() > parseInt(expiration)){
                this.destroyToken()
                return null
            } else{
                return token
            }
        },
        //destroy token
        destroyToken(){
            localStorage.removeItem('access_token')
            localStorage.removeItem('expires_in')
            localStorage.removeItem('tokent_type')
        },
        //is Authenticated
        isAuthenticated(){
            if(this.getToken() != null)
                return true
            else
                return false
        }
    }

    Object.defineProperties(Vue.prototype, {
        $auth:{
            get(){
                return Vue.auth
            }
        }
    })
}