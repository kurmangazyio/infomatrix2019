import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './../view/auth/Login.vue'
import Register from './../view/auth/Register.vue'
import Forgot from './../view/auth/Forgot.vue'
import Logout from './../view/auth/Logout.vue'

import User from './../view/dashboard/Main.vue'
import Upload from './../view/dashboard/Upload.vue'
import Setting from './../view/dashboard/Setting.vue'
import Error404 from './../view/errors/404.vue'

import Checker from './../view/errors/Checker.vue'

import Students from './../components/users/StudentEdit.vue'
import Projects from './../components/users/ProjectsEdit.vue'
import Manage from './../view/dashboard/Manage.vue'
import View from './../components/project/ProjectView.vue'
import Notification from './../view/dashboard/Notification.vue'
import Conversation from './../components/inbox/Conversation.vue'

import PaymentsMain from './../view/dashboard/PaymentsMain.vue'
Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: '/user/',
	routes:[
		{
			path: "/",
			component: User,
			meta : {
				forAuth : true,
				title : 'User panel | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/upload",
			component: Upload,
			meta : {
				forAuth : true,
				title : 'Upload Project | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/setting",
			component: Setting,
			meta : {
				forAuth : true,
				title : 'Supervisor Setting Panel | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/students/:studentID",
			component: Students,
			meta : {
				forAuth : true,
				title : 'Students | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/projects/:projectsID",
			component: Projects,
			meta : {
				forAuth : true,
				title : 'Projects | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/view/:projectsID",
			component: View,
			meta : {
				forAuth : true,
				title : 'Projects | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/manage",
			component: Manage,
			meta : {
				forAuth : true,
				title : 'Manage your projects and students | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/inbox",
			component: Notification,
			meta : {
				forAuth : true,
				title : 'Notifications | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		/*{
			path: "/rtyup",
			component: Checker,
			meta : {
				forAuth : true,
				title : 'Notifications | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		*/
		{
			path: "/conversation/:convid",
			component: Conversation,
			meta : {
				forAuth : true,
				title : 'Conversation | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/payments",
			component: PaymentsMain,
			meta : {
				forAuth : true,
				title : 'Payments | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},

		{
			path: "/login",
			component: Login,
			meta : {
				forVisitors : true,
				title : 'Login | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/register",
			component: Register,
			meta : {
				forVisitors : true,
				title : 'Register Account | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/forgot",
			component: Forgot,
			meta : {
				forVisitors : true,
				title : 'Recover Account | Infomatrix - Asia ' + new Date().getFullYear(), 
			}
		},
		{
			path: "/logout",
			component: Logout,
			meta : {
				forAuth : true,
				title : 'Logging out ...', 
			}
		},
		{
			path: '*',
			component: Error404,
			meta : {
				title : '404! Page not Found!' 
			}
			
		}
	]
})

export default router