<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationsFiles extends Model
{
    //
    protected $table = 'notifications_files';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'n_notification_id', 
        'n_admin_id',

        'file',
        'link',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
