<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    //
    protected $table = 'project_conversations';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'messager',
        'role',
        'teams_id',
        'message',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
