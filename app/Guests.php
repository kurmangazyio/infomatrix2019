<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guests extends Model
{
    //
    protected $table = 'guests';
    public $timestamps = true;

    protected $fillable = [
        'guest_id',  
        'p_supervisor', 

        'name',  
        'surname',  
        'lastname',
 
        'passport', 
        'phone',
        'email',
        
        'country',
        'region', 
        'city',
        'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'p_supervisor',
        'updated_at'
    ];
}
