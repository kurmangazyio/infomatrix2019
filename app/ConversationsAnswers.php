<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationsAnswers extends Model
{
    //
    protected $table = 'conversations_answers';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'conv_answer_user_id', 
        'conv_answer_user_level',

        'conv_answer_conv_id',
        'conv_answer_code',
        'conv_answer_content',
        'is_readn'
    ];

    protected $hidden = [
        'updated_at'
    ];
}
