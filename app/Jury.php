<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Jury extends Authenticatable
{
    //
    use HasApiTokens, Notifiable;

    protected $guard = 'jury';
    protected $table = 'juries';

    protected $fillable = [
        'jury_id',

        'name',
        'surname',
        'bio',
        'image',

        'category',
        
        'email',
        'password',
        'b_password',

        'phone',
        'phone_code',
        
        'email_notification_state',

        'is_email_activated',
        'is_phone_activated',
        'is_assistant'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',

        'phone_code',

        'email_notification_state',

        'is_email_activated',
        'is_phone_activated',

        'remember_token'
    ];
}
