<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tours extends Model
{
    protected $table = 'jtours';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        
        'jtcategory',
        'jttitle' ,
        'jtfirst'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
