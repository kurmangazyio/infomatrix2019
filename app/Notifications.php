<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    //
    protected $table = 'notifications';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'n_creator', 
        'n_title',

        'n_content',
        'is_important',
        'n_to'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
