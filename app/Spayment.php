<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spayment extends Model
{
    //
    protected $table = 'payment_students';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'student_id',
        'student_supervisor_id' ,
        'student_team_id',
        'payment_unique',
        'payment_status',
        'payment_option',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
