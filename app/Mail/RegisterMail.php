<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Crypt;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $emailer;
    public $newpassword;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $newpassword, $emailer)
    {
        $this->user = $user;
        $this->emailer = $emailer;
        $this->newpassword = $newpassword;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = "Infomatrix Account Activation!";
        $subject = 'Infomatrix-Asia 2025';

        $encrypted = Crypt::encryptString($this->emailer);

        return $this->view('mails.verification')
                    ->from("mail-no-reply@infomatrix.asia", $name)
                    ->subject($subject)
                    ->with([ 'user' => $this->user, 'emailer' => $this->emailer, 'newpassword' => $this->newpassword, 'link' => route('main').'/activate/'.$encrypted]);
    }
}
