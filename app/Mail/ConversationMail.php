<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConversationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $convid;
    public $convtitle;
    public $convcontent;
    public $convreply;

    public function __construct($convid, $convtitle, $convcontent, $convreply)
    {
        //
        $this->convid = $convid;
        $this->convtitle = $convtitle;
        $this->convcontent = $convcontent;
        $this->convreply = $convreply;
    }

    public function build()
    {
        $address = "mail-no-reply@infomatrix.asia";
        $name = "Infomatrix-Asia 2025";
        $subject = "Conversation Reply";

        return $this->view('mails.conversation')
                    ->from($address, $name)
                    ->subject($subject)
                    ->with();
    }
}
