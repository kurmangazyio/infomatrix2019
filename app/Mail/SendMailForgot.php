<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailForgot extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $emailer;
    public $newpassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $newpassword, $emailer)
    {
        $this->user = $user;
        $this->emailer = $emailer;
        $this->newpassword = $newpassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = "mail-no-reply@infomatrix.asia";
        $name = "Infomatrix-Asia 2025";
        $subject = 'Forgot Password';
        return $this->view('mails.password')
                    ->from($address, $name)
                    ->subject($subject)
                    ->with([ 'user' => $this->user, 'emailer' => $this->emailer, 'newpassword' => $this->newpassword]);
    }
}
