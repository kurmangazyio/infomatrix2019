<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marks extends Model
{
    //
    protected $table = 'assistant_marks';
    public $timestamps = false;

    protected $fillable = [
        'id', 
        'assistant',
        'team_id' ,
        'mark',
        'comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
