<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
    //
    protected $table = 'regions';
    public $timestamps = false;

    protected $fillable = [
    	'id',	
		'en',
		'kz',
		'ru',
		'country',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'country', 'created_at', 'updated_at'
    ];
}
