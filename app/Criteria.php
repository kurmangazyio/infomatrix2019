<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $table = 'jcriteria';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        
        'jccategory',
        'jctour' ,
        'jctitle',
        'jcmin',
        'jcmax'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
