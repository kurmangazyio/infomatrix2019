<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faqs extends Model
{
    //
    protected $table = 'faqs';

    protected $fillable = [
        'id', 
        'faqs_code',
        'user_level',
        'question', 
        'answer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
