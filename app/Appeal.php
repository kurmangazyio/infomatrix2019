<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appeal extends Model
{
    //
    protected $table = 'appeals';
    public $timestamps = true;

    protected $fillable = [
        'student_id',
        'tour',
        'point',
        'comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
