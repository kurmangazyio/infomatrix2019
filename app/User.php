<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',

        'name',
        'surname',
        'bio',
        'image',

        'email',
        'password',
        'b_password',

        'phone',
        'phone_code',
        
        'email_notification_state',

        'is_email_activated',
        'is_phone_activated',
        
        'country',
        'region',
        'city',
        'zip',
        'school',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'b_password',
        
        'phone_code',

        'email_notification_state',

        'remember_token',
    ];
}
