<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Superpayments extends Model
{
    //
    protected $table = 'payment_supervisor';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'supervisor_id' ,
        'payment_unique',
        'payment_status',
        'payment_option',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
