<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendForgotMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $userEmail;
    public $userPassword;
    public $userFullname;

    public function __construct($userEmail, $userPassword, $userFullname)
    {
        $this->userEmail = $userEmail;
        $this->userPassword = $userPassword;
        $this->userFullname = $userFullname;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return [];
    }
}