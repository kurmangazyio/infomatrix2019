<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityTrackerModel extends Model
{
    //
    protected $table = 'activities';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        
        'user', 
        'user_level',
        'user_ip',

        'activity',
        'is_important'
    ];

    protected $hidden = [
        'updated_at'
    ];
}
