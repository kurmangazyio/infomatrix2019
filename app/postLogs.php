<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postLogs extends Model
{
    //
    protected $table = 'post_logs';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'user_code',
        'action',
        'action_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'created_at', 'updated_at'
    ];
}
