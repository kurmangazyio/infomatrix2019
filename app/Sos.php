<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sos extends Model
{
    //
    protected $table = 'sos';

    protected $fillable = [
        'id', 
        'url',
        'body'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
