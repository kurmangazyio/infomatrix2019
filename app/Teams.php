<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    //
    protected $table = 'teams';
    public $timestamps = true;

    protected $fillable = [
        't_team_id',
        't_supervisor',
        't_name',
        't_project_code',

        't_project_mark',
        't_project_mark_comment',

        't_f_student_code',    
        't_s_student_code',    
        't_t_student_code',    
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
