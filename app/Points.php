<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Points extends Model
{
    protected $table = 'jpoints';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        
        'jpcriteria_id',
        'jpstudent_id' ,
        'jpproject_id',
        'jpjury_id',
        'jppoint',
        'jpcomment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
