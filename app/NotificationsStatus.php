<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationsStatus extends Model
{
    //
    protected $table = 'notifications_status';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'n_notification_id', 
        
        'n_role',
        'n_user',
        'is_read'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
