<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guestpayments extends Model
{
    //
    protected $table = 'guest_payments';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'guest_id' ,
        'guest_supervisor',
        'payment_unique',
        'payment_status',
        'payment_option',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];
}
