<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medals extends Model
{
    //
    protected $table = 'medals';
    public $timestamps = true;

    protected $fillable = [
        'studentid',
        'categoryid',
        'medal'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
     'created_at', 'updated_at'
    ];

}
