<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    //
    protected $table = 'students';
    public $timestamps = true;

    protected $fillable = [
        's_student_id',  
        's_supervisor', 

        's_en_name',  
        's_en_surname',  
        's_en_lastname',  
        's_ru_name',  
        's_ru_surname',  
        's_ru_lastname', 

        's_birthdate',  
        's_gender',  
        's_passport', 
        's_passport_file',
        's_phone',
        
        's_country',
        's_region',
        's_zip',  
        's_city',
        's_school',
        's_grade',
        's_daryn'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        's_supervisor',
        'updated_at'
    ];
}
