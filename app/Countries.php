<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    //
    protected $table = 'countries';
    public $timestamps = false;

    protected $fillable = [
        'id', 
        'name', 
        'cordinate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'cordinate', 'created_at', 'updated_at'
    ];
}
