<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postregister extends Model
{
    //
    protected $table = 'postregisters';

    protected $fillable = [
        'code', 
        'role',
        'is_participant',
        'is_food', 
        'is_transport',
        'is_gift'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'updated_at'
    ];

    protected $casts = [
        'created_at' => 'datetime:d/m/Y', // Change your format
        'updated_at' => 'datetime:d/m/Y',
    ];
}
