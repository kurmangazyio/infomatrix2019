<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationsCategories extends Model
{
    //
    protected $table = 'conversations_categories';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'conv_cat_title', 
        'conv_cat_desciption'
    ];

    protected $hidden = [
        'updated_at'
    ];
}
