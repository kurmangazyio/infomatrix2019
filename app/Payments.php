<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    //
    protected $table = 'payments';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'payment_id',

        'payment_email',
        'payment_supervisor',
        'payment_details',

        'amount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
