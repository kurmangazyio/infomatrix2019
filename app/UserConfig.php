<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConfig extends Model
{
    //
    protected $table = 'user_config';
    public $timestamps = false;

    protected $fillable = [
        'id', 
        'user_register',
        'user_spam_emails', 
        
        'user_project_register',
        'user_project_edit',
        'user_project_delete',

        'user_student_register',
        'user_student_edit',
        'user_student_delete',

        'user_conversation_open',
        'user_conversation_blocked',

        'user_profile_edit'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
