<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $table = 'offline_invoice';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'i_supervisor',

        'i_code',
        'i_fullname',
        'i_role',
        'i_project',
        'i_method',
        'i_amount',
        'i_file',

        'i_comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
