<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversations extends Model
{
    //
    protected $table = 'conversations';
    public $timestamps = true;

    protected $fillable = [
        'id', 
        'conv_creator_level', 
        'conv_creator',

        'conv_code',
        'conv_title',
        'conv_content',
        'conv_category',
        'conv_state'
    ];

    protected $hidden = [
        'updated_at'
    ];
}
