<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    //
    protected $table = 'logs';

    protected $fillable = [
        'action_id', 
        'action_user',
        'action_code',
        'action_comment', 

        'action_tracker',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'updated_at'
    ];

    protected $casts = [
	    'created_at' => 'datetime:d/m/Y', // Change your format
	    'updated_at' => 'datetime:d/m/Y',
	];
}
