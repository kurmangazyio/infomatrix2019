<?php

namespace App\Listeners;

use App\Events\SendVerificationMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Crypt;
use Mail;

class SendVerificationMailFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendVerificationMail  $event
     * @return void
     */
    public function handle(SendVerificationMail $event)
    {
        //

        $encrypted = Crypt::encryptString($event->useremail);
        $data = array(
            'fullname' => $event->userfullname,
            'emailer' => $event->useremail,
            'link' => route('main').'/activate/'.$encrypted
        );

        $user = array(
            'emailof' => $event->useremail, 
            'fullname' => $event->userfullname,
        );

        Mail::send('mails.verification', $data, function ($message) use ($user) {
            $message->to($user['emailof'], $user['fullname'])->subject('Activation link for your INFOMATRIX-ASIA account!');
            $message->from('infomatrix@bil.edu.kz','INFOMATRIX-ASIA 2025');
        });
    }
}
