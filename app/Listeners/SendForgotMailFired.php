<?php

namespace App\Listeners;

use App\Events\SendForgotMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailForgot;

class SendForgotMailFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendForgotMail  $event
     * @return void
     */
    public function handle(SendForgotMail $event)
    {
        //
        $data = array(
            'user' => $event->userFullname,
            'emailer' => $event->userEmail,
            'newpassword' => $event->userPassword
        );

        $user = array(
            'emailof' => $event->userEmail, 
            'fullname' => $event->userFullname,
        );
        Mail::to($user['emailof'])->from('infomatrix@bil.edu.kz','INFOMATRIX-ASIA 2025')->send(new SendMailForgot($event->userFullname, $event->userEmail, $event->userPassword));
   

        /*Mail::send('mails.password', $data, function ($message) use ($user) {
            $message->to($user['emailof'], $user['fullname'])->subject('Password changed for your INFOMATRIX-ASIA account!');
            $message->from('infomatrix@bil.edu.kz','INFOMATRIX-ASIA 2025');
        });*/
    }
}
