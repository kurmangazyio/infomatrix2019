<?php

namespace App\Listeners;

use App\Events\SendNewPasswordByEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Crypt;
use Mail;

class SendNewPasswordByEmailFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendNewPasswordByEmail  $event
     * @return void
     */
    public function handle(SendNewPasswordByEmail $event)
    {
        //
        $data = array(
            'user' => $event->userfullname,
            'emailer' => $event->useremail,
            'password' => $event->newpassword,
            'description' => $event->description,
        );

        $user = array(
            'emailof' => $event->useremail, 
            'fullname' => $event->userfullname,
        );

        Mail::send('mails.password', $data, function ($message) use ($user) {
            $message->to($user['emailof'], $user['fullname'])->subject('Password changed for your INFOMATRIX-ASIA account!');
            $message->from('infomatrix@bil.edu.kz','INFOMATRIX-ASIA 2025');
        });
    }
}
