<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    //
    protected $table = 'projects';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'p_project_id',
        'p_supervisor',
        
        'p_category',
        'p_title',
        'p_description',
        'p_file',
        'p_addins',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'p_supervisor',
        'updated_at'
    ];
}
