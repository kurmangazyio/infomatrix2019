<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postActions extends Model
{
    //
    protected $table = 'post_actions';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'code',
        'code_role',
        'is_food',
        'is_transport'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'created_at', 'updated_at'
    ];
}
