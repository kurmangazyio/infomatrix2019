<?php

namespace App\Http\Controllers\Activities;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ActivityTracker;

class ActivitiesTracker extends Controller
{
    //
    public function track($user, $level, $action, $additional = "", $old = "", $important = 0)
    {
    	# code...
    	$tracked = ActivityTracker::create([
    		'user' => $user, 
	        'user_level' => $level,
	        'user_ip' => $this->get_client_ip(),
            'is_important' => $important,
	        'activity' => $this->content($action, $level, $additional, $old)
    	]);
    	return $tracked;
    }

    public function content($action, $level, $additional = "", $old = "")
    {
    	switch ($action) {
    		case 'login':
    			switch ($level) {
    				case 1: return 'Supervisor logged in'; break;
    				case 2: return 'Jury logged in'; break;
    				case 3: return 'Admin logged in'; break;
    			}
    			break;
    		case 'register':
    			switch ($level) {
    				case 1: return 'Supervisor registered'; break;
    				case 2: return 'Jury registered'; break;
    				case 3: return 'Admin registered'; break;
    			}
    			break;
    		case 'student-registed':
    			switch ($level) {
    				case 1: return 'Supervisor registered a student('.$additional.')!'; break;
    				case 2: return 'Jury  registered a student('.$additional.')!'; break;
    				case 3: return 'Admin  registered a student('.$additional.')!'; break;
    			}
    		case 'student-changed':
    			switch ($level) {
    				case 1: return 'Supervisor changed a student!'; break;
    				case 2: return 'Jury  changed a student!'; break;
    				case 3: return 'Admin  changed a student!'; break;
    			}
    			break;
    		case 'student-deleted':
    			switch ($level) {
    				case 1: return 'Supervisor deleted a student!'; break;
    				case 2: return 'Jury  deleted a student!'; break;
    				case 3: return 'Admin  deleted a student!'; break;
    			}
    			break;
    		case 'project-registed':
    			switch ($level) {
    				case 1: return 'Supervisor registered a project(Title: '.$additional.')!'; break;
    				case 2: return 'Jury  registered a project(Title: '.$additional.')!'; break;
    				case 3: return 'Admin  registered a project(Title: '.$additional.')!'; break;
    			}
    			break;
    		case 'project-changed':
    			switch ($level) {
    				case 1: return 'Supervisor changed a project!'; break;
    				case 2: return 'Jury  changed a project!'; break;
    				case 3: return 'Admin  changed a project!'; break;
    			}
    			break;
    		case 'project-deleted':
    			switch ($level) {
    				case 1: return 'Supervisor deleted a project!'; break;
    				case 2: return 'Jury  deleted a project!'; break;
    				case 3: return 'Admin  deleted a project!'; break;
    			}
    			break;
    		case 'team-registed':
    			switch ($level) {
    				case 1: return 'Supervisor registered a team(Code: '.$additional.')!'; break;
    				case 2: return 'Jury  registered a team(Code: '.$additional.')!'; break;
    				case 3: return 'Admin  registered a team(Code: '.$additional.')!'; break;
    			}
    			break;
    		case 'team-name-changed':
    			switch ($level) {
    				case 1: return 'Supervisor changed a team name(Old: '.$additional.')!'; break;
    				case 2: return 'Jury changed a team name(Old: '.$additional.')!'; break;
    				case 3: return 'Admin changed a team name(Old: '.$additional.')!'; break;
    			}
    			break;
    		case 'team-student-changed':
    			switch ($level) {
    				case 1: return 'Supervisor added a team partisipant(Code: '.$additional.')!'; break;
    				case 2: return 'Jury added a team partisipant(Code: '.$additional.')!'; break;
    				case 3: return 'Admin added a team partisipant(Code: '.$additional.')!'; break;
    			}
    			break;
    		case 'team-student-removed':
    			switch ($level) {
    				case 1: return 'Supervisor removed a team partisipant(Code: '.$additional.')!'; break;
    				case 2: return 'Jury removed a team partisipant(Code: '.$additional.')!'; break;
    				case 3: return 'Admin removed a team partisipant(Code: '.$additional.')!'; break;
    			}
    			break;
    		case 'profile':
    			switch ($level) {
    				case 1: return 'Supervisor changed own profile data!(Changed: '.$additional.'), (Old: '.$old.')'; break;
    				case 2: return 'Jury changed own profile data!(Changed: '.$additional.'), (Old: '.$old.')'; break;
    				case 3: return 'Admin changed own profile data!(Changed: '.$additional.'), (Old: '.$old.')'; break;
    			}
    			break;
    		case 'assistant':
    			switch ($level) {
    				case 2: return 'New Jury Assistant!'; break;
    				case 3: return 'New Admin Assistant!'; break;
    			}
    			break;
    		case 'assistant-deleted':
    			switch ($level) {
    				case 2: return 'Jury Assistant deleted!(Details: '.$additional.')'; break;
    				case 3: return 'Admin Assistant deleted!(Details: '.$additional.')'; break;
    			}
    			break;
    		case 'project-marked':
    			switch ($level) {
    				case 2: return 'Jury marked project: '.$old.' as '.$additional.'!'; break;
    			}
    			break;
    		case 'profile-deleted':
    			switch ($level) {
    				case 1: return 'Supervisor deleted own profile'; break;
    				case 2: return 'Jury deleted own profile'; break;
    				case 3: return 'Admin deleted own profile'; break;
    			}
    			break;
    		case 'conversation-created':
    			switch ($level) {
    				case 1: return 'Supervisor created new conversation(Question: '.$additional.')!'; break;
    				case 2: return 'Jury created new conversation!(Question: '.$additional.')'; break;
    				case 3: return 'Admin created new conversation(Question: '.$additional.')!'; break;
    			}
    			break;
    		case 'conversation-answer-sent':
    			switch ($level) {
    				case 1: return 'Supervisor created new reply for conversation(Reply: '.$additional.')!'; break;
    				case 2: return 'Jury created new reply for conversation!(Reply: '.$additional.')'; break;
    				case 3: return 'Admin created new reply for conversation(Reply: '.$additional.')!'; break;
    			}
    			break;
    	}
    }
    public function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';

	    return $ipaddress;
	}
}
