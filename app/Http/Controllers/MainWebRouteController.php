<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use Validator;

use App\Students;
use App\Projects;
use App\Teams;
use App\User;

use App\Jury;
use App\Tours;
use App\Criteria;
use App\Points;

use App\Postregister;

class MainWebRouteController extends Controller
{
    //
    public function accomodation()
    {
        # code...
        return view('accomodation');
    }

    public function index()
    {
    	# code...
    	return view('welcome');
    }
    public function user()
    {
    	# code...
    	return view('user'); 
    }

    public function admin()
    {
    	# code...
    	return view('admin'); 
    }

    public function jury()
    {
    	# code...
    	return view('jury'); 
    }

    public function moderator()
    {
        # code...
        return view('moderator'); 
    }


    public function UserGenerator()
    {
      # code...
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function login()
    {
    	# code...
    	return redirect(\URL::to('/').'/user/login');
    }

    public function register()
    {
    	# code...
    	return redirect(\URL::to('/').'/user/register');
    }
}
