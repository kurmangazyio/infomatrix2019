<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;

use Validator;
use Carbon\Carbon;

use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Events\SendVerificationMail;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;

class UserRegisterController extends Controller
{
    //
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
        $this->ActivitiesTracker = $ActivitiesTracker;
    }
    
    public function RegisterUser(Request $request)
    {
    	# code...
    	$newsupervisor = $request->user;

	    // validating info
	    $validator = Validator::make($newsupervisor, [
	        'name' => 'required|max:40|min:2',
	        'surname' => 'required|max:40|min:2',
	        'country' => 'required',
	        'email' => 'required|email|unique:users|max:255',
	        'password' => 'required|max:18|min:6'
	    ]);

	    //returning errors if not correct
	    if ($validator->fails()) {
	        return response()->json([ "success" => false, "error" => true, 'message' => $validator->errors()->first()], $this->successStatus);            
	    }

	    $user = User::create([
	        'user_id' => $this->UserGenerator($newsupervisor['country']),

	        'name' => $newsupervisor['name'],
	        'surname' => $newsupervisor['surname'],
	        'bio' => "",
	        'image' => "/general/images/user.png",

	        'email'  => $newsupervisor['email'],
	        'password' => bcrypt($newsupervisor['password']),
	        'b_password' => Crypt::encryptString($newsupervisor['password']),

	        'phone' => "",
	        'phone_code' => "",
	        
	        'email_notification_state' => 1,

	        'is_email_activated' => 1,
	        'is_phone_activated' => 0,
	        
	        'country' => $newsupervisor['country'],
	        'region' => "",
	        'city' => "",
	        'zip' => "",
	        'school' => "",
	    ]);

	    Mail::to($newsupervisor['email'])->send(new RegisterMail($newsupervisor['name'].' '.$newsupervisor['surname'], $newsupervisor['password'], $newsupervisor['email']));
	    
	    //\Event::fire(new SendVerificationMail($request->input('email'), $request->input('name').' '.$request->input('surname')));

	    // creating user token
	    //$objToken = $user->createToken('API Info-User');
	    //$token = $objToken->accessToken;
	    //$expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now());

	    // Tracker
	    $this->ActivitiesTracker->track($user->id, 1, 'register','', '', 1);

	    // returning respond
	    return response()->json([
	        "success" => true,
	        "error" => false,
	        "message" => "You successfully registered! Please, do not forget to activate your email! You can log in with your account"
	        //"token_type" => "Bearer", 
	        //"expires_in" => $expiration, 
	        //"access_token" => $token
	        ], $this->successStatus);
    }

    public function UserGenerator($country)
    {
      # code...
      $thisyear = date("Y");
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_'.$country.'_'.$randomString;
    }
}
