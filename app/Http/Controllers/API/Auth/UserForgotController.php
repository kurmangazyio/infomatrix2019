<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Admin;
use App\User;

use Validator;
use Carbon\Carbon;
use App\Events\SendForgotMail;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailForgot;

class UserForgotController extends Controller
{
    //
    public $successStatus = 200;

    public function ForgotUser(Request $request)
    {
    	# code...
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        $user = User::where('email', $request->email)->first();

        if (sizeof($user)) {
        	# code...
            $emailing = $user->email;
            $newpass = $this->temp_password();

            $user->password = bcrypt($newpass);
            $user->b_password = Crypt::encryptString($newpass);
            $user->save();
            
            Mail::to($emailing)->send(new SendMailForgot($user->name.' '.$user->surname, $newpass, $user->email));

        	return response()->json([
                "success" => true,
                "error" => false,
                'message' => "Email with new password and Account details sent to your email!"
            ], $this->successStatus);  

        }else{
        	return response()->json([
                "success" => false,
                "error" => true,
                'message' => "User with email: '".$request->email."' not found!"
            ], $this->successStatus);   
        }

    }

    public function temp_password()
    {
      # code...
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 10; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
}
