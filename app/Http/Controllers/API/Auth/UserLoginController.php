<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Admin;
use App\Jury;
use App\Moderator;

use Validator;
use Carbon\Carbon;

use App\Http\Controllers\Activities\ActivitiesTracker;

class UserLoginController extends Controller
{
    //
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
        $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function LoginUser(Request $request)
    {
        # code...
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            // getting user
            $user = Auth::user();

            // creating user token
            $objToken = $user->createToken('API Info-User');
            $token = $objToken->accessToken;
            $expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now());

            // Tracker
            $this->ActivitiesTracker->track($user->id, 1, 'login');

            // returning respond
            return response()->json([
                "success" => true,
                "error" => false,
                "message" => "You successfully authenticated!",
                "token_type" => "Bearer", 
                "expires_in" => $expiration, 
                "access_token" => $token
            ], 200);
        }
        else{
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => 'Email or password is incorrect!'
            ], 401);
        } 
    }

    public function LoginAdmin(Request $request)
    {
    	# code...
    	if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Admin::where('email', $request->email)->first();
            
            $objToken = $user->createToken('API Info-User');
	        $token = $objToken->accessToken;

	        $expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now());
	        return response()->json([
	        	"success" => true,
	        	"error" => false,
	        	"message" => "You successfully authenticated!",
	        	"token_type" => "Bearer", 
	        	"expires_in" => $expiration, 
	        	"access_token" => $token
	        	], 200);
        }
        else{
        	return response()->json([
            	"success" => false,
	        	"error" => true,
            	'message' => 'Email or password is incorrect!'
            ], 401);
        } 
    }

    public function LoginJury(Request $request)
    {
        # code...
        if(Auth::guard('jury')->attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Jury::where('email', $request->email)->first();
            
            $objToken = $user->createToken('API Info-User');
            $token = $objToken->accessToken;

            $expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now());

            // Tracker
            $this->ActivitiesTracker->track($user->id, 2, 'login');

            return response()->json([
                "success" => true,
                "error" => false,
                "message" => "You successfully authenticated!",
                "token_type" => "Bearer", 
                "expires_in" => $expiration, 
                "access_token" => $token
                ], $this->successStatus);
        }
        else{
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => 'Email or password is incorrect!'
            ], 401);
        } 
    }

    public function LoginModerator(Request $request)
    {
        # code...
        if(Auth::guard('moderator')->attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Moderator::where('email', $request->email)->first();
            
            $objToken = $user->createToken('API Info-Moderator');
            $token = $objToken->accessToken;

            $expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now());

            // Tracker
            //$this->ActivitiesTracker->track($user->id, 2, 'login');

            return response()->json([
                "success" => true,
                "error" => false,
                "message" => "You successfully authenticated!",
                "token_type" => "Bearer", 
                "expires_in" => $expiration, 
                "access_token" => $token
                ], $this->successStatus);
        }
        else{
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => 'Email or password is incorrect!'
            ], 401);
        } 
    }

}
