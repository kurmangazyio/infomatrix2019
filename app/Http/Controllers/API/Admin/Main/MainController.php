<?php

namespace App\Http\Controllers\API\Admin\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Jury;
use App\Admin;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use App\ActivityTrackerModel;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use Mail;

class MainController extends Controller
{
    //
    protected $ActivitiesTracker;
    public $successStatus = 200;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:admin-api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
    	# code...
    	
    	//Supervisors
    	$supervisors = User::all();
    	//Supervisors
    	$juries = Jury::all();
    	//Admins
    	$admins = Admin::all();

    	//Supervisors
    	$students = Students::all();

    	//Activities
    	$acts = ActivityTrackerModel::select()->limit(15)->orderBy('created_at', 'desc')->get();
    	foreach ($acts as $act) {
    		# code...
    		switch ($act->user_level) {
    			case 1:
    				# code...
    				foreach ($supervisors as $supervisor) {
    					# code...
    					if ($supervisor->id = $act->user) {
    						# code...
    						$act['actioner'] = $supervisor->name.' '.$supervisor->surname;
    						$act['rank'] = 'Role: Supervisor';
    					}
    				}
    				break;
    			case 2:
    				# code...
    				foreach ($juries as $jury) {
    					# code...
    					if ($jury->id = $act->user) {
    						# code...
    						$act['actioner'] = $jury->name.' '.$jury->surname;
    						$act['rank'] = 'Role: Jury';
    					}
    				}
    				break;
    			case 3:
    				# code...
    				foreach ($admins as $adm) {
    					# code...
    					if ($adm->id = $act->user) {
    						# code...
    						$act['actioner'] = $adm->name.' '.$adm->surname;
    						$act['rank'] = 'Role: Admin';
    					}
    				}
    				break;
    		}

    		$act['newdate'] = $this->time_elapsed_string($act->created_at);
    	}

    	//Important Activities
    	$important = ActivityTrackerModel::select('*')->where('is_important', 1)->limit(15)->orderBy('created_at', 'desc')->get();
    	foreach ($important as $imp) {
    		# code...
    		switch ($imp->user_level) {
    			case 1:
    				# code...
    				foreach ($supervisors as $supervisor) {
    					# code...
    					if ($supervisor->id = $imp->user) {
    						# code...
    						$imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
    						$imp['image'] = $supervisor->image;
    						$imp['country'] = Countries::where('id',  $supervisor->country)->first()->name;
    					}
    				}
    				break;
    			case 2:
    				# code...
    				foreach ($juries as $jury) {
    					# code...
    					if ($jury->id = $imp->user) {
    						# code...
    						$imp['actioner'] = $jury->name.' '.$jury->surname;
    						$imp['image'] = $supervisor->image;
    						$imp['country'] = 'Role: Jury!';
    					}
    				}
    				break;
    			case 3:
    				# code...
    				foreach ($admins as $adm) {
    					# code...
    					if ($adm->id = $imp->user) {
    						# code...
    						$imp['actioner'] = $adm->name.' '.$adm->surname;
    						$imp['image'] = $supervisor->image;
    						$imp['country'] = 'Role: Admin!';
    					}
    				}break;
    		}

    		$imp['newdate'] = $this->time_elapsed_string($imp->created_at);
    	}

    	$projects = Projects::select()
    		->join('teams', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->join('categories', 'projects.p_category', '=', 'categories.id')
            ->select('projects.*', 'categories.category', 'teams.t_project_mark as mark')
            ->get();
        $cats = Categories::select('category')->get();

        $pps = array();
        foreach ($cats as $value) {
        	# code...
        	$all = 0;
        	$notchecked = 0; 
        	$rejected = 0;
        	$half = 0;
        	$accepted = 0;

        	foreach ($projects as $project) {
        		# code...
        		if ($value->category == $project->category) {
        			# code...
        			switch ($project->mark) {
        				case 0:
        					# code...
        					$all += 1;
        					$notchecked += 1; 
        					break;
        				case 1:
        					# code...
        					$all += 1;
        					$rejected += 1;
        					break;
        				case 2:
        					# code...
        					$all += 1;
        					$half += 1;
        					break;
        				case 3:
        					# code...
        					$all += 1;
        					$accepted += 1;
        					break;
        			}
        		}
        	}

        	$pps[] = array('category' => $value->category, 'all' => $all, 'notchecked' => $notchecked, 'rejected' => $rejected, 'half' => $half, 'accepted' => $accepted);

        }

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
        	# code...
        	$value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
        	->where('conversations.conv_state', 0)
        	->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
        	->orderBy('conversations.created_at', 'desc')
        	->limit(4)
        	->get();

        foreach ($conversations as $imp) {
    		# code...
    		switch ($imp->conv_creator_level) {
    			case 1:
    				# code...
    				foreach ($supervisors as $supervisor) {
    					# code...
    					if ($supervisor->id = $imp->conv_creator) {
    						# code...
    						$imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
    						$imp['image'] = $supervisor->image;
    					}
    				}
    				break;
    			case 2:
    				# code...
    				foreach ($juries as $jury) {
    					# code...
    					if ($jury->id = $imp->conv_creator) {
    						# code...
    						$imp['actioner'] = $jury->name.' '.$jury->surname;
    						$imp['image'] = $supervisor->image;
    					}
    				}
    				break;
    			case 3:
    				# code...
    				foreach ($admins as $adm) {
    					# code...
    					if ($adm->id = $imp->conv_creator) {
    						# code...
    						$imp['actioner'] = $adm->name.' '.$adm->surname;
    						$imp['image'] = $supervisor->image;
    					}
    				}break;
    		}

    		$imp['newdate'] = $this->time_elapsed_string($imp->created_at);
    	}

    	return response()->json([
        	
    		'admin' => Auth::user(),
    		'supervisors' => $supervisors,
    		'students' => $students,
    		'juries' => $juries,
    		'admins' => $admins,
    		'moderators' => [],

    		'projects' => $projects,
    		'important' => $important,
    		'activities' => $acts,
    		'conversations' => Conversations::all(),
    		'navconversations' => $conversations,
    		'notifications' => $notfications,
    		'pps' => $pps
        ], $this->successStatus); 
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function activities()
    {
        # code...
        
        //Supervisors
        $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();

        //Supervisors
        $students = Students::all();

        //Activities
        $acts = ActivityTrackerModel::select()->orderBy('created_at', 'desc')->get();
        foreach ($acts as $act) {
            # code...
            switch ($act->user_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $act->user) {
                            # code...
                            $act['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $act['rank'] = 'Role: Supervisor';
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $act->user) {
                            # code...
                            $act['actioner'] = $jury->name.' '.$jury->surname;
                            $act['rank'] = 'Role: Jury';
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $act->user) {
                            # code...
                            $act['actioner'] = $adm->name.' '.$adm->surname;
                            $act['rank'] = 'Role: Admin';
                        }
                    }
                    break;
            }

            $act['newdate'] = $this->time_elapsed_string($act->created_at);
        }

        //Important Activities
        $important = ActivityTrackerModel::select('*')->where('is_important', 1)->orderBy('created_at', 'desc')->get();
        foreach ($important as $imp) {
            # code...
            switch ($imp->user_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->user) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                            $imp['country'] = Countries::where('id',  $supervisor->country)->first()->name;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->user) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                            $imp['country'] = 'Role: Jury!';
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->user) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                            $imp['country'] = 'Role: Admin!';
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }

        return response()->json([
            
            'admin' => Auth::user(),
            'important' => $important,
            'activities' => $acts,
            'navconversations' => $conversations,
            'notifications' => $notfications,
        ], $this->successStatus); 
    }

    public function emailnotification(Request $request)
    {
        # code...
        $title = $request->input('title');
        $rec = $request->input('recievers');
        $recscs = $request->input('content');
        
        $recss = explode(" ", $rec);

        for ($i=0; $i < count($recss); $i++) { 
            # code...
            try {
                Mail::send('mails.notification', $recss, function($message) {
                    $message->to($recss[$i])->subject('No Reply');
                    $message->from('infomatrix@bil.edu.kz');
                });
            } catch (Exception $e) {}

        }
        
        return response()->json([
            'error' => false,
            'success' => true,
            'message' => $recss
        ], $this->successStatus); 
    }

}
