<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Jury;
use App\Admin;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Teams;
use App\Projects;
use App\Students;
use App\Marks;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;


class FinalistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:admin-api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
        //
        $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();
        //Supervisors
        $students = Students::all();

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }


        $projects = Projects::select(
            //ids
            'projects.p_project_id as pid',
            'teams.t_team_id as tid',
            //project info
            'projects.p_title as title', 
            'categories.category as category',
            'categories.short as short',
            'teams.t_project_mark as state',
            \DB::raw("(select name from users where id = teams.t_supervisor) as sname"),
            \DB::raw("(select surname from users where id = teams.t_supervisor) as ssurname"),

            //project students
            \DB::raw("(select s_en_name from students where s_student_id = teams.t_f_student_code) as s1_name"),
            \DB::raw("(select s_en_surname from students where s_student_id = teams.t_f_student_code) as s1_surname"),
            \DB::raw("(select s_en_lastname from students where s_student_id = teams.t_f_student_code) as s1_lastname"),

            \DB::raw("(select s_en_name from students where s_student_id = teams.t_s_student_code) as s2_name"),
            \DB::raw("(select s_en_surname from students where s_student_id = teams.t_s_student_code) as s2_surname"),
            \DB::raw("(select s_en_lastname from students where s_student_id = teams.t_s_student_code) as s2_lastname"),

            \DB::raw("(select s_en_name from students where s_student_id = teams.t_t_student_code) as s3_name"),
            \DB::raw("(select s_en_surname from students where s_student_id = teams.t_t_student_code) as s3_surname"),
            \DB::raw("(select s_en_lastname from students where s_student_id = teams.t_t_student_code) as s3_lastname")
            )
                ->join('categories', 'projects.p_category', '=', 'categories.id')
                ->join('teams', 'projects.p_project_id', '=', 'teams.t_project_code')
                ->where('teams.t_project_mark', '<>', 0)
                ->where('teams.t_project_mark', '<>', 1)
                ->orderBy('teams.t_project_mark', 'DESC')
                ->get();

        return response()->json(
            [
                'admin' => Auth::user(),
                'navconversations' => $conversations,
                'notifications' => $notfications,
                'projects' => $projects,
                'notchecked' => Teams::where('teams.t_project_mark', '=', 0)->count()
            ], $this->successStatus);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
