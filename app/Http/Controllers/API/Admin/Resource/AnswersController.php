<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Jury;
use App\Admin;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use Validator;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public $successStatus = 200;
    public $conv_creator;
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $validator = Validator::make( $request->all(), [
            'reply' => 'required|min:15',
            'conversation' => 'required|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        $user = Auth::user();
        $conv = Conversations::where('conv_code', $request->input('conversation'))->first();

        $answer = new ConversationsAnswers;

        $answer->conv_answer_user_id = $user->id;
        $answer->conv_answer_user_level = 3;
        $answer->conv_answer_conv_id = $conv->id;
        $answer->conv_answer_code = 'a_'.$this->CodeGenerator();
        $answer->conv_answer_content = $request->input('reply');
        $answer->save();
        
        if ($request->input('openclose')) {
            # code...
            
            $conv->conv_state = 1;
            $conv->save();
        }

        
        if ($conv->conv_creator_level == 1) {
            # code...
           $this->conv_creator = $conv->conv_creator;

            \Mail::send('mails.conversation', [ 'convid' => $conv->conv_code, 'convtitle' => $conv->conv_title, 'convcontent' => $conv->conv_content, 'convreply' => $request->input('reply')], function($message)
            {
                $senderof = User::where('id', $this->conv_creator)->first();

                $message->from('infomatrix@bil.edu.kz', 'Conversation Reply');
                $message->to($senderof->email)->cc('infomatrix@bil.edu.kz');
            });
        }
        
        $answers = ConversationsAnswers::where('conv_answer_conv_id', $conv->id)->orderBy('created_at', 'asc')->get();
        foreach ($answers as $answer) {
          # code...
          switch ($answer->conv_answer_user_level) {
            case 1:
              # code...
              $answer['creator'] = User::select('name', 'surname', 'image')->where('id', $answer->conv_answer_user_id)->first();
              break;
            case 2:
              # code...
              $answer['creator'] = Jury::select('name', 'surname',  'image')->where('id', $answer->conv_answer_user_id)->first();
              break;
            case 3:
              # code...
              $answer['creator'] = Admin::select('name', 'surname', 'image')->where('id', $answer->conv_answer_user_id)->first();
              break;
          }

          $answer['newdate'] =  $this->time_elapsed_string($answer->created_at);
        }

        return response()->json([
            "success" => true,
            "error" => false,
            'message' => 'Your message successfully sent!',
            'answers' => $answers
        ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CodeGenerator()
    {
      # code...
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 10; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
