<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;

use Validator;
use Carbon\Carbon;
use App\Http\Controllers\Activities\ActivitiesTracker;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $newsupervisor = $request->user;
        $way = $request->way;

        switch ($way) {
            case 'admin':
                # code...
                $validator = Validator::make($newsupervisor, [
                    'name' => 'required|max:40|min:2',
                    'surname' => 'required|max:40|min:2',
                    'country' => 'required',
                    'email' => 'required|email|unique:users|max:255',
                    'password' => 'required|max:18|min:6',
                    'phone' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $user = new User;

                $user->user_id = $this->UserGenerator($newsupervisor['country']);
                $user->name = $newsupervisor['name'];
                $user->surname = $newsupervisor['surname'];
                $user->bio = $newsupervisor['bio'];
                $user->image = "/general/images/user.png";
                $user->email = $newsupervisor['email'];
                $user->password = bcrypt($newsupervisor['password']);
                $user->b_password = Crypt::encryptString($newsupervisor['password']);
                $user->phone = $newsupervisor['phone'];
                $user->phone_code = "";
                $user->email_notification_state = 1;
                $user->is_email_activated = ($newsupervisor['emailact']) ? 1 : 0 ;
                $user->is_phone_activated = ($newsupervisor['phoneact']) ? 1 : 0 ;
                $user->country = $newsupervisor['country'];
                $user->region = "";
                $user->city = "";
                $user->zip = "";
                $user->school = "";
                $user->save();

                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'You registered Supervisor successfully!'
                ], $this->successStatus);     

                break;

                
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UserGenerator($country)
    {
      # code...
        $thisyear = date("Y");
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 5; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $thisyear.'_'.$country.'_'.$randomString;
    }
}
