<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Jury;
use App\Admin;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Teams;
use App\Projects;
use App\Students;
use App\Marks;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $successStatus = 200;

    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:admin-api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
        //
        $projects = Projects::select('projects.*', 'categories.category as p_category_title','categories.image as p_category_image', 'teams.t_team_id')
                ->join('categories', 'projects.p_category', '=', 'categories.id')
                ->join('teams', 'projects.p_project_id', '=', 'teams.t_project_code')
                ->where('p_supervisor', Auth::user()->id)
                ->get();

        return response()->json($projects, $this->successStatus);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:2',
            'category' => 'required|max:2',
            'description' => 'required|min:100',
            'file' => 'required',
            'ext' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], $this->successStatus);            
        }

        $project = $this->ProjectGenerator();

        $newproject = new Projects;

        $newproject->p_project_id = $project;
        $newproject->p_supervisor = Auth::user()->id;

        $newproject->p_category = $request->input('category');

        $newproject->p_title = $request->input('name');
        $newproject->p_description = $request->input('description');


        $exploded = explode(',', $request->input('file'));
        $decoded = base64_decode($exploded[1]);

        $file = Auth::user()->id.'_'.$request->input('category').'_'.$project.'.'.$request->input('ext');
        $path = public_path().'/general/files/'.$file;
        file_put_contents($path, $decoded);
        $newproject->p_file = '/general/files/'.$file;
        $newproject->p_addins = json_encode($request->additionals);
        
        $newproject->save();
        // Tracker
        $this->ActivitiesTracker->track(Auth::user()->id, 1, 'project-registed', $request->input('name'));
        return response()->json(
            [
                'pname' => $request->input('name'),
                'code' => $project,
                'description' => $request->input('description'),
                'success' => "Your project successfully registered!",
            ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
        $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();
        //Supervisors
        $students = Students::all();
        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }

        $team = Teams::where('t_team_id', $id)->first();
        
        if (sizeof($team) == 0) {
          # code...
            return response()->json([
              'admin' => Auth::user(),
              'navconversations' => $conversations,
              'notifications' => $notfications,
              'notfound' => true
          ], $this->successStatus);
        }
          

        $project = Projects::where('p_project_id', $team['t_project_code'])->first();
        $user = User::where('id', $team->t_supervisor)->first();
        $country = Countries::all();
        $regions = Regions::all();

        foreach ($country as $key) {
            if ($key['id'] == $user->country) {
                $user->country = $key['name'];
            }
        }
        if (is_numeric($user->region)) {
            foreach ($regions as $key) {
                if ($key['id'] == $user->region) {
                    $user->region = $key['en'];
                }
            }
        }

        
        //Student 1
        $s1 = Students::where('s_student_id', $team['t_f_student_code'])->first();
        foreach ($country as $key) {
            if ($key['id'] == $s1->s_country) {
                $s1->s_country = $key['name'];
            }
        }
        if (is_numeric($s1->s_region)) {
            foreach ($regions as $key) {
                if ($key['id'] == $s1->s_region) {
                    $s1->s_region = $key['en'];
                }
            }
        }
        $students = array($s1);
        //student 2
        if($team->t_s_student_code != ""){
            $s2 = Students::where('s_student_id', $team['t_s_student_code'])->first();
            if (is_numeric($s2->s_region)) {
                foreach ($regions as $key) {
                    if ($key['id'] == $s2->s_region) {
                        $s2->s_region = $key['en'];
                    }
                }
            }
            foreach ($country as $key) {
                if ($key['id'] == $s2->s_country) {
                    $s2->s_country = $key['name'];
                }
            }
            array_push($students, $s2);
        }
        //student 3
        if($team->t_t_student_code != ""){
            $s3 = Students::where('s_student_id', $team['t_t_student_code'])->first();
            if (is_numeric($s3->s_region)) {
                foreach ($regions as $key) {
                    if ($key['id'] == $s3->s_region) {
                        $s3->s_region = $key['en'];
                    }
                }
            }
            foreach ($country as $key) {
                if ($key['id'] == $s3->s_country) {
                    $s3->s_country = $key['name'];
                }
            }
            array_push($students, $s3);
        }
        $category = Categories::where('id', $project->p_category)->first();

        $marks = Marks::select('assistant_marks.*', 'juries.name', 'juries.surname', 'juries.image')
                  ->join('juries', 'juries.id', '=', 'assistant_marks.assistant')
                  ->where('team_id', $team->id)
                  ->get();

        

        $project['p_category'] = $category['category'];

        return response()->json([
            'admin' => Auth::user(),
            'navconversations' => $conversations,
            'notifications' => $notfications,
            'project' => $project,
            'supervisor' => $user,
            'students' => $students,
            'additionals' => json_decode($project->p_addins, true),
            'team' => $team,
            'marks' => $marks
        ], $this->successStatus);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
      switch ($request->input('action')) {
        case 'edit':
          # code...
          $project = $request->input('project');

          $validator = Validator::make($project, [
              'p_title' => 'required|max:255|min:2',
              'p_category' => 'required|max:2',
              'p_description' => 'required|min:100',
          ]);

          if ($validator->fails()) {
              return response()->json([ 
                "success" => false ,
                "error" => true,
                "message" => $validator->errors()->first()
              ], $this->successStatus);           
          }

          $editing = Projects::where('p_project_id', $id)->first();
          $labels = "";

          if ($editing->p_category != $project['p_category']) {
            # code...
            $editing->p_category = $project['p_category'];
            $editing->p_addins = "";
            $labels .= "Category, ";
          }

          if ($editing->p_title != $project['p_title']) {
            # code...
            $editing->p_title = $project['p_title'];
            $labels .= "Project title, ";
          }

          if ($editing->p_description != $project['p_description']) {
            # code...
            $editing->p_description = $project['p_description'];
            $labels .= "Project description, ";
          }

          if ($labels == "") {
                    # code...
              return response()->json([ 
                "success" => false,
                "error" => true,
                "message" => "Nothing is changed!"
              ], $this->successStatus);

          }else{
              $editing->save();
              $labels = substr($labels, 0, -2);

              // Tracker
              $this->ActivitiesTracker->track(Auth::user()->id, 1, 'project-changed');

              return response()->json([ 
                "success" => true ,
                "error" => false,
                "message" => "Your project details( ".$labels." ) successfully changed!"
              ], $this->successStatus);
          }

          break;
        case 'file':
          # code...
          $counter = 0;
          $editing = Projects::where('p_project_id', $id)->first();

          if ($request->input('file')['file'] != "") {
            # code...
            $exploded = explode(',', $request->input('file')['file']);
            $decoded = base64_decode($exploded[1]);

            $file = Auth::user()->id.'_'.$editing->p_category.'_'.$editing->p_project_id.'.'.$request->input('file')['ext'];
            $path = public_path().'/general/files/'.$file;
            file_put_contents($path, $decoded);
            $editing->p_file = '/general/files/'.$file;

            $editing->save();
            $counter++;
          }

          if (sizeof($request->input('file')['additionals']) != 0) {
            # code...
            $editing->p_addins = json_encode($request->input('file')['additionals']);

            $editing->save();
            $counter++;
          }

          if ($counter > 0) {
            # code...
            // Tracker
            $this->ActivitiesTracker->track(Auth::user()->id, 1, 'project-changed');

            return response()->json([ 
                "success" => true ,
                "error" => false,
                "message" => "Your project documentation file successfully changed!"
              ], $this->successStatus);
          }else{
            return response()->json([ 
                "success" => false,
                "error" => true,
                "message" => "Nothing is changed!"
              ], $this->successStatus);
          }
          
          break;

        default:
          # code...
          break;
      }
      
      return response()->json([$project, $action, $file], $this->successStatus);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
      $team = Teams::where('t_team_id', $id)->first();

      $marks = \DB::table('assistant_marks')->where('team_id','=', $team->id)->delete();
      $project = Projects::where('p_project_id', $team['t_project_code'])->first();

      Storage::delete($project->p_file);
      $project->delete();
      $team->delete();

      // Tracker
      $this->ActivitiesTracker->track(Auth::user()->id, 1, 'project-deleted');

      return response()->json([ 
                "success" => "Project successfully deleted!"
            ], $this->successStatus);
    }

    public function ProjectGenerator()
    {
      # code...
      $thisyear = date("Y");
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_pp_'.$randomString;
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
