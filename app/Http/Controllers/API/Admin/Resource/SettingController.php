<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Jury;
use App\Admin;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use App\Faqs;

use Validator;

use App\UserConfig;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index()
    {
        //
        $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();
        //Supervisors
        $students = Students::all();

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }

        $faqs = Faqs::all();

        foreach ($faqs as $value) {
            # code...
            switch ($value->user_level) {
                case 1: $value->user_level = "This Faqs for Supervisors"; break;
                case 2: $value->user_level = "This Faqs for Juries"; break;
                case 3: $value->user_level = "This Faqs for Admins"; break;
                case 4: $value->user_level = "This Faqs for Moderators"; break;
            }
        }
        return response()->json(
          [
              'admin' => Auth::user(),
              'navconversations' => $conversations,
              'notifications' => $notfications,
              'faqs' => $faqs,
              'config' => UserConfig::get()->first()
          ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $setting = UserConfig::get()->first();

        switch ($request->input('param')) {
            case 'user_register':
                # code...
                $setting->user_register = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_project_register':
                # code...
                $setting->user_project_register = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_project_edit':
                # code...
                $setting->user_project_edit = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_project_delete':
                # code...
                $setting->user_project_delete = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_student_register':
                # code...
                $setting->user_student_register = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_student_edit':
                # code...
                $setting->user_student_edit = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_student_delete':
                # code...
                $setting->user_student_delete = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_conversation_open':
                # code...
                $setting->user_conversation_open = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
            case 'user_profile_edit':
                # code...
                $setting->user_profile_edit = ($request->input('value')) ? 1 : 0 ;
                $setting->save();
                
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'Settings successfully updated!'
                ], $this->successStatus);
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
