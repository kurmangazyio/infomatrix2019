<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Admin;
use App\Countries;

use Validator;


use Event;
use App\Events\SendNewPasswordByEmail;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index()
    {
        //return data
        return response()->json(['admins' => Admin::all()], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //viewer data
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return data
        $newsupervisor = $request->user;
        $way = $request->way;

        switch ($way) {
            case 'admin':
                # code...
                $validator = Validator::make($newsupervisor, [
                    'name' => 'required|max:40|min:2',
                    'surname' => 'required|max:40|min:2',
                    'email' => 'required|email|unique:users|max:255',
                    'password' => 'required|max:18|min:6',
                    'phone' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $user = new Admin;

                $user->admin_id = $this->UserGenerator();

                $user->name = $newsupervisor['name'];
                $user->surname = $newsupervisor['surname'];
                $user->bio = $newsupervisor['bio'];
                $user->image = "/general/images/user.png";

                $user->email = $newsupervisor['email'];
                $user->password = bcrypt($newsupervisor['password']);
                $user->b_password = Crypt::encryptString($newsupervisor['password']);

                $user->phone = $newsupervisor['phone'];
                $user->phone_code = "";
                
                $user->email_notification_state = 1;
                $user->is_email_activated = ($newsupervisor['emailact']) ? 1 : 0 ;
                $user->is_phone_activated = ($newsupervisor['phoneact']) ? 1 : 0 ;
                $user->is_assistant = ($newsupervisor['assistant']) ? 1 : 0 ;

                $user->save();

                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'You registered Adminsitrator, successfully!'
                ], $this->successStatus);     

                break;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //view data
        return response()->json(['admin' => Admin::where('admin_id', $id)->first()], $this->successStatus);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $way = $request->input('way');

        switch ($way) {
            case 'profile':
                # code...
                $validator = Validator::make($request->input('admin'), [
                    'name' => 'required|max:40|min:2',
                    'surname' => 'required|max:40|min:2',
                    'bio' => 'required|min:6',
                    'phone' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $admin = Admin::where('admin_id', $id)->first();
                $fields = "";

                if ($admin->name != $request->input('admin')['name']) {
                    # code...
                    $admin->name = $request->input('admin')['name'];
                    $fields .= 'Name, ';
                }
                if ($admin->surname != $request->input('admin')['surname']) {
                    # code...
                    $admin->surname = $request->input('admin')['surname'];
                    $fields .= 'surname, ';
                }
                if ($admin->bio != $request->input('admin')['bio']) {
                    # code...
                    $admin->bio = $request->input('admin')['bio'];
                    $fields .= 'bio, ';
                }
                if ($admin->phone != $request->input('admin')['phone']) {
                    # code...
                    $admin->phone = $request->input('admin')['phone'];
                    $fields .= 'phone, ';
                }

                if (sizeof($fields) > 0) {
                    # code...
                    $admin->save();
                    $fields = substr($fields, 0, -2);

                    return response()->json([
                        "success" => true,
                        "error" => false,
                        'message' => 'Those '.$fields.' fields are successfully changed!'
                    ], $this->successStatus); 
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => 'Nothing to changed!'
                    ], $this->successStatus); 
                }
                break;
            case 'image':
                # code...
                $admin = Admin::where('admin_id', $id)->first();

                $newimage = $request->image;

                if ($newimage['file'] != "") {
                    # code...
                    $exploded = explode(',', $newimage['file']);
                    $decoded = base64_decode($exploded[1]);

                    $file = $admin->admin_id.'.'.$newimage['ext'];
                    $path = public_path().'/general/admin/'.$file;
                    file_put_contents($path, $decoded);

                    $admin->image = '/general/admin/'.$file;
                    
                    $admin->save();
                    return response()->json([
                        "success" => true,
                        "error" => false,
                        'message' => 'Image is successfully changed!'
                    ], $this->successStatus);                  
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => 'Please, select an image!'
                    ], $this->successStatus); 
                }
                break;
            case 'password':
                # code...
                $admin = Admin::where('admin_id', $id)->first();

                $password = $request->password;
                $validator = Validator::make( $password, [
                    'old_password' => 'required|max:18|min:6',
                    'new_password' => 'required|max:18|min:6',
                    'confirm_password' => 'required|same:new_password'
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                if ($password['old_password'] == Crypt::decryptString($admin->b_password)) {
                    # code...
                    if ($password['new_password'] != Crypt::decryptString($admin->b_password)) {
                        # code...
                        $admin->password =  bcrypt($password['new_password']);
                        $admin->b_password = Crypt::encryptString($password['new_password']);

                        $admin->save();

                        return response()->json([
                            "success" => true,
                            "error" => false,
                            'message' => "New password successfully saved!"
                        ], $this->successStatus);   
                    }else{
                        return response()->json([
                            "success" => false,
                            "error" => true,
                            'message' => "Old password matchs with new password!"
                        ], $this->successStatus);
                    }
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => "Old password not match with entered value!"
                    ], $this->successStatus);   
                }
                break;
        }
        return response()->json([
            "success" => true,
            "error" => false ,
            'message' => $way
        ], $this->successStatus); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //view delete
        $user = Auth::user();

        if ($user->is_assistant) {
            # code...
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => 'Assistant can\'t delete Users!'
            ], $this->successStatus);    
        }

        $admin = Admin::where('admin_id', $id)->first();

        if ($user->id == $admin['id']) {
            # code...
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => 'You can not delete yourself!'
            ], $this->successStatus);
        }else{
            $admin->delete();
            return response()->json([
                "success" => true,
                "error" => false,
                'message' => 'You deleted '.$admin['name'].' '.$admin['surname'].', successfully!',
                'users' => Admin::all()
            ], $this->successStatus); 
        }

    }

    public function UserGenerator()
    {
      # code...
        $thisyear = date("Y");
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
