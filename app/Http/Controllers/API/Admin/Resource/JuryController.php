<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Jury;
use App\Countries;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class JuryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
        $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $newsupervisor = $request->user;
        $way = $request->way;

        switch ($way) {
            case 'admin':
                # code...
                $validator = Validator::make($newsupervisor, [
                    'name' => 'required|max:40|min:2',
                    'surname' => 'required|max:40|min:2',
                    'email' => 'required|email|unique:juries|max:255',
                    'password' => 'required|max:18|min:6',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                /*if (!$newsupervisor['otherdomain']) {
                    # code...
                    $emailregex = explode("@", $newsupervisor['email']);

                    if ($emailregex[1] != 'sdu.edu.kz' || $emailregex[1] != 'stu.sdu.edu.kz') {
                        # code...
                        return response()->json([
                            "success" => false,
                            "error" => true,
                            'message' => "Juries with @sdu.edu.kz and @stu.sdu.edu.kz domains accepted"
                        ], $this->successStatus);      
                    }
                }*/
                

                $user = new Jury;

                $user->jury_id = $this->UserGenerator();

                $user->name = $newsupervisor['name'];
                $user->surname = $newsupervisor['surname'];
                $user->bio = $newsupervisor['bio'];
                $user->image = "/general/images/user.png";

                $user->category = $newsupervisor['category'];
                $user->email = $newsupervisor['email'];
                $user->password = bcrypt($newsupervisor['password']);
                $user->b_password = Crypt::encryptString($newsupervisor['password']);

                $user->phone = $newsupervisor['phone'];
                $user->phone_code = "";
                
                $user->email_notification_state = 1;
                $user->is_email_activated = ($newsupervisor['emailact']) ? 1 : 0 ;
                $user->is_phone_activated = ($newsupervisor['phoneact']) ? 1 : 0 ;
                $user->is_assistant = ($newsupervisor['assistant']) ? 1 : 0 ;

                $user->save();

                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'You registered Jury, successfully!'
                ], $this->successStatus);  

                break;
            case 'jury':
                $newsupervisor = $request->jury;

                $validator = Validator::make($newsupervisor, [
                    'name' => 'required|max:40|min:2',
                    'surname' => 'required|max:40|min:2',
                    'email' => 'required|email|unique:juries|max:255',
                    'password' => 'required|max:18|min:6',
                ]);

                if (!$this->validEmail($newsupervisor['email'])) {
                    # code...
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => "Juries with @sdu.edu.kz and @stu.sdu.edu.kz domains accepted! "
                    ], $this->successStatus);      
                }

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $jury = Jury::create([
                    'jury_id' => $this->UserGenerator(),

                    'name' => $newsupervisor['name'],
                    'surname'  => $newsupervisor['surname'],
                    'bio' => "",
                    'image' => "/general/images/user.png",

                    'category' => Auth::user()->category,

                    'email' => $newsupervisor['email'],
                    'password' => bcrypt($newsupervisor['password']),
                    'b_password' => Crypt::encryptString($newsupervisor['password']),

                    'phone' => $newsupervisor['phone'],
                    'phone_code' => '',
                    
                    'email_notification_state' => 1,

                    'is_email_activated' => 1,
                    'is_phone_activated' => 1,
                    'is_assistant' => 1
                ]);

                // Tracker
                $this->ActivitiesTracker->track(Auth::user()->id, 2, 'assistant', '', '', 1);
                return response()->json([
                    "success" => true,
                    "error" => false,
                    "message" => "You successfully registered an assistant!",
                    "assistents" => Jury::where('is_assistant', 1)->where('category', Auth::user()->category)->get()
                    ], $this->successStatus);
                break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $jury = Jury::where('jury_id', $id)->first();

        $jury['pass'] = Crypt::decryptString($jury->b_password);
        return response()->json(['jury' =>  $jury], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
      $way = $request->input('way');

        switch ($way) {
            case 'profile':
                # code...
                $validator = Validator::make($request->input('jury'), [
                    'name' => 'required|max:40|min:2',
                    'surname' => 'required|max:40|min:2',
                    'bio' => 'required|min:6',
                    'phone' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $jury = Jury::where('jury_id', $id)->first();
                $fields = "";

                if ($jury->name != $request->input('jury')['name']) {
                    # code...
                    $jury->name = $request->input('jury')['name'];
                    $fields .= 'Name, ';
                }
                if ($jury->surname != $request->input('jury')['surname']) {
                    # code...
                    $jury->surname = $request->input('jury')['surname'];
                    $fields .= 'surname, ';
                }
                if ($jury->bio != $request->input('jury')['bio']) {
                    # code...
                    $jury->bio = $request->input('jury')['bio'];
                    $fields .= 'bio, ';
                }
                if ($jury->phone != $request->input('jury')['phone']) {
                    # code...
                    $jury->phone = $request->input('jury')['phone'];
                    $fields .= 'phone, ';
                }

                if (sizeof($fields) > 0) {
                    # code...
                    $jury->save();
                    $fields = substr($fields, 0, -2);

                    return response()->json([
                        "success" => true,
                        "error" => false,
                        'message' => 'Those '.$fields.' fields are successfully changed!'
                    ], $this->successStatus); 
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => 'Nothing to changed!'
                    ], $this->successStatus); 
                }
                break;
            case 'image':
                # code...
                $jury = Jury::where('jury_id', $id)->first();

                $newimage = $request->image;

                if ($newimage['file'] != "") {
                    # code...
                    $exploded = explode(',', $newimage['file']);
                    $decoded = base64_decode($exploded[1]);

                    $file = $jury->jury_id.'.'.$newimage['ext'];
                    $path = public_path().'/general/jury/'.$file;
                    file_put_contents($path, $decoded);

                    $jury->image = '/general/jury/'.$file;
                    
                    $jury->save();
                    return response()->json([
                        "success" => true,
                        "error" => false,
                        'message' => 'Image is successfully changed!'
                    ], $this->successStatus);                  
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => 'Please, select an image!'
                    ], $this->successStatus); 
                }
                break;
            case 'password':
                # code...
                $jury = Jury::where('jury_id', $id)->first();

                $password = $request->password;
                $validator = Validator::make( $password, [
                    'old_password' => 'required|max:18|min:6',
                    'new_password' => 'required|max:18|min:6',
                    'confirm_password' => 'required|same:new_password'
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                if ($password['old_password'] == Crypt::decryptString($jury->b_password)) {
                    # code...
                    if ($password['new_password'] != Crypt::decryptString($jury->b_password)) {
                        # code...
                        $jury->password =  bcrypt($password['new_password']);
                        $jury->b_password = Crypt::encryptString($password['new_password']);

                        $jury->save();

                        return response()->json([
                            "success" => true,
                            "error" => false,
                            'message' => "New password successfully saved!"
                        ], $this->successStatus);   
                    }else{
                        return response()->json([
                            "success" => false,
                            "error" => true,
                            'message' => "Old password matchs with new password!"
                        ], $this->successStatus);
                    }
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => "Old password not match with entered value!"
                    ], $this->successStatus);   
                }
                break;
        }
        return response()->json([
            "success" => true,
            "error" => false ,
            'message' => $way
        ], $this->successStatus); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = Auth::user();

        if ($user->is_assistant) {
            # code...
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => 'Assistant can\'t delete Users!'
            ], $this->successStatus);    
        }

        $jury = Jury::where('jury_id', $id)->first();

        if (isset($user['admin_id'])) {
            # code...
            $jury->delete();
            return response()->json([
                "success" => true,
                "error" => false,
                'message' => 'You deleted '.$jury['name'].' '.$jury['surname'].', successfully!',
                'users' => Admin::all()
            ], $this->successStatus); 
        }else{
            if ($user->id == $jury['id']) {
                # code...
                return response()->json([
                    "success" => false,
                    "error" => true,
                    'message' => 'You can not delete yourself!'
                ], $this->successStatus);
            }else{
                \DB::table('assistant_marks')->where('assistant', $jury->id)->delete();
                $jury->delete();
                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => 'You deleted '.$jury['name'].' '.$jury['surname'].', successfully!',
                    'assistents' => Jury::where('category', $user->category)->where('is_assistant', 1)->orderBy('is_assistant', 'asc')->get()
                ], $this->successStatus); 
            }
        }
    }

    public function UserGenerator()
    {
      # code...
      $thisyear = date("Y");
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_'.$randomString;
    }

    public function validEmail($email)
    {
        $allowedDomains = array('sdu.edu.kz', 'stu.sdu.edu.kz');
        list($user, $domain) = explode('@', $email);
        if (checkdnsrr($domain, 'MX') && in_array($domain, $allowedDomains))
        {
            return true;
        }
        return false;
    }
}
