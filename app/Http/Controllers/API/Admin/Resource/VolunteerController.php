<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Volunteer;
use Validator;

class VolunteerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function index()
    {
        //
        return response()->json([
            'admin' =>  Auth::user(),
            'users' => Volunteer::all()
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:2',
            'surname' => 'required|max:255|min:2',
            'description' => 'required|max:255|min:2',
            'email' => 'required|max:255|min:2',
            'phone' => 'required|max:255|min:2',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false,'error' => true, 'message' => $validator->errors()->first()], $this->successStatus);            
        }

        if (Volunteer::where('email', $request->email)->first() != null) {
            # code...
            return response()->json(['success' => false, 'error' => true, 'message' => 'This Volunteer is exists'], $this->successStatus);   
        }

        Volunteer::create([
            'volunteer_id' => $this->codegen(),
            'name' => $request->name ,
            'surname' => $request->surname ,
            'description' => $request->description ,
            'email' => $request->email ,
            'phone' => $request->phone ,
        ]);

        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Volunteer successfully imported!',
            'users' => Volunteer::all()
        ], 200);
    }

    public function codegen()
    {
        # code...
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 5; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return 'vol_'.$randomString;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json([
            'success' => true,
            'error' => false,
            'vol' => Volunteer::where('id', $id)->first()
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:2',
            'surname' => 'required|max:255|min:2',
            'description' => 'required|max:255|min:2',
            'email' => 'required|max:255|min:2',
            'phone' => 'required|max:255|min:2',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false,'error' => true, 'message' => $validator->errors()->first()], $this->successStatus);            
        }

        $vol = Volunteer::where('id', $id)->first();
        $vol->name = $request->name;
        $vol->surname = $request->surname;
        $vol->description = $request->description;
        $vol->email = $request->email;
        $vol->phone = $request->phone;

        $vol->save();

        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Volunteer successfully edited!'
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Volunteer::where('id', $id)->delete();

        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Volunteer successfully deleted!',
        ], 200);
    }
}
