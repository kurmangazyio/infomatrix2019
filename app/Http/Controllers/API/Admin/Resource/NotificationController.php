<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use File;

use App\Admin;
use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use Validator;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index()
    {
        //
        return response()->json(["notifications" => Notifications::all(), 'files' => NotificationsFiles::all()], $this->successStatus); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:5',
            'content' => 'required',
            'isTo' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        $note = new Notifications;

        $note->n_creator = Auth::user()->id;
        $note->n_title = $request->input('title');
        $note->n_content = $request->input('content');

        if (sizeof($request->input('isImportant')) > 0) {
            # code...
            $note->is_important = 1;
        }else{
            $note->is_important = 0;
        }

        $note->n_to = $request->input('isTo');

        $note->save();

        $newnote = Notifications::where('n_title', $request->input('title'))->first();
        $file = $request->input('file');

        if ($file['file'] != "") {
            # code...
            $exploded = explode(',', $file['file']);
            $decoded = base64_decode($exploded[1]);

            $ff = Auth::user()->id.'_'.$file['filename'].'.'.$file['ext'];
            $path = public_path().'/general/notifications/'.$ff;
            file_put_contents($path, $decoded);

            $newfile = new NotificationsFiles;

            $newfile->n_notification_id = $newnote->id;
            $newfile->n_admin_id = Auth::user()->id;
            $newfile->file = $file['filename'];
            $newfile->link = '/general/notifications/'.$ff;

            $newfile->save();
        }

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'asc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        return response()->json([
            "success" => true,
            "error" => false,
            'message' => "Successfully imported!",
            'notes' => Notifications::all(),
            'files' => NotificationsFiles::all(),
            'notifications' => $notfications
        ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $notification = Notifications::where('id', $id)->first();
        $notification['files'] = NotificationsFiles::where('n_notification_id', $id)->get();
        $notification['filescount'] = NotificationsFiles::where('n_notification_id', $id)->count();
        $notification['status'] = NotificationsStatus::where('n_notification_id', $id)->count();
        $notification['newdate'] = $this->time_elapsed_string($notification->created_at);

        return response()->json([
            "notification" => $notification
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        NotificationsStatus::where('n_notification_id', $id)->delete();
        $files = NotificationsFiles::where('n_notification_id', $id)->get();
        if (sizeof($files) > 0) {
            # code...
            foreach ($files as $file) {
                # code...
                Storage::delete($file->file);
                NotificationsFiles::where('id', $file->id)->delete();
            }
        }

        Notifications::where('id', $id)->delete();

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        return response()->json([
            "success" => true,
            "error" => false,
            'message' => "Notification successfully deleted!",
            'notes' => Notifications::all(),
            'notifications' => $notfications
        ], $this->successStatus); 
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
