<?php

namespace App\Http\Controllers\API\Admin\Export;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Projects;
use App\Teams;
use App\Students;
use App\Marks;

use App\Regions;

class ExportController extends Controller
{
    //
    public $successStatus = 200;

    public function index()
    {
    	# code...
    	return response()->json([
            'admin' => Auth::user(),
            'finallist' => $this->getExport(),
            'supers' => $this->getSupervisors()
        ], $this->successStatus); 
    }

    public function getExport()
    {
        # code...
        $teams = Teams::select('projects.p_title as title','users.name as sname','users.email as email', 'users.surname as ssurname', 'teams.t_project_mark as mark','teams.*', 'categories.category as pct')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->join('users', 'users.id','=', 'teams.t_supervisor')
            ->join('categories', 'categories.id','=', 'projects.p_category')
            ->get();

        $arr = array();
        foreach ($teams as $v) {
            # code...
            $marker = '';
            switch ($v['mark']) {
            	case 0:
            		# code...
            		$marker = 'Not checked';
            		break;
            	case 1:
            		# code...
            		$marker = 'Rejected';
            		break;
            	case 2:
            		# code...
            		$marker = '50/50';
            		break;
            	case 3:
            		# code...
            		$marker = 'Accepted';
            		break;
            }

            if ($v->t_f_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_f_student_code'] )->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();

                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'].' '.$stu['s_ru_name'].' '.$stu['s_ru_lastname'],
                    'fullname'=> $stu['s_en_surname'].' '.$stu['s_en_name'].' '.$stu['s_en_lastname'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'].' '.$v['ssurname'],
                    'project' => $v['title'],
                    'category'=> $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip'=> $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' =>  $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' =>  $stu['s_daryn'],
                    'code' => $v->t_f_student_code,
                    'gender' => $stu['s_gender'] == 1 ? 'Male': 'Female' 

                ]);
            }
            if ($v->t_s_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_s_student_code'] )->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();
                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'].' '.$stu['s_ru_name'].' '.$stu['s_ru_lastname'],
                    'fullname'=> $stu['s_en_surname'].' '.$stu['s_en_name'].' '.$stu['s_en_lastname'],
                    'project' => $v['title'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'].' '.$v['ssurname'],
                    'category'=> $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip'=> $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' =>  $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' =>  $stu['s_daryn'],
                    'code' => $v->t_s_student_code,
                    'gender' => $stu['s_gender'] == 1 ? 'Male': 'Female' 
                ]);
            }
            if ($v->t_t_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_t_student_code'] )->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();
                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'].' '.$stu['s_ru_name'].' '.$stu['s_ru_lastname'],
                    'fullname'=> $stu['s_en_surname'].' '.$stu['s_en_name'].' '.$stu['s_en_lastname'],
                    'project' => $v['title'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'].' '.$v['ssurname'],
                    'category'=> $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip'=> $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' =>  $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' =>  $stu['s_daryn'],
                    'code' => $v->t_t_student_code,
                    'gender' => $stu['s_gender'] == 1 ? 'Male': 'Female' 
                ]);
            }
        }

        return $arr;
    }

    public function getSupervisors()
    {
    	# code...
    	$super = User::select('users.*', 'countries.name as cname')
            ->join('countries', 'users.country','=', 'countries.id')
            ->get();

        $teams = Teams::select('users.email as email', 'teams.t_project_mark')
            ->join('users', 'users.id','=', 'teams.t_supervisor')
            ->where('teams.t_project_mark', 3)
            ->get();

        foreach ($super as $sss) {
        	# code...
        	$counter = 0;
        	foreach ($teams as $value) {
        		# code...
        		if ($value['email'] == $sss['email']) {
        			# code...
        			$counter++;
        		}
        	}
        	$sss['accepted'] = $counter;
        }

        $reg = Regions::all();

        foreach ($super as $value) {
        	# code...
        	if($value['country'] == 110){
        		foreach ($reg as $key) {
        			# code...
        			if ($key['id'] == $value['region']) {
        				# code...
        				$value['region'] = $key['kz'];
        			}
        		}
        	}

            $value['enco'] = '**************'; // Crypt::decryptString($value->b_password);
        }
        
        return $super;
    }
}
