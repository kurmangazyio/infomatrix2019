<?php

namespace App\Http\Controllers\API\Admin\Online;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Projects;
use App\Teams;
use App\Students;
use App\Marks;

class OnlineController extends Controller
{
    //
    public $successStatus = 200;

    public function index()
    {
    	# code...
    	$projects = Projects::select(
    		'projects.p_title as title', 
    		'categories.category as category',
    		'teams.t_name',
    		'teams.t_project_mark as mark',
    		\DB::raw("(select s_en_name from students where s_student_id = teams.t_f_student_code) as s1_name"),
            \DB::raw("(select s_en_surname from students where s_student_id = teams.t_f_student_code) as s1_surname"),
            \DB::raw("(select s_en_lastname from students where s_student_id = teams.t_f_student_code) as s1_lastname"),
            \DB::raw("(select s_daryn from students where s_student_id = teams.t_f_student_code) as s1_daryn"),

            \DB::raw("(select s_en_name from students where s_student_id = teams.t_s_student_code) as s2_name"),
            \DB::raw("(select s_en_surname from students where s_student_id = teams.t_s_student_code) as s2_surname"),
            \DB::raw("(select s_en_lastname from students where s_student_id = teams.t_s_student_code) as s2_lastname"),
            \DB::raw("(select s_daryn from students where s_student_id = teams.t_f_student_code) as s2_daryn"),

            \DB::raw("(select s_en_name from students where s_student_id = teams.t_t_student_code) as s3_name"),
            \DB::raw("(select s_en_surname from students where s_student_id = teams.t_t_student_code) as s3_surname"),
            \DB::raw("(select s_en_lastname from students where s_student_id = teams.t_t_student_code) as s3_lastname"),
            \DB::raw("(select s_daryn from students where s_student_id = teams.t_t_student_code) as s3_daryn"),

            \DB::raw("(select name from countries where id = users.country) as country"),
            'users.name as sname',
    		'users.surname as ssurname',
    		'projects.p_project_id'
    		)
        ->join('categories', 'projects.p_category', '=', 'categories.id')
        ->join('teams', 'projects.p_project_id', '=', 'teams.t_project_code')
        ->join('users', 'users.id', '=', 'teams.t_supervisor')
        ->orderBy('teams.t_project_mark', 'DESC')
        ->get();

        $studets = Students::all();

        $usersgroupedbycountry = User::select(
        	\DB::raw('count(id) as counted'), 
        	\DB::raw('(select name from countries where id = users.country) as cname')

        )
        ->groupBy('country')->get();

    	return response()->json([
            'admin' => Auth::user(),
            'projects' => $projects,
            'studets' => $studets
        ], $this->successStatus);  
    }

    public function getstats()
    {
    	# code...
    	$usersgroupedbycountry = User::select(
        	\DB::raw('count(id) as counted'), 
        	\DB::raw('(select name from countries where id = users.country) as cname')

        )
        ->groupBy('country')->get();

        //projects get grouped
        $projectsgrouped = User::select(
        	\DB::raw('count(projects.id) as projects'), 
        	'countries.name as country'
        )
        ->join('projects', 'projects.p_supervisor', '=', 'users.id')
        ->join('countries', 'users.country', '=', 'countries.id')
        ->groupBy('countries.name')->get();

        $newarray = array();
        foreach ($projectsgrouped as $value) {
        	# code...
        	$newarray[$value['country']] = $value['projects'];
        }

        //students get grouped
        $stdudentsgrouped = User::select(
        	\DB::raw('count(students.id) as students'), 
        	'countries.name as country'
        )
        ->join('students', 'students.s_supervisor', '=', 'users.id')
        ->join('countries', 'users.country', '=', 'countries.id')
        ->groupBy('countries.name')->get();

        $newarray2 = array();

        foreach ($stdudentsgrouped as $value) {
        	# code...
        	$newarray2[$value['country']] = $value['students'] == null ? 0 : $value['students'];
        }

        $stats = array();
        foreach ($usersgroupedbycountry as $key) {
        	# code...
        	$stats[$key['cname']] = [
        		'total' => $key['counted'],
        		'projects' => array_key_exists($key['cname'], $newarray) ? $newarray[$key['cname']]: 0,
        		'students' => array_key_exists($key['cname'], $newarray2) ? $newarray2[$key['cname']]: 0,
        	];
        }

        return response()->json($stats, $this->successStatus); 
    }
}
