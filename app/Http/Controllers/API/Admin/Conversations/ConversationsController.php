<?php

namespace App\Http\Controllers\API\Admin\Conversations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use Validator;

class ConversationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public $successStatus = 200;

    public function index()
    {
        //
        $convusers = Conversations::select('conversations.*', 'users.image', 'users.name', 'users.surname', 'users.user_id', 'users.email')
                ->join('users', 'conversations.conv_creator', '=', 'users.id')
                ->where('conversations.conv_creator_level', 1)
                ->get();

        return response()->json(['conversations' => $convusers], $this->successStatus); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $convusers = Conversations::select('conversations.*', 'users.image', 'users.name', 'users.surname', 'users.user_id', 'users.email', 'conversations_categories.conv_cat_title as category')
                ->join('users', 'conversations.conv_creator', '=', 'users.id')
                ->join('conversations_categories', 'conversations.conv_category', '=', 'conversations_categories.id')
                ->where('conversations.conv_code', $id)
                ->first();

        return response()->json([
            'conversation' => $convusers, 
            'answers' => ConversationsAnswers::where('conv_answer_conv_id', $convusers->id)->get()
        ], $this->successStatus); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        switch ($request->input('action')) {
            case 'close':
                # code...
                $conv = Conversations::where('conv_code', $id)->first();

                if ($conv->conv_state != $request->input('state')) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => 'Conversation is already closed!',
                        'conversations' => Conversations::select('conversations.*', 'users.image', 'users.name', 'users.surname', 'users.user_id', 'users.email')->join('users', 'conversations.conv_creator', '=', 'users.id')->where('conversations.conv_creator_level', 1)->get()
                    ], $this->successStatus);            
                }

                $conv->conv_state = 1;
                $conv->save();

                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => "Conversation state successfully changed!",
                    'conversations' => $convusers = Conversations::select('conversations.*', 'users.image', 'users.name', 'users.surname', 'users.user_id', 'users.email')->join('users', 'conversations.conv_creator', '=', 'users.id')->where('conversations.conv_creator_level', 1)->get()
                ], $this->successStatus); 

                break;
            case 'open':
                # code...
                $conv = Conversations::where('conv_code', $id)->first();

                if ($conv->conv_state != $request->input('state')) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => 'Conversation is already closed!',
                        'conversations' => Conversations::select('conversations.*', 'users.image', 'users.name', 'users.surname', 'users.user_id', 'users.email')->join('users', 'conversations.conv_creator', '=', 'users.id')->where('conversations.conv_creator_level', 1)->get()
                    ], $this->successStatus);            
                }

                $conv->conv_state = 0;
                $conv->save();

                return response()->json([
                    "success" => true,
                    "error" => false,
                    'message' => "Conversation state successfully changed!",
                    'conversations' => $convusers = Conversations::select('conversations.*', 'users.image', 'users.name', 'users.surname', 'users.user_id', 'users.email')->join('users', 'conversations.conv_creator', '=', 'users.id')->where('conversations.conv_creator_level', 1)->get()
                ], $this->successStatus); 

                break;
            default:
                # code...
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
