<?php

namespace App\Http\Controllers\API\Admin\Medal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use Validator;

use App\Students;
use App\Projects;
use App\Teams;
use App\User;

use App\Jury;
use App\Tours;
use App\Criteria;
use App\Points;

use App\Postregister;
use App\Appeal;

use App\Categories;
use App\Medals;

class MedalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'student' => 'required',
            'medal' => 'required',
            'category' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $validator->errors()->first()
            ], 200);            
        }

        $student = Students::where('s_student_id', $request->student)->first();

        if ($request->medal == 0) {
            # code...
            Medals::where('studentid', $student->id)->where('categoryid', $request->category)->delete();
        }else{
            if (Medals::where('studentid', $student->id)->where('categoryid', $request->category)->first() == null) {
                # code...
                Medals::create([
                    'studentid' => $student->id,
                    'categoryid' => $request->category,
                    'medal' => $request->medal
                ]);
            }else{
                Medals::where('studentid', $student->id)->where('categoryid', $request->category)->update([ 'medal' => $request->medal ]);
            }
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'asd' => $request->all(),
            'message' => $student['s_en_name'].' '.$student['s_en_surname'].' successfully addet appeal!'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
