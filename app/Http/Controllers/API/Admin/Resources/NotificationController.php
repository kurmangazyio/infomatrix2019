<?php

namespace App\Http\Controllers\API\Admin\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Admin;
use App\Notifications;
use App\NotificationsFiles;

use Validator;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index()
    {
        //
        return response()->json(["notifications" => Notifications::all(), 'files' => NotificationsFiles::all()], $this->successStatus); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:5',
            'content' => 'required',
            'isTo' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        $note = new Notifications;

        $note->n_creator = Auth::user()->id;
        $note->n_title = $request->input('title');
        $note->n_content = $request->input('content');

        if (sizeof($request->input('isImportant')) > 0) {
            # code...
            $note->is_important = 1;
        }else{
            $note->is_important = 0;
        }

        $note->n_to = $request->input('isTo');

        $note->save();

        $newnote = Notifications::where('n_title', $request->input('title'))->first();
        $file = $request->input('file');

        if ($file['file'] != "") {
            # code...
            $exploded = explode(',', $file['file']);
            $decoded = base64_decode($exploded[1]);

            $ff = Auth::user()->id.'_'.$file['filename'].'.'.$file['ext'];
            $path = public_path().'/general/notifications/'.$ff;
            file_put_contents($path, $decoded);

            $newfile = new NotificationsFiles;

            $newfile->n_notification_id = $newnote->id;
            $newfile->n_admin_id = Auth::user()->id;
            $newfile->file = $file['filename'];
            $newfile->link = '/general/notifications/'.$ff;

            $newfile->save();
        }

        return response()->json([
            "success" => true,
            "error" => false,
            'message' => "Successfully imported!",
            'notes' => Notifications::all(),
            'files' => NotificationsFiles::all()
        ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $note = Notifications::where('id', $id)->first();
        $note->delete();

        return response()->json([
            "success" => true,
            "error" => false,
            'message' => "Notification successfully deleted!",
            'notes' => Notifications::all(),
            'files' => NotificationsFiles::all()
        ], $this->successStatus); 
    }
}
