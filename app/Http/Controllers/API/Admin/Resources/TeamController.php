<?php

namespace App\Http\Controllers\API\Admin\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Teams;
use App\Students;

use App\Countries;
use App\Regions;

use App\Categories;
use App\Marks;
use Validator;

use App\Http\Controllers\Activities\ActivitiesTracker;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $team_code = $this->TeamGenerator();
        $validator = Validator::make($request->all(), [
            'team-name' => 'required|max:255|min:2',
        ]);

        if ($validator->fails()) {
            return response()->json(['report' => $validator->errors()->first()], $this->successStatus);            
        }

        $project = $request->input('project');
        $students = $request->input('students');

        //owner
        $newteam = new Teams;
        $newteam->t_supervisor= Auth::user()->id;
        //team
        $newteam->t_team_id= $team_code;
        $newteam->t_name= $request->input('team-name');

        //members
        
        $newteam->t_project_code = $project['code'];

        $newteam->t_project_mark = 0;
        $newteam->t_project_mark_comment = "";
        
        switch (sizeof($students)) {
            case 1:
                # code...
                $newteam->t_f_student_code  = $students[0]['code'];
                $newteam->t_s_student_code = "";
                $newteam->t_t_student_code = "";
                break;
            case 2:
                # code...
                $newteam->t_f_student_code  = $students[0]['code'];
                $newteam->t_s_student_code = $students[1]['code'];
                $newteam->t_t_student_code = "";
                break;
            case 3:
                # code...
                $newteam->t_f_student_code  = $students[0]['code'];
                $newteam->t_s_student_code = $students[1]['code'];
                $newteam->t_t_student_code = $students[2]['code'];
                break;
        }

        $newteam->save();
        // Tracker
        $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-registed', $team_code);

        return response()->json(
            [
                'report' => "Your team (also team-members) and project successfully registered!",
            ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        switch ($request->input('action')) {
            case 'name':
              # code...
              $edited = $request->input('team');
              

              $validator = Validator::make($edited, [
                  't_name' => 'required|max:255|min:5',
              ]);

              if ($validator->fails()) {
                return response()->json(["success" => false,"error" => true,"message" => $validator->errors()->first()], $this->successStatus);
              }

              $teams = Teams::where('t_team_id', $id)->first();

              $old = $teams->t_name;

              if ($teams->t_name != $edited['t_name']) {
                # code...
                $teams->t_name = $edited['t_name'];
                $teams->save();

                // Tracker
                $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-name-changed', $old);
                return response()->json([ 
                        "success" => true,
                        "error" => false,
                        "message" => "Team name successfully changed"
                    ], $this->successStatus);
              }else{
                    return response()->json([ 
                        "success" => false ,
                        "error" => true,
                        "message" => "Nothing to change!"
                    ], $this->successStatus);
                }
              break;
            case 'addstudent':
              # code...
              $teams = Teams::where('t_team_id', $id)->first();

              if ($teams->t_f_student_code == $request->input('student') || 
                  $teams->t_s_student_code == $request->input('student') || 
                  $teams->t_t_student_code == $request->input('student')) {
                # code....
                return response()->json([ 
                    "success" => false, "error" => true, "message" => "The student is in the team!"], $this->successStatus);
              }
              
              if ($teams->t_f_student_code == '') {
                # code...
                $teams->t_f_student_code = $request->input('student');
                $teams->save();

                // Tracker
                $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-student-changed', $request->input('student'));
                return response()->json([ 
                    "success" => true,
                    "error" => false,
                    "message" => "Team student successfully imported!",
                    'students' => $this->getstudent($id)
                ], $this->successStatus);
              }
              elseif ($teams->t_s_student_code == '') {
                # code...
                $teams->t_s_student_code = $request->input('student');
                $teams->save();

                // Tracker
                $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-student-changed', $request->input('student'));

                return response()->json([ 
                    "success" => true,
                    "error" => false,
                    "message" => "Team student successfully imported!",
                    'students' => $this->getstudent($id)
                ], $this->successStatus);
              }
              elseif ($teams->t_t_student_code == '') {
                # code...
                $teams->t_t_student_code = $request->input('student');
                $teams->save();

                // Tracker
                $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-student-changed', $request->input('student'));
                return response()->json([ 
                    "success" => true,
                    "error" => false,
                    "message" => "Team student successfully imported!",
                    'students' => $this->getstudent($id)
                ], $this->successStatus);
              }
              else{
                  return response()->json([ 
                    "success" => false ,
                    "error" => true,
                    "message" => "The team has three students! The maximum number of participants in one team is three students!"
                ], $this->successStatus);
              }
              break;

            case 'removestudent':
              # code...
              $teams = Teams::where('t_team_id', $id)->first();

              if ($teams->t_f_student_code == $request->input('student')) {
                # code...
                if ($teams->t_s_student_code == "") {
                  # code...
                  return response()->json([ 
                      "success" => false ,
                      "error" => true,
                      "message" => "Team must have at least one participant!"
                  ], $this->successStatus);
                }
                $teams->t_f_student_code = $teams->t_s_student_code;
                $teams->t_s_student_code = $teams->t_t_student_code;
                $teams->t_t_student_code = "";
                $teams->save();

                // Tracker
                $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-student-removed', $request->input('student'));
                return response()->json([ 
                    "success" => true,
                    "error" => false,
                    "message" => "Team student successfully dropped!",
                    'students' => $this->getstudent($id)
                ], $this->successStatus);
              }
                elseif ($teams->t_s_student_code == $request->input('student')) {
                  # code...
                  $teams->t_s_student_code = $teams->t_t_student_code;
                  $teams->t_t_student_code = "";
                  $teams->save();

                  // Tracker
                  $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-student-removed', $request->input('student'));
                  return response()->json([ 
                      "success" => true,
                      "error" => false,
                      "message" => "Team student successfully dropped!",
                      'students' => $this->getstudent($id)
                  ], $this->successStatus);
                }
                elseif ($teams->t_t_student_code == $request->input('student')) {
                  # code...
                  $teams->t_t_student_code = "";
                  $teams->save();

                  // Tracker
                  $this->ActivitiesTracker->track(Auth::user()->id, 1, 'team-student-removed', $request->input('student'));
                  return response()->json([ 
                      "success" => true,
                      "error" => false,
                      "message" => "Team student successfully dropped!",
                      'students' => $this->getstudent($id)
                  ], $this->successStatus);
                }
                else{
                    return response()->json([ 
                      "success" => false ,
                      "error" => true,
                      "message" => "Noone is droped"
                  ], $this->successStatus);
                }
              break;
            case 'mark':
                # code...
                $mark = $request->input('mark');
                
                if ($mark['mark'] == 1 || $mark['mark'] == 2 || $mark['mark'] == 3) {
                    # code...
                    $team = Teams::where('t_team_id', $id)->first();

                    $team->t_project_mark = $mark['mark'];
                    $team->t_project_mark_comment = $mark['comment'];
                    $team->save();

                    return response()->json([ 
                        "success" => true,
                        "error" => false,
                        "message" => "Successfully marked the project!"
                    ], $this->successStatus);
                }else{
                    return response()->json([ 
                        "success" => false ,
                        "error" => true,
                        "message" => "Please, select mark!"
                    ], $this->successStatus);
                }
                break;
            case 'assistant':
                # code...
                if ($request->input('mark')['mark'] == 1 || $request->input('mark')['mark'] == 2 || $request->input('mark')['mark'] == 3) {
                    # code...
                    $team = Teams::where('t_team_id', $id)->first();
                    $marked = Marks::where('assistant', Auth::user()->id)->where('team_id', $team->id)->first();

                    switch (sizeof($marked)) {
                        case 0:
                            # code...
                            $mark = new Marks;
                            $mark->assistant = Auth::user()->id;
                            $mark->team_id = $team->id;
                            $mark->mark = $request->input('mark')['mark'];
                            $mark->comment = $request->input('mark')['comment'];
                            $mark->save();

                            return response()->json([ 
                                "success" => true,
                                "error" => false,
                                "message" => "Successfully marked the project!"
                            ], $this->successStatus);

                            break;
                        case 1:
                            # code...
                            if ($marked->mark == $request->input('mark')['mark']) {
                                # code...
                                return response()->json([ 
                                    "success" => false ,
                                    "error" => true,
                                    "message" => "You already marked this project!"
                                ], $this->successStatus);
                            }else{
                                $marked->mark = $request->input('mark')['mark'];
                                $marked->comment = $request->input('mark')['comment'];
                                $marked->save();

                                return response()->json([ 
                                    "success" => true,
                                    "error" => false,
                                    "message" => "Successfully marked changed of this project!"
                                ], $this->successStatus);
                            }
                            break;
                    }
                    
                }else{
                    return response()->json([ 
                        "success" => false ,
                        "error" => true,
                        "message" => "Please, select mark!"
                    ], $this->successStatus);
                }
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
      
    }
    
    public function TeamGenerator()
    {
      # code...
      $thisyear = date("Y");
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_tt_'.$randomString;
    }

    public function getstudent($id)
    {
      # code...
      $team = Teams::where('t_team_id', $id)->first();
      $country = Countries::all();
      $regions = Regions::all();
      $s1 = Students::where('s_student_id', $team['t_f_student_code'])->first();
      foreach ($country as $key) {
          if ($key['id'] == $s1->s_country) {
              $s1->s_country = $key['name'];
          }
      }
      if (is_numeric($s1->s_region)) {
          foreach ($regions as $key) {
              if ($key['id'] == $s1->s_region) {
                  $s1->s_region = $key['en'];
              }
          }
      }
      $students = array($s1);
      //student 2
      if($team->t_s_student_code != ""){
          $s2 = Students::where('s_student_id', $team['t_s_student_code'])->first();
          if (is_numeric($s2->s_region)) {
              foreach ($regions as $key) {
                  if ($key['id'] == $s2->s_region) {
                      $s2->s_region = $key['en'];
                  }
              }
          }
          foreach ($country as $key) {
              if ($key['id'] == $s2->s_country) {
                  $s2->s_country = $key['name'];
              }
          }
          array_push($students, $s2);
      }
      //student 3
      if($team->t_t_student_code != ""){
          $s3 = Students::where('s_student_id', $team['t_t_student_code'])->first();
          if (is_numeric($s3->s_region)) {
              foreach ($regions as $key) {
                  if ($key['id'] == $s3->s_region) {
                      $s3->s_region = $key['en'];
                  }
              }
          }
          foreach ($country as $key) {
              if ($key['id'] == $s3->s_country) {
                  $s3->s_country = $key['name'];
              }
          }
          array_push($students, $s3);
      }

      return $students;
    }
}
