<?php

namespace App\Http\Controllers\API\Admin\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;
use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public $successStatus = 200;

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $validator = Validator::make( $request->all(), [
            'reply' => 'required|min:15',
            'code' => 'required|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'answer' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        $user = Auth::user();
        $conv = Conversations::where('conv_code', $request->input('code'))->first();

        $answer = new ConversationsAnswers;

        if (isset($user['user_id'])) {
            # code...
            $answer->conv_answer_user_id = $user->id;
            $answer->conv_answer_user_level = 1;
        }elseif (isset($user['jury_id'])) {
            # code...
            $answer->conv_answer_user_id = $user->id;
            $answer->conv_answer_user_level = 2;
        } else {
            # code...
            $answer->conv_answer_user_id = $user->id;
            $answer->conv_answer_user_level = 3;
        }

        $answer->conv_answer_conv_id = $conv->id;
        $answer->conv_answer_code = 'a_'.$this->CodeGenerator();
        $answer->conv_answer_content = $request->input('reply');
        $answer->save();
        
        // Tracker
        $this->ActivitiesTracker->track(Auth::user()->id, 1, 'conversation-answer-sent', $request->input('reply'));

        return response()->json([
                'answers' => ConversationsAnswers::where('conv_answer_conv_id', $conv->id)->get(),
                'answer' => 'Your message successfully sent!'
        ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CodeGenerator()
    {
      # code...
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 10; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
}
