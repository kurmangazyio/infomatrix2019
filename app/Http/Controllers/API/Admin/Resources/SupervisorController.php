<?php

namespace App\Http\Controllers\API\Admin\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;

use Validator;
use Carbon\Carbon;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\UserConfig;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $newsupervisor = $request->user;
        $way = $request->way;

        switch ($way) {
            case 1:
                # code...
                $validator = Validator::make($newsupervisor, [
                    'name' => 'required|max:40|min:2',
                    'surname' => 'required|max:40|min:2',
                    'country' => 'required',
                    'email' => 'required|email|unique:users|max:255',
                    'password' => 'required|max:18|min:6',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $user = User::create([
                    'user_id' => $this->UserGenerator($newsupervisor['country']),

                    'name' => $newsupervisor['name'],
                    'surname' => $newsupervisor['surname'],
                    'bio' => "",
                    'image' => "/general/images/user.png",

                    'email'  => $newsupervisor['email'],
                    'password' => bcrypt($newsupervisor['password']),
                    'b_password' => Crypt::encryptString($newsupervisor['password']),

                    'phone' => "",
                    'phone_code' => "",
                    
                    'email_notification_state' => 1,

                    'is_email_activated' => 0,
                    'is_phone_activated' => 0,
                    
                    'country' => $newsupervisor['country'],
                    'region' => "",
                    'city' => "",
                    'zip' => "",
                    'school' => "",
                ]);

                //Event::fire(new SendVerificationMail($request->input('email'), $request->input('name').' '.$request->input('surname')));

                $objToken = $user->createToken('API Info-User');
                $token = $objToken->accessToken;

                $expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now());
                return response()->json([
                    "success" => true,
                    "error" => false,
                    "message" => "You successfully authenticated!",
                    "token_type" => "Bearer", 
                    "expires_in" => $expiration, 
                    "access_token" => $token
                    ], $this->successStatus);
                break;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = Auth::user();
        $useris = $request->user;

        $config = UserConfig::get()->first();

        switch ($request->set) {
            case 'profile':
                # code...
                $mess = "";
                $message = "Nothing to change!";
                if ($config->user_profile_edit == 0) {
                    return response()->json(['profile' => "Administrators disabled profile settings!"], $this->successStatus);
                }

                $validator = Validator::make($useris, [
                    'name' => 'required|max:25|min:2',
                    'surname' => 'required|max:25|min:2',
                    'bio' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'success' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                if ($user->name != $useris['name']) {
                    # code...
                    $user->name = $useris['name'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", name" ;
                    }else{
                        $mess = $mess."Name";
                    }
                }
                if ($user->surname != $useris['surname'] ) {
                    # code...
                    $user->surname = $useris['surname'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", surname"; 
                    }else{
                        $mess = $mess."Surname";
                    }
                }
                if ($user->bio != $useris['bio']) {
                    # code...
                    $user->bio = $useris['bio'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", bio" ;
                    }else{
                        $mess = $mess."Bio";
                    }
                }

                if ($mess != "") {
                    # code...
                    $user->save();
                    // Tracker
                    $this->ActivitiesTracker->track(Auth::user()->id, 1, 'profile', $mess);
                    return response()->json(['profile' => $mess." successfully changed!"], $this->successStatus);
                }else{
                    return response()->json(['profile' => $message], $this->successStatus);
                }

                break;
            case 'image':
                $message = "Nothing to change!";

                $image = $request->input('image');

                if ($config->user_profile_edit == 0) {
                    return response()->json(['image' => "Administrators disabled profile settings!"], $this->successStatus);
                }

                if ($image['file'] == "") {
                    # code...
                    return response()->json(['image' => $message], $this->successStatus);
                }

                $user = Auth::user();

                $image = $request->input('image');
                if (sizeof($image) > 0) {
                    # code...
                    $exploded = explode(',', $image['file']);
                    $decoded = base64_decode($exploded[1]);

                    $file = Auth::user()->user_id.'.'.$image['ext'];
                    $path = public_path().'/general/users/'.$file;
                    file_put_contents($path, $decoded);

                    $user->image = '/general/users/'.$file;
                    $user->save();

                    // Tracker
                    $this->ActivitiesTracker->track(Auth::user()->id, 1, 'profile', 'Image is changed!');
                    return response()->json(
                        ['image' => "Image successfully changed!",'user' => Auth::user()], $this->successStatus);
                }else{
                    return response()->json(['image' => $message], $this->successStatus);
                }
                break;
            case 'address':
                $mess = "";
                $message = "Nothing to change!";

                if ($config->user_profile_edit == 0) {
                    return response()->json(['address' => "Administrators disabled profile settings!"], $this->successStatus);
                }

                $validator = Validator::make($useris, [
                    'country' => 'required',
                    'region' => 'required',
                    'zip' => 'required',
                    'city' => 'required',
                    'school' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'address' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                if ($user->country != $useris['country']) {
                    # code...
                    $user->country = $useris['country'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", country" ;
                    }else{
                        $mess = $mess."Country";
                    }
                }

                if ($user->region != $useris['region']) {
                    # code...
                    $user->region = $useris['region'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", region" ;
                    }else{
                        $mess = $mess."Region";
                    }
                }

                if ($user->zip != $useris['zip']) {
                    # code...
                    $user->zip = $useris['zip'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", zip code" ;
                    }else{
                        $mess = $mess."Zip code";
                    }
                }

                if ($user->city != $useris['city']) {
                    # code...
                    $user->city = $useris['city'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", city" ;
                    }else{
                        $mess = $mess."City";
                    }
                }

                if ($user->school != $useris['school']) {
                    # code...
                    $user->school = $useris['school'];
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", school" ;
                    }else{
                        $mess = $mess."School";
                    }
                }

                if ($mess != "") {
                    # code...
                    $user->save();
                    // Tracker
                    $this->ActivitiesTracker->track(Auth::user()->id, 1, 'profile', $mess);
                    return response()->json(['address' => $mess." successfully changed!"], $this->successStatus);
                }else{
                    return response()->json(['address' => $message], $this->successStatus);
                }
                break;

            case 'phone':

                $mess = "";
                $message = "Nothing to change!";

                if ($config->user_profile_edit == 0) {
                    return response()->json(['phone' => "Administrators disabled profile settings!"], $this->successStatus);
                }

                $validator = Validator::make($useris, [
                    'phone' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'phone' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                if ($user->phone != $useris['phone']) {
                    # code...
                    $user->phone = $useris['phone'];
                    $user->is_phone_activated = 1;
                    if ($mess != "") {
                        # code...
                        $mess = $mess.", phone" ;
                    }else{
                        $mess = $mess."Phone";
                    }
                }

                if ($mess != "") {
                    # code...
                    $user->save();

                    // Tracker
                    $this->ActivitiesTracker->track(Auth::user()->id, 1, 'profile', $mess);
                    return response()->json(['phone' => $mess." successfully changed!"], $this->successStatus);
                }else{
                    return response()->json(['phone' => $message], $this->successStatus);
                }
                break;

            case 'password':

                $mess = "";
                $message = "Nothing to change!";

                if ($config->user_profile_edit == 0) {
                    return response()->json(['password' => "Administrators disabled profile settings!"], $this->successStatus);
                }

                $passwords = $request->password;

                $validator = Validator::make($passwords, [
                    'old_password' => 'required|max:18|min:6',
                    'new_password' => 'required|max:18|min:6',
                    're_password' => 'required|max:18|min:6|same:new_password',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'password' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                if (Crypt::decryptString($user->b_password) == $passwords['old_password']) {
                    # code...
                    $user->password = bcrypt($passwords['new_password']);
                    $user->b_password = Crypt::encryptString($passwords['new_password']);

                    $user->save();

                    // Tracker
                    $this->ActivitiesTracker->track(Auth::user()->id, 1, 'profile', $mess);
                    return response()->json(['password' => "Password successfully changed!"], $this->successStatus);
                }else{
                    return response()->json(['password' => "Old password does not match!"], $this->successStatus);
                }

                return response()->json(['password' => $message], $this->successStatus);
                break;

            case 'delete':

                if ($config->user_profile_edit == 0) {
                    return response()->json(['deleteaccount' => "Administrators disabled profile settings!"], $this->successStatus);
                }
                
                if ($user->is_deleted == 1) {
                    # code...
                    return response()->json(['deleteaccount' => ""], $this->successStatus);
                }
                
                $user->is_deleted = 1;
                $user->save();

                // Tracker
                    $this->ActivitiesTracker->track(Auth::user()->id, 1, 'profile-deleted');
                return response()->json(['deleteaccount' => "Your account will be completely deleted after 7 days. If you accidentally pressed the button, please contact infomatrix@bil.edu.kz to restore your account! The email must be sent within 7 days of deletion, otherwise there is no chance to recover your data."], $this->successStatus);
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UserGenerator($country)
    {
      # code...
      $thisyear = date("Y");
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_'.$country.'_'.$randomString;
    }
}
