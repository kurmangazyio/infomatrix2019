<?php

namespace App\Http\Controllers\API\Admin\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Admin;
use App\Countries;

use Validator;


use Event;
use App\Events\SendNewPasswordByEmail;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index()
    {
        //return data
        return response()->json(
            [
                'admins' => Admin::all(),
                
                
            ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //viewer data
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return data
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //view data
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return
        switch ($request->action) {
            case 'bio':
                $validator = Validator::make($request->admin, [
                    'name' => 'required|max:50|min:2',
                    'surname' => 'required|max:50|min:2',
                    'bio' => 'required|max:255|min:10',
                    'phone' => 'required|min:9',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }
                # code...
                $fields = "";

                $edited = $request->admin;
                $newimage = $request->image;

                $admin = Admin::where('admin_id', $id)->first();

                if ($admin->name != $edited['name']) {
                    # code...
                    $admin->name = $edited['name'];
                    $fields .= "name, ";
                }

                if ($admin->surname != $edited['surname']) {
                    # code...
                    $admin->surname = $edited['surname'];
                    $fields .= "surname, ";
                }

                if ($admin->bio != $edited['bio']) {
                    # code...
                    $admin->bio = $edited['bio'];
                    $fields .= "bio, ";
                }

                if ($admin->phone != $edited['phone']) {
                    # code...
                    $admin->phone = $edited['phone'];
                    $fields .= "phone, ";
                }

                if ($newimage['file'] != "") {
                    # code...
                    $exploded = explode(',', $newimage['file']);
                    $decoded = base64_decode($exploded[1]);

                    $file = $admin->admin_id.'.'.$newimage['ext'];
                    $path = public_path().'/general/users/'.$file;
                    file_put_contents($path, $decoded);

                    $admin->image = '/general/users/'.$file;
                    $fields .= "image, ";
                }
                if ($fields == "") {
                    # code...
                    return response()->json([ 
                        "success" => false,
                        "error" => true,
                        'message' => "Nothing is changed!"
                    ], $this->successStatus);
                }else{
                    $admin->save();
                    $fields = substr($fields, 0, -2);

                    return response()->json([ 
                        "success" => true,
                        "error" => false,
                        "message" => "Your profile details( ".$fields." ) successfully changed!",
                        'admin' => $admin
                    ], $this->successStatus);
                }
                break;
            case 'pass':
                # code...
                $password = $request->password;

                $validator = Validator::make( $password, [
                    'old-password' => 'required|max:18|min:6',
                    'new-password' => 'required|max:18|min:6',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $admin = Admin::where('admin_id', $id)->first();

                if ($password['old-password'] == Crypt::decryptString($admin->b_password)) {
                    # code...
                    if ($password['new-password'] != Crypt::decryptString($admin->b_password)) {
                        # code...
                        $admin->password =  bcrypt($password['new-password']);
                        $admin->b_password = Crypt::encryptString($password['new-password']);

                        $admin->save();

                        /*if ($password['send'] == 1) {
                            # code...
                            Event::fire(new SendNewPasswordByEmail($admin->email, $admin->name.' '.$admin->surname, $password['new-password'], "Your password changed at ".date("Y-m-d h:i:sa")));
                        }*/

                        return response()->json([
                            "success" => true,
                            "error" => false,
                            'message' => "New password successfully saved!"
                        ], $this->successStatus);   
                    }else{
                        return response()->json([
                            "success" => false,
                            "error" => true,
                            'message' => "Old password matchs with new password!"
                        ], $this->successStatus);
                    }
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => "Old password not match with entered value!"
                    ], $this->successStatus);   
                }
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //view delete
    }
}
