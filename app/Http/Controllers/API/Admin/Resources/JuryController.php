<?php

namespace App\Http\Controllers\API\Admin\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Jury;
use App\Countries;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class JuryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
        $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $newsupervisor = $request->jury;

        $validator = Validator::make($newsupervisor, [
            'name' => 'required|max:40|min:2',
            'surname' => 'required|max:40|min:2',
            'email' => 'required|email|unique:juries|max:255',
            'password' => 'required|max:18|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        $jury = Jury::create([
            'jury_id' => $this->UserGenerator(),

            'name' => $newsupervisor['name'],
            'surname'  => $newsupervisor['surname'],
            'bio' => "",
            'image' => "/general/images/user.png",

            'category' => Auth::user()->category,

            'email' => $newsupervisor['email'],
            'password' => bcrypt($newsupervisor['password']),
            'b_password' => Crypt::encryptString($newsupervisor['password']),

            'phone' => $newsupervisor['phone'],
            'phone_code' => '',
            
            'email_notification_state' => 1,

            'is_email_activated' => 0,
            'is_phone_activated' => 0,
            'is_assistant' => 1
        ]);

        // Tracker
        $this->ActivitiesTracker->track(Auth::user()->id, 2, 'assistant', '', '', 1);
        return response()->json([
            "success" => true,
            "error" => false,
            "message" => "You successfully registered an assistant!",
            "assistents" => Jury::where('is_assistant', 1)->where('category', Auth::user()->category)->get()
            ], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        switch ($request->action) {
            case 'bio':
                $validator = Validator::make($request->jury, [
                    'name' => 'required|max:50|min:2',
                    'surname' => 'required|max:50|min:2',
                    'bio' => 'required|max:255|min:10',
                    'phone' => 'required|min:9',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }
                # code...
                $fields = "";
                $old = "";

                $edited = $request->jury;
                $newimage = $request->image;

                $admin = Jury::where('jury_id', $id)->first();

                if ($admin->name != $edited['name']) {
                    # code...
                    $old .= $admin->name.', ';
                    $admin->name = $edited['name'];
                    $fields .= "name, ";
                }

                if ($admin->surname != $edited['surname']) {
                    # code...
                    $old .= $admin->surname.', ';
                    $admin->surname = $edited['surname'];
                    $fields .= "surname, ";
                }

                if ($admin->bio != $edited['bio']) {
                    # code...
                    $old .= $admin->bio.', ';
                    $admin->bio = $edited['bio'];
                    $fields .= "bio, ";
                }

                if ($admin->phone != $edited['phone']) {
                    # code...
                    $old .= $admin->phone.', ';
                    $admin->phone = $edited['phone'];
                    $fields .= "phone, ";
                }

                if ($newimage['file'] != "") {
                    # code...
                    $exploded = explode(',', $newimage['file']);
                    $decoded = base64_decode($exploded[1]);

                    $file = $admin->jury_id.'.'.$newimage['ext'];
                    $path = public_path().'/general/jury/'.$file;
                    file_put_contents($path, $decoded);

                    $admin->image = '/general/jury/'.$file;
                    $fields .= "image, ";
                }
                if ($fields == "") {
                    # code...
                    return response()->json([ 
                        "success" => false,
                        "error" => true,
                        'message' => "Nothing is changed!"
                    ], $this->successStatus);
                }else{
                    $admin->save();
                    $fields = substr($fields, 0, -2);
                    $old = substr($old, 0, -2); 
                    // Tracker
                    $this->ActivitiesTracker->track(Auth::user()->id, 2, 'profile', $fields, $old);

                    return response()->json([ 
                        "success" => true,
                        "error" => false,
                        "message" => "Your profile details( ".$fields." ) successfully changed!",
                        'jury' => $admin
                    ], $this->successStatus);
                }
                break;
            case 'pass':
                # code...
                $password = $request->password;

                $validator = Validator::make( $password, [
                    'old-password' => 'required|max:18|min:6',
                    'new-password' => 'required|max:18|min:6',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => $validator->errors()->first()
                    ], $this->successStatus);            
                }

                $admin = Jury::where('jury_id', $id)->first();

                if ($password['old-password'] == Crypt::decryptString($admin->b_password)) {
                    # code...
                    if ($password['new-password'] != Crypt::decryptString($admin->b_password)) {
                        # code...
                        $admin->password =  bcrypt($password['new-password']);
                        $admin->b_password = Crypt::encryptString($password['new-password']);

                        $admin->save();

                        /*if ($password['send'] == 1) {
                            # code...
                            Event::fire(new SendNewPasswordByEmail($admin->email, $admin->name.' '.$admin->surname, $password['new-password'], "Your password changed at ".date("Y-m-d h:i:sa")));
                        }*/
                        // Tracker
                        $this->ActivitiesTracker->track(Auth::user()->id, 2, 'profile', ' Password');

                        return response()->json([
                            "success" => true,
                            "error" => false,
                            'message' => "New password successfully saved!"
                        ], $this->successStatus);   
                    }else{
                        return response()->json([
                            "success" => false,
                            "error" => true,
                            'message' => "Old password matchs with new password!"
                        ], $this->successStatus);
                    }
                }else{
                    return response()->json([
                        "success" => false,
                        "error" => true,
                        'message' => "Old password not match with entered value!"
                    ], $this->successStatus);   
                }
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $jury = Jury::where('jury_id', $id)->first();

        $this->ActivitiesTracker->track(Auth::user()->id, 2, 'assistant-deleted', ' Email : '.$jury->email);
        $jury->delete();

        return response()->json([
            "success" => true,
            "error" => false,
            "message" => "You successfully Assistent deleted!",
            "assistents" => Jury::where('is_assistant', 1)->where('category', Auth::user()->category)->get()
            ], $this->successStatus);
    }

    public function UserGenerator()
    {
      # code...
      $thisyear = date("Y");
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_'.$randomString;
    }
}
