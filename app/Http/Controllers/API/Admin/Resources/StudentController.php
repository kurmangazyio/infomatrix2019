<?php

namespace App\Http\Controllers\API\Admin\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;
use App\Teams;

use App\Countries;
use App\Regions;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = Auth::user();

        $exstudents = \DB::table('students')
                    ->where('s_en_name', 'like', '%'.$request->input('enname').'%')
                    ->where('s_en_surname', 'like', '%'.$request->input('ensurname').'%')
                    ->where('s_passport', 'like', '%'.$request->input('passport').'%')
                    ->where('s_supervisor', $user->id)
                    ->count();

        if ($exstudents > 0) {
            return response()->json(['error' => 'You are trying to add an existing student to the database! You can add from list if you already registerd your student!'], $this->successStatus);            
        }

        if ($request->input('country') != 110) {
            # code...
            $validator = Validator::make($request->all(), [
                'enname' => 'required|max:40|min:2',
                'ensurname' => 'required|max:40|min:2',

                'birthdate' => 'required',
                'gender' => 'required',
                'passport' => 'required|min:6',
                'phone' => 'required|min:7',

                'country' => 'required',
                'region' => 'required',
                'zip' => 'required',
                'city' => 'required',
                'school' => 'required',
                'grade' => 'required',

            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], $this->successStatus);            
            }
        }else{
            $validator = Validator::make($request->all(), [
                'enname' => 'required|max:40|min:2',
                'ensurname' => 'required|max:40|min:2',
                
                'runame' => 'required|max:40|min:2',
                'rusurname' => 'required|max:40|min:2',

                'birthdate' => 'required',
                'gender' => 'required',
                'passport' => 'required|min:9',
                'phone' => 'required|min:7',

                'country' => 'required',
                'region' => 'required',
                'zip' => 'required',
                'city' => 'required',
                'school' => 'required',
                'grade' => 'required',

            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], $this->successStatus);            
            }
        }
        

        $newstudent = new Students;
        $code = $this->UserGenerator($request->input('country'));

        $newstudent->s_student_id = $code;
        $newstudent->s_supervisor = $user->id;

        $newstudent->s_en_name = $request->input('enname');
        $newstudent->s_en_surname = $request->input('ensurname');
        if ($request->input('enlastname') == "") {
            # code...
            $newstudent->s_en_lastname = "";
        }else{
            $newstudent->s_en_lastname = $request->input('enlastname');
        }
        

        if ($request->input('country') == 110) {
            # code...
            $newstudent->s_ru_name = $request->input('runame');
            $newstudent->s_ru_surname = $request->input('rusurname');
            if ($request->input('rulastname') == "") {
            # code...
                $newstudent->s_ru_lastname = "";
            }else{
                $newstudent->s_ru_lastname = $request->input('rulastname');
            }
        }else{
            $newstudent->s_ru_name = "";
            $newstudent->s_ru_surname = "";
            $newstudent->s_ru_lastname = "";
        }

        $newstudent->s_birthdate = $request->input('birthdate');
        $newstudent->s_gender = $request->input('gender');
        $newstudent->s_passport = $request->input('passport');
        $newstudent->s_phone = $request->input('phone');

        $newstudent->s_country = $request->input('country');

        if ($request->input('country') == 110) {
            # code...
            $region = \DB::table('regions')->where('en', $request->input('region'))->first();
            $newstudent->s_region = $region->id;
        }else{
            $newstudent->s_region = $request->input('region');
        }
        
        $newstudent->s_zip = $request->input('zip');
        $newstudent->s_city = $request->input('city');
        $newstudent->s_school = $request->input('school');
        $newstudent->s_grade = $request->input('grade');

        $passfile = $request->input('passfile');

        $exploded = explode(',', $passfile['file']);
        $decoded = base64_decode($exploded[1]);

        $file = $code.'.'.$passfile['ext'];
        $path = public_path().'/general/files/passports/'.$file;
        file_put_contents($path, $decoded);

        $newstudent->s_passport_file = '/general/files/passports/'.$file;

        $newstudent->save();
        // Tracker
        $this->ActivitiesTracker->track($user->id, 1, 'student-registed', $request->input('enname').' '.$request->input('ensurname'));

        return response()->json(
            [
                'fullname' => $request->input('enname').' '.$request->input('ensurname'),
                'code' => $code,
                'success' => "Successfully registered!"
            ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
      $student = Students::where('s_student_id', $id)->first();
      $notes = Notifications::where('n_to', 1)->get();
      $statuses = NotificationsStatus::all();

      $counter = sizeof($notes);
 
      foreach ($notes as $note) {
          # code...
          if (sizeof($statuses) > 0) {
              # code...
              foreach ($statuses as $status) {
                  # code...
                  if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                      # code...
                      $counter--;
                  }
              }
          }
      }

      if (sizeof($student)> 0) {
        # code...
        return response()->json(
            [
                'user' => Auth::user(),
                'student' => $student,
                'countries' => Countries::all(),
                'regions' => Regions::all(),
                'status' => $counter
            ], $this->successStatus); 
      }else{
        return response()->json(
            [
                'notfound' => true
            ], $this->successStatus);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $labels = "";

        $student = Students::where('s_student_id', $id)->first();
        $edited = $request->input('student');

        if ($student->s_en_name != $edited['s_en_name']) {
          # english name...
          $student->s_en_name = $edited['s_en_name'];
          $labels .= "EN-name, ";
        }
         
        if ($student->s_en_surname != $edited['s_en_surname']) {
          # english surname...
          $student->s_en_surname = $edited['s_en_surname'];
          $labels .= "EN-surname, ";
        }  
        if ($student->s_en_lastname != $edited['s_en_lastname'] || !is_null($edited['s_ru_lastname'])) {
          # lastname...
          $student->s_en_lastname = $edited['s_en_lastname'];
          $labels .= "EN-lastname, ";
        }  

        if ($student->s_ru_name != $edited['s_ru_name']) {
          # ru name...
          $student->s_ru_name = $edited['s_ru_name'];
          $labels .= "RU-name, ";
        }  

        if ($student->s_ru_surname != $edited['s_ru_surname']) {
          # ru surname...
          $student->s_ru_surname = $edited['s_ru_surname'];
          $labels .= "RU-surname, ";
        }  

        if ($student->s_ru_lastname != $edited['s_ru_lastname'] || !is_null($edited['s_ru_lastname'])) {
          # ru last name...
          $student->s_ru_lastname = $edited['s_ru_lastname'];
          $labels .= "RU-lastname, ";
        } 


        if ($student->s_birthdate != $edited['s_birthdate']) {
          # ru name...
          $student->s_birthdate = $edited['s_birthdate'];
          $labels .= "birth date, ";
        }  
        if ($student->s_gender != $edited['s_gender']) {
          # ru name...
          $student->s_gender = $edited['s_gender'];
          $labels .= "gender, ";
        }  
        if ($student->s_passport != $edited['s_passport']) {
          # ru name...
          $student->s_passport = $edited['s_passport'];
          $labels .= "passport, ";
        }  
        if ($student->s_phone != $edited['s_phone']) {
          # ru name...
          $student->s_phone = $edited['s_phone'];
          $labels .= "phone, ";
        }  

        
        if ($student->s_country != $edited['s_country']) {
          # ru name...
          $student->s_country = $edited['s_country'];
          $labels .= "country, ";
        } 
        if ($student->s_region != $edited['s_region']) {
          # ru name...
          $student->s_region = $edited['s_region'];
          $labels .= "region, ";
        } 
        if ($student->s_zip != $edited['s_zip']) {
          # ru name...
          $student->s_zip = $edited['s_zip'];
          $labels .= "zip, ";
        } 
        if ($student->s_city != $edited['s_city']) {
          # ru name...
          $student->s_city = $edited['s_city'];
          $labels .= "city, ";
        } 
        if ($student->s_school != $edited['s_school']) {
          # ru name...
          $student->s_school = $edited['s_school'];
          $labels .= "school, ";
        } 
        if ($student->s_grade != $edited['s_grade']) {
          # ru name...
          $student->s_grade = $edited['s_grade'];
          $labels .= "grade, ";
        } 

        if ($labels == "") {
                    # code...
            return response()->json([ 
                "error" => "Nothing is changed!"
            ], $this->successStatus);
        }else{
            $student->save();
            $labels = substr($labels, 0, -2);

            // Tracker
            $this->ActivitiesTracker->track(Auth::user()->id, 1, 'student-changed');

            return response()->json([ 
                "success" => "Your profile details( ".$labels." ) successfully changed!"
            ], $this->successStatus);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
      $line1 = Teams::where('t_f_student_code', $id)->get();
      $line2 = Teams::where('t_s_student_code', $id)->get();
      $line3 = Teams::where('t_t_student_code', $id)->get();

      if (sizeof($line1) > 0 || sizeof($line2) > 0 || sizeof($line3) > 0) {
        # code...
        return response()->json([ 
                "error" => "The student is registered in one of your projects. Please remove from project students, then try again!"
            ], $this->successStatus);
      }else{
        $student = Students::where('s_student_id', $id)->first();
        $student->delete();

        // Tracker
        $this->ActivitiesTracker->track(Auth::user()->id, 1, 'student-deleted');
        return response()->json([ 
                "success" => "Student successfully deleted!"
            ], $this->successStatus);
      }
      
    }

    public function UserGenerator($country)
    {
      # code...
      $thisyear = date("Y");
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_ss_'.$country.'_'.$randomString;
    }
}
