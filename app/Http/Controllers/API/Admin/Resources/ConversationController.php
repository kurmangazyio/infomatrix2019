<?php

namespace App\Http\Controllers\API\Admin\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class ConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public $successStatus = 200;

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = Auth::user();
        $con = $request->conversations;

        $validator = Validator::make($con, [
            'question' => 'required|max:255|min:5',
            'category' => 'required|min:1',
            'content' => 'required|min:5',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'newconanwser' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        
        $newcon = new Conversations;
        
        if (isset($user['user_id'])) {
            # code...
            $newcon->conv_creator_level = 1;
        }elseif (isset($user['jury_id'])) {
            # code...
            $newcon->conv_creator_level = 2;
        } else {
            # code...
            $newcon->conv_creator_level = 3;
        }
        
        
        $newcon->conv_creator = $user->id;
        $newcon->conv_code = $this->CodeGenerator();

        $newcon->conv_title = $con['question'];
        $newcon->conv_category = $con['category'];
        $newcon->conv_content = $con['content'];
        $newcon->conv_state = 0;

        $newcon->save();

        if (isset($user['user_id'])) {
            # code...
            // Tracker
            $this->ActivitiesTracker->track(Auth::user()->id, 1, 'conversation-created',$con['question']);

            return response()->json([
                'newconanwser' => "Your request is successfully sent!",
                'conversations' => Conversations::select('conversations.*', \DB::raw('(select count(conv_answer_conv_id) from conversations_answers where conv_answer_conv_id = conversations.id and conv_answer_user_level = 3) as answer'))->where('conv_creator', $user->id)->where('conv_creator_level', 1)->get(),
            ], $this->successStatus); 

        }elseif (isset($user['jury_id'])) {
            # code...
            // Tracker
            $this->ActivitiesTracker->track(Auth::user()->id, 2, 'conversation-created', $con['question']);
            return response()->json([
                'newconanwser' => "Your request is successfully sent!",
                'conversations' => Conversations::where('conv_creator', $user->id)->where('conv_creator_level', 2)->get()
            ], $this->successStatus); 

        } else {
            # code...
            // Tracker
            $this->ActivitiesTracker->track(Auth::user()->id, 3, 'conversation-created', $con['question']);
            return response()->json([
                'newconanwser' => "Your request is successfully sent!",
                'conversations' => Conversations::where('conv_creator', $user->id)->where('conv_creator_level', 3)->get()
            ], $this->successStatus); 
        }

        

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $conv = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title as conv_category')
                ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
                ->where('conv_code', $id)->first();

        $answers = ConversationsAnswers::where('conv_answer_conv_id', $conv->id)->get();

        $notes = Notifications::where('n_to', 1)->get();
        $statuses = NotificationsStatus::all();

        $counter = sizeof($notes);
   
        foreach ($notes as $note) {
            # code...
            if (sizeof($statuses) > 0) {
                # code...
                foreach ($statuses as $status) {
                    # code...
                    if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                        # code...
                        $counter--;
                    }
                }
            }
        }

        return response()->json([
                'user' => Auth::user(),
                'conversations' => $conv,
                'answers' =>$answers,
                'status' => $counter
        ], $this->successStatus); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $con = Conversations::where('id', $id)->first();
        ConversationsAnswers::where('conv_answer_conv_id', $con->id)->delete();
        $con->delete();
        return response()->json(['deleteconv' => "Your request is successfully deleted!"], $this->successStatus); 
    }
    public function CodeGenerator()
    {
      # code...
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 10; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
}
