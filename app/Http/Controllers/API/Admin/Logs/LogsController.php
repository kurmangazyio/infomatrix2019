<?php

namespace App\Http\Controllers\API\Admin\Logs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Admin;
use App\Volunteer;

use App\User;
use App\Guests;
use App\Students;
use App\Jury;

use App\Postregister;
use App\Logs;
use Validator;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $logs = Logs::select('logs.*', \DB::raw('(select concat(surname, " ", name) from moderators where id = logs.action_tracker) as moding'))->join('moderators', 'moderators.id', '=', 'logs.action_tracker')->get();

        $admins = Admin::all();
        $volunteers = Volunteer::all();
        $supervisors = User::all();
        $students = Students::all();
        $guests = Guests::all();
        $juries = Jury::all();

        $export = array();

        $hours = 6;

        foreach ($logs as $log) {
            # code...
            foreach ($admins as $admin) {
                # code...
                if ($log->action_user == $admin->admin_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $admin->surname.' '.$admin->name,
                        'role' => $this->getrole(1),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }

            foreach ($volunteers as $volunteer) {
                # code...
                if ($log->action_user == $volunteer->volunteer_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $volunteer->surname.' '.$volunteer->name,
                        'role' => $this->getrole(2),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }

            foreach ($supervisors as $supervisor) {
                # code...
                if ($log->action_user == $supervisor->user_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $supervisor->surname.' '.$supervisor->name,
                        'role' => $this->getrole(3),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }

            foreach ($students as $student) {
                # code...
                if ($log->action_user == $student->s_student_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $student->s_en_name.' '.$student->s_en_surname,
                        'role' => $this->getrole(4),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }


            foreach ($guests as $guest) {
                # code...
                if ($log->action_user == $guest->guest_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $guest->surname.' '.$guest->name,
                        'role' => $this->getrole(5),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }

            foreach ($juries as $jury) {
                # code...
                if ($log->action_user == $jury->jury_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $jury->surname.' '.$jury->name,
                        'role' => $this->getrole(6),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }
        }
        

        return response()->json([
                'admin' =>  Auth::user(),
                'logs' => $export,
                'users' => $this->getusers()
        ], 200);
    }

    public function getusers()
    {
        # code...
        $posts = Postregister::all();
        $users = array();

        $supervisors = User::select('users.name as name', 'users.*', \DB::raw('(select name from countries where id = users.country) as countryname'))->get();

        foreach ($supervisors as $supervisor) {
            # code...
            foreach ($posts as $value) {
                # code...
                if ($value->code == $supervisor->user_id) {
                    # code...
                    array_push($users, [
                        "fullname" => $supervisor->surname.' '.$supervisor->name,
                        "countryname" => $supervisor->countryname,
                        "code" => $supervisor->user_id ,
                        "role" => $this->getrole($value->role),
                        "participation" => $value->is_food == 1 ? "Pass" : 'Not Pass',
                        "transport" => $value->is_transport == 1 ? 'Dormitory paid' : 'Dormitory not paid',
                        'gift' => $value->is_gift == 1 ? 'Given' : 'Not Given',
                        'gender' => 'unspecified',
                        'regtime' => $value->created_at
                    ]);
                }
            }
        }

        $students = Students::select('students.*', \DB::raw('(select name from countries where id = students.s_country) as countryname'))->get();
        foreach ($students as $student) {
            # code...
            foreach ($posts as $value) {
                # code...
                if ($student->s_student_id == $value->code) {
                    # code...
                    array_push($users, [
                        "fullname" => $student->s_en_name.' '.$student->s_en_surname,
                        "countryname" => $student->countryname,
                        "code" => $student->s_student_id ,
                        "role" => $this->getrole($value->role),
                        "participation" => $value->is_food == 1 ? "Pass" : 'Not Pass',
                        "transport" => $value->is_transport == 1 ? 'Dormitory paid' : 'Dormitory not paid',
                        'gift' => $value->is_gift == 1 ? 'Given' : 'Not Given',
                        'gender' => $student->s_gender == 1 ? 'Male' : 'Female',
                        'regtime' => $value->created_at
                    ]);
                }
            }
        }

        $guests = Guests::select('guests.name as gname', 'guests.*', \DB::raw('(select name from countries where id = guests.country) as countryname'))->get();
        foreach ($guests as $guest) {
            # code...
            foreach ($posts as $value) {
                # code...
                if ($guest->guest_id == $value->code) {
                    # code...
                    array_push($users, [
                        "fullname" => $guest->surname.' '.$guest->gname,
                        "countryname" => $guest->countryname,
                        "code" => $guest->guest_id,
                        "role" => $this->getrole($value->role),
                        "participation" => $value->is_food == 1 ? "Pass" : 'Not Pass',
                        "transport" => $value->is_transport == 1 ? 'Dormitory paid' : 'Dormitory not paid',
                        'gift' => $value->is_gift == 1 ? 'Given' : 'Not Given',
                        'gender' => 'unspecified',
                        'regtime' => $value->created_at
                    ]);
                }
            }
        }

        return $users;
    }

    public function getword($code)
    {
        # code...
        $word = '';
        switch ($code) {
            case 'btst':
                # code...
                $word = 'Breakfast';
                break;
            case 'lnch':
                # code...
                $word = 'Lunch';
                break;
            case 'dnnr':
                # code...
                $word = 'Dinner';
                break;
            case 'totp':
                # code...
                $word = 'To Transport';
                break;
            case 'fmtp':
                # code...
                $word = 'From Transport';
                break;
        }

        return $word;
    }

    public function getrole($code)
    {
        # code...
        $word = '';
        switch ($code) {
            case 1:
                # code...
                $word = 'Admin';
                break;
            case 2:
                # code...
                $word = 'Volunteer';
                break;
            case 3:
                # code...
                $word = 'Supervisor';
                break;
            case 4:
                # code...
                $word = 'Student';
                break;
            case 5:
                # code...
                $word = 'Guest';
                break;
            case 6:
                # code...
                $word = 'Jury';
                break;
        }

        return $word;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
