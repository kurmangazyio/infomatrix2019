<?php

namespace App\Http\Controllers\API\Admin\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Jury;
use App\Admin;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use App\ActivityTrackerModel;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class AdminProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index($category)
    {
        $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();
        //Supervisors
        $students = Students::all();

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }


        if ($category == 'all') {
            # code...
            $teams = Teams::all();
            foreach ($teams as $team) {
                # code...
                $team['supervisor'] = User::select(
                    'users.user_id', 
                    'users.name', 
                    'users.surname', 
                    'users.image',
                    'users.email',
                    'users.phone',
                    'users.phone_code',
                    'users.is_email_activated',
                    'users.is_phone_activated',
                    'countries.name as country'
                )
                    ->join('countries', 'users.country', '=', 'countries.id')
                    ->where('users.id', $team->t_supervisor)
                    ->first();
                $team['project'] = Projects::select('p_title', 'p_description','p_category')->where('p_project_id', $team->t_project_code)->first();
                $team['category'] = Categories::select('category','short')->where('id', $team['project']['p_category'])->first();
                switch ($team->t_project_mark) {
                    case 0:
                        # code...
                        $team['state'] = 'Not checked';
                        break;
                    case 1:
                        # code...
                        $team['state'] = 'Rejected';
                        break;
                    case 2:
                        # code...
                        $team['state'] = '50/50';
                        break;
                    case 3:
                        # code...
                        $team['state'] = 'Accepted';
                        break;
                }
            }

            return response()->json(
            [
                'admin' => Auth::user(),
                'navconversations' => $conversations,
                'notifications' => $notfications,
                'projects' => $teams,
                'categories' => Categories::all()
            ], $this->successStatus);
        }else{

            $teams = Teams::all();

            foreach ($teams as $team) {
                # code...
                $team['supervisor'] = User::select('user_id', 'name', 'surname', 'image')->where('id', $team->t_supervisor)->first();
                $team['project'] = Projects::select('p_title', 'p_description','p_category')->where('p_project_id', $team->t_project_code)->first();
                $team['category'] = Categories::select('category','short')->where('id', $team['project']['p_category'])->first();
                switch ($team->t_project_mark) {
                    case 0:
                        # code...
                        $team['state'] = 'Not checked';
                        break;
                    case 1:
                        # code...
                        $team['state'] = 'Rejected';
                        break;
                    case 2:
                        # code...
                        $team['state'] = '50/50';
                        break;
                    case 3:
                        # code...
                        $team['state'] = 'Accepted';
                        break;
                }
                $team['short'] = $team['category']['short'];
            }

            $projects = array();
            for ($i=0; $i < sizeof($teams); $i++) { 
                if ($category == $teams[$i]['short']) {
                    # code...
                    array_push($projects, $teams[$i]);
                }
            }

            return response()->json(
            [
                'admin' => Auth::user(),
                'navconversations' => $conversations,
                'notifications' => $notfications,
                'projects' => $projects,
                'categories' => Categories::all()
            ], $this->successStatus);
        }
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
