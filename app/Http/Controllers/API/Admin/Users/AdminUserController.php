<?php

namespace App\Http\Controllers\API\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Jury;
use App\Admin;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use App\ActivityTrackerModel;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index($role)
    {
        $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();
        //Supervisors
        $students = Students::all();

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }


        $acts = ActivityTrackerModel::select()->where('user_level', 3)->where('user', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        foreach ($acts as $act) {
            # code...
            $act['actioner'] = $adm->name.' '.$adm->surname;
            $act['rank'] = 'Role: Admin';
            $act['newdate'] = $this->time_elapsed_string($act->created_at);
        }

        switch ($role) {
            case 'supervisors':
                # code...
                $users = User::select('*', 
                                \DB::raw('(select count(id) from projects where p_supervisor = users.id) as projects'), 
                                \DB::raw('(select count(id) from students where s_supervisor = users.id) as students'),
                                \DB::raw('(select count(id) from teams where t_supervisor = users.id) as teams')
                        )->get();

                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'users' => $users
                    ], $this->successStatus);
                break;
            case 'students':
                # code...
                $users = Students::select('*', 
                                \DB::raw('(select name from users where id = students.s_supervisor) as sname'), 
                                \DB::raw('(select surname from users where id = students.s_supervisor) as ssurname'),
                                \DB::raw('(select name from countries where id = students.s_country) as country')
                        )->get();
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'users' => $users
                    ], $this->successStatus);
                break;
            case 'admins':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'users' => Admin::all()
                    ], $this->successStatus);
                break;
            case 'juries':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'users' => Jury::select('*', \DB::raw('(select category from categories where id = juries.category) as categoryname'))->orderBy('is_assistant', 'asc')->get()
                    ], $this->successStatus);
                break;
            case 'moderators':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'users' => []
                    ], $this->successStatus);
                break;
        }
        
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function newuser($role)
    {
        # code...
        $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();
        //Supervisors
        $students = Students::all();

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }


        $acts = ActivityTrackerModel::select()->where('user_level', 3)->where('user', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        foreach ($acts as $act) {
            # code...
            $act['actioner'] = $adm->name.' '.$adm->surname;
            $act['rank'] = 'Role: Admin';
            $act['newdate'] = $this->time_elapsed_string($act->created_at);
        }

        switch ($role) {
            case 'supervisors':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'countries' => Countries::all()
                    ], $this->successStatus);
                break;
            case 'students':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'countries' => Countries::all()
                    ], $this->successStatus);
                break;
            case 'admins':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'countries' => Countries::all()
                    ], $this->successStatus);
                break;
            case 'juries':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'countries' => Countries::all(),
                        'categories' => Categories::all()
                    ], $this->successStatus);
                break;
            case 'moderators':
                # code...
                return response()->json(
                    [
                        'admin' => Auth::user(),
                        'navconversations' => $conversations,
                        'notifications' => $notfications,
                        'activities' => $acts,
                        'countries' => Countries::all()
                    ], $this->successStatus);
                break;
        }
    }
}
