<?php

namespace App\Http\Controllers\API\Jury\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Jury;
use App\Teams;
use App\Categories;
use App\Marks;

use App\Notifications;
use App\NotificationsFiles;

use App\ActivityTracker;

use Validator;
use Carbon\Carbon;

class JuryProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:jury-api');
    }

    public function index()
    {
        $user = Auth::user();

        $jury = Jury::select(
            'juries.*', 
            'categories.category as title'
        )
            ->where('juries.id', $user->id)
            ->join('categories', 'juries.category', '=', 'categories.id')
            ->first();

        $activities = ActivityTracker::where('user_level', 2)->where('user', Auth::user()->id)->limit(7)->orderBy('created_at', 'desc')->get();

        foreach ($activities as $act) {
            # code...
            $act['newdate'] = $this->time_elapsed_string($act->created_at);
        }

        $notes = array();

        return response()->json([
            'jury' => $jury,
            'category_id' => $user->category, 
            "notifications" => $notes,
            'activities' => $activities
        ], $this->successStatus);
    }

    public function getCategoriess()
    {
        # code...
        return response()->json(
            [
                'robotics' => json_decode(Categories::where('id', 1)->first()->config, true),
                'cprogramming' => json_decode(Categories::where('id', 2)->first()->config, true),
                'cgraphicsart' => json_decode(Categories::where('id', 3)->first()->config, true),
                'hardcontrol' => json_decode(Categories::where('id', 4)->first()->config, true),
                'shortmovie' => json_decode(Categories::where('id', 5)->first()->config, true),
                'droneracing' => json_decode(Categories::where('id', 6)->first()->config, true),
                'mathproject' => json_decode(Categories::where('id', 7)->first()->config, true),
            ], $this->successStatus);
        
    }

    public function newConfig(Request $request)
    {
        # code...
        $newconfig = json_decode($request->input('newconfig'));

        $user = Auth::user();
        $category = Categories::where('id', $user->category)->first();

        $old = json_decode($category->config);
        $new = array_push($old, $newconfig);
        return response()->json([ 
            "success" => true,
            "error" => false,
            "message" => "Configuration successfully added!",
            'category' => gettype($old) 
        ], $this->successStatus);

        $category->config = json_decode($old,  true);

       // $category->save();

        return response()->json([ 
            "success" => true,
            "error" => false,
            "message" => "Configuration successfully added!",
            'category' => $category
        ], $this->successStatus);
    }

    public function getAssistent()
    {
        # code...
        $user = Auth::user();

        $jury = Jury::select(
            'juries.*', 
            'categories.category as title'
        )
            ->where('juries.id', $user->id)
            ->join('categories', 'juries.category', '=', 'categories.id')
            ->first();

        return response()->json(['jury' => $jury,'assistents' => Jury::where('is_assistant', 1)->where('category', $user->category)->get(), 'category_id' => $user->category], $this->successStatus);
    }

    public function getProjects(Request $request)
    {
        # code...
        $point = 0;
        $parameters = "";

        if ($request->input('parameter') == "all") {
            # code...
            $projects = Teams::select("teams.id", "teams.t_name", "teams.t_team_id", "teams.t_project_mark", "users.name", "users.surname", "categories.category", "projects.p_title", "projects.p_category","teams.id as teamid", "projects.created_at")
                ->join('projects', 'teams.t_project_code', '=', 'projects.p_project_id')
                    ->join('users', 'users.id', '=', 'teams.t_supervisor')
                        ->join('categories', 'categories.id', '=', 'projects.p_category')
                            ->where('projects.p_category', Auth::user()->category)
                                    ->get();

            $parameters = "All";
        }else{
            switch ($request->input('parameter')) {
                case 'rejected':
                    # code...
                    $point = 1;
                    $parameters = "Rejected";
                    break;
                case 'middle':
                    # code...
                    $point = 2;
                    $parameters = "50/50";
                    break;
                case 'accepted':
                    # code...
                    $point = 3;
                    $parameters = "Accepted";
                    break;
            }

            $projects = Teams::select("teams.id", "teams.t_name", "teams.t_team_id","teams.t_project_mark" , "users.name", "users.surname", "categories.category", "projects.p_title", "projects.p_category", "teams.id as teamid", "projects.created_at")
                ->join('projects', 'teams.t_project_code', '=', 'projects.p_project_id')
                    ->join('users', 'users.id', '=', 'teams.t_supervisor')
                        ->join('categories', 'categories.id', '=', 'projects.p_category')
                            ->where('projects.p_category', Auth::user()->category)
                                ->where('teams.t_project_mark','=', $point)
                                    ->get();
        }
        
        $jury = Auth::user();
        $category = Categories::where('id', Auth::user()->category)->first();
        $marks = Marks::select('juries.name', 'juries.surname', 'assistant_marks.*')
            ->join('juries', 'juries.id', '=', 'assistant_marks.assistant')
                ->get();
                
        return response()->json([
            'jury' => $jury,
                'category' => $category,
                    'parameter' => $parameters,
                        'projects' => $projects,
                            'marks' => $marks,
                                "notifications" => Notifications::where('n_to', 2)->limit(3)->orderBy('created_at', 'desc')->get(),
        ], $this->successStatus);
    }

    public function getProject(Request $request)
    {
        # code...
        
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
