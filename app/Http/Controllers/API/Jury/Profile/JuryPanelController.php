<?php

namespace App\Http\Controllers\API\Jury\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Jury;
use App\Teams;
use App\Marks;

use App\Notifications;
use App\NotificationsFiles;

use Validator;

class JuryPanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:jury-api');
    }

    public function index()
    {
        $user = Auth::user();

        $jury = Jury::select(
            'juries.*',
            'categories.category as title'
        )
            ->where('juries.id', $user->id)
            ->join('categories', 'juries.category', '=', 'categories.id')
            ->first();

        unset($jury->b_password);

        $projects = Teams::select(
            "teams.id",
            "teams.t_name",
            "teams.t_team_id",

            "users.name",
            "users.surname",
            
            'categories.category',

            "projects.p_title",
            "projects.created_at",
            "teams.t_project_mark",
            "teams.id as teamid"
        )
            ->join('projects', 'teams.t_project_code', '=', 'projects.p_project_id')
            ->join('users', 'users.id', '=', 'teams.t_supervisor')
            ->join('categories', 'categories.id', '=', 'projects.p_category')
            ->where('projects.p_category', $user->category)
            ->get();

        $marks = Marks::select('juries.name', 'juries.surname', 'assistant_marks.*')
            ->join('juries', 'juries.id', '=', 'assistant_marks.assistant')
                ->get();

        $notes = \DB::table('project_conversations')->select('*', \DB::raw('(select p_title from projects where p_category = '.Auth::user()->category.' and projects.p_project_id = teams.t_project_code) as title'))->join('teams', 'teams.id', '=', 'project_conversations.teams_id')->where('project_conversations.role', 2)->get();

        return response()->json(['jury' => $jury, 'projects' => $projects, 'marks' => $marks,"notifications" => $notes], $this->successStatus);
    }
}
