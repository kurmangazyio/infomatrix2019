<?php

namespace App\Http\Controllers\API\Jury\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Jury;
use App\Admin;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use Validator;

class ConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:jury-api');
    }

    public $successStatus = 200;

    public function index()
    {
      //
      $supervisors = User::all();
        //Supervisors
        $juries = Jury::all();
        //Admins
        $admins = Admin::all();
        //Supervisors
        $students = Students::all();

        $notfications = Notifications::select('*')->limit(3)->orderBy('created_at', 'desc')->get();
        foreach ($notfications as $value) {
            # code...
            $value['newdate'] = $this->time_elapsed_string($value->created_at);
        }

        $conversations = Conversations::select('conversations.*', 'conversations_categories.conv_cat_title')
            ->where('conversations.conv_state', 0)
            ->join('conversations_categories', 'conversations_categories.id', '=', 'conversations.conv_category')
            ->orderBy('conversations.created_at', 'desc')
            ->limit(4)
            ->get();

        foreach ($conversations as $imp) {
            # code...
            switch ($imp->conv_creator_level) {
                case 1:
                    # code...
                    foreach ($supervisors as $supervisor) {
                        # code...
                        if ($supervisor->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $supervisor->name.' '.$supervisor->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 2:
                    # code...
                    foreach ($juries as $jury) {
                        # code...
                        if ($jury->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $jury->name.' '.$jury->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }
                    break;
                case 3:
                    # code...
                    foreach ($admins as $adm) {
                        # code...
                        if ($adm->id = $imp->conv_creator) {
                            # code...
                            $imp['actioner'] = $adm->name.' '.$adm->surname;
                            $imp['image'] = $supervisor->image;
                        }
                    }break;
            }

            $imp['newdate'] = $this->time_elapsed_string($imp->created_at);
        }

        $categoryyy =  Auth::user()->category + 2;
        
        $convsopen = Conversations::orderBy('created_at', 'desc')->where('conv_state', 0)->where('conversations.conv_category', '=' , $categoryyy)->get();
        foreach ($convsopen as $conv) {
          # code...
          switch ($conv->conv_creator_level) {
            case 1:
              # code...
              $conv['creator'] = User::select('name', 'surname', 'user_id', 'image')->where('id', $conv->conv_creator)->first();
              break;
            case 2:
              # code...
              $conv['creator'] = User::select('name', 'surname', 'jury_id', 'image')->where('id', $conv->conv_creator)->first();
              break;
            case 3:
              # code...
              $conv['creator'] = User::select('name', 'surname', 'admin_id', 'image')->where('id', $conv->conv_creator)->first();
              break;
          }

          $conv['category'] = ConversationsCategories::where('id', $conv->conv_category)->first()->conv_cat_title;
          $conv['answers'] = ConversationsAnswers::where('conv_answer_conv_id', $conv->id)->count();
          $conv['newdate'] = $this->time_elapsed_string($conv->created_at);
        }

        $convsclose = Conversations::orderBy('created_at', 'desc')->where('conv_state', 1)->where('conversations.conv_category', '=' , $categoryyy)->get();
        foreach ($convsclose as $conv) {
          # code...
          switch ($conv->conv_creator_level) {
            case 1:
              # code...
              $conv['creator'] = User::select('name', 'surname', 'user_id', 'image')->where('id', $conv->conv_creator)->first();
              break;
            case 2:
              # code...
              $conv['creator'] = User::select('name', 'surname', 'jury_id', 'image')->where('id', $conv->conv_creator)->first();
              break;
            case 3:
              # code...
              $conv['creator'] = User::select('name', 'surname', 'admin_id', 'image')->where('id', $conv->conv_creator)->first();
              break;
          }

          $conv['category'] = ConversationsCategories::where('id', $conv->conv_category)->first()->conv_cat_title;
          $conv['answers'] = ConversationsAnswers::where('conv_answer_conv_id', $conv->id)->count();
          $conv['newdate'] = $this->time_elapsed_string($conv->created_at);
        }


        return response()->json(
          [
              'jury' => Auth::user(),
              'navconversations' => $conversations,
              'notifications' => $notfications,
              'allconversationsopened' => $convsopen,
              'allconversationsclosed' => $convsclose,
          ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
        $details = Conversations::where('conv_code', $id)->first();
        $creator = array();
        switch ($details->conv_creator_level) {
          case 1:
            # code...
            $creator = User::select('name', 'surname', 'user_id', 'image')->where('id', $details->conv_creator)->first();
            break;
          case 2:
            # code...
            $creator = Jury::select('name', 'surname', 'jury_id', 'image')->where('id', $details->conv_creator)->first();
            break;
          case 3:
            # code...
            $creator = Admin::select('name', 'surname', 'admin_id', 'image')->where('id', $details->conv_creator)->first();
            break;
        }

        $details['category'] = ConversationsCategories::where('id', $details->conv_category)->first()->conv_cat_title;
        $details['newdate'] = $this->time_elapsed_string($details->created_at);

        $answers = ConversationsAnswers::where('conv_answer_conv_id', $details->id)->orderBy('created_at', 'asc')->get();
        foreach ($answers as $answer) {
          # code...
          switch ($answer->conv_answer_user_level) {
            case 1:
              # code...
              $answer['creator'] = User::select('name', 'surname', 'image')->where('id', $answer->conv_answer_user_id)->first();
              break;
            case 2:
              # code...
              $answer['creator'] = Jury::select('name', 'surname',  'image')->where('id', $answer->conv_answer_user_id)->first();
              break;
            case 3:
              # code...
              $answer['creator'] = Admin::select('name', 'surname', 'image')->where('id', $answer->conv_answer_user_id)->first();
              break;
          }

          $answer['newdate'] =  $this->time_elapsed_string($answer->created_at);
        }
        return response()->json(
          [
              'conversationDetails' => $details,
              'answers' => $answers,
              'creator' => $creator
          ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
      $way = $request->input('way');

      switch ($way) {
        case 'state':
          # code...
          $conv = Conversations::where('conv_code', $id)->first();

          if (ConversationsAnswers::where('conv_answer_conv_id', $conv->id)->count() == 0) {
            # code...
            return response()->json([
                "success" => false,
                "error" => true,
                'message' => 'The conversation can not be closed until answered!'
            ], $this->successStatus); 
          }
          
          $conv->conv_state = $request->input('state');
          $conv->save();

          return response()->json([
              "success" => true,
              "error" => false,
              'message' => 'Conversation state is successfully changed!',
              'conversation' => Conversations::where('conv_code', $id)->first()
          ], $this->successStatus); 

          break;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // 

    }
    public function CodeGenerator()
    {
      # code...
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 10; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
