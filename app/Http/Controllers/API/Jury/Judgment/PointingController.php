<?php

namespace App\Http\Controllers\API\Jury\Judgment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use Validator;

use App\Students;
use App\Projects;
use App\Teams;
use App\User;

use App\Jury;
use App\Tours;
use App\Criteria;
use App\Points;

use App\Postregister;

class PointingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->datas, [
            'comment' => 'required',
            'student' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $validator->errors()->first()
            ], 200);            
        }

        $student = Students::select('*')->where('s_student_id', $request->datas['student'])->first();
        $project = Projects::select('*')->where('p_project_id', $request->datas['project'])->first();

        //Points::where('jpstudent_id', $student->id)->where('jpproject_id', $project->id)->where('jpjury_id', Auth::user()->id)->delete();

        foreach ($request->datas as $key => $value) {
            # code...
            $criteria = Criteria::where('id', $key)->first();
            if ($criteria != null) {
                # code...
                Points::create([
                    'jpcriteria_id' => $criteria->id,
                    'jpstudent_id' => $student->id,
                    'jpproject_id' => $project->id,
                    'jpjury_id' => Auth::user()->id,
                    'jppoint' => $value,
                    'jpcomment' => $request->datas['comment']
                ]);
            }
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'message' => $student['s_en_name'].' '.$student['s_en_surname'].' successfully marked!'
        ], 200);
    }


    public function updatepoints(Request $request)
    {
        //
        $validator = Validator::make($request->datas, [
            'comment' => 'required',
            'student' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $validator->errors()->first()
            ], 200);            
        }

        $student = Students::select('*')->where('s_student_id', $request->datas['student'])->first();
        $project = Projects::select('*')->where('p_project_id', $request->datas['project'])->first();

        foreach ($request->datas as $key => $value) {
            Points::where('id', $key)->update(['jppoint' => $value]);
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'message' => $student['s_en_name'].' '.$student['s_en_surname'].' successfully marked!'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team = Teams::select('*', 'projects.id as pid')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->where('teams.t_project_mark', 3)
            ->where('projects.p_category', Auth::user()->category)
            ->where('projects.p_project_id', $id)
            ->first();

        
        $students = array();

        if ($team->t_f_student_code != '') {
            # code...
            $coun = Postregister::where('code', $team->t_f_student_code)->where('role', 4)->first();
            if ($coun != null) {
                    # code...
                $student = Students::select('*', 'students.id as ssid')
                    ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                    ->join('countries', 'countries.id', '=', 'students.s_country')
                    ->where('s_student_id', $team->t_f_student_code)
                    ->first();

                array_push($students, $student);
            }
            
        }
        if ($team->t_s_student_code != '') {
            # code...
            $coun = Postregister::where('code', $team->t_s_student_code)->where('role', 4)->first();
            if ($coun != null) {
                    # code...
                $student = Students::select('*', 'students.id as ssid')
                    ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                    ->join('countries', 'countries.id', '=', 'students.s_country')
                    ->where('s_student_id', $team->t_s_student_code)
                    ->first();

                array_push($students, $student);
            }
        }
        if ($team->t_t_student_code != '') {
            # code...
            $coun = Postregister::where('code', $team->t_t_student_code)->where('role', 4)->first();
            if ($coun != null) {
                    # code...
                $student = Students::select('*', 'students.id as ssid')
                    ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                    ->join('countries', 'countries.id', '=', 'students.s_country')
                    ->where('s_student_id', $team->t_t_student_code)
                    ->first();

                array_push($students, $student);
            }
        }

        $team['students'] = $students; 

        $tours = Tours::where('jtcategory', Auth::user()->category)->get();
        foreach ($tours as $value) {
            # code...
            $value['criteria'] = Criteria::select('*', 'jcriteria.id as sid')->join('jtours', 'jtours.id', '=', 'jcriteria.jctour')->where('jtours.id', $value->id)->get();
            $value['average'] = 0;
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'tours' => $tours,
            'project' => $team,
            'jury' => Auth::user(),
            'points' => Points::select('*')->where('jpproject_id', $team->pid)->get()
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function geteditor($student, $project)
    {
        # code...
        $student = Students::select('*')->where('s_student_id', $student)->first();
        $project = Projects::select('*')->where('p_project_id', $project)->first();

        $points = Points::where('jpstudent_id', $student->id)->where('jpproject_id', $project->id)->get();
        return response()->json([
            'success' => true, 
            'error' => false, 
            'points' => $points
        ], 200);
    }

    public function getupdatepoint($student, $project)
    {
        # code...
        $student = Students::select('*')->where('s_student_id', $student)->first();
        $project = Projects::select('*')->where('p_project_id', $project)->first();

        $points = Points::where('jpstudent_id', $student->id)
            ->where('jpproject_id', $project->id)
            ->where('jpjury_id', Auth::user()->id)
            ->get();
            
        return response()->json([
            'success' => true, 
            'error' => false, 
            'points' => $points
        ], 200);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
