<?php

namespace App\Http\Controllers\API\Jury\Judgment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use Validator;

use App\Students;
use App\Projects;
use App\Teams;
use App\User;

use App\Jury;
use App\Tours;
use App\Criteria;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json([
            'success' => true, 
            'error' => false, 
            'tours' => Tours::where('jtcategory', Auth::user()->category)->get(),
            'criteria' => Criteria::select('*', 'jcriteria.id as sid')->join('jtours', 'jtours.id', '=', 'jcriteria.jctour')->where('jccategory', Auth::user()->category)->get()
        ], 
        200); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'name' => 'required',
            'isfirst' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $validator->errors()->first()
            ], 200);            
        }

        if ($request->isfirst) {
            Tours::where('jtcategory', Auth::user()->category)->update(['jtfirst' => false]);         
        }

        Tours::create([
            'jtcategory' => Auth::user()->category,
            'jttitle' => $request->name,
            'jtfirst' => $request->isfirst
        ]);

        return response()->json([
            'success' => true, 
            'error' => false, 
            'tours' => Tours::where('jtcategory', Auth::user()->category)->get()
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Tours::where('id', $id)->delete();

        return response()->json([
            'success' => true, 
            'error' => false, 
            'tours' => Tours::where('jtcategory', Auth::user()->category)->get()
        ], 200);
    }
}
