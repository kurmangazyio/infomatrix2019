<?php

namespace App\Http\Controllers\API\User\Userpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use Validator;
use App\Notifications;
use App\NotificationsStatus;

use App\UserConfig;

class UserManageController extends Controller
{
    //
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $notes = Notifications::where('n_to', 1)->get();
        $statuses = NotificationsStatus::all();

        $counter = sizeof($notes);
       
        foreach ($notes as $note) {
            # code...
            if (sizeof($statuses) > 0) {
                # code...
                foreach ($statuses as $status) {
                    # code...
                    if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                        # code...
                        $counter--;
                    }
                }
            }
        }

        $students = Students::select('students.*', 'countries.name as s_country_name')
                ->join('countries', 'students.s_country', '=', 'countries.id')
                ->where('s_supervisor', Auth::user()->id)
                ->get();

        foreach ($students as $key => $value) {
            # code...
            if (is_numeric($students[$key]['s_region'])) {
                # code...
                $students[$key]['s_region'] = Regions::select('en')->where('id', $students[$key]['s_region'])->first()->en;
            }
        }

        return response()->json([
            'user' => Auth::user(),
                'students' => $students,
                    'teams' => Teams::where('t_supervisor', Auth::user()->id)->get(),
                        'countries' => Countries::all(),
                            'regions' => Regions::all(),
                                'status' => $counter,
                                    'config' => UserConfig::get()->first()
        ], $this->successStatus);
    }

    public function get_projects($id)
    {
        # code...
        $team = Teams::where('t_team_id', $id)->first();
        
        if (sizeof($team) == 0) {
            # code...
            return response()->json([
                'notefound' => true,
            ], $this->successStatus);  
        }
        

        $project = Projects::where('p_project_id', $team['t_project_code'])->first();
        $additional = json_decode($project->p_addins);
        $country = Countries::all();
        $regions = Regions::all();

        $s1 = Students::where('s_student_id', $team['t_f_student_code'])->first();
        foreach ($country as $key) {
            if ($key['id'] == $s1->s_country) {
                $s1->s_country = $key['name'];
            }
        }
        if (is_numeric($s1->s_region)) {
            foreach ($regions as $key) {
                if ($key['id'] == $s1->s_region) {
                    $s1->s_region = $key['en'];
                }
            }
        }
        $students = array($s1);
        //student 2
        if($team->t_s_student_code != ""){
            $s2 = Students::where('s_student_id', $team['t_s_student_code'])->first();
            if (is_numeric($s2->s_region)) {
                foreach ($regions as $key) {
                    if ($key['id'] == $s2->s_region) {
                        $s2->s_region = $key['en'];
                    }
                }
            }
            foreach ($country as $key) {
                if ($key['id'] == $s2->s_country) {
                    $s2->s_country = $key['name'];
                }
            }
            array_push($students, $s2);
        }
        //student 3
        if($team->t_t_student_code != ""){
            $s3 = Students::where('s_student_id', $team['t_t_student_code'])->first();
            if (is_numeric($s3->s_region)) {
                foreach ($regions as $key) {
                    if ($key['id'] == $s3->s_region) {
                        $s3->s_region = $key['en'];
                    }
                }
            }
            foreach ($country as $key) {
                if ($key['id'] == $s3->s_country) {
                    $s3->s_country = $key['name'];
                }
            }
            array_push($students, $s3);
        }

        $notes = Notifications::where('n_to', 1)->get();
        $statuses = NotificationsStatus::all();

        $counter = sizeof($notes);
       
        foreach ($notes as $note) {
            # code...
            if (sizeof($statuses) > 0) {
                # code...
                foreach ($statuses as $status) {
                    # code...
                    if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                        # code...
                        $counter--;
                    }
                }
            }
        }

        return response()->json([
            'user' => Auth::user(),
                    'team' => $team,
                        'students' => $students,
                            'allstudents' => Students::where('s_supervisor', Auth::user()->id)->get(),
                                'project' => $project ,
                                    'additional' => $additional,
                                        'categories' => Categories::all(),
                                            'status' => $counter,
                                                'config' => UserConfig::get()->first()
        ], $this->successStatus);  
    }
}
