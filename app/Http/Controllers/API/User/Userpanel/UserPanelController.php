<?php

namespace App\Http\Controllers\API\User\Userpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;

use App\Categories;
use App\Countries;
use App\Regions;

use Validator;

use App\Projects;
use App\Teams;
use App\Students;

use App\Notifications;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsAnswers;

use App\UserConfig;

class UserPanelController extends Controller
{
    //
    public $successStatus = 200;

    public function __construct()
    {
        //$this->middleware('auth:api');
    }

    public function index()
    {
        
        $notes = Notifications::where('n_to', 1)->get();
        $statuses = NotificationsStatus::all();

        $counter = sizeof($notes);
       
        foreach ($notes as $note) {
            # code...
            if (sizeof($statuses) > 0) {
                # code...
                foreach ($statuses as $status) {
                    # code...
                    if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                        # code...
                        $counter--;
                    }
                }
            }
        }

        $answers = \DB::table('conversations')
            ->join('conversations_answers', 'conversations.id', '=', 'conversations_answers.conv_answer_conv_id')
            ->select('conversations.*')
            ->where('conversations.conv_creator_level', '=', 1)
            ->where('conversations.conv_creator', '=', Auth::user()->id)
            ->where('conversations_answers.conv_answer_user_level', '=', 3)
            ->where('conversations_answers.is_readn', '=', 0)
            ->count();
        return response()->json([
            'user' => Auth::user(), 
            'status' => $counter, 
            'answers' => $answers,
            'config' => UserConfig::get()->first()
        ], $this->successStatus);
    }

    public function statuses()
    {
        # code...

        $teams = Teams::select('projects.p_title as title','users.name as sname', 'users.surname as ssurname', 'teams.t_project_mark as mark','teams.*')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->join('users', 'users.id','=', 'teams.t_supervisor')
            ->where('teams.t_supervisor', Auth::user()->id)
            ->orderBy('teams.t_project_mark', 'DESC')
            ->get();

        foreach ($teams as $v) {
            # code...
            $arrayName = array();
            if ($v->t_f_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_f_student_code'] )->first();
                array_push($arrayName, [
                    'name'=> $stu['s_en_name'],
                    'surname'=> $stu['s_en_surname'],
                    'lastname'=> $stu['s_en_lastname'],
                    'daryn' => $stu['s_daryn']
                ]);
            }
            if ($v->t_s_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_s_student_code'] )->first();
                array_push($arrayName, [
                    'name'=> $stu['s_en_name'],
                    'surname'=> $stu['s_en_surname'],
                    'lastname'=> $stu['s_en_lastname'],
                    'daryn' => $stu['s_daryn']
                ]);
            }
            if ($v->t_t_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_t_student_code'] )->first();
                array_push($arrayName, [
                    'name'=> $stu['s_en_name'],
                    'surname'=> $stu['s_en_surname'],
                    'lastname'=> $stu['s_en_lastname'],
                    'daryn' => $stu['s_daryn']
                ]);
            }
            
            $v['students'] = $arrayName;
        }
        return response()->json(['projects' => $teams], $this->successStatus);
    }

    public function upoad()
    {
    	# code...
        $notes = Notifications::where('n_to', 1)->get();
        $statuses = NotificationsStatus::all();

        $counter = sizeof($notes);
       
        foreach ($notes as $note) {
            # code...
            if (sizeof($statuses) > 0) {
                # code...
                foreach ($statuses as $status) {
                    # code...
                    if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                        # code...
                        $counter--;
                    }
                }
            }
        }

    	return response()->json([
            'user' => Auth::user(), 
            'categories' => Categories::all(),
            'status' => $counter,
            'UserConfig' => UserConfig::get()->first()
        ], $this->successStatus);
    }

    public function upoadStudentsView()
    {
    	# code...
    	$user = Auth::user();
    	$students = Students::where('s_supervisor', $user->id)->get();

    	return response()->json(['user' => $user, 'students' => $students,'countries' => Countries::all(), 'regions' => Regions::all() ], $this->successStatus);
    }

    public function upoadStudents()
    {
    	# code...
    	$user = Auth::user();
    	$students = Students::where('s_supervisor', $user->id)->get();

    	return response()->json(['students' => $students], $this->successStatus);
    }

}
