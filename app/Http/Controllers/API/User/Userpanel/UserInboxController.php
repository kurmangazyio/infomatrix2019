<?php

namespace App\Http\Controllers\API\User\Userpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;
use App\Teams;
use App\Projects;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;


use Validator;
use App\UserConfig;

class UserInboxController extends Controller
{
    //
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
    	$user =  Auth::user();
    	if (isset($user['user_id'])) {
            # code...
            $notes = Notifications::where('n_to', 1)->get();
            $statuses = NotificationsStatus::all();

            $counter = sizeof($notes);
       
            foreach ($notes as $note) {
                # code...
                if (sizeof($statuses) > 0) {
                    # code...
                    foreach ($statuses as $status) {
                        # code...
                        if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                            # code...
                            $counter--;
                        }
                    }
                }
            }

            return response()->json([
            	'user' => $user,
            	"notifications" => Notifications::all(), 
            	'files' => NotificationsFiles::all(),
            	"conversationcategories" => ConversationsCategories::all(),
                'conversations' => Conversations::select('conversations.*', \DB::raw('(select count(conv_answer_conv_id) from conversations_answers where conv_answer_conv_id = conversations.id and conv_answer_user_level = 3) as answer'))->where('conv_creator', $user->id)->where('conv_creator_level', 1)->get(),
                'status' => $counter,
                'answers' => 0,
                'config' => UserConfig::get()->first()
            ], $this->successStatus); 

        }elseif (isset($user['jury_id'])) {
            # code...
            return response()->json([
                'conversations' => Conversations::where('conv_creator', $user->id)->where('conv_creator_level', 2)->get()
            ], $this->successStatus); 

        } else {
            # code...
            return response()->json([
                'conversations' => Conversations::where('conv_creator', $user->id)->where('conv_creator_level', 3)->get()
            ], $this->successStatus); 
        }
    }

    public function makeReaden(Request $request)
    {
        # code...
        $notes = Notifications::where('n_to', 1)->get();
        $statuses = NotificationsStatus::where('n_role', 1)->where('n_user', Auth::user()->id)->delete();

        if (sizeof($notes) > 0) {
            # code...
            foreach ($notes as $note) {
                # code...
                NotificationsStatus::create([
                    'n_notification_id' => $note->id, 
                    'n_role' => 1,
                    'n_user' => Auth::user()->id,
                    'is_read' => 1
                ]);
            }
        }
        

        return response()->json(['status' => 0], $this->successStatus); 
    }

    public function ReadnAnswer(Request $request)
    {
        # code...
        $answer = ConversationsAnswers::where('conv_answer_code', $request->input('code'))->first();
        $answer->is_readn = 1;
        $answer->save();

        return response()->json(['answer' => 'Anwer is read!', 'answers' => ConversationsAnswers::where('conv_answer_conv_id', $answer->conv_answer_conv_id)->get()], $this->successStatus); 
    }
}
