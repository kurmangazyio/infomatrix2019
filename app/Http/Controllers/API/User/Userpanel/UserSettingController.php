<?php

namespace App\Http\Controllers\API\User\Userpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;

use App\Categories;
use App\Countries;
use App\Regions;

use Validator;

use App\Notifications;
use App\NotificationsStatus;

use App\UserConfig;

class UserSettingController extends Controller
{
    //
    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $notes = Notifications::where('n_to', 1)->get();
        $statuses = NotificationsStatus::all();

        $counter = sizeof($notes);
       
        foreach ($notes as $note) {
            # code...
            if (sizeof($statuses) > 0) {
                # code...
                foreach ($statuses as $status) {
                    # code...
                    if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                        # code...
                        $counter--;
                    }
                }
            }
        }

        return response()->json([
            'user' => Auth::user(), 
            'countries' => Countries::all(), 
            'regions' => Regions::all(),
            'status' => $counter,
            'config' => UserConfig::get()->first()
        ], $this->successStatus);
    }
}
