<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use Validator;

use App\Students;
use App\Projects;
use App\Teams;
use App\User;

use App\Jury;
use App\Tours;
use App\Criteria;
use App\Points;

use App\Postregister;
use App\Appeal;

use App\Categories;
use App\Medals;

class PointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $teams = Teams::select('*', 'projects.id as pid')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->where('teams.t_project_mark', 3)
            ->where('teams.t_supervisor', Auth::user()->id)
            ->get();

        foreach ($teams as $team) {
            # code...
            $students = array();

            if ($team->t_f_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_f_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_f_student_code)
                        ->first();

                    array_push($students, $student);
                }
                
            }
            if ($team->t_s_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_s_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_s_student_code)
                        ->first();

                    array_push($students, $student);
                }
            }
            if ($team->t_t_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_t_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_t_student_code)
                        ->first();

                    array_push($students, $student);
                }
            }

            $team['students'] = $students; 

        }
        
        $hereisarray = array();
        foreach ($teams as $team) {
            # code...
            foreach ($team['students'] as $student) {
                # code...
                $toursss = Tours::where('jtcategory', $team['p_category'])->get();

                array_push($hereisarray, [
                    'studentid' => $student['s_student_id'],
                    'student' => $student['s_en_name'].' '.$student['s_en_surname'],
                    'project' => $team['p_title'],
                    'tourone' => $this->calculateAVG($student['ssid'], $team['pid'], $toursss[0]['id'], $toursss[0]['jtcategory']),
                    'tourtwo' => $this->calculateAVG($student['ssid'], $team['pid'], $toursss[1]['id'], $toursss[1]['jtcategory']),
                    'avg' => $this->calculatetaemAVG($team['students'], $toursss, $team['pid'], $team['p_category']),
                    'firsttourappeal' => $this->getappeals($student['ssid'], 0),
                    'secondtourappeal' => $this->getappeals($student['ssid'], 1),
                    'sumappeal' => $this->getsumappeal(
                        $this->calculateAVG($student['ssid'], $team['pid'], $toursss[0]['id'], $toursss[0]['jtcategory']),
                        $this->calculateAVG($student['ssid'], $team['pid'], $toursss[1]['id'], $toursss[1]['jtcategory']),
                        $this->getappeals($student['ssid'], 0),
                        $this->getappeals($student['ssid'], 1)
                    ),
                    'avgappeal' => $this->avgAppeal($team['students'], $toursss, $team['pid'], $team['p_category']),
                ]);
            }
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'students' => $hereisarray,
            'user' => Auth::user()
        ], 200);
    }

    public function calculatetaemAVG($students, $tours, $projectid, $category)
    {
        # code...
        $sum = 0;

        foreach ($tours as $tour) {
            # code...
            foreach ($students as $student) {
                # code...
                $sum += $this->calculateAVG($student['ssid'], $projectid, $tour['id'], $category);
            }
        }
        
        return $sum/(sizeof($students));
    }

    public function calculateAVG($studentid, $projectid, $tour, $category)
    {
        # sucm
        $calc = 0;

        // points geting;
        $points = Points::select('*')
            ->join('jcriteria', 'jcriteria.id', '=','jpoints.jpcriteria_id')
            ->join('jtours', 'jtours.id', '=','jcriteria.jctour')
            ->where('jtours.id', $tour)
            ->where('jtours.jtcategory', $category)
            ->where('jpoints.jpstudent_id', $studentid)
            ->where('jpoints.jpproject_id', $projectid)
            ->get();

        //geting juries;
        $juries = array();
        foreach ($points as $point) {
            array_push($juries, $point->jpjury_id);
        }
        
        //juries counted!
        $juries = array_unique($juries);

        foreach ($points as $point) {
            # code...
            $calc += $point->jppoint;
        }

        if ($calc == 0) {
            # code...
            return "0";
        }else{
            return $calc/sizeof($juries);
        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getcategorys()
    {
        //
        $jury = Auth::user();

        $teams = Teams::select('*', 'projects.id as pid')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->where('teams.t_project_mark', 3)
            ->where('projects.p_category', $jury->category)
            ->get();

        foreach ($teams as $team) {
            # code...
            $students = array();

            if ($team->t_f_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_f_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_f_student_code)
                        ->first();

                    array_push($students, $student);
                }
                
            }
            if ($team->t_s_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_s_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_s_student_code)
                        ->first();

                    array_push($students, $student);
                }
            }
            if ($team->t_t_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_t_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_t_student_code)
                        ->first();

                    array_push($students, $student);
                }
            }

            $team['students'] = $students; 

        }
        
        $hereisarray = array();
        foreach ($teams as $team) {
            # code...
            foreach ($team['students'] as $student) {
                # code...
                $toursss = Tours::where('jtcategory', $team['p_category'])->get();

                array_push($hereisarray, [
                    'studentid' => $student['s_student_id'],
                    'student' => $student['s_en_name'].' '.$student['s_en_surname'],
                    'project' => $team['p_title'],
                    'tourone' => $this->calculateAVG($student['ssid'], $team['pid'], $toursss[0]['id'], $toursss[0]['jtcategory']),
                    'tourtwo' => $this->calculateAVG($student['ssid'], $team['pid'], $toursss[1]['id'], $toursss[1]['jtcategory']),
                    'avg' => $this->calculatetaemAVG($team['students'], $toursss, $team['pid'], $team['p_category']),
                    'firsttourappeal' => $this->getappeals($student['ssid'], 0),
                    'secondtourappeal' => $this->getappeals($student['ssid'], 1),
                    'sumappeal' => $this->getsumappeal(
                        $this->calculateAVG($student['ssid'], $team['pid'], $toursss[0]['id'], $toursss[0]['jtcategory']),
                        $this->calculateAVG($student['ssid'], $team['pid'], $toursss[1]['id'], $toursss[1]['jtcategory']),
                        $this->getappeals($student['ssid'], 0),
                        $this->getappeals($student['ssid'], 1)
                    ),

                    'avgappeal' => $this->avgAppeal($team['students'], $toursss, $team['pid'], $team['p_category'])
                ]);
            }
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'students' => $hereisarray,
            'jury' => Auth::user()
        ], 200);
    }


    public function getappeals($student, $tour)
    {
        # code...
        $app = Appeal::where('student_id', $student)->where('tour', $tour)->first();
        
        if ($app == null) {
            # code...
            return 0;
        }

        return $app->point;
    }

    public function getbycategorys()
    {
        //
        $jury = Auth::user();

        $teams = Teams::select('*', 'projects.id as pid')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->where('teams.t_project_mark', 3)
            ->get();

        foreach ($teams as $team) {
            # code...
            $students = array();

            if ($team->t_f_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_f_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid', 'countries.name as countryname')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_f_student_code)
                        ->first();

                    array_push($students, $student);
                }
                
            }
            if ($team->t_s_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_s_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid', 'countries.name as countryname')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_s_student_code)
                        ->first();

                    array_push($students, $student);
                }
            }
            if ($team->t_t_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_t_student_code)->where('role', 4)->first();
                if ($coun != null) {
                        # code...
                    $student = Students::select('*', 'students.id as ssid', 'countries.name as countryname')
                        ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                        ->join('countries', 'countries.id', '=', 'students.s_country')
                        ->where('s_student_id', $team->t_t_student_code)
                        ->first();

                    array_push($students, $student);
                }
            }

            $team['students'] = $students; 

        }
        
        $hereisarray = array();
        foreach ($teams as $team) {
            # code...
            foreach ($team['students'] as $student) {
                # code...
                $toursss = Tours::where('jtcategory', $team['p_category'])->get();

                array_push($hereisarray, [
                    'studentid' => $student['s_student_id'],
                    'category' => Categories::where('id', $team->p_category)->first()->category,
                    'categoryid' => $team->p_category,
                    'country' => $student['countryname'],
                    'student' => $student['s_en_name'].' '.$student['s_en_surname'],
                    'project' => $team['p_title'],
                    'tourone' => $this->calculateAVG($student['ssid'], $team['pid'], $toursss[0]['id'], $toursss[0]['jtcategory']),
                    'tourtwo' => $this->calculateAVG($student['ssid'], $team['pid'], $toursss[1]['id'], $toursss[1]['jtcategory']),
                    'avg' => $this->calculatetaemAVG($team['students'], $toursss, $team['pid'], $team['p_category']),
                    'firsttourappeal' => $this->getappeals($student['ssid'], 0),
                    'secondtourappeal' => $this->getappeals($student['ssid'], 1),
                    'sumappeal' => $this->getsumappeal(
                        $this->calculateAVG($student['ssid'], $team['pid'], $toursss[0]['id'], $toursss[0]['jtcategory']),
                        $this->calculateAVG($student['ssid'], $team['pid'], $toursss[1]['id'], $toursss[1]['jtcategory']),
                        $this->getappeals($student['ssid'], 0),
                        $this->getappeals($student['ssid'], 1)
                    ),
                    'avgappeal' => $this->avgAppeal($team['students'], $toursss, $team['pid'], $team['p_category']),

                    'medal' => $this->getMedal($student['ssid'], $team->p_category)
                ]);
            }
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'students' => $hereisarray,
            'admin' => Auth::user()
        ], 200);
    }


    public function avgAppeal($students, $tours, $projectid, $category)
    {
        # code...
        $sum = 0;

        foreach ($tours as $tour) {
            # code...
            foreach ($students as $student) {
                # code...
                $sum += $this->calculateAVG($student['ssid'], $projectid, $tour['id'], $category);
                $sum += $this->getappeals($student['ssid'], 0);
                $sum += $this->getappeals($student['ssid'], 1);
            }
        }
        
        return $sum/(sizeof($students));
    }

    public function getsumappeal($one, $two, $aone, $atwo)
    {
        # code...
        $aone = str_replace('.', ',', $aone);
        $atwo = str_replace('.', ',', $atwo);

        $sum = $one + $two + $aone + $atwo;
        return round($sum, 2);
    }

    public function getMedal($student, $category)
    {
        # code...
        $medal = Medals::where('studentid', $student)
            ->where('categoryid', $category)
            ->first();

        if ($medal == null) {
            # code...
            return 0;
        }

        return $medal->medal;
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
