<?php

namespace App\Http\Controllers\API\User\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Jury;
use App\Admin;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Teams;
use App\Projects;
use App\Students;
use App\Marks;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use App\Payments;
use App\Spayment;
use App\Superpayments;

use App\Guests;
use App\Guestpayments;

class PaymentsController extends Controller
{
    //
    public $successStatus = 200;
    public $userid;

    public function index()
    {
    	# code...
    	$user =  Auth::user();
    	if (isset($user['user_id'])) {
            # code...
            $notes = Notifications::where('n_to', 1)->get();
            $statuses = NotificationsStatus::all();

            $counter = sizeof($notes);
       
            foreach ($notes as $note) {
                # code...
                if (sizeof($statuses) > 0) {
                    # code...
                    foreach ($statuses as $status) {
                        # code...
                        if ($status->n_role == 1 && $status->n_user == Auth::user()->id && $status->n_notification_id == $note->id) {
                            # code...
                            $counter--;
                        }
                    }
                }
            }

            $projects = Projects::all();

            $students = Spayment::select( 'students.s_en_name as name','students.s_en_surname as surname','students.s_daryn as daryn', 'teams.t_project_code as code', 'payment_students.*')
            ->join('students', 'payment_students.student_id', '=', 'students.id')
            ->join('teams', 'payment_students.student_team_id', '=', 'teams.id')
            ->where('payment_students.student_supervisor_id', Auth::user()->id)
            ->orderBy('students.s_daryn', 'DESC')
            ->get();

            foreach ($students as $key) {
                    # code...
                foreach ($projects as $value) {
                    if ($value->p_project_id == $key['code']) {
                        # code...
                        $key['title'] = $value->p_title;
                        $key['projectid'] = $value->p_project_id;
                        $key['sselected'] = false;
                    }
                }
            }
            
            $supervisor = Superpayments::select('payment_supervisor.*', 'users.name', 'users.surname')->where('supervisor_id', Auth::user()->id)->join('users', 'payment_supervisor.supervisor_id', '=', 'users.id')->first();

            $guests = Guestpayments::select('guest_payments.*', 'guests.name', 'guests.surname', 'guests.guest_id', 'guests.p_supervisor')
                ->join('guests', 'guest_payments.guest_id', '=', 'guests.id')
                ->where('guests.p_supervisor', Auth::user()->id)
                ->get();

            return response()->json([
            	'user' => $user,
            	"notifications" => Notifications::all(), 
            	'students' => $students,
                'supervisor' => $supervisor,
                'guests' => $guests,
                'status' => $counter
            ], $this->successStatus); 

        }elseif (isset($user['jury_id'])) {
            # code...
            return response()->json([
                'conversations' => Conversations::where('conv_creator', $user->id)->where('conv_creator_level', 2)->get()
            ], $this->successStatus); 

        } else {
            # code...
            return response()->json([
                'conversations' => Conversations::where('conv_creator', $user->id)->where('conv_creator_level', 3)->get()
            ], $this->successStatus); 
        }
    }

    public function refreshGuest()
    {
        # code...
        $guests = Guestpayments::select('guest_payments.*', 'guests.name', 'guests.surname', 'guests.guest_id', 'guests.p_supervisor')
            ->join('guests', 'guest_payments.guest_id', '=', 'guests.id')
            ->where('guests.p_supervisor', Auth::user()->id)
            ->get();
                
        return response()->json([
            'guests' => $guests
        ], $this->successStatus); 
    }

    public function deleteGuest(Request $request)
    {
        # code...
        $guest = Guests::where('guest_id', $request->id)->first();

        Guestpayments::where('guest_id', $guest->id)->delete();
        Guests::where('id', $guest->id)->delete();

        $guests = Guestpayments::select('guest_payments.*', 'guests.name', 'guests.surname', 'guests.guest_id')
            ->join('guests', 'guest_payments.guest_id', '=', 'guests.id')
            ->get();
                
        return response()->json([
            'success' => 'Success',
            'guests' => $guests
        ], $this->successStatus); 
    }

    public function addToPayments(Request $request)
    {
        # code...
        Payments::where('payment_id', $request->input('invoce'))->delete();
        
        Payments::create([
            'payment_id' => $request->input('invoce'),

            'payment_email' => $request->input('email'),
            'payment_supervisor' => $request->input('userid'),
            'payment_details' => $request->input('details'),

            'amount'  => $request->input('amount'),
        ]);

        $data = json_decode($request->input('details'), TRUE);

        foreach ($data as $value) {
            # code...
            $id = substr($value['invoce'], 0, 4);
            switch ($id) {
                case 'pmsp':
                    # code...
                    $sp = Superpayments::where('payment_unique', $value['invoce'])->first();
                    $sp->payment_status = 1;
                    $sp->save();
                    break;
                case 'pmgs':
                    # code...
                    $guest = Guestpayments::where('payment_unique', $value['invoce'])->first();
                    $guest->payment_status = 1;
                    $guest->save();
                    break;
                case 'pmst':
                    # code...
                    $student = Spayment::where('payment_unique', $value['invoce'])->first();
                    $student->payment_status = 1;
                    $student->save();
                    break;
            }
        }
        $this->userid = $request->input('userid');

        \Mail::send('mails.invoice', [ 'details' => $data,'paid' => $request->input('amount'), 'id' => $request->input('invoce')], function($message)
            {
                $senderof = User::where('id', $this->userid )->first();
                $message->from('infomatrix@bil.edu.kz', 'Invoice Details');
                $message->to($senderof->email)->cc('infomatrix@bil.edu.kz');
            });

        return response()->json([
            'data' => $data
        ], $this->successStatus); 
    }
}
