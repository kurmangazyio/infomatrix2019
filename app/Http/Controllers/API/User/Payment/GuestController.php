<?php

namespace App\Http\Controllers\API\User\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Guests;
use App\Guestpayments;
use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:40|min:2',
            'surname' => 'required|max:40|min:2',

            'email' => 'required',
            'passport' => 'required|min:6',
            'phone' => 'required|min:7',

            'country' => 'required',
            'region' => 'required',
            'city' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first()
            ], $this->successStatus);            
        }

        $guest = Guests::create([
            'guest_id' => $this->UserGenerator(),  
            'p_supervisor'  => Auth::user()->id, 

            'name'  => $request->input('name'),  
            'surname'  => $request->input('surname'),  
            'lastname'  => '',
     
            'passport'  => $request->input('passport'), 
            'phone'  => $request->input('phone'),
            'email'  => $request->input('email'),
            
            'country'  => $request->input('country'),
            'region'  => $request->input('region'), 
            'city'  => $request->input('city'),
            'address'  => $request->input('address')
        ]);

        Guestpayments::create([
            'guest_id' => $guest->id,
            'payment_unique' => $this->PaymentGenerator('pmgs'),
            'payment_status' => 0,
            'payment_option' => " ",
        ]);

        return response()->json([
            'success' => true,
            'message' => "Guest successfully imported"
        ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UserGenerator()
    {
      # code...
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return 'gust'.$randomString;
    }

    public function PaymentGenerator($role)
    {
      # code...
      $thisyear = date("Y");
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 7; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $role.$randomString;
    }
}
