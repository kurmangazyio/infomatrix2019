<?php

namespace App\Http\Controllers\API\User\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Projects;
use App\Teams;
use App\Students;
use App\Marks;


use App\Countries;
use App\Regions;
use App\Categories;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\UserConfig;
use App\Invoice;
use App\Payments;
use App\Guests;

class OfflineInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function index()
    {
        //
        $payments = Payments::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'payments.created_at as created_at')
            ->join('users', 'users.id', '=', 'payments.payment_supervisor')
            ->where('payments.payment_supervisor', Auth::user()->id)
            ->get();

        $offline = Invoice::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'offline_invoice.created_at as created_at')
            ->join('users', 'users.id', '=', 'offline_invoice.i_supervisor')
            ->where('offline_invoice.i_supervisor', Auth::user()->id)
            ->get();

        return response()->json([
                'user' =>  Auth::user(),
                'payments' => $payments,
                'offline'=> $offline
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255|min:2',
            'role' => 'required',
            'project' => 'required',
            'paymethod' => 'required',
            'amount' => 'required',
            'file' => 'required',
            'ext' => 'required',
            'filename' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'msg' => $validator->errors()->first()], 
            $this->successStatus);            
        }

        $code = $this->Generator();
        

        $invoice = new Invoice;
        $invoice->i_supervisor = Auth::user()->id;

        $invoice->i_code = $code;
        $invoice->i_fullname =  $request->fullname;
        $invoice->i_role = $request->role;
        $invoice->i_project = $request->project;
        $invoice->i_method = $request->paymethod;
        $invoice->i_amount = $request->amount;

        //file
        $exploded = explode(',', $request->input('file'));
        $decoded = base64_decode($exploded[1]);

        $file = Auth::user()->id.'_'.$code.'.'.$request->input('ext');
        $path = public_path().'/general/files/invoices/'.$file;
        file_put_contents($path, $decoded);
        $invoice->i_file = '/general/files/invoices/'.$file;

        $invoice->i_comment = $request->comments == null ? '': $request->comments;

        $invoice->save();

        return response()->json(
            [
                'success' => true,
                'error' => false,
                'msg' => 'Invoice successfully uploaded!',
            ], $this->successStatus); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Invoice::where('i_code', $id)->delete();
        
        $offline = Invoice::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'offline_invoice.created_at as created_at')
            ->join('users', 'users.id', '=', 'offline_invoice.i_supervisor')
            ->where('offline_invoice.i_supervisor', Auth::user()->id)
            ->get();

        return response()->json(
            [
                'success' => true,
                'error' => false,
                'msg' => 'Invoice successfully deleted!',
                'offline' => $offline
            ], $this->successStatus); 
    }

    public function Generator()
    {
      # code...
      $thisyear = date("Y");
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return 'inv-'.$randomString;
    }
}
