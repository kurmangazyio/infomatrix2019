<?php

namespace App\Http\Controllers\API\User\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Jury;
use App\Admin;

use App\Categories;
use App\Countries;
use App\Regions;

use App\Teams;
use App\Projects;
use App\Students;
use App\Marks;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Conversations;
use App\ConversationsCategories;
use App\ConversationsAnswers;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    protected $ActivitiesTracker;

    public function __construct(ActivitiesTracker $ActivitiesTracker)
    {
      $this->middleware('auth:api');
      $this->ActivitiesTracker = $ActivitiesTracker;
    }

    public function index()
    {
        //
        $projects = Projects::select('projects.*', 'categories.category as p_category_title','categories.image as p_category_image', 'teams.t_team_id')
                ->join('categories', 'projects.p_category', '=', 'categories.id')
                ->join('teams', 'projects.p_project_id', '=', 'teams.t_project_code')
                ->where('p_supervisor', Auth::user()->id)
                ->get();

        return response()->json($projects, $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
