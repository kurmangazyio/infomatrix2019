<?php

namespace App\Http\Controllers\API\User\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Projects;
use App\Teams;
use App\Students;
use App\Marks;


use App\Countries;
use App\Regions;
use App\Categories;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\UserConfig;
use App\Invoice;
use App\Payments;
use App\Guests;

use App\ActivityTracker;

class OfflineTwoInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;

    public function index()
    {
        //
        $payments = Payments::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'payments.created_at as created_at')
            ->join('users', 'users.id', '=', 'payments.payment_supervisor')
            ->where('payments.payment_supervisor', Auth::user()->id)
            ->get();

        $offline = Invoice::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'offline_invoice.created_at as created_at')
            ->join('users', 'users.id', '=', 'offline_invoice.i_supervisor')
            ->where('offline_invoice.i_supervisor', Auth::user()->id)
            ->get();

        return response()->json([
                'user' =>  Auth::user(),
                'payments' => $payments,
                'offline'=> $offline
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255|min:2',
            'role' => 'required',
            'project' => 'required',
            'paymethod' => 'required',
            'amount' => 'required',
            'file' => 'required',
            'ext' => 'required',
            'filename' => 'required',
            'supervisorid' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'msg' => $validator->errors()->first()], 
            $this->successStatus);            
        }

        $code = $this->Generator();
        
        $user = User::where('user_id', $request->supervisorid)->first();

        $invoice = new Invoice;
        $invoice->i_supervisor = $user->id;

        $invoice->i_code = $code;
        $invoice->i_fullname = $request->fullname;
        $invoice->i_role = $request->role;
        $invoice->i_project = $request->project;
        $invoice->i_method = $request->paymethod;
        $invoice->i_amount = $request->amount;

        //file
        $exploded = explode(',', $request->input('file'));
        $decoded = base64_decode($exploded[1]);

        $file = $user->id.'_'.$code.'.'.$request->input('ext');
        $path = public_path().'/general/files/invoices/'.$file;
        file_put_contents($path, $decoded);
        $invoice->i_file = '/general/files/invoices/'.$file;

        $invoice->i_comment = $request->comments == null ? '': $request->comments;

        $invoice->save();

        ActivityTracker::create([
            'user' => Auth::user()->id, 
            'user_level' => 3,
            'user_ip' => $this->get_client_ip(),

            'activity' => Auth::user()->name.' '.Auth::user()->surname.' registered invoice, code is '.$code,
            'is_important' => 5
        ]);

        return response()->json(
            [
                'success' => true,
                'error' => false,
                'msg' => 'Invoice successfully uploaded by Moderator!',
            ], $this->successStatus); 
    }

    public function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $user = User::where('user_id', $id)->first();

        $projects = Projects::select('projects.*', 'categories.category as p_category_title','categories.image as p_category_image', 'teams.t_team_id')
                ->join('categories', 'projects.p_category', '=', 'categories.id')
                ->join('teams', 'projects.p_project_id', '=', 'teams.t_project_code')
                ->where('p_supervisor', $user->id )
                ->get();

        return response()->json([
                'super' => $user,
                'projects' => $projects
        ], $this->successStatus);
    }

    public function getinvoices($teamid)
    {
        # code...
        $team = Teams::where('t_team_id', $teamid)->first();

        $payments = Payments::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'payments.created_at as created_at')
            ->join('users', 'users.id', '=', 'payments.payment_supervisor')
            ->where('payments.payment_supervisor', $team->t_supervisor)
            ->get();

        $offline = Invoice::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'offline_invoice.created_at as created_at')
            ->join('users', 'users.id', '=', 'offline_invoice.i_supervisor')
            ->where('offline_invoice.i_supervisor', $team->t_supervisor)
            ->get();

        return response()->json([
                'payments' => $payments,
                'offline' => $offline
        ], $this->successStatus);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Invoice::where('i_code', $id)->delete();
        
        $offline = Invoice::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'offline_invoice.created_at as created_at')
            ->join('users', 'users.id', '=', 'offline_invoice.i_supervisor')
            ->where('offline_invoice.i_supervisor', Auth::user()->id)
            ->get();

        return response()->json(
            [
                'success' => true,
                'error' => false,
                'msg' => 'Invoice successfully deleted!',
                'offline' => $offline
            ], $this->successStatus); 
    }

    public function Generator()
    {
      # code...
      $thisyear = date("Y");
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return 'inv-'.$randomString;
    }
}
