<?php

namespace App\Http\Controllers\API\User\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Jury;
use App\Admin;

use App\Messages;
use App\Teams;
use App\Projects;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    public $user_email = '';

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = Auth::user();
        $team = Teams::where('t_team_id', $request->input('id'))->first();
        

        Messages::create([
            'messager' => $user->id,
            'role' => $request->input('role'),
            'teams_id' => $team->id,
            'message' => $request->input('message'),
        ]);

        if ($request->input('role') == 1) {
            # code...
            $reciever =  User::where('id', $team->t_supervisor)->first();
            $project = Projects::where('p_project_id', $team->t_project_code)->first();
            $this->user_email = $reciever->email;

            \Mail::send('mails.conversation', [ 'convid' => $team->t_team_id, 'convtitle' => "Message from Infomatrix Jury!", 'convcontent' => "Project: ".$project->p_title , 'convreply' => $request->input('message')], function($message)
            {
                $senderof = $this->user_email;
                $message->from('infomatrix@bil.edu.kz', 'Project Reply');
                $message->to($senderof)->cc('infomatrix@bil.edu.kz');
            });
        }
        

        return response()->json([
            'messages' => Messages::where('teams_id', $team->id)->get(),
            'user' => $user
        ], $this->successStatus); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team = Teams::where('t_team_id', $id)->first()->id;
        $messages = Messages::where('teams_id', $team)->get();

                
        return response()->json([
            'messages' => $messages
        ], $this->successStatus); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
