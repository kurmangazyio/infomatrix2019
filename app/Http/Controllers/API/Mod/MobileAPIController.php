<?php

namespace App\Http\Controllers\API\Mod;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Guests;
use App\Students;

use App\postActions;

class MobileAPIController extends Controller
{
    //
    public function getallusers()
    {
    	# code...
    	$users = User::all();
    	$guests = Guests::all();
    	$students = Students::all();

    	$actioners = postActions::all();

    	foreach ($actioners as $act) {
    		# code...
    		switch ($act->code_role) {
    			case 1:
    				# code...
    				foreach ($users as $value) {
    					# code...
    					if ($value->user_id == $act->code) {
    						# code...
    						$actioners['fullname'] = $value->name.' '.$value->surname;
    					}
    				}
    				break;
    			case 2:
    				# code...
    				foreach ($students as $value) {
    					# code...
    					if ($value->s_student_id == $act->code) {
    						# code...
    						$actioners['fullname'] = $value->s_en_name.' '.$value->s_en_surname;
    					}
    				}
    				break;
    			case 3:
    				# code...
    				foreach ($guests as $value) {
    					# code...
    					if ($value->guest_id == $act->code) {
    						# code...
    						$actioners['fullname'] = $value->name.' '.$value->surname;
    					}
    				}
    				break;
    		}
    	}

    	return response()->json(['users' => $actioners], 200); 
    }
}
