<?php

namespace App\Http\Controllers\API\Mod\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Admin;
use App\Volunteer;

use App\User;
use App\Guests;
use App\Guestpayments;
use App\Students;

use App\Postregister;

use Validator;
use App\ActivityTracker;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    public function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'isparticipant' => 'required', 
            'isdormotry' => 'required',
            'isgift' => 'required',
            'guestid' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $validator->errors()->first()
            ], 200);            
        }

        $post = Postregister::create([
            'code' => $request->guestid, 
            'role' => 5,
            'is_participant' => $request->isparticipant,
            'is_food' => $request->isparticipant, 
            'is_transport' => $request->isdormotry,
            'is_gift' => $request->isdormotry
        ]);

        $guest = Guests::where('guest_id', $request->guestid)->first();
        //Here is tracker
        if ($request->isparticipant) {
            # code...
            $paying = Guestpayments::where('guest_id', $guest['id'])->first();
            $paying->payment_status = 1;
            $paying->save();
        }

        if (!$request->isparticipant) {
            # code...
            $paying = Guestpayments::where('guest_id', $guest['id'])->first();
            $paying->payment_status = 0;
            $paying->save();
        }

        
        //Here is tracker
        ActivityTracker::create([
            'user' => Auth::user()->id, 
            'user_level' => 5,
            'user_ip' => $this->get_client_ip(),

            'activity' => Auth::user()->name.' '.Auth::user()->surname.' registered guest, name is '.$guest['name'].' '.$guest['surname'].'changes: Participation: '.$request->isparticipant.', Dormitory: '.$request->isdormotry.', Gift: '.$request->isgift.'!',
            'is_important' => 6
        ]);

        return response()->json([
            'success' => true, 
            'error' => false, 
            'message' => 'Guest registration provided!',
            'user' => $post
        ], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $guest = Postregister::join('guests','guests.guest_id', '=', 'postregisters.code')->where('guests.guest_id', $id)->first();

        return response()->json([
            'success' => true, 
            'error' => false,
            'message' => 'Guest load!',
            'user' => $guest
        ], 200); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'isparticipant' => 'required', 
            'isdormotry' => 'required',
            'isgift' => 'required',
            'guestid' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $validator->errors()->first()
            ], 200);            
        }

        $post = Postregister::where('code', $request->guestid)->first();

        $post->is_participant = $request->isparticipant;
        $post->is_food = $request->isparticipant;
        $post->is_transport =  $request->isdormotry;
        $post->is_gift = $request->isgift;
        $post->save();

        $guest = Guests::where('guest_id', $request->guestid)->first();
        //Here is tracker

        if ($request->isparticipant) {
            # code...
            $paying = Guestpayments::where('guest_id', $guest['id'])->first();
            $paying->payment_status = 1;
            $paying->save();
        }

        if (!$request->isparticipant) {
            # code...
            $paying = Guestpayments::where('guest_id', $guest['id'])->first();
            $paying->payment_status = 0;
            $paying->save();
        }

        ActivityTracker::create([
            'user' => Auth::user()->id, 
            'user_level' => 5,
            'user_ip' => $this->get_client_ip(),

            'activity' => Auth::user()->name.' '.Auth::user()->surname.' updated guest, name is '.$guest->name.' '.$guest->surname.'changes: Participation: '.$request->isparticipant.', Dormitory: '.$request->isdormotry.', Gift: '.$request->isgift.'!',
            'is_important' => 6
        ]);

        return response()->json([
            'success' => true, 
            'error' => false, 
            'message' => 'Guest Participation Updated!',
            'user' => $post
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
