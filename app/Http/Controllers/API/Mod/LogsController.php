<?php

namespace App\Http\Controllers\API\Mod;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Admin;
use App\Volunteer;

use App\User;
use App\Guests;
use App\Students;

use App\Postregister;
use App\Logs;

use Validator;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $logs = Logs::select('logs.*', \DB::raw('(select concat(surname, " ", name) from moderators where id = logs.action_tracker) as moding'))->join('moderators', 'moderators.id', '=', 'logs.action_tracker')->get();

        $admins = Admin::all();
        $volunteers = Volunteer::all();
        $supervisors = User::join('teams', 'teams.t_supervisor', '=', 'users.id')->where('teams.t_project_mark', 3)->get();
        $students = Students::all();
        $guests = Guests::all();

        $export = array();

        $hours = 6;

        foreach ($logs as $log) {
            # code...
            foreach ($admins as $admin) {
                # code...
                if ($log->action_user == $admin->admin_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $admin->surname.' '.$admin->name,
                        'role' => $this->getrole(1),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }

            foreach ($volunteers as $volunteer) {
                # code...
                if ($log->action_user == $volunteer->volunteer_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $volunteer->surname.' '.$volunteer->name,
                        'role' => $this->getrole(2),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }

            foreach ($supervisors as $supervisor) {
                # code...
                if ($log->action_user == $supervisor->user_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $supervisor->surname.' '.$supervisor->name,
                        'role' => $this->getrole(3),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }

            foreach ($students as $student) {
                # code...
                if ($log->action_user == $student->s_student_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $student->s_en_name.' '.$student->s_en_surname,
                        'role' => $this->getrole(4),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }


            foreach ($guests as $guest) {
                # code...
                if ($log->action_user == $guest->guest_id) {
                    # code...
                    array_push($export, [
                        'lid' => $log->action_id,
                        'user' => $guest->surname.' '.$guest->name,
                        'role' => $this->getrole(5),
                        'code' => $log->action_code,
                        'action' => $this->getword($log->action_code),
                        'tracedby' => $log->moding,
                        'dateof' => $log->created_at
                    ]);
                }
            }
        }

        return response()->json([
                'mod' =>  Auth::user(),
                'logs' => $export
        ], 200);
    }

    public function getword($code)
    {
        # code...
        $word = '';
        switch ($code) {
            case 'btst':
                # code...
                $word = 'Breakfast';
                break;
            case 'lnch':
                # code...
                $word = 'Lunch';
                break;
            case 'dnnr':
                # code...
                $word = 'Dinner';
                break;
            case 'totp':
                # code...
                $word = 'To Transport';
                break;
            case 'fmtp':
                # code...
                $word = 'From Transport';
                break;
        }

        return $word;
    }

    public function getrole($code)
    {
        # code...
        $word = '';
        switch ($code) {
            case 1:
                # code...
                $word = 'Admin';
                break;
            case 2:
                # code...
                $word = 'Volunteer';
                break;
            case 3:
                # code...
                $word = 'Supervisor';
                break;
            case 4:
                # code...
                $word = 'Student';
                break;
            case 5:
                # code...
                $word = 'Guest';
                break;
        }

        return $word;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'id' => 'required|max:255|min:1', 
            'user' => 'required|max:255|min:1',
            'action' => 'required|max:255|min:1',
            'comment' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $validator->errors()->first()
            ], 200);            
        }

        Logs::create([
            'action_id' => $request->id, 
            'action_user' => $request->user,
            'action_code' => $request->action,
            'action_comment' => $request->comment, 

            'action_tracker' => Auth::user()->id
        ]);

        return response()->json([
                'success' => true,
                'error' => false,
                'message' => 'Successfully tracked!'
            ], 200);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
