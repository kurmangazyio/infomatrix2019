<?php

namespace App\Http\Controllers\API\Mod;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Admin;
use App\Volunteer;

use App\User;
use App\Guests;
use App\Students;
use App\Jury;

use App\Postregister;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Postregister::all();

        $users = array();
        
        $admins = Admin::all();
        foreach ($admins as $admin) {
            # code...
            array_push($users, [
                "fullname" => $admin->surname.' '.$admin->name,
                "code" => $admin->admin_id ,
                "role" => 1,
                "is_food" => true,
                "is_transport" => false
            ]);
        }

        $juries = Jury::all();
        foreach ($juries as $jury) {
            # code...
            array_push($users, [
                "fullname" => $jury->surname.' '.$jury->name.' (Jury)',
                "code" => $jury->jury_id,
                "role" => 1,
                "is_food" => true,
                "is_transport" => false
            ]);
        }

        $volunteers = Volunteer::all();
        foreach ($volunteers as $volunteer) {
            # code...
            array_push($users, [
                "fullname" => $volunteer->surname.' '.$volunteer->name,
                "code" => $volunteer->volunteer_id ,
                "role" => 2,
                "is_food" => true,
                "is_transport" => true
            ]);
        }

        $supervisors = User::join('teams', 'teams.t_supervisor', '=', 'users.id')->where('teams.t_project_mark', 3)->get();
        foreach ($supervisors as $supervisor) {
            # code...
            foreach ($posts as $value) {
                # code...
                if ($value->code == $supervisor->user_id) {
                    # code...
                    array_push($users, [
                        "fullname" => $supervisor->surname.' '.$supervisor->name,
                        "code" => $supervisor->user_id ,
                        "role" => $value->role,
                        "is_food" => $value->is_food == 1 ? true : false,
                        "is_transport" => $value->is_transport == 1 ? true : false
                    ]);
                }
            }
        }

        $students = Students::all();
        foreach ($students as $student) {
            # code...
            foreach ($posts as $value) {
                # code...
                if ($student->s_student_id == $value->code) {
                    # code...
                    array_push($users, [
                        "fullname" => $student->s_en_name.' '.$student->s_en_surname,
                        "code" => $student->s_student_id,
                        "role" => $value->role,
                        "is_food" => $value->is_food == 1 ? true : false,
                        "is_transport" => $value->is_transport == 1 ? true : false
                    ]);
                }
            }
        }

        $guests = Guests::all();
        foreach ($guests as $guest) {
            # code...
            foreach ($posts as $value) {
                # code...
                if ($guest->guest_id == $value->code) {
                    # code...
                    array_push($users, [
                        "fullname" => $guest->surname.' '.$guest->name,
                        "code" => $guest->guest_id,
                        "role" => $value->role,
                        "is_food" => $value->is_food == 1 ? true : false,
                        "is_transport" => $value->is_transport == 1 ? true : false
                    ]);
                }
            }
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'users' => $users
        ], 200); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
