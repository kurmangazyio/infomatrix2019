<?php

namespace App\Http\Controllers\API\Mod;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Students;
use App\Projects;
use App\Teams;
use App\User;

use App\Jury;

use App\Postregister;

class JudmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $teams = Teams::join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->where('teams.t_project_mark', 3)
            ->where('projects.p_category', Auth::user()->category)
            ->get();

        

        foreach ($teams as $team) {
            # code...
            $counter = 0;

            $team['project'] = Projects::select('*')
                ->join('categories', 'categories.id', '=', 'projects.p_category')
                ->where('p_project_id', $team->t_project_code)->first();
            
            $super = User::select('*', 'countries.name as countryname', \DB::raw('concat(users.name, " ", users.surname) as supervisor'))
                    ->join('countries', 'countries.id', '=', 'users.country')
                    ->where('users.id', $team->t_supervisor)
                    ->first();
                    
            $team['supervisor'] = $super;

            $team['email'] = $super['email'];

            $students = array();
            if ($team->t_f_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_f_student_code)->where('role', 4)->first();
                
                $student = Students::select('*')
                    ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                    ->join('countries', 'countries.id', '=', 'students.s_country')
                    ->where('s_student_id', $team->t_f_student_code)
                    ->first();

                if ($coun != null) {
                    # code...
                    $counter++;
                    $student['hereboy'] = true;
                }else{
                    $student['hereboy'] = false;
                }
                array_push($students, $student);

            }
            
            if ($team->t_s_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_s_student_code)->where('role', 4)->first();
                
                $student = Students::select('*')
                    ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                    ->join('countries', 'countries.id', '=', 'students.s_country')
                    ->where('s_student_id', $team->t_s_student_code)
                    ->first();

                if ($coun != null) {
                    # code...
                    $counter++;
                    $student['hereboy'] = true;
                }else{
                    $student['hereboy'] = false;
                }
                array_push($students, $student);
            }

            if ($team->t_t_student_code != '') {
                # code...
                $coun = Postregister::where('code', $team->t_t_student_code)->where('role', 4)->first();
                
                $student = Students::select('*')
                    ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                    ->join('countries', 'countries.id', '=', 'students.s_country')
                    ->where('s_student_id', $team->t_t_student_code)
                    ->first();

                if ($coun != null) {
                    # code...
                    $counter++;
                    $student['hereboy'] = true;
                }else{
                    $student['hereboy'] = false;
                }
                array_push($students, $student);
            }

            $team['students'] = $students; 
            $team['counter'] = $counter;
        }

        return response()->json([
            'success' => true, 
            'error' => false, 
            'category' => Jury::select('categories.category', 'categories.id')->join('categories', 'categories.id', '=', 'juries.category')->where('juries.id', Auth::user()->id)->first(),
            'jury' => Auth::user(),
            'projects' => $teams
        ], 
        200); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
