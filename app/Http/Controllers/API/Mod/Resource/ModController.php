<?php

namespace App\Http\Controllers\API\Mod\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Students;
use App\Projects;
use App\Teams;
use App\User;

use App\Payments;
use App\Guests;
use App\Invoice;

use App\Spayment;
use App\Superpayments;

class ModController extends Controller
{
    //
    public $successStatus = 200;
    function __construct()
    {
    	$this->middleware('auth');
    }
    public function index()
    {
    	# code...
    	return response()->json([
                'mod' =>  Auth::user()
        ], $this->successStatus);

    }

    public function daryn()
    {
    	# code...
    	$students = Students::select('students.*', 'users.name as supername', 'users.surname as supersurname', 'regions.kz', 'regions.ru')
                ->join('users', 'users.id', '=', 'students.s_supervisor')
                ->join('regions', 'regions.id', '=', 'students.s_region')
                ->where('users.country', '=', 110)
                ->get();

        foreach ($students as $value) {
        	# code...
        	$value['fullname'] = $value['s_en_surname'].' '.$value['s_en_name'].' '.$value['s_en_lastname'];
        	$value['supervisor'] = $value['supername'].' '.$value['supersurname'];
        	$value['daryn'] = $value['s_daryn'] ? 'Approved' : 'Not Approved';
        }

    	return response()->json([
                'mod' =>  Auth::user(),
                'students' => $this->getStudents(),
                'export' => $this->getExport()
        ], $this->successStatus);
    }

    public function getPayments()
    {
        # code...
        $payments = Payments::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'))
            ->join('users', 'users.id', '=', 'payments.payment_supervisor')
            ->get();

        $offline = Invoice::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'), 'offline_invoice.created_at as created_at')
            ->join('users', 'users.id', '=', 'offline_invoice.i_supervisor')
            ->get();

        return response()->json([
                'mod' =>  Auth::user(),
                'payments' => $payments,
                'offline'=> $offline
        ], $this->successStatus);
    }

    public function postRegister()
    {
        # code..
        $projects = Teams::select(
            'teams.t_team_id as tid','users.email', 
            'projects.p_title as title',
            'countries.name as country', 
            \DB::raw('concat(users.name, " ", users.surname) as supervisor'),
            'teams.t_f_student_code',
            'teams.t_s_student_code',
            'teams.t_t_student_code'
        )
                ->join('users', 'users.id', '=', 'teams.t_supervisor')
                ->join('countries', 'countries.id', '=', 'users.country')
                ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
                ->where('teams.t_project_mark', 3)
                ->get();

        foreach ($projects as $value) {
            # code...
            $counter = 0;

            if ($value->t_f_student_code != '') {
                # code...
                $counter += 1;
            }
            if ($value->t_s_student_code != '') {
                # code...
                $counter += 1;
            }
            if ($value->t_t_student_code != '') {
                # code...
                $counter += 1;
            }

            $value['students'] = $counter;
        }

        $supers = array();

        foreach ($projects as $value) {
            # code...
            if (!in_array($value['email'], $supers)) {
                array_push($supers, $value['email']);
            }
        }
        return response()->json([
                'mod' =>  Auth::user(),
                'projects' => $projects,
                'supers' => $supers
        ], $this->successStatus);
    }

    public function approvement(Request $req)
    {
    	# code...
    	$student = Students::where('s_student_id', $req->input('id'))->first();

    	if (sizeof($student) > 0) {
    		# code...
    		$student->s_daryn = $req->input('approve');
    		$student->save();

    		return response()->json([
	            'success' =>  "Student approved successfully!",
                'export' => $this->getExport()
	        ], $this->successStatus);
    	}else{
    		return response()->json([
	            'success' =>  "Student approved failed!",
                'export' => $this->getExport()
	        ], $this->successStatus);
    	}
    }

    public function getStudents()
    {
    	# code...
    	$students = Students::select('students.*', 'users.name as supername', 'users.surname as supersurname', 'regions.kz', 'regions.ru')
                ->join('users', 'users.id', '=', 'students.s_supervisor')
                ->join('regions', 'regions.id', '=', 'students.s_region')
                ->where('users.country', '=', 110)
                ->get();

        $export = array();

        foreach ($students as $value) {
        	# code...
        	$value['fullname'] = $value['s_en_surname'].' '.$value['s_en_name'].' '.$value['s_en_lastname'];
        	$value['supervisor'] = $value['supername'].' '.$value['supersurname'];
        	$value['daryn'] = $value['s_daryn'] ? 'Approved' : 'Not Approved';
        }

        return $students;
    }

    public function getprojectdetails($teamid)
    {
        # code...
        $team = Teams::where('t_team_id', $teamid)->first();
        $project = Projects::select('*')
            ->join('categories', 'categories.id', '=', 'projects.p_category')
            ->where('p_project_id', $team->t_project_code)->first();
        


        $sfir = Superpayments::where('supervisor_id', $team->t_supervisor)->first();
        if ($sfir == null) {
            # code...
            Superpayments::create([
                'supervisor_id' => $team->t_supervisor,
                'payment_unique' => $this->paymentgeingsecond(),
                'payment_status' => 0,
                'payment_option' => "",
            ]);
        }

        $supervisor = User::select('*', 'countries.name as countryname', 'users.name as name')
                ->join('payment_supervisor', 'payment_supervisor.supervisor_id', '=', 'users.id')
                ->join('countries', 'countries.id', '=', 'users.country')
                ->where('users.id', $team->t_supervisor)
                ->first();

        $students = array();
        if ($team->t_f_student_code != '') {
            # code...
            $first = Students::where('s_student_id', $team->t_f_student_code)->first();
            $paying = Spayment::where('student_id',$first->id)->where('student_supervisor_id', $supervisor->id)->where('student_team_id', $team->id)->first();

            if ($paying == null) {
                # code...
                Spayment::create([
                    'student_id' => $first->id,
                    'student_supervisor_id' => $supervisor->id,
                    'student_team_id' => $team->id,
                    'payment_unique' => $this->paymentgeing(),
                    'payment_status' => 0,
                    'payment_option' => "",
                ]);
            }

            $student = Students::select('*')
                ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                ->join('countries', 'countries.id', '=', 'students.s_country')
                ->where('s_student_id', $team->t_f_student_code)
                ->first();
            array_push($students, $student);
        }
        if ($team->t_s_student_code != '') {
            # code...
            $first = Students::where('s_student_id', $team->t_s_student_code)->first();
            $paying = Spayment::where('student_id',$first->id)->where('student_supervisor_id', $supervisor->id)->where('student_team_id', $team->id)->first();

            if ($paying == null) {
                # code...
                Spayment::create([
                    'student_id' => $first->id,
                    'student_supervisor_id' => $supervisor->id,
                    'student_team_id' => $team->id,
                    'payment_unique' => $this->paymentgeing(),
                    'payment_status' => 0,
                    'payment_option' => "",
                ]);
            }

            $student = Students::select('*')
                ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                ->join('countries', 'countries.id', '=', 'students.s_country')
                ->where('s_student_id', $team->t_s_student_code)
                ->first();
            array_push($students, $student);
        }
        if ($team->t_t_student_code != '') {
            # code...
            $first = Students::where('s_student_id', $team->t_t_student_code)->first();
            $paying = Spayment::where('student_id',$first->id)->where('student_supervisor_id', $supervisor->id)->where('student_team_id', $team->id)->first();

            if ($paying == null) {
                # code...
                Spayment::create([
                    'student_id' => $first->id,
                    'student_supervisor_id' => $supervisor->id,
                    'student_team_id' => $team->id,
                    'payment_unique' => $this->paymentgeing(),
                    'payment_status' => 0,
                    'payment_option' => "",
                ]);
            }

            $student = Students::select('*')
                ->join('payment_students', 'payment_students.student_id', '=', 'students.id')
                ->join('countries', 'countries.id', '=', 'students.s_country')
                ->where('s_student_id', $team->t_t_student_code)
                ->first();
            array_push($students, $student);
        }
        
        $payments = Payments::select('*', \DB::raw('concat(users.name, " ", users.surname) as supervisor'))
            ->join('users', 'users.id', '=', 'payments.payment_supervisor')
            ->where('payment_supervisor', $supervisor->supervisor_id)
            ->get();

        $guests = Guests::select('*', 'countries.name as countryname', 'guests.guest_id as guesting', 'guests.name as nname')
            ->join('guest_payments', 'guest_payments.guest_id', '=', 'guests.id')
            ->join('countries', 'countries.id', '=', 'guests.country')
            ->where('guests.p_supervisor', $supervisor->supervisor_id)
            ->get();

        return response()->json([
                'success' => true,
                'team' => $team,
                'project' => $project,
                'supervisor' => $supervisor,
                'students' => $students,
                'payments' => $payments,
                'guests' => $guests
        ], $this->successStatus);
    }

    public function getExport()
    {
        # code...
        $teams = Teams::select('projects.p_title as title','users.name as sname', 'users.surname as ssurname', 'teams.t_project_mark as mark','teams.*', 'categories.category as pct', 'users.email as emmm')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->join('users', 'users.id','=', 'teams.t_supervisor')
            ->join('categories', 'categories.id','=', 'projects.p_category')
            ->where('users.country', 110)
            ->get();

        $arr = array();
        foreach ($teams as $v) {
            # code...
            switch ($v['mark']) {
                case 1:
                    # code...
                    $v['mark'] = "Not Checked!";
                case 1:
                    # code...
                    $v['mark'] = "Rejected";
                    break;
                case 2:
                    # code...
                    $v['mark'] = "50/50";
                    break;
                case 3:
                    # code...
                    $v['mark'] = "Accepted";
                    break;
            }
            if ($v->t_f_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_f_student_code'] )->join('regions', 'regions.id', '=', 'students.s_region')->orderBy('students.s_region', 'DESC')->first();

                $daryntt = '';
                if ($stu['s_daryn'] == 2) {
                    # code...
                    $daryntt = 'Approved';

                }elseif ($stu['s_daryn'] == 1) {
                    # code...
                    $daryntt = 'Not Approved';

                }else{
                    $daryntt = 'Pending';
                }

                array_push($arr, [
                    'id' => $stu['s_student_id'],
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'].' '.$stu['s_ru_name'].' '.$stu['s_ru_lastname'],
                    'fullname'=> $stu['s_en_surname'].' '.$stu['s_en_name'].' '.$stu['s_en_lastname'],
                    'project' => $v['title'],
                    'category'=> $v['pct'],
                    'country' => 'Kazakhstan',
                    'region' => $stu['en'].' / '.$stu['kz'],
                    'city' => $stu['s_city'],
                    'zip'=> $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'projectstate' =>  $v['mark'],
                    'darynstate' =>  $daryntt,
                    'daryn' =>  $stu['s_daryn'],
                    'superemail' =>  $v['emmm'],
                    'code' => $v->t_f_student_code,
                    'gender' => $stu['s_gender'] == 1 ? 'Male': 'Female' 
                ]);
            }
            if ($v->t_s_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_s_student_code'] )->join('regions', 'regions.id', '=', 'students.s_region')->orderBy('students.s_region', 'DESC')->first();

                $daryntt = '';
                if ($stu['s_daryn'] == 2) {
                    # code...
                    $daryntt = 'Approved';

                }elseif ($stu['s_daryn'] == 1) {
                    # code...
                    $daryntt = 'Not Approved';

                }else{
                    $daryntt = 'Pending';
                }

                array_push($arr, [
                    'id' => $stu['s_student_id'],
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'].' '.$stu['s_ru_name'].' '.$stu['s_ru_lastname'],
                    'fullname'=> $stu['s_en_surname'].' '.$stu['s_en_name'].' '.$stu['s_en_lastname'],
                    'project' => $v['title'],
                    'category'=> $v['pct'],
                    'country' => 'Kazakhstan',
                    'region' => $stu['en'].' / '.$stu['kz'],
                    'city' => $stu['s_city'],
                    'zip'=> $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'projectstate' =>  $v['mark'],
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' =>  $stu['s_daryn'],
                    'superemail' =>  $v['emmm'],
                    'code' => $v->t_s_student_code,
                    'gender' => $stu['s_gender'] == 1 ? 'Male': 'Female' 
                ]);
            }
            if ($v->t_t_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_t_student_code'] )->join('regions', 'regions.id', '=', 'students.s_region')->orderBy('students.s_region', 'DESC')->first();

                $daryntt = '';
                if ($stu['s_daryn'] == 2) {
                    # code...
                    $daryntt = 'Approved';

                }elseif ($stu['s_daryn'] == 1) {
                    # code...
                    $daryntt = 'Not Approved';

                }else{
                    $daryntt = 'Pending';
                }

                array_push($arr, [
                    'id' => $stu['s_student_id'],
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'].' '.$stu['s_ru_name'].' '.$stu['s_ru_lastname'],
                    'fullname'=> $stu['s_en_surname'].' '.$stu['s_en_name'].' '.$stu['s_en_lastname'],
                    'project' => $v['title'],
                    'category'=> $v['pct'],
                    'country' => 'Kazakhstan',
                    'region' => $stu['en'].' / '.$stu['kz'],
                    'city' => $stu['s_city'],
                    'zip'=> $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'projectstate' =>  $v['mark'],
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' =>  $stu['s_daryn'],
                    'superemail' => $v['emmm'],
                    'code' => $v->t_t_student_code,
                    'gender' => $stu['s_gender'] == 1 ? 'Male': 'Female' 

                ]);
            }
        }

        return $arr;
    }

     public function paymentgeing()
    {
      # code...
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 8; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return 'pmst'.$randomString;
    }

    public function paymentgeingsecond()
    {
        # code...
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return 'pmsp'.$randomString;
    }
}
