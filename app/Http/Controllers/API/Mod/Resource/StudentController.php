<?php

namespace App\Http\Controllers\API\Mod\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\User;
use App\Students;
use App\Teams;

use App\Countries;
use App\Regions;

use Validator;
use App\Http\Controllers\Activities\ActivitiesTracker;

use App\Notifications;
use App\NotificationsFiles;
use App\NotificationsStatus;

use App\Spayment;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function addstudent(Request $request)
    {
        //
        $user = User::where('user_id', $request->supervisor)->first();
        /*
        $exstudents = \DB::table('students')
                    ->where('s_en_name', 'like', '%'.$request->input('enname').'%')
                    ->where('s_en_surname', 'like', '%'.$request->input('ensurname').'%')
                    ->where('s_passport', 'like', '%'.$request->input('passport').'%')
                    ->where('s_supervisor', $user->id)
                    ->count();

        if ($exstudents > 0) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => 'You are trying to add an existing student to the database!'
            ], $this->successStatus);            
        }
        */

        if ($request->input('country') != 110) {
            # code...
            $validator = Validator::make($request->all(), [
                'enname' => 'required|max:40|min:2',
                'ensurname' => 'required|max:40|min:2',

                'birthdate' => 'required',
                'gender' => 'required',
                'passport' => 'required|min:6',
                'phone' => 'required|min:7',

                'country' => 'required',
                'region' => 'required',
                'zip' => 'required',
                'city' => 'required',
                'school' => 'required',
                'grade' => 'required',

            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'error' => true,
                    'message' => $validator->errors()->first()
                ], $this->successStatus);             
            }
        }else{
            $validator = Validator::make($request->all(), [
                'enname' => 'required|max:40|min:2',
                'ensurname' => 'required|max:40|min:2',
                
                'runame' => 'required|max:40|min:2',
                'rusurname' => 'required|max:40|min:2',

                'birthdate' => 'required',
                'gender' => 'required',
                'passport' => 'required|min:9',
                'phone' => 'required|min:7',

                'country' => 'required',
                'region' => 'required',
                'zip' => 'required',
                'city' => 'required',
                'school' => 'required',
                'grade' => 'required',

            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'error' => true,
                    'message' => $validator->errors()->first()
                ], $this->successStatus);             
            }
        }

        $newstudent = new Students;
        $code = $this->UserGenerator($request->input('country'));

        $newstudent->s_student_id = $code;
        $newstudent->s_supervisor = $user->id;

        $newstudent->s_en_name = $request->input('enname');
        $newstudent->s_en_surname = $request->input('ensurname');
        if ($request->input('enlastname') == "") {
            # code...
            $newstudent->s_en_lastname = "";
        }else{
            $newstudent->s_en_lastname = $request->input('enlastname');
        }
        

        if ($request->input('country') == 110) {
            # code...
            $newstudent->s_ru_name = $request->input('runame');
            $newstudent->s_ru_surname = $request->input('rusurname');
            if ($request->input('rulastname') == "") {
            # code...
                $newstudent->s_ru_lastname = "";
            }else{
                $newstudent->s_ru_lastname = $request->input('rulastname');
            }
        }else{
            $newstudent->s_ru_name = "";
            $newstudent->s_ru_surname = "";
            $newstudent->s_ru_lastname = "";
        }

        $newstudent->s_birthdate = $request->input('birthdate');
        $newstudent->s_gender = $request->input('gender');
        $newstudent->s_passport = $request->input('passport');
        $newstudent->s_phone = $request->input('phone');

        $newstudent->s_country = $request->input('country');

        if ($request->input('country') == 110) {
            # code...
            $region = \DB::table('regions')->where('en', $request->input('region'))->first();
            $newstudent->s_region = $region->id;
        }else{
            $newstudent->s_region = $request->input('region');
        }
        
        $newstudent->s_zip = $request->input('zip');
        $newstudent->s_city = $request->input('city');
        $newstudent->s_school = $request->input('school');
        $newstudent->s_grade = $request->input('grade');

        $newstudent->save();

        $team = Teams::where('t_team_id', $request->teamid)->first();

        if ($team->t_f_student_code == '') {
            # code...
            $team->t_f_student_code = $code;
            $team->save();
        }
        elseif ($team->t_s_student_code == '') {
            # code...
            $team->t_s_student_code = $code;
            $team->save();
        }
        elseif ($team->t_t_student_code == '') {
            # code...
            $team->t_t_student_code = $code;
            $team->save();
        }
        else{
            return response()->json([
                'success' => false,
                'error' => true,
                'message' => "Nowhere to add!"
            ], $this->successStatus);
        }

        Spayment::create([
            'student_id' => $newstudent->id,
            'student_supervisor_id' => $user->id,
            'student_team_id' => $team->id,
            'payment_unique' => $this->paymentgeno(),
            'payment_status' => 0,
            'payment_option' => " "
        ]);

        return response()->json([
                'success' => true,
                'error' => false,
                'message' => "Student created and added to the team!"
            ], $this->successStatus);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $student = Students::select('students.*', 'countries.name as country')->join('countries', 'countries.id', '=', 'students.s_country')->where('s_student_id', $id)->first();
      
        return response()->json(
        [
            'user' => Auth::user(),
            'student' => $student
        ], 200); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $student = Students::where('s_student_id', $id)->first();
        
        if($request->name != $student->s_en_name){
            $student->s_en_name = $request->name;
        }
        if($request->surname != $student->s_en_surname){
            $student->s_en_surname = $request->surname;
        }
        if($request->lastname != $student->s_en_lastname){
            $student->s_en_lastname = $request->lastname == '' ? '' : $request->lastname;
        }
        if($request->passport != $student->s_passport){
            $student->s_passport = $request->passport;
        }

        if($request->runame != $student->s_ru_name){
            $student->s_ru_name = $request->runame;
        }
        if($request->rusurname != $student->s_ru_surname){
            $student->s_ru_surname = $request->rusurname;
        }
        if($request->rulastname != $student->s_ru_lastname){
            $student->s_ru_lastname = $request->rulastname == '' ? '' : $request->rulastname;
        }

        $student->save();

        return response()->json(
        [
            'success' => true,
            'message' => 'Successfully changed!'
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UserGenerator($country)
    {
      # code...
      $thisyear = date("Y");
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $thisyear.'_ss_'.$country.'_'.$randomString;
    }

    public function paymentgeno()
    {
      # code...
      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return 'pmst'.$randomString;
    }
}
