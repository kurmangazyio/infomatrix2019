<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

use Session;

use App\Faqs;
use App\Countries;
use App\User;
use App\Projects;
use App\Teams;
use App\Students;
use App\Marks;
use App\Regions;

class MainController extends Controller
{
    //
    public function index()
    {
        # code...
        $data = array(
            'appname' => 'WELCOME TO INFOMATRIX-ASIA 2025',
            'pagetitle' => 'Infomatrix 2025',
            'pageimage' => route('main') . '',
        );
        //return view('welcome', $data);
        return view('panel-general.main', $data);
    }

    public function finalists()
    {
        # code...
        // return redirect()->to('https://infomatrix.asia');
        
        $data = array(
            'appname' => 'INFOMATRIX-ASIA 2025',
            'pagetitle' => 'Infomatrix 2025',
            'pageimage' => route('main') . '',
        );
        //return view('welcome', $data);


        return view('finalists', $data);
    }

    public function kazakhstan()
    {
        # code...
        $data = array(
            'appname' => 'WELCOME TO INFOMATRIX-ASIA 2025',
            'pagetitle' => 'Welcome to Kazakhstan',
            'pageimage' => route('main') . '/general/images/kazakhstan.jpg',
        );

        return view('panel-general.kazakhstan', $data);

    }

    public function rules()
    {
        return redirect()->to('https://infomatrix.asia/INFOMATRIX 2025 Regulations.pdf');
        # code...
        $data = array(
            'appname' => 'WELCOME TO INFOMATRIX-ASIA 2025',
            'pagetitle' => 'Rules and Regulations',
            'pageimage' => route('main') . '/general/images/rule-cover.jpg',
        );

        return view('panel-general.rules', $data);

    }

    public function faqs()
    {
        # code...
        $data = array(
            'appname' => 'WELCOME TO INFOMATRIX-ASIA 2025',
            'pagetitle' => 'FAQs section.',
            'pageimage' => route('main') . '/general/images/inner-6.jpg',
        );

        return view('panel-general.faq', $data);

    }

    public function payment()
    {
        return redirect()->to('https://infomatrix.asia');
        # code...
        $data = array(
            'appname' => 'WELCOME TO INFOMATRIX-ASIA 2025',
            'pagetitle' => 'Payment section.',
            'pageimage' => route('main') . '/general/images/inner-6.jpg',
        );

        return view('panel-general.payment', $data);

    }

    public function FaqsPost(Request $request)
    {
        # code...
        $newfaqs = new Faqs;

        $newfaqs->name = $request->input('name');
        $newfaqs->email = $request->input('email');
        $newfaqs->message = $request->input('message');
        $newfaqs->is_answered = 0;
        $newfaqs->answer = '';

        $newfaqs->save();


        Session::flash('message', 'Your question is successfully sent. You will receive answer by email, as soon as possible!');
        Session::flash('alert-class', 'form-success');

        return redirect()->route('faqs');
    }

    public function terms()
    {
        # code...
        $data = array(
            'appname' => 'Infomatrix 2025',
            'pagetitle' => 'Terms of use',
            'pageimage' => route('main') . '/general/images/terms.jpg',
        );

        return view('panel-general.terms', $data);
    }


    public function ShowRegisterForm()
    {
        # code...
    }

    public function AcceptRegisterForm(Request $request)
    {
        # code...
    }

    public function activate($theemail)
    {
        # code...
        $decrypted = Crypt::decryptString($theemail);

        $user = User::where('email', $decrypted)->first();
        $user->is_email_activated = 1;
        $user->save();

        echo '<h1> Your Infomatrix-Asia 2025 account successfully activated! </h1> <a href="' . route('main') . '/user/login">go to Login page</a>';
        //return redirect()->route('login');
    }

    public function online()
    {
        return redirect()->to('https://infomatrix.asia/finalists');
        # code...
        $data = array(
            'appname' => 'Infomatrix 2025',
            'pagetitle' => 'Infomatrix Online',
            'pageimage' => route('main') . '/general/images/terms.jpg',
            'exporter' => $this->AcceptedStudents(),
            'foreign' => $this->getExport()
        );


        return view('online', $data);

    }
    public function AcceptedStudents()
    {
        # code...
        $teams = Teams::select('projects.p_title as title', 'users.name as sname', 'users.email as email', 'users.surname as ssurname', 'teams.t_project_mark as mark', 'teams.*', 'categories.category as pct')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->join('users', 'users.id', '=', 'teams.t_supervisor')
            ->join('categories', 'categories.id', '=', 'projects.p_category')
            ->where('teams.t_project_mark', 3)
            ->where('users.country', 110)
            ->orderBy('categories.category', 'ASC')
            ->get();

        $arr = array();
        foreach ($teams as $v) {
            # code...
            $marker = '';
            switch ($v['mark']) {
                case 0:
                    # code...
                    $marker = 'Not checked';
                    break;
                case 1:
                    # code...
                    $marker = 'Rejected';
                    break;
                case 2:
                    # code...
                    $marker = '50/50';
                    break;
                case 3:
                    # code...
                    $marker = 'Accepted';
                    break;
            }

            if ($v->t_f_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_f_student_code'])
                    ->where('s_daryn', 2)
                    ->join('countries', 'countries.id', '=', 'students.s_country')
                    ->orderBy('students.s_region', 'DESC')
                    ->first();

                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'] . ' ' . $stu['s_ru_name'] . ' ' . $stu['s_ru_lastname'],
                    'fullname' => $stu['s_en_surname'] . ' ' . $stu['s_en_name'] . ' ' . $stu['s_en_lastname'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'] . ' ' . $v['ssurname'],
                    'project' => $v['title'],
                    'category' => $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip' => $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' => $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' => $stu['s_daryn']

                ]);
            }
            if ($v->t_s_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_s_student_code'])->where('s_daryn', 2)->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();
                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'] . ' ' . $stu['s_ru_name'] . ' ' . $stu['s_ru_lastname'],
                    'fullname' => $stu['s_en_surname'] . ' ' . $stu['s_en_name'] . ' ' . $stu['s_en_lastname'],
                    'project' => $v['title'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'] . ' ' . $v['ssurname'],
                    'category' => $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip' => $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' => $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' => $stu['s_daryn']
                ]);
            }
            if ($v->t_t_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_t_student_code'])->where('s_daryn', 2)->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();
                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'] . ' ' . $stu['s_ru_name'] . ' ' . $stu['s_ru_lastname'],
                    'fullname' => $stu['s_en_surname'] . ' ' . $stu['s_en_name'] . ' ' . $stu['s_en_lastname'],
                    'project' => $v['title'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'] . ' ' . $v['ssurname'],
                    'category' => $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip' => $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' => $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' => $stu['s_daryn']
                ]);
            }
        }

        return $arr;
    }

    public function getExport()
    {
        # code...
        $teams = Teams::select('projects.p_title as title', 'users.name as sname', 'users.email as email', 'users.surname as ssurname', 'teams.t_project_mark as mark', 'teams.*', 'categories.category as pct')
            ->join('projects', 'projects.p_project_id', '=', 'teams.t_project_code')
            ->join('users', 'users.id', '=', 'teams.t_supervisor')
            ->join('categories', 'categories.id', '=', 'projects.p_category')
            ->where('teams.t_project_mark', 3)
            ->orderBy('categories.category', 'ASC')
            ->orderBy('users.country', 'ASC')
            ->get();

        $arr = array();
        foreach ($teams as $v) {
            # code...
            $marker = '';
            switch ($v['mark']) {
                case 0:
                    # code...
                    $marker = 'Not checked';
                    break;
                case 1:
                    # code...
                    $marker = 'Rejected';
                    break;
                case 2:
                    # code...
                    $marker = '50/50';
                    break;
                case 3:
                    # code...
                    $marker = 'Accepted';
                    break;
            }

            if ($v->t_f_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_f_student_code'])->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();

                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'] . ' ' . $stu['s_ru_name'] . ' ' . $stu['s_ru_lastname'],
                    'fullname' => $stu['s_en_surname'] . ' ' . $stu['s_en_name'] . ' ' . $stu['s_en_lastname'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'] . ' ' . $v['ssurname'],
                    'project' => $v['title'],
                    'category' => $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip' => $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' => $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' => $stu['s_daryn']

                ]);
            }
            if ($v->t_s_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_s_student_code'])->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();
                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'] . ' ' . $stu['s_ru_name'] . ' ' . $stu['s_ru_lastname'],
                    'fullname' => $stu['s_en_surname'] . ' ' . $stu['s_en_name'] . ' ' . $stu['s_en_lastname'],
                    'project' => $v['title'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'] . ' ' . $v['ssurname'],
                    'category' => $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip' => $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' => $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' => $stu['s_daryn']
                ]);
            }
            if ($v->t_t_student_code != '') {
                # code...
                $stu = Students::where('s_student_id', $v['t_t_student_code'])->join('countries', 'countries.id', '=', 'students.s_country')->orderBy('students.s_region', 'DESC')->first();
                array_push($arr, [
                    'iin' => $stu['s_passport'],
                    'fio' => $stu['s_ru_surname'] . ' ' . $stu['s_ru_name'] . ' ' . $stu['s_ru_lastname'],
                    'fullname' => $stu['s_en_surname'] . ' ' . $stu['s_en_name'] . ' ' . $stu['s_en_lastname'],
                    'project' => $v['title'],
                    'email' => $v['email'],
                    'supervisor' => $v['sname'] . ' ' . $v['ssurname'],
                    'category' => $v['pct'],
                    'country' => $stu['name'],
                    'region' => $stu['s_region'],
                    'city' => $stu['s_city'],
                    'zip' => $stu['zip'] == null ? '' : $stu['zip'],
                    'school' => $stu['s_school'],
                    'grade' => $stu['s_grade'],
                    'phone' => $stu['s_phone'],
                    'mark' => $marker,
                    'darynstate' => $stu['s_daryn'] == 2 ? 'Approved' : 'Not Approved / Pending',
                    'daryn' => $stu['s_daryn']
                ]);
            }
        }

        return $arr;
    }
}
