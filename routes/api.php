<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/user')->group(function () {

	Route::post('login', 'API\Auth\UserLoginController@LoginUser');
	Route::post('register', 'API\Auth\UserRegisterController@RegisterUser');
	Route::post('forgot', 'API\Auth\UserForgotController@ForgotUser');

	Route::resource('sos', 'API\Sos\SosController');
	Route::resource('countries', 'API\User\Resources\CountriesController');

	Route::group(['middleware' => 'auth:api'], function(){
		Route::get('get-panel', 'API\User\Userpanel\UserPanelController@index');
		Route::get('get-projects-status', 'API\User\Userpanel\UserPanelController@statuses');

		//upload page
		Route::get('get-upload', 'API\User\Userpanel\UserPanelController@upoad');
		Route::get('get-upload-students', 'API\User\Userpanel\UserPanelController@upoadStudentsView');
		Route::get('get-students', 'API\User\Userpanel\UserPanelController@upoadStudents');
		Route::get('get-manage', 'API\User\Userpanel\UserManageController@index');
		Route::get('get-inbox', 'API\User\Userpanel\UserInboxController@index');
		Route::post('read-all', 'API\User\Userpanel\UserInboxController@makeReaden');
		Route::post('readn-answer', 'API\User\Userpanel\UserInboxController@ReadnAnswer');
		Route::get('get-project/{id}', 'API\User\Userpanel\UserManageController@get_projects');

		Route::get('get-setting', 'API\User\Userpanel\UserSettingController@index');
		Route::get('get-notification', 'API\User\Notifications\UserNotificationController@index');
		
		Route::post('set-students', 'API\Admin\Resources\StudentController@store');
		Route::post('set-project', 'API\Admin\Resources\ProjectController@store');
		Route::post('set-team', 'API\Admin\Resources\TeamController@store');

		Route::resource('projects', 'API\Admin\Resources\ProjectController');
		Route::resource('students', 'API\Admin\Resources\StudentController');

		Route::resource('guests', 'API\User\Payment\GuestController');

		Route::resource('teams', 'API\Admin\Resources\TeamController');
		Route::resource('supervisor', 'API\Admin\Resources\SupervisorController');
		Route::resource('notifications', 'API\Admin\Resources\NotificationController');

		Route::resource('conversations', 'API\Admin\Resources\ConversationController');
		Route::resource('answers', 'API\Admin\Resources\AnswersController');

		Route::resource('messages', 'API\User\Resources\MessageController');

		Route::get('get-payment-projects', 'API\User\Payment\PaymentsController@index');
		Route::get('get-payment-guests', 'API\User\Payment\PaymentsController@refreshGuest');
		Route::post('get-payment-guests', 'API\User\Payment\PaymentsController@deleteGuest');

		Route::resource('invoice', 'API\User\Resources\OfflineInvoiceController');

		Route::get('get-point', 'API\User\PointsController@index');
	});
});

Route::prefix('/admin')->group(function () {
	Route::post('login', 'API\Auth\UserLoginController@LoginAdmin');
	Route::get('online-stats', 'API\Admin\Online\OnlineController@getstats');

	Route::group(['middleware' => 'auth:admin-api'], function(){
		
		Route::get('get-main', 'API\Admin\Main\MainController@index');
		Route::get('get-account', 'API\Admin\Profile\AdminProfileController@index');
		Route::get('get-users/{role}', 'API\Admin\Users\AdminUserController@index');
		Route::get('get-users-setup/{role}', 'API\Admin\Users\AdminUserController@newuser');

		Route::get('get-projects/{category}', 'API\Admin\Projects\AdminProjectsController@index');
		Route::get('get-notifications', 'API\Admin\Notifications\AdminNotificationsController@index');

		Route::post('email-notifications', 'API\Admin\Main\MainController@emailnotification');

		Route::get('get-activities', 'API\Admin\Main\MainController@activities');

		Route::get('online', 'API\Admin\Online\OnlineController@index');

		Route::get('export', 'API\Admin\Export\ExportController@index');

		//resources
		Route::resource('supervisors', 'API\Admin\Resource\SupervisorController');
		Route::resource('admins', 'API\Admin\Resource\AdminController');
		Route::resource('juries', 'API\Admin\Resource\JuryController');
		
		Route::resource('notifications', 'API\Admin\Resource\NotificationController');
		Route::resource('conversations', 'API\Admin\Resource\ConversationController');
		Route::resource('answers', 'API\Admin\Resource\AnswersController');

		Route::resource('projects', 'API\Admin\Resource\ProjectController');

		Route::resource('faqs', 'API\Admin\Resource\FaqsController');

		Route::resource('finalists', 'API\Admin\Resource\FinalistsController');

		
		Route::resource('setting', 'API\Admin\Resource\SettingController');

		Route::resource('modirators', 'API\Admin\Resources\AdminController');
		Route::resource('students', 'API\Admin\Resources\AdminController');	

		Route::get('get-payments', 'API\Mod\Resource\ModController@getPayments');

		Route::resource('volunteers', 'API\Admin\Resource\VolunteerController');


		//logs
		Route::resource('logs', 'API\Admin\Logs\LogsController');

		Route::get('get-point', 'API\User\PointsController@getbycategorys');

		Route::resource('medals', 'API\Admin\Medal\MedalController');	
	});
});

Route::prefix('/jury')->group(function () {
	Route::post('login', 'API\Auth\UserLoginController@LoginJury');

	Route::group(['middleware' => 'auth:jury-api'], function(){
		Route::get('get-main', 'API\Jury\Profile\JuryPanelController@index');
		Route::get('get-profile', 'API\Jury\Profile\JuryProfileController@index');
		Route::get('get-categories', 'API\Jury\Profile\JuryProfileController@getCategoriess');
		Route::get('get-assistent', 'API\Jury\Profile\JuryProfileController@getAssistent');

		Route::post('get-projects', 'API\Jury\Profile\JuryProfileController@getProjects');

		Route::post('set-config', 'API\Jury\Profile\JuryProfileController@newConfig');

		Route::resource('juries', 'API\Admin\Resource\JuryController');
		Route::resource('projects', 'API\Jury\Resources\ProjectController');
		Route::resource('teams', 'API\Jury\Resources\TeamController');

		Route::resource('notifications', 'API\Jury\Notification\NotificationController');
		
		Route::resource('conversations', 'API\Jury\Resources\ConversationController');
		Route::resource('answers', 'API\Jury\Resources\AnswersController');

		Route::resource('messages', 'API\User\Resources\MessageController');

		Route::resource('judgement', 'API\Mod\JudmentController');
		Route::resource('tours', 'API\Jury\Judgment\TourController');
		Route::resource('criteria', 'API\Jury\Judgment\CriteriaController');

		Route::get('getpoint/{student}/{project}', 'API\Jury\Judgment\PointingController@geteditor');
		Route::get('getupdatepoint/{student}/{project}', 'API\Jury\Judgment\PointingController@getupdatepoint');
		Route::resource('points', 'API\Jury\Judgment\PointingController');
		Route::post('updatepoints', 'API\Jury\Judgment\PointingController@updatepoints');

		Route::get('get-point', 'API\User\PointsController@getcategorys');

		Route::post('setpoint', 'API\Jury\AppealController@store');
	});
});

Route::prefix('/mod')->group(function () {
	Route::post('login', 'API\Auth\UserLoginController@LoginModerator');

	Route::group(['middleware' => 'auth:moderator-api'], function(){
		Route::get('get-main', 'API\Mod\Resource\ModController@index');
		Route::get('get-daryn', 'API\Mod\Resource\ModController@daryn');
		Route::get('get-registers', 'API\Mod\Resource\ModController@postRegister');
		Route::get('get-payments', 'API\Mod\Resource\ModController@getPayments');
		
		Route::get('getdetails/{teamid}', 'API\Mod\Resource\ModController@getprojectdetails');

		Route::post('set-approve', 'API\Mod\Resource\ModController@approvement');

		Route::post('addsudenttoteam', 'API\Mod\Resource\StudentController@addstudent');

		Route::resource('student', 'API\Mod\Resource\StudentController');
		Route::resource('supervisor', 'API\Mod\Resource\SupervisorController');

		//POST 
		Route::resource('students', 'API\Mod\Post\StudentController');
		Route::resource('supervisors', 'API\Mod\Post\SupervisorController');
		Route::resource('guest', 'API\Mod\Post\GuestController');

		// MOBILE
		Route::resource('users', 'API\Mod\UsersController');
		Route::resource('logs', 'API\Mod\LogsController');

		Route::resource('invoice', 'API\User\Resources\OfflineTwoInvoiceController');

		Route::get('invoiceget/{teamid}', 'API\User\Resources\OfflineTwoInvoiceController@getinvoices');

		Route::get('get-upload-students', 'API\User\Userpanel\UserPanelController@upoadStudentsView');

		Route::resource('guests', 'API\User\Payment\GuestTwoController');
	});
});
