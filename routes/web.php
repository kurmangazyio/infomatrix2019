<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->group(function () {
    Route::get('/kazakhstan', 'MainController@kazakhstan')->name('kazakhstan');
    Route::get('/rules', 'MainController@rules')->name('rules');
    Route::get('/terms', 'MainController@terms')->name('terms');
    //Route::get('/notes', 'MainController@notes')->name('notes');

    Route::get('/online', 'MainController@online')->name('online');

    Route::get('/faqs', 'MainController@faqs')->name('faqs');
    Route::get('/payment', 'MainController@payment')->name('payment');
    Route::post('/faqs', 'MainController@FaqsPost')->name('faqspost');

    Route::get('/accommodation', 'MainWebRouteController@accomodation')->name('accommodation');

    //Login for User
    Route::get('/login', 'MainWebRouteController@login')->name('login');
    Route::get('/register', 'MainWebRouteController@register')->name('register');

    Route::get('/activate/{theemail}', 'MainController@activate')->name('activate');

    Route::get('/finalists', 'MainController@finalists')->name('finalists');

    Route::get('/', 'MainController@index')->name('main');
});



Route::prefix('/user')->group(function () {
    Route::post('add-to-payments', 'API\User\Payment\PaymentsController@addToPayments');
    
	Route::get('/{any}', 'MainWebRouteController@user')->where('any', '.*')->name('user');
	Route::get('/', 'MainWebRouteController@user');
});

Route::prefix('/jury')->group(function () {
	Route::get('/{any}', 'MainWebRouteController@jury')->where('any', '.*')->name('jury');
	Route::get('/', 'MainWebRouteController@jury');
});

Route::prefix('/admin')->group(function () {
	Route::get('/{any}', 'MainWebRouteController@admin')->where('any', '.*')->name('admin');
	Route::get('/', 'MainWebRouteController@admin');
});

Route::prefix('/moderator')->group(function () {
    Route::get('/{any}', 'MainWebRouteController@moderator')->where('any', '.*')->name('admin');
    Route::get('/', 'MainWebRouteController@moderator');
});




//Auth::routes();